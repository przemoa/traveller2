Project issues: https://trello.com/b/9qyttuLv/traveller2

# PySide2 + Pyinstaller + QtWebEngine + Linux
Pyinstaller doesn't pack QtWebEngineProcess and some required plugins. 
After building with pyinstaller (and single directory instead of single executable), copying missing files and setting QTWEBENGINEPROCESS_PATH,
dialog loads, but still some errors like 
    Error initializing NSS with a persistent database (sql:/home/przemek/.pki/nssdb): libsoftokn3.so: cannot open shared object file: No such file or directory
or after copying library:
   ContextResult::kTransientFailure: Failed to send GpuChannelMsg_CreateCommandBuffer.
After adding: 
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/nss/
it worked, but it is not the solution.

For PyQt5 QtWebEngine even not included in package for Ubuntu.

Therefore it is time to stop using Pyinstaller and move to installing Python interpreter and virtual environment after 0.1.0 release.


# items generation
module with items (collection + items) is generated automatically from items definition in `misc.items_generator`

# AdminEntity
city must belong to region_minor, region_minor to region_major, region_major to country
admin location name cannot be same within same parent location (e.g. in one region_minor only one city with specific name allowed)


# Development notes
 - use Qt designer to edit ui files
 - ui files mocking is not necessary before each run, it can be done once

