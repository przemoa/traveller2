__version__ = '0.6.3'

# if it is developer build instead of released version - use dummy version number 0.0.1
version_for_deb_package = __version__ if not 'dev' in __version__ else '0.0.1'
