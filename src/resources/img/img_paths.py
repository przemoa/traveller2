import os
import sys

source_img_dir_path = os.path.dirname(os.path.abspath(__file__))


def get_img_path(img_name):
    dir_path = getattr(sys, '_MEIPASS', source_img_dir_path)
    return os.path.join(dir_path, img_name)


traveller2 = 'traveller2.png'
traveller2_icon = 'traveller2.ico'

