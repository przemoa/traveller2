import logging
import threading
import time

from PySide2 import QtWidgets, QtGui
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QMessageBox

from engine.engine import Engine


class EngineLoader(QtWidgets.QWidget):
    """ may be called only from main Qt thread """

    signal_engine_loaded = Signal(object, object)  # engine, error_message
    loader_id_counter = 0

    def __init__(self, parent, database_path: str, prompts_database_path: str):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.loader_id_counter += 1
        self._loader_id = self.loader_id_counter

        self._database_path = database_path
        self._prompts_database_path = prompts_database_path

        self._thread = threading.Thread(target=self._run, name=f'EngineLoader_{self._loader_id}')
        self._ignore_result_event = threading.Event()

        self._loading_dialog = QMessageBox(self)
        self._loading_dialog.setWindowTitle('Loading Database...')
        self._loading_dialog.setText('Please wait. \n'
                                     'Traveller database and prompts database are being loaded...')
        self._loading_dialog.setIcon(QMessageBox.Information)
        self._loading_dialog.setModal(False)
        self._loading_dialog.setStandardButtons(QMessageBox.Cancel)
        self._loading_dialog.buttonClicked.connect(self._cancel_clicked)
        self._loading_dialog.show()

    def _cancel_clicked(self):
        logging.debug('Loading database canceled')
        self.ignore_result()

    def start(self):
        self._thread.start()

    def _loading_canceled(self):
        self._ignore_result_event.set()

    def ignore_result(self):
        self._ignore_result_event.set()

    def _run(self):
        time.sleep(0.5)  # give some time for loading_dialog to load (so QT GUI thread will have some time for processing)
        engine = None
        error_message = ''

        try:
            engine, error_message = Engine.create_new_engine_for_paths(database_path=self._database_path,
                                                        prompts_database_path=self._prompts_database_path)
        except Exception as e:
            logging.exception('Error during engine loading')
            error_message = str(e)

        if self._ignore_result_event.is_set() or self._loading_dialog.isHidden():
            logging.debug('Loaded database ignored')
        else:
            self.signal_engine_loaded.emit(engine, error_message)
        self._loading_dialog.close()



