import logging
import time

from common import settings
from engine.collection_utils import load_collection_from_file_path, save_collection_content_to_file
from items.collection import Collection, CollectionContent


class EmptyCollection(Collection):
    pass


class Engine:
    def __init__(self):
        self._original_collection_content = CollectionContent()
        self.collection = Collection()  # type: Collection
        self.collection_file_path = None

        self.prompts_collection = Collection()  # type: Collection
        self.prompts_collection_file_path = None

        # After engine modification, (that may require other tabs to refresh), this field should be set.
        # It is checked regulary and causes tabs refresh (from main_window)
        self.last_engine_modification_time = time.time()

    def load_prompts_collection(self, file_path):
        if file_path:
            self.prompts_collection, _ = load_collection_from_file_path(
                file_path=file_path,
                is_prompts_database=True,
                validate_field_types = settings.s.validate_field_types_during_load_engine)
            self.prompts_collection_file_path = file_path
        else:
            self.prompts_collection = Collection()
            self.prompts_collection_file_path = None

    def load_collection(self, file_path):
        # TODO: check if collection was changed
        if file_path:
            self.collection, self._original_collection_content = load_collection_from_file_path(
                file_path=file_path,
                validate_field_types=settings.s.validate_field_types_during_load_engine)
            self.collection_file_path = file_path
        else:
            self._original_collection_content = CollectionContent()
            self.collection = Collection()
            self.collection_file_path = None

    def save_collection(self, file_path):
        collection_content = self.collection.to_content()
        collection_content.update_modification_times(old_collection_content=self._original_collection_content)

        save_collection_content_to_file(file_path=file_path, collection_content=collection_content)
        self._original_collection_content = collection_content
        # TODO: set also modification and creation time for items in collection
        self.collection_file_path = file_path

    def set_engine_as_modified(self):
        """ Should be called after modyfing the engine, that is after adding some new items or modyfing existing ones.
        It will trigger (after some time) all tabs to reload and display new content """
        logging.debug('Engine marked as modified')
        self.last_engine_modification_time = time.time()

    @staticmethod
    def create_new_engine_for_paths(database_path: str, prompts_database_path: str):
        engine = Engine()
        error_msg = ''
        try:
            engine.load_collection(database_path)

        except Exception as e:
            error_msg = f'Failed to load database with path "{database_path}: {str(e)}"'
            logging.exception(error_msg)
        try:
            engine.load_prompts_collection(prompts_database_path)
        except Exception as e:
            error_msg = f'Failed to load prompts database with path "{prompts_database_path}: {str(e)}"'
            logging.exception(error_msg)
        return engine, error_msg
