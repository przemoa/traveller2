# TODO - this file should be merged with collection.py - after refactoring automatic code generation..

import pickle
from typing import Tuple

from items.collection import CollectionContent, Collection


def load_collection_from_file_path(file_path, is_prompts_database: bool = False, validate_field_types: bool = True) -> Tuple[Collection, CollectionContent]:
    with open(file_path, 'rb') as file:
        collection_content_read = pickle.load(file)
        collection_content = CollectionContent(**collection_content_read.__dict__)  # to open old database without new item classes
    collection = Collection.from_content(collection_content, validate_field_types=validate_field_types)
    if is_prompts_database:
        for item in collection.all_items_by_id.values():
            item.set_as_from_prompts_database()
    return collection, collection_content


def save_collection_content_to_file(file_path, collection_content: CollectionContent):
    with open(file_path, 'wb') as file:
        pickle.dump(file=file, obj=collection_content, protocol=pickle.HIGHEST_PROTOCOL)
