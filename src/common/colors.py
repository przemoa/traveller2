from PySide2.QtCore import Qt

class LineEditColors:
    NEW_ITEM_COLOR = Qt.white
    EXISTING_ITEM_COLOR = Qt.green
    ITEM_FROM_PROMPT_COLOR = Qt.cyan
