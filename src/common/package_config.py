import os


GUI_ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')


class ProjectConfig:
    def __init__(self,
                 prompts_database_path: str,
                 database_path: str,
                 config_file_path: str,
                 ):
        self.prompts_database_path = os.path.abspath(prompts_database_path) if prompts_database_path else ''
        self.database_path = os.path.abspath(database_path) if database_path else ''
        self.config_file_path = os.path.abspath(config_file_path)


package_config_devel = ProjectConfig(
    prompts_database_path=os.path.join(
        GUI_ROOT_DIR, '..', '..', 'traveller2', 'prompts', 'data', 'traveller2_prompts_EU.pkl'),
    database_path='',
    config_file_path=os.path.join(
        GUI_ROOT_DIR, 'traveller2_config.json'),
)

package_config_release = ProjectConfig(
    prompts_database_path='/usr/local/share/traveller2/prompts_database_EU.pkl',
    database_path='',
    config_file_path = os.path.join(os.path.expanduser("~"), '.config', 'super-hat', 'gui', 'traveller2_config.json'),
)
