import json
import logging
import os
from dataclasses import dataclass, field
from json import JSONEncoder
from typing import List

from common.package_config import ProjectConfig

IS_DEBUG = os.environ.get('IS_DEBUG', False) in [True, 'true', 'True', 1, '1']  # enables some debug options
IS_DEVELOPMENT = os.environ.get('USE_DEVEL_CONFIG', False) in [True, 'true', 'True', 1, '1']  # use devel build configuration (e.g. config file path)


def print_warning_about_debug_version_or_development_config():
    if IS_DEBUG or IS_DEVELOPMENT:
        print('#'*60)
        if IS_DEBUG:
            print('Warning! You are using DEBUG mode (because environment variable IS_DEBUG is set)')
        if IS_DEVELOPMENT:
            print('Warning! You are using DEVELOPMENT mode (because environment variable USE_DEVEL_CONFIG is set)')
        print('#'*60)


print_warning_about_debug_version_or_development_config()

USE_DEBUG_CHECKS = IS_DEBUG
DISPLAY_QMESSAGE_AFTER_UNHANDLED_EXCEPTION = True
TEMPORARY_FILES_DIR = '/tmp/traveller2/'


class JsonEncoderToDict(JSONEncoder):
    def default(self, o):
        return o.__dict__


@dataclass
class Settings:
    prompts_database_path: str = ''  # path to database with admin entities prompts
    last_database_path: str = ''  # path to main database with data
    files_root_dir_path: str = os.path.expanduser("~")  # base dir where to look for photos and other files
    console_log_level: int = logging.DEBUG
    file_log_level: int = logging.DEBUG
    ask_before_copying_prompt_to_traveller_db: bool = True  # If prompt was selected, but only item from traveller db is allowed, prompt needs to be copied. Show information with question?
    automatically_refresh_tabs_after_engine_change: bool = True
    automatic_engine_refresh_time_ms: int = 1000
    common_last_browse_database_dir_path: str = ''  # last selected dir path in browse dialogs to save/load databse. Doesn't affect browse dialogs with path specified manually

    first_language_for_wikipedia: str = 'en'
    second_language_for_wikipedia: str = 'pl'
    google_maps_api_key: str = ''
    google_application_credentials_file_path: str = ''

    thumbnail_size_in_pixels: int = 255
    force_annotating_already_annotated_photos: bool = False
    radius_of_photos_decimation_in_space_in_meters: int = 500

    main_window_geometry: str = ''
    main_window_state: str = ''

    use_single_dialog_for_all_tabs_for_map_and_wiki: bool = True  # should be renamed. If true, all tabs will have only one common dialog for displaying additonal info
    validate_field_types_during_load_engine: bool = True  # slows down significantly loading of database (like 3 times slower)
    display_items_sorted_by_date: bool = True

    recently_opened_databases: List[str] = field(default_factory=lambda: [])

    def load_from_file(self, package_config: ProjectConfig):
        file_path = package_config.config_file_path
        try:
            with open(file_path, "r", encoding="utf8") as infile:
                config_dict = json.load(infile)
            self._load_from_dict(config_dict)
            logging.info('Configuration read from file %s', file_path)
        except (IOError, ValueError, KeyError, TypeError) as e:
            logging.warning('Cannot load configuration from json file: %s. Using default configuration' % file_path)

    def save_to_file(self, package_config: ProjectConfig):
        file_path = package_config.config_file_path
        try:
            with open(file_path, 'w') as outfile:
                json.dump(self, outfile, indent=4, cls=JsonEncoderToDict)
            logging.info('Configuration saved to file %s', file_path)
        except (IOError, ValueError) as e:
            logging.exception('Cannot save configuration to json file: %s' % file_path)

    def _load_from_dict(self, dict_from_file):
        for key, value in self.__dict__.items():
            if key in dict_from_file:
                value_from_file = dict_from_file[key]
                if type(value) == type(value_from_file):
                    self.__dict__[key] = value_from_file
                else:
                    logging.warning('Invalid data type in settings file for field: {}. Using default.'.format(key))
            else:
                logging.warning('Missing field in settings file: {}. Using default.'.format(key))


s = None  # type: Settings
reset_before_saving = False


def delete_settings_file(package_config: ProjectConfig):
    try:
        os.remove(package_config.config_file_path)
    except Exception as e:
        logging.warning(f'Cannot delete settings file: {e}')


def load_settings(package_config: ProjectConfig = None):
    global s
    if package_config is not None:
        create_required_dir(os.path.dirname(package_config.config_file_path))
        s = Settings(
            prompts_database_path=package_config.prompts_database_path,
            last_database_path=package_config.database_path,
        )
        s.load_from_file(package_config)
    else:
        s = Settings()


def create_required_dir(dir_path):
    try:
        os.makedirs(dir_path, exist_ok=True)
    except Exception as e:
        msg = 'Failed to create directory required by config. ' + str(e)
        logging.exception(msg)
        import sys
        sys.exit()


def save_settings(package_config: ProjectConfig):
    global s
    global reset_before_saving
    if reset_before_saving:
        logging.info('Resetting settings before saving them')
        s = Settings()
    s.save_to_file(package_config)
