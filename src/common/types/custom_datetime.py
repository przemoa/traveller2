import datetime
import time
from dataclasses import dataclass
from typing import Union


@dataclass
class CustomDateTime:
    unix_time: int  # seconds since 1970

    def __post_init__(self):
        if not isinstance(self.unix_time, int):
            raise TypeError('CustomDateTime unix_time has to be integer!')

    @classmethod
    def now(cls):
        return cls.from_datetime(datetime.datetime.now())

    @classmethod
    def from_int_or_none(cls, unix_time: Union[int, float, None]):
        if unix_time is None:
            return None
        return cls(int(unix_time))

    @classmethod
    def from_calendar_time(cls, year):
        raise NotImplementedError

    @classmethod
    def from_datetime(cls, date_time: datetime.datetime):
        if date_time is None:
            unix_time = None
        else:
            unix_time = int(time.mktime(date_time.timetuple()))
        return cls(unix_time=unix_time)

    def to_unix_time(self):
        return self.unix_time

    @classmethod
    def to_unix_time_from_instance(cls, instance):  # instance: Union[CustomDateTime, None]
        if instance is None:
            return None
        return instance.to_unix_time()

    def __lt__(self, other):
        return self.unix_time < other.unix_time

    def __le__(self, other):
        return self.unix_time <= other.unix_time

    def to_datetime(self):
        if self.unix_time is None:
            return None
        return datetime.datetime.fromtimestamp(self.unix_time)

    def to_month_year_string(self):
        return '{0:%m.%Y}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_date_string(self):
        return '{0:%d.%m-%Y}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_time_string(self):
        return '{0:%H:%M:%S}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_date_time_string(self):
        return '{0:%d.%m-%Y %H:%M}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def to_chronological_date_time_string(self):
        return '{0:%Y.%m.%d %H:%M:%S}'.format(datetime.datetime.fromtimestamp(self.unix_time))

    def __add__(self, seconds):
        if not isinstance(seconds, int):
            raise Exception('Unsupported operand type for CustomDateTime addtion!')
        return CustomDateTime.from_int_or_none(self.to_unix_time() + seconds)

    def __sub__(self, seconds):
        return self + (-seconds)


if __name__ == '__main__':
    print(CustomDateTime(31536000).to_chronological_date_time_string())
