import math
from dataclasses import dataclass
from typing import Union, Any


@dataclass
class Coordinates:
    lat: float
    lon: float

    def __post_init__(self):
        if self.lat > 90 or self.lat < -90:
            raise ValueError('Latitude has to be in range <-90, 90>!')
        if self.lon > 180 or self.lon < -180:
            raise ValueError('Longitude has to be in range <-180, 180>!')

    @classmethod
    def to_tuple_from_instance(cls, coordinates: Union[Any, None]):
        if coordinates is None:
            return None
        else:
            return coordinates.to_tuple()

    def to_tuple(self):
        return self.lat, self.lon

    @classmethod
    def from_tuple(cls, vals):
        if vals is None or vals[0] is None or vals[1] is None:
            return None
        else:
            return cls(lat=vals[0], lon=vals[1])

    def get_short_description(self):
        return f'{self.lat:.6f}, {self.lon:.6f}'

    def get_distance_to_other_coordinates(self, coordinates):
        # in meters
        return haversine_distance(lat1=self.lat, lon1=self.lon, lat2=coordinates.lat, lon2=coordinates.lon) * 1000


def may_be_coordinates_distance_smaller(c_1: Coordinates, c_2: Coordinates, distance):
    """ Because precise distance calculation is time consuming,
    simple test can be performed, to quickly confirm that distance is bigger"""
    lat_diff = abs(c_1.lat - c_2.lat) * 110000
    if lat_diff > distance:
        return False

    if abs(c_1.lat) > 65 or abs(c_2.lat) > 65:
        return True
    meters_per_degree_at_65 = 46900  # math.cos(65*3.1415/180)*111*1000
    lon_diff = abs(c_1.lon - c_2.lon) * meters_per_degree_at_65
    if lon_diff > distance:
        return False

    return True


def haversine_distance(lat1, lon1, lat2, lon2):
    # https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude/43211266#43211266
    """
    Calculate the Haversine distance.

    Parameters
    ----------
    origin : tuple of float
        (lat, long)
    destination : tuple of float
        (lat, long)

    Returns
    -------
    distance_in_km : float

    Examples
    --------
    >>> origin = (48.1372, 11.5756)  # Munich
    >>> destination = (52.5186, 13.4083)  # Berlin
    >>> round(haversine_distance(origin, destination), 1)
    504.2
    """
    # radius = 6371  # km
    #
    # dlat = math.radians(lat2 - lat1)
    # dlon = math.radians(lon2 - lon1)
    # a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
    #      math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
    #      math.sin(dlon / 2) * math.sin(dlon / 2))
    # c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    # d = radius * c
    # return d
    dlat = (lat2 - lat1) * 0.008726646259972222
    dlon = (lon2 - lon1) * 0.008726646259972222
    a = (math.sin(dlat) * math.sin(dlat) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon) * math.sin(dlon))
    return 12742 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
