import pprint
from functools import lru_cache
from typing import Union, Any, List

from common.types.coordinates import Coordinates


class ImageAnnotation:
    """
    Class to represent image annotation received from google vision API and convert it to convenient forms
    TODO: use MID to obtain meaningful name in other languages or description (MID=
    """

    class AnnotationFields:
        LABEL_ANNOTATIONS = 'label_annotations'
        LANDMARK_ANNOTATIONS = 'landmark_annotations'
        LOCALIZED_OBJECT_ANNOTATIONS = 'localized_object_annotations'

    def __init__(self, response_dict: dict):
        self._response_dict = response_dict

    @property
    def response_dict(self):
        return self._response_dict

    def _get_landmark_field(self):
        landmark_fields = self._response_dict.get(self.AnnotationFields.LANDMARK_ANNOTATIONS, None)
        if not landmark_fields:
            return {}
        return landmark_fields[0]

    def get_coordinates(self) -> Union[Coordinates, None]:
        landmark_field = self._get_landmark_field()
        landmark_locations = landmark_field.get('locations', None)
        if not landmark_locations:
            return None
        landmark_location_0 = landmark_locations[0]
        location_coordinates_dict = landmark_location_0.get('lat_lng', None)
        if not location_coordinates_dict:
            return None
        coordinates = Coordinates.from_tuple((location_coordinates_dict['latitude'], location_coordinates_dict['longitude']))
        return coordinates

    def get_landmark_name(self) -> Union[str, None]:
        landmark_name = self._get_landmark_field().get("description", None)
        return landmark_name

    def get_landmark_probability(self) -> Union[float, None]:
        score = self._get_landmark_field().get("score", None)
        if score is None:
            return None
        return score * 100

    def get_landmark_name_with_probability(self):
        landmark_name = self.get_landmark_name()
        if not landmark_name:
            return None
        landmark_probability = self.get_landmark_probability()
        landmark_name_with_probability = f'{landmark_name} ({round(landmark_probability)} %)'
        return landmark_name_with_probability

    @lru_cache(maxsize=10000)  # TODO - confirm that working
    def get_long_description(self):
        txt = self.get_short_description()
        txt += '\n'
        txt += 'Labels: ' + ', '.join(self.get_labels()) + '\n'
        txt += 'Localized objects: ' + ', '.join(self.get_labels_for_localized_objects())
        return txt

    def get_full_text(self):
        return self.get_long_description() + '\n\n' + pprint.pformat(self._response_dict, indent=4)

    def get_short_description(self):
        landmark_txt = self.get_landmark_name_with_probability() or '-'
        txt = f'Landmark: {landmark_txt}'
        coordinates = self.get_coordinates()
        if coordinates:
            txt += '\n'
            txt += f'Coordinates: {coordinates.get_short_description()}'
        return txt

    def get_labels(self) -> List[str]:
        labels = []
        label_annotations = self._response_dict.get(self.AnnotationFields.LABEL_ANNOTATIONS, [])
        for label_annotation in label_annotations:
            label = label_annotation['description']
            labels.append(label)
            # label_with_score = label_annotation['description'] + f' ({round(label_annotation["score"]*100)} %)'
        return labels

    def get_labels_for_localized_objects(self) -> List[str]:
        labels = []
        label_annotations = self._response_dict.get(self.AnnotationFields.LOCALIZED_OBJECT_ANNOTATIONS, [])
        for label_annotation in label_annotations:
            label = label_annotation['name']
            labels.append(label)
            # label_with_score = label_annotation['description'] + f' ({round(label_annotation["score"]*100)} %)'
        return labels

    @classmethod
    def to_dict_from_instance(cls, instance: Union[Any, None]):
        """ Required by parsiong interdace to save to a file """
        if instance is None:
            return None
        else:
            return instance.response_dict

    @classmethod
    def from_dict(cls, value):
        if value is None:
            return None
        else:
            return cls(value)
