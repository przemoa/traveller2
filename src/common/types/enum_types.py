import enum


class SelectableInCombobxType(enum.Enum):
    @staticmethod
    def get_readable_name(enum_type):
        return enum_type.name.lower().title().replace('_', ' ')

    @classmethod
    def get_all_names(cls):
        all_names = [cls.get_readable_name(e) for e in cls]
        return all_names

    @classmethod
    def get_enum_for_unknown_value(cls):
        raise NotImplementedError(f'get_enum_corresponding_to_empty_or_none_value '
                                  f'not implemented in "{cls.__name__}!"')


class PlaceType(SelectableInCombobxType):
    UNKNOWN = enum.auto()
    RIVER = enum.auto()
    LAKE = enum.auto()
    MOUNTAIN_PEAK = enum.auto()
    MUSEUM = enum.auto()
    CASTLE = enum.auto()
    LIGHTHOUSE = enum.auto()
    CHURCH = enum.auto()
    PARK = enum.auto()

    @classmethod
    def get_enum_for_unknown_value(cls):
        return cls.UNKNOWN


class JourneyType(SelectableInCombobxType):
    UNKNOWN = enum.auto()
    PACKAGE_HOLIDAY = enum.auto()
    SELF_ORGANISED = enum.auto()
    BUSINESS_TRIP = enum.auto()
    SCHOOL_TRIP = enum.auto()

    @classmethod
    def get_enum_for_unknown_value(cls):
        return cls.UNKNOWN


class PersonType(SelectableInCombobxType):
    UNKNOWN = enum.auto()
    FAMILY = enum.auto()
    FRIEND = enum.auto()
    COWORKER = enum.auto()

    @classmethod
    def get_enum_for_unknown_value(cls):
        return cls.UNKNOWN



if __name__ == '__main__':
    print(PlaceType.get_all_names())
