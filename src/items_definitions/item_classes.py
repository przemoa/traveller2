import re
from dataclasses import dataclass
from typing import List

from items_definitions.item_fields import ItemField, ItemFieldInt, ItemFieldDateTime, ItemFieldBigStr, ItemFieldBool, \
    ItemFieldStr, ItemFieldDateTimePoint, \
    ItemFieldLocation, ItemFieldDirectlyAssociatedItems, ItemFieldJourney, ItemFieldCoordinates, \
    ItemFieldPhoto, ItemFieldAdminEntity, ItemFieldPlaceType, ItemFieldFileRelativeOrAbsolutePath, \
    ItemFieldCountry, ItemFieldRegionMajor, ItemFieldRegionMinor, ItemFieldFileBlob, \
    ItemFieldAttachmentParent, ItemFieldGeoname, ItemFieldStrWithShowWikipediaButton, ItemFieldReferencedByItems, \
    ItemFieldItem, ItemFieldAirport, ItemFieldDateTimeStart, ItemFieldDateTimeEnd, ItemFieldDateTimeAfter, \
    ItemFieldDateTimeBefore, ItemFieldJourneyType, ItemFieldPersonType, ItemFieldPerson, \
    ItemFieldPersonOrCompanions, ItemFieldGoogleImageAnnotation


@dataclass
class ItemClassDefinition:

    name: str
    base_class: str
    fields: List[ItemField]
    path: str  # file location in project directory
    is_final: bool = True
    additional_code: str = ''
    short_description_code: str = 'str(self)'
    additional_imports: str = ''
    additional_validator_before_adding_to_db: str = ''
    special_is_same_item_checker: str = ''

    def __post_init__(self):
        if self.name != 'Item' and self.base_class != 'Item' and not self.is_final:
            raise Exception("For now: only one base class allowed between "
                            "'Item' and final class. e.g. Item->Attachement->File. "
                            "To change it, modify generator to handle it correctly")

    def assert_that_valid(self):
        for field in self.fields:
            if not isinstance(field, ItemFieldItem):
                continue

            for allowed_item_type in field.allowed_item_types:
                class_for_allowed_item = get_item_class_name_definition(name=allowed_item_type)
                if field.should_go_into_belongings_of_associated_item:
                    requried_item_field_class = ItemFieldDirectlyAssociatedItems
                else:
                    requried_item_field_class = ItemFieldReferencedByItems

                if not any([isinstance(f, requried_item_field_class) for f in class_for_allowed_item.get_all_fields_with_base_classes()]):
                    raise Exception(f'Error! Class "{self.name}" in field "{field.field_name}" allows for items '
                                    f'of class "{allowed_item_type}", but it needs to '
                                    f'have field of type "{requried_item_field_class.__name__}"!')
        if len([f for f in self.fields if isinstance(f, ItemFieldDateTimeStart)]) > 1  \
                or len([f for f in self.fields if isinstance(f, ItemFieldDateTimeEnd)]) > 1 \
                or len([f for f in self.fields if isinstance(f, ItemFieldDateTimePoint)]) > 1:
            raise Exception('Item has more than one ItemFieldDateTime of specific type!')

        for field in self.fields:
            if isinstance(field, (ItemFieldDateTimeAfter, ItemFieldDateTimeBefore)):
                raise Exception(f'Class field cannot be of type {field.__class__}!')

        if self.is_final:
            required_to_be_unique_fields = [field for field in self.get_all_fields_with_base_classes()
                                            if field.has_to_be_unique_for_item_class]
            if not required_to_be_unique_fields:
                if self.special_is_same_item_checker:
                    pass  # ok
                else:
                    raise Exception(f'Every item needs to have unique some field, but "{self.name}" has not! '
                                    f'(consider to allow for uniqueness of combination of fields)')
            else:
                if self.special_is_same_item_checker:
                    raise Exception(f'Item class cannot have special "is_same" checker and unique '
                                    f'fields at the same time! (class "{self.name}")')
                field = required_to_be_unique_fields[0]
                if not (field.not_none_value_required_in_constructor
                        and field.not_none_value_required_in_database):
                    raise Exception(f'For unique field "{field.field_name}" in item {self.name} not non default value is '
                                    f'required in constructor and database!')

    @property
    def member_name(self):
        return self.get_member_name_for_class_name(self.name)

    @staticmethod
    def get_member_name_for_class_name(class_name):
        subnames = re.findall('[A-Z][^A-Z]*', class_name)
        return '_'.join(subnames).lower()

    def get_base_class_import(self):
        if self.base_class is None:
            return ''
        else:
            base_class = get_item_class_name_definition(self.base_class)
            return base_class.get_class_import() + base_class.get_base_class_import()

    def get_class_import(self):
        if self.path:
            subpath = self.path.replace("/", ".") + '.'
        else:
            subpath = ''
        return f'from items.{subpath}{self.get_class_file_name()} import {self.name}, {self.name}Content\n'

    def get_class_file_name(self):
        return self.member_name

    def get_all_fields_with_base_classes(self):
        base_class_fields = []
        if self.base_class:
            base_class = get_item_class_name_definition(self.base_class)
            base_class_fields = base_class.get_all_fields_with_base_classes()
        return self.fields + base_class_fields

    def has_belongings(self):
        for field in self.get_all_fields_with_base_classes():
            if isinstance(field, ItemFieldDirectlyAssociatedItems):
                return True
        return False

    def can_be_referenced_by(self):
        for field in self.get_all_fields_with_base_classes():
            if isinstance(field, ItemFieldReferencedByItems):
                return True
        return False

    def has_belongings_or_can_be_referenced_by(self):
        return self.has_belongings() or self.can_be_referenced_by()

    def can_be_time_filtered(self) -> bool:
        for field in self.fields:
            if isinstance(field, (ItemFieldDateTimeStart, ItemFieldDateTimeEnd, ItemFieldDateTimePoint)):
                return True
        return False

ITEM_CLASSES = [
    ItemClassDefinition(
        name='Item',
        base_class=None,
        is_final=False,
        path='',
        additional_imports='import itertools',
        fields=[
            ItemFieldInt(field_name='item_id', display=True,
                         not_none_value_required_in_database=True,
                         not_none_value_required_in_constructor=False,
                         editable_in_ui=False,
                         filtering_enabled=True),
            ItemFieldDateTime(field_name='item_creation_time', display=True, editable_in_ui=False, filtering_enabled=False),
            ItemFieldDateTime(field_name='item_modification_time', display=False, editable_in_ui=False, filtering_enabled=False),
            ItemFieldBigStr(field_name='note', filtering_enabled=True),
            # ItemFieldBool(field_name='is_in_collection', saved_in_file=False, is_private=True, display=False)  # redundant, just check item_id
        ],
        additional_code=\
'''
    @staticmethod
    def get_id_for_item(item):
        if not item:
            return None
        assert item.item_id is not None
        return item.item_id
    
    def is_from_prompts_database(self):
        return hasattr(self, '_is_from_prompts_database') and self._is_from_prompts_database
        
    def set_as_from_prompts_database(self):
        self._is_from_prompts_database = True
    
    def get_belongings_set_recursively(self):
        belongings_belongings = [self.belongings]
        for item in self.belongings:
            if hasattr(item, 'belongings') and item.belongings:
                belongings_belongings.append(item.get_belongings_set_recursively())
        return set(itertools.chain.from_iterable(belongings_belongings))
'''

    ),

    ItemClassDefinition(
        name='Journey',
        base_class='Item',
        path='',
        fields=[
            ItemFieldStr(field_name='name', not_none_value_required_in_database=True, has_to_be_unique_for_item_class=True),
            ItemFieldDateTimeStart(field_name='start_time'),
            ItemFieldDateTimeEnd(field_name='end_time'),
            ItemFieldLocation(field_name='start_location', filtering_enabled=True),
            ItemFieldJourneyType(field_name='type', filtering_enabled=True),
            ItemFieldPersonOrCompanions(field_name='companion', filtering_enabled=True),
            ItemFieldDirectlyAssociatedItems(),
        ],
        short_description_code='f"{self.name}" + (f" ({self.start_time.to_month_year_string()})" if self.start_time else "")'
    ),

    ItemClassDefinition(
        name='Visit',
        base_class='Item',
        path='',
        fields=[
            ItemFieldDateTimeStart(field_name='start_time'),
            ItemFieldDateTimeEnd(field_name='end_time'),
            ItemFieldLocation(field_name='location', filtering_enabled=True,
                              not_none_value_required_in_database=True,
                              not_none_value_required_in_constructor=True),
            ItemFieldJourney(field_name='journey', filtering_enabled=True),
            ItemFieldPersonOrCompanions(field_name='companion', filtering_enabled=True),
            ItemFieldDirectlyAssociatedItems(),
            ItemFieldStr(field_name='name_suffix')
        ],
        short_description_code='(self.location.get_short_description() + (self.name_suffix or "") if self.location else "???") '
                               '+ (" (" + self.start_time.to_month_year_string() + ")" if self.start_time else "")',
        special_is_same_item_checker="{indentation}# if visit cannot be distinct basing on its description, it is not a unique visit!\n"
                                     "{indentation}return (self.get_short_description() == item.get_short_description())\n"
    ),

    ItemClassDefinition(
        name='Attachment',
        base_class='Item',
        path='attachment',
        is_final=False,
        fields=[
            ItemFieldFileRelativeOrAbsolutePath(field_name='original_full_path',
                                                not_none_value_required_in_database=True,
                                                not_none_value_required_in_constructor=True,
                                                # display=False,
                                                has_to_be_unique_for_item_class=True,
                                                editable_in_ui=False,
                                                filtering_enabled=True),
            ItemFieldFileRelativeOrAbsolutePath(field_name='relative_file_path',
                                                editable_in_ui=False,
                                                filtering_enabled=True,),
            ItemFieldAttachmentParent(field_name='attached_to', filtering_enabled=True),
        ],
        additional_imports='from utils.misc import get_full_file_path_for_absolute_or_relative_path',
        additional_code= \
"""
    def get_absolute_path_from_relative(self):
        return get_full_file_path_for_absolute_or_relative_path(self.relative_file_path or self.original_full_path)        
""",
    ),



    ItemClassDefinition(
        name='Photo',
        base_class='Attachment',
        path='attachment',
        fields=[
            ItemFieldPhoto(field_name='thumbnail'),
            ItemFieldDateTimePoint(field_name='photo_time', editable_in_ui=False),
            ItemFieldCoordinates(),
            ItemFieldStr(field_name='camera_model', editable_in_ui=False, filtering_enabled=True),
            ItemFieldGoogleImageAnnotation(field_name='annotation', editable_in_ui=True, filtering_enabled=True),
            ItemFieldBool(field_name='ignore_annotation', editable_in_ui=True, filtering_enabled=True),
        ],
        short_description_code='ntpath.basename(self.original_full_path)',
        additional_imports='import ntpath\nimport os\nfrom utils.misc import get_dir_name_from_file_path',
        additional_code=\
'''
    def get_dir_name(self):
        return get_dir_name_from_file_path(self.original_full_path)
    
    def get_file_name(self):
        return os.path.basename(self.original_full_path)
'''
    ),

    ItemClassDefinition(
        name='File',
        base_class='Attachment',
        path='attachment',
        fields=[
            ItemFieldStr(field_name='file_checksum_md5', editable_in_ui=False),
            ItemFieldFileBlob(field_name='file_blob'),
        ],
        short_description_code='ntpath.basename(self.original_full_path)',
        additional_imports='import ntpath',
    ),


    ItemClassDefinition(
        name='AdminEntity',
        base_class='Item',
        path='location/admin_entity',
        is_final=False,
        fields=[
            ItemFieldCoordinates(),
            ItemFieldReferencedByItems(comment='Items other than AdminEntity items referencing it...'),
        ],
    ),

    ItemClassDefinition(
        name='Country',
        base_class='AdminEntity',
        path='location/admin_entity',
        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='name', not_none_value_required_in_database=True, filtering_enabled=True, has_to_be_unique_for_item_class=True),
            ItemFieldStr(field_name='iso_code', not_none_value_required_in_database=True, filtering_enabled=True, has_to_be_unique_for_item_class=True),
            ItemFieldDirectlyAssociatedItems(comment='List of Regions Major'),
        ],
        short_description_code='f"{self.name} ({self.iso_code})"',
    ),

    ItemClassDefinition(
        name='RegionMajor',
        base_class='AdminEntity',
        path='location/admin_entity',
        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='name', not_none_value_required_in_database=True, filtering_enabled=True),
            ItemFieldStr(field_name='non_ascii_name'),
            ItemFieldGeoname(field_name='geoname_code', editable_in_ui=True, has_to_be_unique_for_item_class=True,
                             not_none_value_required_in_database=True),
            ItemFieldCountry('country', filtering_enabled=True, should_go_into_belongings_of_associated_item=True),
            ItemFieldDirectlyAssociatedItems(comment='List of Regions Minor'),
        ],
        short_description_code='f"{self.name} ({self.country.iso_code})"',
        additional_validator_before_adding_to_db="""
{intendation}if not item.country:
{intendation}    raise Exception('country field is required for region major!')
{intendation}for other_region_major in item.country.belongings:
{intendation}    if item != other_region_major and other_region_major.name == item.name:
{intendation}        raise ItemNotUniqueError('Region major name is not unique among country!')
""",
        additional_code="""
    def get_parent_location(self):
        return self.country
            
    def set_parent_location(self, parent_location):
        self.country = parent_location
""",
    ),

    ItemClassDefinition(
        name='RegionMinor',
        base_class='AdminEntity',
        path='location/admin_entity',
        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='name', not_none_value_required_in_database=True, filtering_enabled=True),
            ItemFieldStr(field_name='non_ascii_name'),
            ItemFieldGeoname(field_name='geoname_code', editable_in_ui=True, has_to_be_unique_for_item_class=True,
                             not_none_value_required_in_database=True),
            ItemFieldRegionMajor('region_major', filtering_enabled=True, should_go_into_belongings_of_associated_item=True),
            ItemFieldDirectlyAssociatedItems(comment='List of Cities'),
        ],
        short_description_code='f"{self.name} ({self.region_major.country.iso_code})"',
        additional_validator_before_adding_to_db="""
{intendation}if not item.region_major:
{intendation}    raise Exception('region_major field is required for region minor!')
{intendation}for other_region_minor in item.region_major.belongings:
{intendation}    if item != other_region_minor and other_region_minor.name == item.name:
{intendation}        raise ItemNotUniqueError('Region minor name is not unique among region major!')
""",
        additional_code="""
    def get_parent_location(self):
        return self.region_major
            
    def set_parent_location(self, parent_location):
        self.region_major = parent_location
""",
    ),

    ItemClassDefinition(
        name='City',
        base_class='AdminEntity',
        path='location/admin_entity',
        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='name', not_none_value_required_in_database=True, filtering_enabled=True),
            ItemFieldStr(field_name='non_ascii_name'),
            ItemFieldGeoname(field_name='geoname_code', editable_in_ui=True, has_to_be_unique_for_item_class=True,
                             not_none_value_required_in_database=True),
            ItemFieldRegionMinor('region_minor', filtering_enabled=True, should_go_into_belongings_of_associated_item=True),
            ItemFieldInt(field_name='population'),
        ],
        short_description_code='f"{self.name} ({self.region_minor.region_major.country.iso_code})"',
        additional_validator_before_adding_to_db="""
{intendation}if not item.region_minor:
{intendation}    raise Exception('region_minor field is required for city!')
{intendation}for other_city in item.region_minor.belongings:
{intendation}    if item != other_city and other_city.name == item.name:
{intendation}        raise ItemNotUniqueError('City name is not unique among region minor!')
""",
        additional_code="""
    def get_parent_location(self):
        return self.region_minor
    
    def set_parent_location(self, parent_location):
        self.region_minor = parent_location
""",
    ),

    ItemClassDefinition(
        name='Airport',
        base_class='Item',
        path='',

        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='municipality', filtering_enabled=True, not_none_value_required_in_database=True),
            ItemFieldStrWithShowWikipediaButton(field_name='name', filtering_enabled=True, not_none_value_required_in_database=True, has_to_be_unique_for_item_class=True),
            ItemFieldCoordinates(),
            ItemFieldAdminEntity(field_name='location'),
            ItemFieldReferencedByItems(),
        ],
        short_description_code = 'self._get_name_for_display()',

        additional_code="""
    def _get_name_for_display(self):
        location_str = f" - {self.location.get_short_description()}" if self.location else ""
        name_str = self.name.replace(self.municipality, "").replace("Airport", "").strip(" ").strip("-")  
        name_str_with_braces = (" [" + name_str + "]") if name_str else ''
        return self.municipality + name_str_with_braces + location_str
        """
    ),

    ItemClassDefinition(
        name='Flight',
        base_class='Item',
        path='',
        fields=[
            ItemFieldDateTimeStart(field_name='start_time'),
            ItemFieldDateTimeEnd(field_name='end_time'),
            ItemFieldAttachmentParent(field_name='attached_to', filtering_enabled=True),
            ItemFieldAirport(field_name='starting_airport', not_none_value_required_in_database=True),
            ItemFieldAirport(field_name='destination_airport', not_none_value_required_in_database=True),
        ],
        short_description_code='f"{self.starting_airport.municipality} - {self.destination_airport.municipality}" + (f" ({self.start_time.to_month_year_string()} )" if self.start_time else "")',
        special_is_same_item_checker="{indentation}# if flight cannot be distinct basing on its description (or date), it is not a unique flight!\n"
                                     "{indentation}return (self.get_short_description() == item.get_short_description() and self.start_time == item.start_time)\n"
    ),

    ItemClassDefinition(
        name='Place',
        base_class='Item',
        path='location',
        fields=[
            ItemFieldStrWithShowWikipediaButton(field_name='name', not_none_value_required_in_database=True,
                                                filtering_enabled=True, has_to_be_unique_for_item_class=True),
            ItemFieldPlaceType(filtering_enabled=True, field_name='type'),
            ItemFieldCoordinates(),
            ItemFieldAdminEntity(field_name='location'),
            ItemFieldReferencedByItems(),
        ],
        short_description_code='f"{self.name}"',
    ),

    ItemClassDefinition(
        name='Person',
        base_class='Item',
        path='',
        fields=[
            ItemFieldStr(field_name='name', filtering_enabled=True,
                         not_none_value_required_in_database=True, has_to_be_unique_for_item_class=True),
            ItemFieldPersonType(filtering_enabled=True, field_name='type'),
            ItemFieldReferencedByItems(),
        ],
        short_description_code='self.name + (" (" + self.type + ")" if self.type else "")',
    ),

    ItemClassDefinition(
        name='Companion',
        base_class='Item',
        path='',
        fields=[
            ItemFieldStr(field_name='title', filtering_enabled=True,
                         not_none_value_required_in_database=True, has_to_be_unique_for_item_class=True),
            ItemFieldPerson(field_name='person_1', not_none_value_required_in_constructor=True, not_none_value_required_in_database=True),
            ItemFieldPerson(field_name='person_2'),
            ItemFieldPerson(field_name='person_3'),
            ItemFieldPerson(field_name='person_4'),
            ItemFieldPerson(field_name='person_5'),
            ItemFieldReferencedByItems(),
        ],
        short_description_code='self.title + " [" + ", ".join([c.get_short_description() for c in self.get_persons()]) + "]"',
        additional_code=\
"""
    def get_persons(self):
        all_persons = []
        if self.person_1:
            all_persons.append(self.person_1)
        if self.person_2:
            all_persons.append(self.person_2)
        if self.person_3:
            all_persons.append(self.person_3)
        if self.person_4:
            all_persons.append(self.person_4)
        if self.person_5:
            all_persons.append(self.person_5)
        return all_persons
"""
        ),

]


def get_item_class_name_definition(name: str) -> ItemClassDefinition:
    for item_class_definition in ITEM_CLASSES:
        if item_class_definition.name == name:
            return item_class_definition
    raise Exception(f'Unknown item class defnition for "{name}"')


# def get_own_fields_for_item_class_definition_by_name(name: str) -> list:
#     """ From item itself WITHOUT base classes """
#     item_class_definition = get_item_class_name_definition(name)
#     return item_class_definition.fields


def get_all_fields_for_item_class_definition_by_name(name: str) -> list:
    """ From item itself WITH all base classes (recursively)"""

    item_class_definition = get_item_class_name_definition(name)
    own_items = item_class_definition.fields
    base_classes_items = []
    if item_class_definition.base_class is not None:
        base_classes_items = \
            get_all_fields_for_item_class_definition_by_name(item_class_definition.base_class)
    return base_classes_items + own_items


# mandatory validation
for item_class in ITEM_CLASSES:
    item_class.assert_that_valid()
