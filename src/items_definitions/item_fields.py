from typing import List

from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from common.types.image_annotation import ImageAnnotation
from items_definitions.item_field_ui import ItemFieldUiLineEdit, ItemFieldUiSpinBox, ItemFieldUiCheckbox, \
    ItemFieldUiToDo, ItemFieldUiDateTime, ItemFieldUiPlainTextEdit, ItemFieldUiItem, ItemFieldUiPhoto, \
    ItemFieldUiCoordinates, ItemFieldUiFileBlob, ItemFieldUiMultiplePhotos, ItemFieldUiLineEditWithGuidGeneration, \
    ItemFieldUiLineEditWithWikipediaButton, ItemFieldUiPlaceType, ItemFieldUiJourneyType, ItemFieldUiPersonType, \
    ItemFieldUiLineEditWithOpenFile, ItemFieldUiGoogleImageAnnotation


class ItemField:
    def __init__(self,
                 field_name: str,
                 display: bool,
                 type_in_item: str,
                 item_field_ui=ItemFieldUiToDo,
                 type_in_file: str = None,
                 setter: str = None,
                 saved_in_file: bool = True,
                 serializer: str = None,
                 deserializer: str = None,
                 required_imports: List[str] = [],
                 default_value=None,
                 not_none_value_required_in_database: bool = False,  # if field is required to be in database to create item. Sets also not_none_value_required_in_constructor by default
                 not_none_value_required_in_constructor=None,  # if field is required during item contruction. e.g. 'item_id' is not. None if same as above value
                 is_private: bool = False,
                 is_item_subtype: bool = False,  # if field type (in item) is item subtype
                 should_go_into_belongings_of_associated_item=True,  # if some class (e.g. Visit) contains field that is item (e.g. Location, which can be e.g. City), 'False' will prevent from including into belongings (visit will not go into belongings of the city)
                 editable_in_ui: bool = True,
                 comment = '',
                 filtering_enabled: bool = False,
                 has_to_be_unique_for_item_class: bool = False  # e.g. if journy name have to be unique among other journies (before adding to db)
                 ):
        self.field_name = field_name
        self.type_in_item = type_in_item

        if display and not item_field_ui:
            raise Exception('ItemField cannot be displayed, because there is no ui assigned for it!')

        if type_in_file is None:
            type_in_file = type_in_item
        self.type_in_file = type_in_file

        self.setter = setter
        self.saved_in_file = saved_in_file
        self.serializer = serializer
        self.deserializer = deserializer
        self.required_imports = required_imports
        self.item_field_ui = item_field_ui
        self.display = display
        self.default_value = default_value
        self.not_none_value_required_in_database = not_none_value_required_in_database

        if not_none_value_required_in_constructor is None:
            not_none_value_required_in_constructor = not_none_value_required_in_database
        self.not_none_value_required_in_constructor = not_none_value_required_in_constructor

        self.is_private = is_private
        self.is_item_subtype = is_item_subtype
        self.should_go_into_belongings_of_associated_item = should_go_into_belongings_of_associated_item  # if not, item will go to 'referenced_by` list in referenced item
        self.editable_in_ui = editable_in_ui
        self.comment = comment
        self.filtering_enabled = filtering_enabled
        self.has_to_be_unique_for_item_class = has_to_be_unique_for_item_class

    def get_setter_for_associated_item(self):
        member_name = f'_{self.field_name}'
        if self.should_go_into_belongings_of_associated_item:
            container_field_name = 'belongings'
        else:
            container_field_name = 'referenced_by'
        txt = ''
        txt += ' ' * 8 + f'if self.{member_name} is not None and self.item_id:\n'
        txt += ' ' * 12 + f'assert isinstance(self.{member_name}, Item)\n'
        txt += ' ' * 12 + f'self.{member_name}.{container_field_name}.remove(self)\n\n'

        txt += ' ' * 8 + f'self.{member_name} = value\n'
        txt += ' ' * 8 + f'if value is not None and self.item_id:\n'
        txt += ' ' * 12 + f'if value.item_id is None:\n'
        txt += ' ' * 16 + f'raise ValueError("Associated item have to be already in database!")\n'
        txt += ' ' * 12 + f'value.{container_field_name}.append(self)'
        return txt


class ItemFieldStr(ItemField):
    def __init__(self,
                 field_name: str,
                 display: bool = True,
                 item_field_ui=ItemFieldUiLineEdit,
                 **kwargs):
        super().__init__(display=display,
                         item_field_ui=item_field_ui,
                         field_name=field_name,
                         type_in_item='str',
                         **kwargs)


class ItemFieldGoogleImageAnnotation(ItemField):
    def __init__(self,
                 field_name: str,
                 display: bool = True,
                 item_field_ui=ItemFieldUiGoogleImageAnnotation,
                 **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item=ImageAnnotation.__name__,
                         type_in_file='dict',
                         serializer='ImageAnnotation.to_dict_from_instance',
                         deserializer='ImageAnnotation.from_dict',
                         item_field_ui=item_field_ui,
                         required_imports=['from common.types.image_annotation import ImageAnnotation'],
                         **kwargs)


class ItemFieldStrWithShowWikipediaButton(ItemFieldStr):
    def __init__(self,
                 field_name: str,
                 item_field_ui=ItemFieldUiLineEditWithWikipediaButton,
                 **kwargs):
        super().__init__(item_field_ui=item_field_ui,
                         field_name=field_name,
                         **kwargs)


class ItemFieldGeoname(ItemFieldStr):
    def __init__(self,
                 field_name: str,
                 display: bool = True,
                 item_field_ui=ItemFieldUiLineEditWithGuidGeneration,
                 **kwargs):
        super().__init__(display=display,
                         item_field_ui=item_field_ui,
                         field_name=field_name,
                         **kwargs)


class ItemFieldFileRelativeOrAbsolutePath(ItemFieldStr):
    def __init__(self, **kwargs):
        super().__init__(item_field_ui=ItemFieldUiLineEditWithOpenFile,
                         **kwargs)


class ItemFieldInt(ItemField):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='int',
                         item_field_ui=ItemFieldUiSpinBox,
                         **kwargs)


class ItemFieldBool(ItemField):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='bool',
                         item_field_ui=ItemFieldUiCheckbox,
                         **kwargs)


class ItemFieldBlob(ItemField):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='bytes',
                         editable_in_ui=False,
                         **kwargs)


class ItemFieldPhoto(ItemFieldBlob):
    def __init__(self, **kwargs):
        super().__init__(**kwargs, item_field_ui=ItemFieldUiPhoto)


class ItemFieldMultiplePhotos(ItemField):
    """ Not used in db, not stored in any item.
     Used to conform to existing interface in displaying widgets """
    def __init__(self, **kwargs):
        super().__init__(**kwargs, item_field_ui=ItemFieldUiMultiplePhotos, display=True, type_in_item=None)


class ItemFieldFileBlob(ItemFieldBlob):
    def __init__(self, **kwargs):
        super().__init__(**kwargs, item_field_ui=ItemFieldUiFileBlob)


class ItemFieldBigStr(ItemField):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='str',
                         item_field_ui=ItemFieldUiPlainTextEdit,
                         **kwargs)


class ItemFieldDateTime(ItemField):
    def __init__(self, field_name: str, display: bool = True, filtering_enabled: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item=CustomDateTime.__name__,
                         type_in_file='int',
                         filtering_enabled=filtering_enabled,
                         item_field_ui=ItemFieldUiDateTime,
                         serializer='CustomDateTime.to_unix_time_from_instance',
                         deserializer='CustomDateTime.from_int_or_none',
                         required_imports=['from common.types.custom_datetime import CustomDateTime'],
                         **kwargs)


class ItemFieldDateTimeStart(ItemFieldDateTime):
    """ Start of action (journey, visit) """
    pass


class ItemFieldDateTimeEnd(ItemFieldDateTime):
    """ End of action (journey, visit) """
    pass


class ItemFieldDateTimePoint(ItemFieldDateTime):
    """ Action time (e.g. photo time) """
    pass


class ItemFieldCoordinates(ItemField):
    def __init__(self, field_name: str = 'coordinates', display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item=Coordinates.__name__,
                         type_in_file='Tuple[float, float]',
                         serializer='Coordinates.to_tuple_from_instance',
                         deserializer='Coordinates.from_tuple',
                         required_imports=['from common.types.coordinates import Coordinates'],
                         item_field_ui=ItemFieldUiCoordinates,
                         **kwargs)


class ItemFieldItem(ItemField):
    """ E.g. for attachment, describes parent item (to which it is attached) """
    def __init__(self, field_name: str,
                 allowed_item_types: List[str],
                 display: bool = True,
                 type_in_item='Item',
                 item_field_ui=ItemFieldUiItem,
                 **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item=type_in_item,
                         type_in_file='int',
                         serializer='Item.get_id_for_item',
                         is_item_subtype=True,
                         is_private=True,
                         item_field_ui=item_field_ui,
                         **kwargs)
        self.allowed_item_types = allowed_item_types


class ItemFieldAttachmentParent(ItemFieldItem):
    def __init__(self, **kwargs):
        super().__init__(allowed_item_types=['Journey', 'Visit'],
                         **kwargs)


class ItemFieldLocation(ItemFieldItem):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='Item',
                         required_imports=[''],
                         allowed_item_types=['Place', 'AdminEntity'],
                         should_go_into_belongings_of_associated_item=False,
                         **kwargs)


class ItemFieldJourney(ItemFieldItem):
    def __init__(self, field_name: str, display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='Journey',
                         allowed_item_types=['Journey'],
                         required_imports=['from items.journey import Journey'],
                         **kwargs)


class ItemFieldDirectlyAssociatedItems(ItemField):  # for items that belong to parent, e.g. photo belongs to journey/visit
    """ Items which are associated to that item (are dependent) and belong to it"""
    def __init__(self, display: bool = True, **kwargs):
        super().__init__(display=False,  # no need to display in edit_widget, will be displayed in special window below
                         field_name='belongings',  # only this name allowed
                         saved_in_file=False,
                         is_private=True,
                         type_in_item='List[Item]',
                         type_in_file=None,
                         default_value=[],
                         not_none_value_required_in_database=True,
                         not_none_value_required_in_constructor=False,
                         editable_in_ui=False,
                         **kwargs)


class ItemFieldReferencedByItems(ItemField):  # for items not belonging, but only referencing other item, e.g. city visit references the city
    """ Items which are associated to that item (are dependent) and jutst reference it"""
    def __init__(self, display: bool = False, **kwargs):
        super().__init__(display=False,
                         field_name='referenced_by',  # only this name allowed
                         saved_in_file=False,
                         is_private=True,
                         type_in_item='List[Item]',
                         type_in_file=None,
                         default_value=[],
                         not_none_value_required_in_database=True,
                         not_none_value_required_in_constructor=False,
                         editable_in_ui=False,
                         **kwargs)


class ItemFieldAdminEntity(ItemFieldItem):
    def __init__(self,
                 field_name: str,
                 allowed_item_types=['AdminEntity'],  # if not single location type specified (Country, City, ...) generic widget for selection of all types will be used
                 display: bool = True,
                 type_in_item='AdminEntity',
                 required_imports=['from items.location.admin_entity.admin_entity import AdminEntity'],
                 should_go_into_belongings_of_associated_item=False,
                 **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item=type_in_item,
                         required_imports=required_imports,
                         item_field_ui=ItemFieldUiItem,
                         allowed_item_types=allowed_item_types,
                         should_go_into_belongings_of_associated_item=should_go_into_belongings_of_associated_item,
                         **kwargs)


class ItemFieldCountry(ItemFieldAdminEntity):
    def __init__(self, field_name: str = 'country', display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='Country',
                         required_imports=['from items.location.admin_entity.country import Country'],
                         allowed_item_types=['Country'],
                         **kwargs)


class ItemFieldRegionMajor(ItemFieldAdminEntity):
    def __init__(self, field_name: str = 'region_major', display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='RegionMajor',
                         required_imports=['from items.location.admin_entity.region_major import RegionMajor'],
                         allowed_item_types=['RegionMajor'],
                         **kwargs)


class ItemFieldRegionMinor(ItemFieldAdminEntity):
    def __init__(self, field_name: str = 'region_minor', display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='RegionMinor',
                         required_imports=['from items.location.admin_entity.region_minor import RegionMinor'],
                         allowed_item_types=['RegionMinor'],
                         **kwargs)


class ItemFieldCity(ItemFieldAdminEntity):
    def __init__(self, field_name: str = 'city', display: bool = True, **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         type_in_item='City',
                         required_imports=['from items.location.admin_entity.city import city'],
                         allowed_item_types=['City'],
                         **kwargs)


class ItemFieldPlaceType(ItemField):
    def __init__(self, display: bool = True, field_name: str = 'place_type', **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         # should_go_into_belongings_of_associated_item=False,
                         type_in_item='str',
                         type_in_file='str',
                         item_field_ui=ItemFieldUiPlaceType,
                         **kwargs)


class ItemFieldJourneyType(ItemField):
    def __init__(self, display: bool = True, field_name: str = 'journey_type', **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         # should_go_into_belongings_of_associated_item=False,
                         type_in_item='str',
                         type_in_file='str',
                         item_field_ui=ItemFieldUiJourneyType,
                         **kwargs)


class ItemFieldPersonType(ItemField):
    def __init__(self, display: bool = True, field_name: str = 'place_type', **kwargs):
        super().__init__(display=display,
                         field_name=field_name,
                         # should_go_into_belongings_of_associated_item=False,
                         type_in_item='str',
                         type_in_file='str',
                         item_field_ui=ItemFieldUiPersonType,
                         **kwargs)


class ItemFieldAirport(ItemFieldItem):
    def __init__(self, **kwargs):
        super().__init__(allowed_item_types=['Airport'],
                         should_go_into_belongings_of_associated_item=False,
                         **kwargs)


# special fields used only for filtering by time. Do not use them for classes in item_classes
class ItemFieldDateTimeBefore(ItemFieldDateTime):
    def __init__(self, field_name='before', **kwargs):
        super().__init__(
            field_name=field_name,
            **kwargs)


class ItemFieldDateTimeAfter(ItemFieldDateTime):
    def __init__(self, field_name='after', **kwargs):
        super().__init__(
            field_name=field_name,
            **kwargs)


class ItemFieldPerson(ItemFieldItem):
    def __init__(self, **kwargs):
        super().__init__(type_in_item='Person',
                         allowed_item_types=['Person'],
                         should_go_into_belongings_of_associated_item=False,
                         required_imports=['from items.person import Person'],
                         **kwargs)


class ItemFieldPersonOrCompanions(ItemFieldItem):
    def __init__(self, **kwargs):
        super().__init__(allowed_item_types=['Person', 'Companion'],
                         should_go_into_belongings_of_associated_item=False,
                         **kwargs)
