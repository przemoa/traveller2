import enum
import logging
import os
import uuid
from dataclasses import dataclass
from typing import Any, Union

from PySide2 import QtWidgets, QtCore
from PySide2.QtCore import Signal, QObject, QDateTime, Slot, QLocale, QRegExp
from PySide2.QtGui import QRegExpValidator
from PySide2.QtWidgets import QFileDialog, QPushButton, QMessageBox

from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from common.types.enum_types import PlaceType, JourneyType, PersonType
from common.types.image_annotation import ImageAnnotation
from dialogs.nonmodal_info.single_marker_map.single_marker_map_dialog import SingleMarkerMapDialog
from dialogs.nonmodal_info.wikipedia_info.wikipedia_info_dialog import WikipediaInfoDialog
from tabs.common_ui.display_image.display_image_widget import DisplaySingleImageWidget, DisplayMultipleImagesWidget
from utils.gui import set_editability_for_edit, add_widget_or_layout_to_ui, ClickableLabel
from utils.misc import open_file_by_absolute_or_relative_path


class InvalidityType(enum.Enum):
    """ See ItemFieldUi docstring """
    ALWAYS_VALID = 'always_valid'
    ADDITIONAL_CHECKBOX = 'additional_checkbox'
    SPECIFIC_VALUE = 'specific_value'


@dataclass
class InvalidValueDefinition:
    """ See ItemFieldUi docstring """
    invalidity_type: InvalidityType = InvalidityType.ALWAYS_VALID
    specific_value: Any = None

    @classmethod
    def create_for_specific_value(cls, specific_value):
        return cls(invalidity_type=InvalidityType.SPECIFIC_VALUE, specific_value=specific_value)

    @classmethod
    def create_for_checkbox_validation(cls):
        return cls(invalidity_type=InvalidityType.ADDITIONAL_CHECKBOX)


class ItemFieldUi(QObject):
    """ ui field contains left and right widget, emits signal when modified, and allows to set/get value

    ui may have:
    - 'always valid value', e.g. each value in spin_box will be returned as not None.
        But ui fild still has to handle setting value from None
    - 'additional checkbox' near name to determine if value is valid
        (if checkbox is uncheck, None will be returned)
    - 'specific value' in ui may be treated as None, e.g. empty text in line edit may be invalid and treated as None.
        In opposite direction, if None value is set, this special 'invalid value' will be set in ui
    """

    MINIMUM_FIRST_WIDGET_SIZE = 150
    DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH = 200
    DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH = 400
    BUTTON_MAX_WIDTH = 150

    signal_field_modified = Signal(object)
    signal_link_clicked = Signal(object)

    def __init__(self,
                 field_name: str,
                 field_definition,
                 engine,
                 invalid_value_definition: InvalidValueDefinition = InvalidValueDefinition(),
                 add_spacer_at_right=True,
                 clickable_links: bool = False,
                 is_filtering_item: bool = False,
                 ):
        super().__init__()
        self.is_filtering_item = is_filtering_item
        self.field_definition = field_definition
        self.clickable_links = clickable_links
        self.field_name = field_name
        self.left_widget = None
        self._right_widget = None
        self.engine = engine
        self.invalid_value_definition = invalid_value_definition  # type: InvalidValueDefinition
        self._validity_checkbox = None  # type: QtWidgets.QCheckBox  # additional checkbox to determine if ui value is valid or undefined
        self._whole_item__read_only = None  # whole displayed item. ONLY read allowed. Do not save to it!

        self.right_widget_layout = QtWidgets.QHBoxLayout()
        self.right_widget_layout.setObjectName(f"horizontalLayout_major__{field_name}_item")
        if add_spacer_at_right:
            self.spacer_at_right = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        else:
            self.spacer_at_right = None

    def setVisible(self, state):
        self.left_widget.setVisible(state)
        self._right_widget.setVisible(state)

    def set_engine(self, engine):
        self.engine = engine

    def _generate_label_widget(self, parent, field_name, is_editable, left_label_clickable=False):
        raw_label = self._generate_raw_label(parent=parent, field_name=field_name, left_label_clickable=left_label_clickable)

        if self.invalid_value_definition.invalidity_type == InvalidityType.ADDITIONAL_CHECKBOX:
            checkbox = QtWidgets.QCheckBox(parent)
            checkbox.setObjectName(f'checkbox_{field_name}_field_validity')
            checkbox.setLayoutDirection(QtCore.Qt.RightToLeft)
            checkbox.setText("")
            checkbox.setEnabled(is_editable)
            checkbox.stateChanged.connect(self._field_value_modified)
            self._validity_checkbox = checkbox

            horizontalLayout = QtWidgets.QHBoxLayout()
            horizontalLayout.setObjectName(f"horizontalLayout_{field_name}_label_widget")
            horizontalLayout.addWidget(raw_label)
            horizontalLayout.addWidget(checkbox)
            horizontalLayout._raw_label = raw_label
            return horizontalLayout
        else:
            return raw_label

    def _generate_raw_label(self, parent, field_name, left_label_clickable):
        if self.clickable_links and left_label_clickable:
            label = ClickableLabel(parent)
        else:
            label = QtWidgets.QLabel(parent)

        label.setObjectName(f'label_{field_name}')
        label.setText(field_name.replace('_', ' ').capitalize() + ':')
        return label

    @property
    def value(self):
        raise Exception('Deprecated property! Use get_value/set_value instead!')

    def _get_ui_value(self) -> Any:
        """ value returned from this function will not be None"""
        raise NotImplementedError

    def _set_ui_value(self, value: Any):
        """ Hear value may be None, ui field has to handle it"""
        raise NotImplementedError

    def _field_value_modified(self):
        self._set_right_widget_enabled_state_in_additional_combobx_mode()
        self.signal_field_modified.emit(self.get_value())

    def _perform_addition_setup_for_base_class(self):
        add_widget_or_layout_to_ui(parent_layout=self.right_widget_layout, widget_or_layout=self._right_widget)
        if self.spacer_at_right:
            add_widget_or_layout_to_ui(parent_layout=self.right_widget_layout, widget_or_layout=self.spacer_at_right)
        self._set_right_widget_enabled_state_in_additional_combobx_mode()

    def get_value(self) -> object:
        ui_value = self._get_ui_value()
        if self.invalid_value_definition.invalidity_type == InvalidityType.ALWAYS_VALID:
            return ui_value
        elif self.invalid_value_definition.invalidity_type == InvalidityType.SPECIFIC_VALUE:
            invalid_value = self.invalid_value_definition.specific_value
            if ui_value == invalid_value:
                return None
            else:
                return ui_value
        elif self.invalid_value_definition.invalidity_type == InvalidityType.ADDITIONAL_CHECKBOX:
            if self._validity_checkbox.isChecked():
                return ui_value
            else:
                return None

    def _set_right_widget_enabled_state_in_additional_combobx_mode(self):
        if self.invalid_value_definition.invalidity_type == InvalidityType.ADDITIONAL_CHECKBOX:
            self._right_widget.setEnabled(self._validity_checkbox.isChecked())

    def set_value(self, value, whole_item=None):
        self._whole_item__read_only = whole_item
        if self.invalid_value_definition.invalidity_type == InvalidityType.ALWAYS_VALID:
            self._set_ui_value(value)
        elif self.invalid_value_definition.invalidity_type == InvalidityType.SPECIFIC_VALUE:
            invalid_value = self.invalid_value_definition.specific_value
            if value is None:
                self._set_ui_value(invalid_value)
            else:
                self._set_ui_value(value)
        elif self.invalid_value_definition.invalidity_type == InvalidityType.ADDITIONAL_CHECKBOX:
            self._validity_checkbox.blockSignals(True)
            self._validity_checkbox.setChecked(value is not None)
            self._validity_checkbox.blockSignals(False)

            if value is None:
                pass
            else:
                self._set_ui_value(value)
            self._set_right_widget_enabled_state_in_additional_combobx_mode()


class ItemFieldUiToDo(ItemFieldUi):
    """ Dummy field for not defined yet fields :) """
    def __init__(self, parent, field_name: str, is_editable: bool, **kwargs):
        super().__init__(field_name, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)

        label_value = QtWidgets.QLabel(parent)
        label_value.setObjectName(f'label_{field_name}_value')
        label_value.setText('** TODO **')

        self._right_widget = label_value
        self._return_value = None
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> Union[Any, None]:
        return self._return_value

    def _set_ui_value(self, value: Union[Any, None]):
        self._return_value = value
        self._right_widget.setText(str(value))


class ItemFieldUiLineEdit(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 invalid_value_definition = InvalidValueDefinition.create_for_specific_value(''),  # by default: empty string is invalid
                 **kwargs):
        super().__init__(field_name, invalid_value_definition=invalid_value_definition, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        line_edit = QtWidgets.QLineEdit(parent)
        line_edit.setObjectName(f'line_edit_{field_name}')
        line_edit.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        line_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)
        set_editability_for_edit(line_edit, is_editable)
        self._right_widget = line_edit

        line_edit.textChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        return self._right_widget.text()

    def _set_ui_value(self, value: Union[str, None]):
        if value is None:
            value = ''
        self._right_widget.blockSignals(True)
        self._right_widget.setText(value)
        self._right_widget.blockSignals(False)


class ItemFieldUiLineEditWithWikipediaButton(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 invalid_value_definition = InvalidValueDefinition.create_for_specific_value(''),  # by default: empty string is invalid
                 **kwargs):
        super().__init__(field_name, invalid_value_definition=invalid_value_definition, **kwargs)

        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        self.line_edit = QtWidgets.QLineEdit(parent)
        self.line_edit.setObjectName(f'line_edit_{field_name}')
        self.line_edit.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        self.line_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)

        set_editability_for_edit(self.line_edit, is_editable)

        self.pushbutton_show_in_wikipedia = QPushButton(parent)
        self.pushbutton_show_in_wikipedia.setText('&Wikipedia')
        self.pushbutton_show_in_wikipedia.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}")
        horizontalLayout.addWidget(self.line_edit)
        horizontalLayout.addWidget(self.pushbutton_show_in_wikipedia)
        self._right_widget = horizontalLayout

        self.pushbutton_show_in_wikipedia.clicked.connect(self._show_in_wikipedia)

        self.line_edit.textChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        return self.line_edit.text()

    def _set_ui_value(self, value: Union[str, None]):
        if value is None:
            value = ''
        self.line_edit.blockSignals(True)
        self.line_edit.setText(value)
        self.line_edit.blockSignals(False)

    @Slot()
    def _show_in_wikipedia(self):
        WikipediaInfoDialog.display_info_in_dialog(parent=self._parent, info=self.line_edit.text())


class ItemFieldUiLineEditWithGuidGeneration(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 maximum_width=ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH,
                 invalid_value_definition = InvalidValueDefinition.create_for_specific_value(''),  # by default: empty string is invalid
                 **kwargs):
        super().__init__(field_name, invalid_value_definition=invalid_value_definition, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        self.line_edit = QtWidgets.QLineEdit(parent)
        self.line_edit.setObjectName(f'line_edit_{field_name}')
        self.line_edit.setMaximumWidth(maximum_width)
        self.line_edit.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        self.line_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)

        set_editability_for_edit(self.line_edit, is_editable)

        self.pushbutton_generate_guid = QPushButton(parent)
        self.pushbutton_generate_guid.setText('Generate random')
        self.pushbutton_generate_guid.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
        set_editability_for_edit(self.pushbutton_generate_guid, is_editable)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}")
        horizontalLayout.addWidget(self.line_edit)
        horizontalLayout.addWidget(self.pushbutton_generate_guid)
        self._right_widget = horizontalLayout

        self.line_edit.textChanged.connect(self._field_value_modified)
        self.pushbutton_generate_guid.clicked.connect(self._generate_guid)
        self._perform_addition_setup_for_base_class()

    def _get_guid(self):
        return str(uuid.uuid4())

    def _generate_guid(self):
        self.line_edit.setText(self._get_guid())

    def _get_ui_value(self) -> str:
        return self.line_edit.text()

    def _set_ui_value(self, value: Union[str, None]):
        if value is None or value == '':
            value = self._get_guid()
        self.line_edit.blockSignals(True)
        self.line_edit.setText(value)
        self.line_edit.blockSignals(False)


class ItemFieldUiSpinBox(ItemFieldUi):
    def __init__(self, parent, field_name: str, is_editable: bool, **kwargs):
        super().__init__(field_name, **kwargs)

        left_label_clickable = (field_name == 'item_id')

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable,
                                                       left_label_clickable=left_label_clickable)
        if left_label_clickable and self.clickable_links:
            if isinstance(self.left_widget, ClickableLabel):
                self.left_widget.clicked.connect(self._label_clicked)

        spin_box = QtWidgets.QSpinBox(parent)
        spin_box.setMaximum(1000000000)
        spin_box.setMinimum(-1000000000)
        spin_box.setObjectName(f'spin_box_{field_name}')
        spin_box.setMinimumWidth(ItemFieldUi.MINIMUM_FIRST_WIDGET_SIZE)
        set_editability_for_edit(spin_box, is_editable)
        self._right_widget = spin_box
        # self._right_widget.setMaximumWidth(max_widget_width)

        spin_box.valueChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    @Slot(str)
    def _label_clicked(self, txt):
        self.signal_link_clicked.emit(self._get_ui_value())

    def _get_ui_value(self) -> int:
        return self._right_widget.value()

    def _set_ui_value(self, value: Union[int, None]):
        if value is None:
            value = 0
        self._right_widget.blockSignals(True)
        self._right_widget.setValue(value)
        self._right_widget.blockSignals(False)


class ItemFieldUiCheckbox(ItemFieldUi):
    def __init__(self, parent, field_name: str, is_editable: bool, **kwargs):
        super().__init__(field_name, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        checkbox = QtWidgets.QCheckBox(parent)
        checkbox.setObjectName(f'checkbox{field_name}')
        set_editability_for_edit(checkbox, is_editable)
        checkbox.setLayoutDirection(QtCore.Qt.LeftToRight)
        checkbox.setText("")
        self._right_widget = checkbox

        checkbox.stateChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> bool:
        return self._right_widget.isChecked()

    def _set_ui_value(self, value: Union[bool, None]):
        if value is None:
            value = False
        self._right_widget.blockSignals(True)
        self._right_widget.setChecked(value)
        self._right_widget.blockSignals(False)


class ItemFieldUiDateTime(ItemFieldUi):
    def __init__(self, parent, field_name: str, is_editable: bool, **kwargs):
        super().__init__(
            field_name,
            invalid_value_definition=InvalidValueDefinition.create_for_checkbox_validation(),
            **kwargs)

        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)

        date_time_edit = QtWidgets.QDateTimeEdit(parent)
        date_time_edit.setObjectName(f'date_time_edit_{field_name}')
        date_time_edit.setProperty("showGroupSeparator", False)
        date_time_edit.setCalendarPopup(True)
        date_time_edit.setDisplayFormat("dd/MM/yyyy hh:mm")
        date_time_edit.setMinimumWidth(ItemFieldUi.MINIMUM_FIRST_WIDGET_SIZE)
        set_editability_for_edit(date_time_edit, is_editable)
        self.date_time_edit = date_time_edit

        self.pushbutton_show_in_timeline = QPushButton(parent)
        self.pushbutton_show_in_timeline.setText('&Timeline')
        self.pushbutton_show_in_timeline.setEnabled(False)
        self.pushbutton_show_in_timeline.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
        self.pushbutton_show_in_timeline.clicked.connect(self._show_in_timeline)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}")
        horizontalLayout.addWidget(self.date_time_edit)
        horizontalLayout.addWidget(self.pushbutton_show_in_timeline)

        self._right_widget = horizontalLayout
        self._right_widget.setEnabled = self._set_enabled

        date_time_edit.dateTimeChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> CustomDateTime:
        end_unix_time = self.date_time_edit.dateTime().toTime_t()
        return CustomDateTime(end_unix_time)

    def _set_enabled(self, state):
        self.pushbutton_show_in_timeline.setEnabled(state)
        self.date_time_edit.setEnabled(state)
        if not state:
            self._set_ui_value(None)

    def _set_ui_value(self, value: Union[CustomDateTime, None]):
        self.date_time_edit.blockSignals(True)
        qdatetime = QDateTime()
        if value is not None:
            qdatetime.setTime_t(value.unix_time)
        else:
            qdatetime.setTime_t(946681200)  # 01.01.2000 00:00

        self.date_time_edit.setDateTime(qdatetime)
        self.date_time_edit.blockSignals(False)

    @Slot()
    def _show_in_timeline(self):
        from dialogs.nonmodal_info.show_on_timeline.show_on_timeline_dialog import ShowOnTimelineDialog
        ShowOnTimelineDialog.display_info_in_dialog(
            parent=self._parent,
            info=self._get_ui_value(),
            engine=self.engine)


class ItemFieldUiPlainTextEdit(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 invalid_value_definition = InvalidValueDefinition.create_for_specific_value(''),  # by default: empty string is invalid
                 **kwargs):
        super().__init__(field_name, add_spacer_at_right=False, invalid_value_definition=invalid_value_definition, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        plain_text_edit = QtWidgets.QPlainTextEdit(parent)
        plain_text_edit.setObjectName(f'plain_text_edit_{field_name}')
        plain_text_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)
        plain_text_edit.setMinimumHeight(30)
        set_editability_for_edit(plain_text_edit, is_editable)
        self._right_widget = plain_text_edit

        plain_text_edit.textChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        return self._right_widget.toPlainText()

    def _set_ui_value(self, value: Union[str, None]):
        if value is None:
            value = ''
        self._right_widget.blockSignals(True)
        self._right_widget.setPlainText(value)
        self._right_widget.blockSignals(False)


class ItemFieldUiGoogleImageAnnotation(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 invalid_value_definition = InvalidValueDefinition.create_for_checkbox_validation(),
                 **kwargs):
        super().__init__(field_name, add_spacer_at_right=False, invalid_value_definition=invalid_value_definition, **kwargs)

        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        self._value = None
        plain_text_edit = QtWidgets.QPlainTextEdit(parent)
        plain_text_edit.setObjectName(f'plain_text_edit_{field_name}')
        plain_text_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)
        plain_text_edit.setMinimumHeight(30)
        set_editability_for_edit(plain_text_edit, is_editable)
        self.plain_text_edit = plain_text_edit

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}")
        horizontalLayout.addWidget(self.plain_text_edit)

        if not self.is_filtering_item:
            self.pushbutton_annotate = QPushButton(parent)
            self.pushbutton_annotate.setText('&Annotate')
            self.pushbutton_annotate.setEnabled(False)
            self.pushbutton_annotate.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
            self.pushbutton_annotate.clicked.connect(self._annotate)
            self.pushbutton_annotate.setVisible(is_editable)

            self.pushbutton_show_wiki = QPushButton(parent)
            self.pushbutton_show_wiki.setText('&Wikipedia')
            self.pushbutton_show_wiki.setEnabled(False)
            self.pushbutton_show_wiki.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
            self.pushbutton_show_wiki.clicked.connect(self._show_in_wikipedia)

            self.pushbutton_show_map = QPushButton(parent)
            self.pushbutton_show_map.setText('&Map')
            self.pushbutton_show_map.setEnabled(False)
            self.pushbutton_show_map.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
            self.pushbutton_show_map.clicked.connect(self._show_on_map)

            verticalLayout = QtWidgets.QVBoxLayout()
            verticalLayout.setObjectName(f"verticalLayout_buttons_{field_name}")
            verticalLayout.addWidget(self.pushbutton_annotate)
            verticalLayout.addWidget(self.pushbutton_show_wiki)
            verticalLayout.addWidget(self.pushbutton_show_map)
            self.verticalLayout_buttons = verticalLayout
            horizontalLayout.addItem(self.verticalLayout_buttons)

        self._right_widget = horizontalLayout
        self._right_widget.setEnabled = self._set_enabled
        if not self.is_filtering_item:
            set_editability_for_edit(self.plain_text_edit, False)

        plain_text_edit.textChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _set_enabled(self, state):
        self.plain_text_edit.setEnabled(state)
        if not state:
            self._set_ui_value(None)

    def _get_ui_value(self) -> str:
        if not self.is_filtering_item:
            return self._value
        else:
            return self.plain_text_edit.toPlainText()

    def _set_ui_value(self, value: Union[ImageAnnotation, None]):
        if not self.is_filtering_item:
            self.pushbutton_annotate.setEnabled(bool(self._whole_item__read_only))

        self._value = value
        if value is None:
            txt = ''
            if not self.is_filtering_item:
                self.pushbutton_show_map.setEnabled(False)
                self.pushbutton_show_wiki.setEnabled(False)
        else:
            txt = value.get_long_description()
            if not self.is_filtering_item:
                self.pushbutton_show_map.setEnabled(bool(value.get_coordinates()))
                self.pushbutton_show_wiki.setEnabled(bool(value.get_landmark_name()))

        self.plain_text_edit.blockSignals(True)
        self.plain_text_edit.setPlainText(txt)
        self.plain_text_edit.blockSignals(False)

    @Slot()
    def _annotate(self):
        from items_utils import annotate_photos

        if not self._whole_item__read_only:
            raise Exception('Cannot annotate photo, because no photo is currently selected!')
        absolute_path = self._whole_item__read_only.get_absolute_path_from_relative()
        if not os.path.isfile(absolute_path):
            raise Exception(f'Cannot annotate photo with path "{absolute_path}" - file does not exist!')
        try:
            annotation = annotate_photos.get_photos_annotations(photos=[self._whole_item__read_only])[0]
        except Exception as e:
            QMessageBox.critical(self._parent, "Error!", str(e))

        self.set_value(value=annotation, whole_item=self._whole_item__read_only)
        self._field_value_modified()

    @Slot()
    def _show_in_wikipedia(self):
        WikipediaInfoDialog.display_info_in_dialog(parent=self._parent, info=self._value.get_landmark_name())

    @Slot()
    def _show_on_map(self):
        SingleMarkerMapDialog.display_info_in_dialog(parent=self._parent, info=self._value.get_coordinates())


class ItemFieldUiItem(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 **kwargs):
        super().__init__(field_name, **kwargs)
        self._parent = parent

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)

        from items_utils.misc import get_item_class_by_name_string
        self._allowed_item_types = [get_item_class_by_name_string(item_name)
                                    for item_name in self.field_definition.allowed_item_types]

        if self.clickable_links:
            self._item_txt_label = ClickableLabel(parent)
            self._item_txt_label.clicked.connect(self._label_link_clicked)
        else:
            self._item_txt_label = QtWidgets.QLabel(parent)

        self._item_txt_label.setObjectName(f'label_item_{field_name}')

        self._item_select = QtWidgets.QPushButton(parent)
        self._item_select.setObjectName(f'push_button_item_{field_name}')
        self._item_select.setText('&Select...')
        self._item_select.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}_item")
        horizontalLayout.addWidget(self._item_txt_label)
        horizontalLayout.addWidget(self._item_select)

        self._item_txt_label.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self._item_txt_label.setSizePolicy(sizePolicy)

        set_editability_for_edit(self._item_select, is_editable)
        self._right_widget = horizontalLayout
        self._current_item = None

        # plain_text_edit.textChanged.connect(self._field_value_modified)
        self._item_select.clicked.connect(self._select_item)
        self._perform_addition_setup_for_base_class()

    def _label_link_clicked(self):
        self.signal_link_clicked.emit(self._current_item)

    def _get_ui_value(self) -> str:
        return self._current_item

    def _set_ui_value(self, value: Union[str, None]):
        self._current_item = value
        self._set_item_label_text()

    def _set_item_label_text(self):
        if self._current_item is None:
            txt = '-'
        else:
            txt = self._current_item.get_short_description()
        self._right_widget.blockSignals(True)
        self._item_txt_label.setText(txt)
        self._right_widget.blockSignals(False)

    def _select_item(self):
        from dialogs.select_item.select_item_dialog import select_item_from_dialog
        new_item = select_item_from_dialog(
            engine=self.engine,
            parent=self._parent,
            allowed_item_types=self._allowed_item_types,
            selected_at_start_item=self._current_item)
        if new_item != self._current_item:
            self._current_item = new_item
            self._set_item_label_text()
            self._field_value_modified()


class ItemFieldUiPhoto(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool = False,
                 **kwargs):
        super().__init__(field_name, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        photo_display_widget = DisplaySingleImageWidget(parent=parent)
        photo_display_widget.setObjectName(f'photo_display_widget_{field_name}')
        photo_display_widget.setMinimumHeight(130)
        photo_display_widget.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        self._right_widget = photo_display_widget

        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        raise NotImplementedError

    def _set_ui_value(self, value: Union[bytes, None]):
        self._right_widget.display_image(value)


class ItemFieldUiMultiplePhotos(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool = False,
                 **kwargs):
        super().__init__(field_name, **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        photos_display_widget = DisplayMultipleImagesWidget(parent=parent)
        photos_display_widget.setObjectName(f'photos_display_widget_{field_name}')
        self._right_widget = photos_display_widget

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self._right_widget.sizePolicy().hasHeightForWidth())
        self._right_widget.setSizePolicy(sizePolicy)

        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        raise NotImplementedError

    def _set_ui_value(self, value: Union[list, None]):  # List[Photo]
        if value is None:
            value = []
        self._right_widget.display_multiple_images(value)


class QDoubleSpinBoxExtended(QtWidgets.QDoubleSpinBox):
    prefered_separeator = QLocale.system().toString(1.23)[1]

    def __init__(self, parent):
        super().__init__(parent)
        self.validator = QRegExpValidator(self)
        self.validator.setRegExp(QRegExp("\\d{1,}(?:[,.]{1})\\d*\s*°*"))

    def valueFromText(self, text: str):
        fixed_text = text.replace(',', self.prefered_separeator).replace('.', self.prefered_separeator)
        return super().valueFromText(fixed_text)

    def validate(self, text, pos):
        return self.validator.validate(text, pos)


class ItemFieldUiCoordinates(ItemFieldUi):
    def __init__(self, parent, field_name: str, is_editable: bool, **kwargs):
        super().__init__(
            field_name,
            invalid_value_definition=InvalidValueDefinition.create_for_checkbox_validation(),
            **kwargs)
        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)

        self._lat_layout = self._create_label_with_spinbox(
            name='latitude', parent=parent, is_editable=is_editable, range=90)
        self._lat_layout.spin_box.valueChanged.connect(self._field_value_modified)

        self._lon_layout = self._create_label_with_spinbox(
            name='longitude', parent=parent, is_editable=is_editable, range=180)
        self._lon_layout.spin_box.valueChanged.connect(self._field_value_modified)

        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.vertical_layout.setObjectName(f"vertical_layout_{field_name}")
        self.vertical_layout.addItem(self._lat_layout)
        self.vertical_layout.addItem(self._lon_layout)

        horizontal_layout = QtWidgets.QHBoxLayout()
        horizontal_layout.setObjectName(f"horizontal_layout_{field_name}")
        horizontal_layout.addItem(self.vertical_layout)

        self.pushbutton_show_on_map = QtWidgets.QPushButton(parent)
        self.pushbutton_show_on_map.setText('Show on map')
        self.pushbutton_show_on_map.setEnabled(False)
        self.pushbutton_show_on_map.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)
        self.pushbutton_show_on_map.clicked.connect(self._show_item_on_map)
        horizontal_layout.addWidget(self.pushbutton_show_on_map)

        self._right_widget = horizontal_layout
        self._right_widget.setEnabled = self._set_enabled_for_spinboxes

        self._perform_addition_setup_for_base_class()

    def _show_item_on_map(self):
        coordinates = self._get_ui_value()
        SingleMarkerMapDialog.display_info_in_dialog(parent=self._parent, info=coordinates)

    def _set_enabled_for_spinboxes(self, state):
        self._lat_layout.spin_box.setEnabled(state)
        self._lon_layout.spin_box.setEnabled(state)
        self.pushbutton_show_on_map.setEnabled(state)
        if not state:
            self._set_ui_value(None)

    def _create_label_with_spinbox(self, name, parent, is_editable, range):
        label = QtWidgets.QLabel(parent)
        label.setObjectName(f'label_{name}')
        label_suffix = {'latitude': ' (N/S)', 'longitude': ' (W/E)'}.get(name, '')
        label.setText(name.capitalize() + label_suffix + ':')
        label.setMinimumWidth(110)

        spin_box = QDoubleSpinBoxExtended(parent)
        spin_box.setObjectName(f'spin_box_{name}')
        spin_box.setMaximum(range)
        spin_box.setMinimum(-range)
        spin_box.setDecimals(6)
        spin_box.setSuffix(' \u00B0')
        set_editability_for_edit(spin_box, is_editable)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{name}")
        horizontalLayout.addWidget(label)
        horizontalLayout.addWidget(spin_box)
        horizontalLayout.label = label
        horizontalLayout.spin_box = spin_box
        return horizontalLayout

    def _get_ui_value(self) -> Coordinates:
        return Coordinates(
            lat=self._lat_layout.spin_box.value(),
            lon=self._lon_layout.spin_box.value())

    def _set_ui_value(self, value: Coordinates):
        if value is None:
            # self.pushbutton_show_on_map.setEnabled(False)
            value = Coordinates(0, 0)
        # else:
        #     self.pushbutton_show_on_map.setEnabled(True)

        self._lat_layout.spin_box.blockSignals(True)
        self._lon_layout.spin_box.blockSignals(True)

        self._lat_layout.spin_box.setValue(value.lat)
        self._lon_layout.spin_box.setValue(value.lon)

        self._lat_layout.spin_box.blockSignals(False)
        self._lon_layout.spin_box.blockSignals(False)


class ItemFieldUiFileBlob(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool = False,
                 **kwargs):
        super().__init__(field_name, **kwargs)
        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        self._file_blob = None
        self._right_widget = QtWidgets.QPushButton(parent)
        self._right_widget.setObjectName(f'push_button_item_{field_name}')
        self._right_widget.setText('Save blob as...')
        self._right_widget.clicked.connect(self._save_as)

        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        raise NotImplementedError

    def _set_ui_value(self, value: Union[bytes, None]):
        self._right_widget.setEnabled(value is not None)
        self._file_blob = value

    def _save_as(self):
        assert self._file_blob is not None
        save_file_dialog = QFileDialog(self._parent)
        save_file_dialog.setWindowTitle("Select output file name")
        save_file_dialog.setAcceptMode(QFileDialog.AcceptSave)
        # save_file_dialog.setDefaultSuffix(".pkl")
        # save_file_dialog.setNameFilter("Pkl files (*.pkl);; All files (*)")
        # save_file_dialog.selectFile(current_path)

        if save_file_dialog.exec_() == QFileDialog.Accepted:
            file_path = save_file_dialog.selectedFiles()[0]
            with open(file_path, 'wb') as file:
                file.write(self._file_blob)


class ItemFieldUiComboboxSelectable(ItemFieldUi):
    def __init__(self, parent, field_name: str, is_editable: bool, enum_type, **kwargs):
        super().__init__(
            field_name,
            invalid_value_definition=InvalidValueDefinition.create_for_checkbox_validation(),
            **kwargs)

        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)

        combobox = QtWidgets.QComboBox(parent)
        combobox.setObjectName(f'combobox_edit_{field_name}')
        set_editability_for_edit(combobox, is_editable)
        self._right_widget = combobox
        self.enum_type = enum_type
        self._right_widget_original_setEnabled = self._right_widget.setEnabled
        self._right_widget.setEnabled = self._set_enabled


        combobox.currentIndexChanged.connect(self._field_value_modified)
        self._combobox_items_txt = enum_type.get_all_names()
        for txt in self._combobox_items_txt:
            combobox.addItem(txt)

        self._perform_addition_setup_for_base_class()

    def _set_enabled(self, state):
        if not state:
            self._set_ui_value(None)
        self._right_widget_original_setEnabled(state)

    def _get_ui_value(self) -> str:
        current_text = self._right_widget.currentText()
        return current_text

    def _set_ui_value(self, value: str):
        self._right_widget.blockSignals(True)
        if value in self._combobox_items_txt:
            self._right_widget.setCurrentText(value)
        else:
            if value:
                logging.warning(f'Unknown {self.enum_type.__class__.__name__}: "{value}"!')
            else:
                pass # do not show warning if the value is empty or None
            self._right_widget.setCurrentText(self.enum_type.get_readable_name(self.enum_type.get_enum_for_unknown_value()))

        self._right_widget.blockSignals(False)


class ItemFieldUiPlaceType(ItemFieldUiComboboxSelectable):
    def __init__(self, **kwargs):
        super().__init__(
            enum_type=PlaceType,
            **kwargs)


class ItemFieldUiJourneyType(ItemFieldUiComboboxSelectable):
    def __init__(self, **kwargs):
        super().__init__(
            enum_type=JourneyType,
            **kwargs)


class ItemFieldUiPersonType(ItemFieldUiComboboxSelectable):
    def __init__(self, **kwargs):
        super().__init__(
            enum_type=PersonType,
            **kwargs)


class ItemFieldUiLineEditWithOpenFile(ItemFieldUi):
    def __init__(self,
                 parent,
                 field_name: str,
                 is_editable: bool,
                 **kwargs):
        super().__init__(field_name, **kwargs)

        self._parent = parent
        self.left_widget = self._generate_label_widget(parent=parent,
                                                       field_name=field_name,
                                                       is_editable=is_editable)
        self.line_edit = QtWidgets.QLineEdit(parent)
        self.line_edit.setObjectName(f'line_edit_{field_name}')
        self.line_edit.setMaximumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MAXIMUM_WIDTH)
        self.line_edit.setMinimumWidth(ItemFieldUi.DEFAULT_RIGHT_WIDGET_MINIMUM_WIDTH)

        set_editability_for_edit(self.line_edit, is_editable)

        self.pushbutton_open_file = QPushButton(parent)
        self.pushbutton_open_file.setText('&Open')
        self.pushbutton_open_file.setMaximumWidth(ItemFieldUi.BUTTON_MAX_WIDTH)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.setObjectName(f"horizontalLayout_{field_name}")
        horizontalLayout.addWidget(self.line_edit)
        horizontalLayout.addWidget(self.pushbutton_open_file)
        self._right_widget = horizontalLayout

        self.pushbutton_open_file.clicked.connect(self._open_file)

        self.line_edit.textChanged.connect(self._field_value_modified)
        self._perform_addition_setup_for_base_class()

    def _get_ui_value(self) -> str:
        return self.line_edit.text()

    def _set_ui_value(self, value: Union[str, None]):
        if value is None:
            value = ''
        self.line_edit.blockSignals(True)
        self.line_edit.setText(value)
        self.line_edit.blockSignals(False)

    @Slot()
    def _open_file(self):
        absolute_or_relative_path=self.line_edit.text()
        open_file_by_absolute_or_relative_path(absolute_or_relative_path)

