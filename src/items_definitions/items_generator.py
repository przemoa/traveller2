""" This module allows to generate files with 'item' classes and 'collection' class.
Run to generate files.
"""
import copy
import os

from items_definitions.item_classes import ItemClassDefinition, ITEM_CLASSES, get_item_class_name_definition
from items_definitions.item_fields import ItemFieldDirectlyAssociatedItems, ItemFieldReferencedByItems, \
    ItemFieldDateTimePoint, ItemFieldDateTimeStart, \
    ItemFieldDateTimeEnd
from utils.misc import replace_invalid_plural_forms

AUTOMATIC_GENERATION_CLAUSE = \
    '# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".\n' \
    '# Edit is inadvisable!\n\n'


def _generate_required_imports(item_class):
    txt = item_class.get_base_class_import()
    required_imports = []
    for field in item_class.get_all_fields_with_base_classes():
        if field.required_imports:
            required_imports += field.required_imports
    txt += '\n'.join(sorted(list(set(required_imports)))) + '\n' + '\n'
    return txt


def _generate_item_class_content_txt(item_class):
    """Generate item class content, e.g. JourneyContent"""
    base_class_content_inheritance = f'({item_class.base_class}Content)' if item_class.base_class else ''
    data_class_name = f'{item_class.name}Content'

    data_class_members = ''
    validate_fields_content = ''
    if item_class.base_class:
        validate_fields_content += ' '*8 + f'super({item_class.name}Content, self).validate_fields_type()\n'

    for field in item_class.fields:
        if not field.saved_in_file:
            continue
        type_annotation = field.type_in_file if field.not_none_value_required_in_database else f'Union[{field.type_in_file}, None]'
        data_class_members += f'{" "*4}{field.field_name}: {type_annotation} = {field.default_value}\n'

        argname = f'field \\"{field.field_name}\\"'
        validate_fields_content += f'{" "*8}typeguard.check_type(argname="{argname}",\n'
        validate_fields_content += f'{" "*8}                     value=self.{field.field_name},\n'
        validate_fields_content += f'{" "*8}                     ' \
            f'expected_type={item_class.name}Content.__annotations__["{field.field_name}"])\n'



    data_class_content = \
f'''
@dataclass
class {data_class_name}{base_class_content_inheritance}:
{data_class_members}
    def validate_fields_type(self):
{validate_fields_content}
'''
    return data_class_content


def _generate_members_for_item_class(item_class):
    members = ''

    for field in item_class.fields:
        # type_in_item = field.type_in_item if not field.is_item_subtype else 'Item'
        type_in_item = field.type_in_item  # with cyclic dependencies...
        member_name = field.field_name
        if field.is_private:
            member_name = '_' + member_name
        # if field.not_none_value_required_in_constructor:
        if isinstance(field, (ItemFieldDirectlyAssociatedItems, ItemFieldReferencedByItems)):
            members += ' ' * 8 + f'if {field.field_name} is None:\n'
            members += ' ' * 12 + f'{field.field_name} = []\n'
        comment = field.comment
        members += ' ' * 8 + f"self.{member_name} = {field.field_name}  # type: {type_in_item}  # {comment}\n"
        # else:
        #     members += ' ' * 8 + f"self.{member_name} = kwargs.get('{field.field_name}', {field.default_value})  # type: Union[{type_in_item}, None]\n"
    return members


def _generate_arguments_for_item_class_init(item_class):
    all_fields = item_class.get_all_fields_with_base_classes()
    args_non_default = ''
    args_default = ''

    for field in all_fields:
        type_in_item = field.type_in_item  # with cyclic dependencies...

        if not field.not_none_value_required_in_constructor:
            # if isinstance(field, ItemFieldDirectlyAssociatedItems):
            #     default_value = []
            #     item_type_hint = type_in_item
            # else:
            default_value = 'None'
            item_type_hint = f'Union[{type_in_item}, None]'
            args_default += '\n' + ' ' * 12 + f"{field.field_name}: {item_type_hint} = {default_value},"
        else:
            args_non_default += '\n' + ' ' * 12 + f"{field.field_name}: {type_in_item},"
    # args += ' ' * 12 + '**kwargs'
    args = args_non_default + args_default
    return args[:-1]


def _generate_arguments_for_item_base_class_init(item_class: ItemClassDefinition):
    base_class = get_item_class_name_definition(item_class.base_class)
    args = ''
    for field in base_class.get_all_fields_with_base_classes():
        # if not field.not_none_value_required_in_constructor:
        #     continue
        args += '\n' + ' ' * 16 + f"{field.field_name}={field.field_name},"
    # args += ' ' * 16 + '**kwargs'
    return args[:-1]


def _generate_properties_for_item_class(item_class):
    properties = ''

    for field in item_class.fields:
        member_name = field.field_name
        if field.is_private:
            member_name = '_' + member_name

        if field.is_private:
            properties += ' ' * 4 + '@property\n'
            properties += ' ' * 4 + f'def {field.field_name}(self):\n'
            if isinstance(field, (ItemFieldDirectlyAssociatedItems, ItemFieldReferencedByItems)):
                properties += ' ' * 8 + '"""Do not modify this list directly. Update associated item instead"""\n'
            properties += ' ' * 8 + f'return self.{member_name}\n\n'

            if field.setter or field.is_item_subtype:
                properties += ' ' * 4 + f'@{field.field_name}.setter\n'
                field_type_txt = 'Item' if field.is_item_subtype else field.type_in_item
                type_annotation = field_type_txt if field.not_none_value_required_in_database else f'Union[{field_type_txt}, None]'
                properties += ' ' * 4 + f'def {field.field_name}(self, value: {type_annotation}):\n'
                if field.setter is not None:
                    properties += field.setter
                else:
                    properties += field.get_setter_for_associated_item()
                properties += '\n\n'
    return properties


def _generate_write_to_data_class(item_class):
    write_to_data_class = f'{" "*4}def _write_to_data_class(self, data_class):\n'
    is_empty = True

    if item_class.base_class:
        write_to_data_class += ' ' * 8 + f'super({item_class.name}, self)._write_to_data_class(data_class)\n'

    for field in item_class.fields:
        member_name = field.field_name
        if field.is_private:
            member_name = '_' + member_name

        if field.saved_in_file:
            if field.serializer is None:
                serialized_field_value = f'self.{member_name}'
            else:
                serialized_field_value = f'{field.serializer}(self.{member_name})'
            write_to_data_class += ' ' * 8 + f'data_class.{field.field_name} = {serialized_field_value}\n'
            is_empty = False
            if field.not_none_value_required_in_database:
                write_to_data_class += ' ' * 8 + f'if {serialized_field_value} is None:\n'
                write_to_data_class += ' ' * 12 + f'raise ValueError("Cannot save item, ' \
                    f'field \\"{field.field_name}\\" is required for class \\"{item_class.name}\\"")\n'
    if is_empty:
        write_to_data_class += f'{" "*8}pass\n'
    write_to_data_class += '\n'
    return write_to_data_class


def _generate_read_from_data_class(item_class):
    read_from_data_class = f'{" "*4}def _read_from_data_class(self, data_class):\n'
    is_empty = True

    if item_class.base_class:
        is_empty = False
        read_from_data_class += ' ' * 8 + f'super({item_class.name}, self)._read_from_data_class(data_class)\n'

    for field in item_class.fields:
        member_name = field.field_name
        if field.is_private:
            member_name = '_' + member_name

        if field.saved_in_file:
            if not field.is_item_subtype:
                if field.deserializer is None:
                    deserialized_field_value = f'data_class.{field.field_name}'
                else:
                    deserialized_field_value = f'{field.deserializer}(data_class.{field.field_name})'
                read_from_data_class += ' ' * 8 + f'self.{member_name} = {deserialized_field_value}\n'
                is_empty = False
    if is_empty:
        read_from_data_class += f'{" "*8}pass'
    return read_from_data_class


def _generate_none_arguments_for_required_fields_in_item_constructor(item_class, intendation=12):
    args = ''

    all_fields = copy.copy(item_class.fields)
    if item_class.base_class:
        base_class = get_item_class_name_definition(item_class.base_class)
        all_fields += base_class.fields
    for field in all_fields:
        if not field.not_none_value_required_in_constructor:
            continue
        args += ' ' * intendation + f"{field.field_name}=None,\n"
    if len(args):
        args = '\n' + args[:-2] + ')  # None just for a moment, will be overwritten below'
    else:
        args += ')'

    return args


def _generate_copy_without_associated_fileds_only(item_class):
    txt = ''
    for field in item_class.fields:
        if field.field_name in ['item_modification_time', 'item_creation_time', 'item_id']:
            continue  # definitively shouldn't be copied

        if not field.saved_in_file or field.is_item_subtype:
            continue
        txt += ' '*8 + f'new_item.{field.field_name} = copy.deepcopy(self.{field.field_name})\n'
    return txt


def _generate_item_filtering_by_time(item_class):
    txt = ''
    txt += ' '*4 + 'def get_date_for_sorting(self):\n'
    if item_class.can_be_time_filtered():
        for field in item_class.fields:
            if isinstance(field, (ItemFieldDateTimeStart, ItemFieldDateTimePoint)):
                txt += ' '*8 + f'return self.{field.field_name} or CustomDateTime(0)\n'
                break
        else:
            raise Exception('Cannot sort item by time!')
    else:
        txt += ' '*8 + 'raise Exception("Item cannot be time sorted!")\n'
    txt += '\n'

    txt += ' '*4 + 'def is_before_date(self, date) -> bool:\n'
    if item_class.can_be_time_filtered():
        for field in item_class.fields:
            if isinstance(field, (ItemFieldDateTimeStart, ItemFieldDateTimePoint)):
                txt += ' '*8 + f'start_date = self.{field.field_name}\n'
                break
        else:
            raise Exception('Cannot filter item by time!')
        txt += ' '*8 + 'return start_date and start_date < date\n'
    else:
        txt += ' '*8 + 'raise Exception("Item cannot be time filtered!")\n'
    txt += '\n'
    txt += ' '*4 + 'def is_after_date(self, date) -> bool:\n'
    if item_class.can_be_time_filtered():
        for field in item_class.fields:
            if isinstance(field, (ItemFieldDateTimeEnd, ItemFieldDateTimePoint)):
                txt += ' '*8 + f'end_date = self.{field.field_name}\n'
                break
        else:
            raise Exception('Cannot filter item by time!')
        txt += ' '*8 + 'return end_date and end_date > date\n'
    else:
        txt += ' '*8 + 'raise Exception("Item cannot be time filtered!")\n'
    return txt + '\n'


def _generate_is_same_item_checker(item_class):
    txt = ''
    txt += ' ' * 4 + 'def is_same_item(self, item) -> bool:\n'
    if item_class.is_final:
        if item_class.special_is_same_item_checker:
            txt += item_class.special_is_same_item_checker.format(indentation=" "*8)
        else:
            for field in item_class.get_all_fields_with_base_classes():
                if field.has_to_be_unique_for_item_class:
                    txt += ' ' * 8 + '# if unique field is same, then it is definitely same object\n'
                    if field.is_item_subtype:
                        txt += ' ' * 8 + f'return (self.{field.field_name}.is_same_item(item.{field.field_name}))\n'
                    else:
                        txt += ' ' * 8 + f'return (self.{field.field_name} == item.{field.field_name})\n'
                    break
            else:
                raise Exception(f'Missing unique field for class {item_class.name}?')
    else:
        txt += ' ' * 8 + f'raise Exception("Cannot compare abstract (not final) class {item_class.name}")\n'

    return txt


def _generate_item_class_txt(item_class):
    data_class_name = f'{item_class.name}Content'

    base_class_inheritance = f'({item_class.base_class})' if item_class.base_class else ''
    item_class_txt = f'class {item_class.name}{base_class_inheritance}:\n' \
        f'{" "*4}def __init__(\n{" "*12}self, {_generate_arguments_for_item_class_init(item_class)}):\n'
    if item_class.base_class:
        item_class_txt += " "*8 + f'super().__init__({_generate_arguments_for_item_base_class_init(item_class)})\n'
    item_class_txt += _generate_members_for_item_class(item_class) + '\n'

    item_class_txt += _generate_properties_for_item_class(item_class)

    serializer = f'    def serialize(self) -> {data_class_name}:\n'
    serializer += ' ' * 8 + f'data_class = {data_class_name}()\n'
    serializer += ' ' * 8 + 'self._write_to_data_class(data_class)\n'
    serializer += ' ' * 8 + 'data_class.validate_fields_type()\n'
    serializer += ' ' * 8 + 'return data_class\n'

    deserializer = f'    @classmethod\n' \
        f'    def deserialize(cls, data_class, validate_field_types=True):\n'
    deserializer += ' ' * 8 + 'if validate_field_types:\n'
    deserializer += ' ' * 12 + 'data_class.validate_fields_type()\n'
    args = _generate_none_arguments_for_required_fields_in_item_constructor(item_class)
    deserializer += ' ' * 8 + f'instance = cls({args}\n'
    deserializer += ' ' * 8 + 'instance._read_from_data_class(data_class)\n'
    deserializer += ' ' * 8 + f'return instance\n'

    serializer += '\n'
    deserializer += '\n'
    item_class_txt += serializer + deserializer

    item_class_txt += ' '*4 + 'def get_short_description(self):\n'
    item_class_txt += ' '*8 + f'return {item_class.short_description_code}\n\n'

    # if item_class.is_final:
    item_class_txt += ' '*4 + 'def copy_fields_without_associated_items(self, new_item=None):\n'
    item_class_txt += ' '*8 + '"""Copy only non-item fields, that are stored in db"""\n'
    args = _generate_none_arguments_for_required_fields_in_item_constructor(item_class, intendation=16)
    item_class_txt += ' '*8 + f'if new_item is None:\n'
    item_class_txt += ' '*12 + f'new_item = self.__class__({args}\n'
    if item_class.base_class:
        item_class_txt += " " * 8 + f'super({item_class.name}, self).copy_fields_without_associated_items(new_item=new_item)\n'
    item_class_txt += _generate_copy_without_associated_fileds_only(item_class=item_class)
    item_class_txt += ' '*8 + 'return new_item\n\n'

    item_class_txt += _generate_item_filtering_by_time(item_class=item_class)
    item_class_txt += _generate_is_same_item_checker(item_class=item_class)


    # item_class_txt += ' '*4 + 'def _copy_without_associated_items(self, new_item):\n'
    # item_class_txt += _generate_copy_without_associated(item_class)
    item_class_txt += '\n'

    delete_from_belongings_or_referenced_by_txt = ''
    for field in item_class.get_all_fields_with_base_classes():
        if field.is_item_subtype:
            delete_from_belongings_or_referenced_by_txt += ' '*8 + f'self.{field.field_name} = None\n'

    if delete_from_belongings_or_referenced_by_txt:
        item_class_txt += ' '*4 + 'def delete_from_belongings_and_referenced_by(self):\n'
        item_class_txt += delete_from_belongings_or_referenced_by_txt + '\n'

    item_class_txt += _generate_write_to_data_class(item_class) + _generate_read_from_data_class(item_class)

    return item_class_txt


def create_file_for_item(item_class: ItemClassDefinition, file_path):
    file_content = AUTOMATIC_GENERATION_CLAUSE
    file_content += 'from dataclasses import dataclass\n'
    file_content += 'from typing import Tuple, Union, List\n'
    file_content += 'import copy\n'
    file_content += item_class.additional_imports + '\n'

    file_content += 'import typeguard\n\n'

    file_content += _generate_required_imports(item_class)

    file_content += _generate_item_class_content_txt(item_class) + '\n'

    file_content += _generate_item_class_txt(item_class)

    if item_class.additional_code:
        file_content += item_class.additional_code

    with open(file_path, 'w') as file:
        file.write(replace_invalid_plural_forms(file_content))


########################### Collection generation ###########################

def _generate_item_imports_for_collection():
    item_imports = ''
    for item_class in ITEM_CLASSES:
        if item_class.base_class is not None and item_class.base_class != 'Item':
            base_calss = get_item_class_name_definition(item_class.base_class)
            item_imports += ItemClassDefinition.get_class_import(base_calss)
        item_imports += item_class.get_class_import()
    return item_imports


def _generate_collection_members():
    collection_members = ''
    for item_class in ITEM_CLASSES:
        collection_member_name = item_class.member_name
        if not item_class.is_final:
            collection_member_name = 'all_' + collection_member_name

        collection_member_line = ' '*4 + collection_member_name + f's: List[{item_class.name}] ' \
            f'= field(default_factory=lambda: [])\n'

        if item_class.is_final:
            collection_members += collection_member_line
    return collection_members


def _generate_modification_time_updaters():
    modification_time_updaters = ''

    for item_class in ITEM_CLASSES:
        if item_class.is_final:
            item_var_name = item_class.member_name
            modification_time_updaters += f'{" " * 8}old_{item_var_name}_collection_content_dict = {{}}\n'
            modification_time_updaters += f'{" " * 8}for {item_var_name} in old_collection_content.{item_var_name}s:\n'
            modification_time_updaters += f'{" " * 12}old_{item_var_name}_collection_content_dict[{item_var_name}.item_id] = {item_var_name}\n'
            modification_time_updaters += f'{" " * 8}for {item_var_name} in self.{item_var_name}s:\n'
            modification_time_updaters += f'{" " * 12}if {item_var_name}.item_creation_time is None:\n'
            modification_time_updaters += f'{" " * 16}{item_var_name}.item_creation_time = current_date_time\n'
            modification_time_updaters += f'{" " * 16}{item_var_name}.item_modification_time = current_date_time\n'
            modification_time_updaters += f'{" " * 12}old_item = old_{item_var_name}_collection_content_dict.get({item_var_name}.item_id, None)\n'
            modification_time_updaters += f'{" " * 12}if old_item is not None and {item_var_name} != old_item:\n'
            modification_time_updaters += f'{" " * 16}{item_var_name}.item_modification_time = current_date_time\n\n'

    return modification_time_updaters


def generate_all_final_classes_list():
    all_final_classes = 'ALL_FINAL_ITEM_CLASSES = [\n'
    for item_class in ITEM_CLASSES:
        if item_class.is_final:
            all_final_classes += ' ' * 4 + item_class.name + ',\n'

    all_final_classes += ']\n'
    return all_final_classes


def generate_all_classes_list():
    all_classes = 'ALL_ITEM_CLASSES = [\n'
    for item_class in ITEM_CLASSES:
        all_classes += ' ' * 4 + item_class.name + ',\n'
    all_classes += ']\n'
    return all_classes


def generate_collection(base_dir):
    file_path = os.path.join(base_dir, 'collection.py')

    collection_content_members = ''
    collection_adders = ''

    for item_class in ITEM_CLASSES:
        collection_member_name = item_class.member_name
        if not item_class.is_final:
            collection_member_name = 'all_' + collection_member_name
        else:
            collection_content_members += ' '*4 + collection_member_name + f's: List[{item_class.name}Content] ' \
            f'= field(default_factory=lambda: [])\n'

        txt_adders_part = ''
        for field in item_class.fields:
            if field.not_none_value_required_in_database and field.field_name != 'item_id':
                txt_adders_part += ' ' * 12 + f'if item.{field.field_name} is None:\n'
                txt_adders_part += ' ' * 16 + f'raise ValueError("Cannot add item, ' \
                    f'field \\"{field.field_name}\\" is required")\n'
        if item_class.is_final:
            for field in item_class.get_all_fields_with_base_classes():
                if field.has_to_be_unique_for_item_class:
                    txt_adders_part += f'{" " * 12}for other_item in self.{collection_member_name}s:\n'
                    txt_adders_part += f'{" " * 16}if item.{field.field_name} == other_item.{field.field_name}:\n'
                    txt_adders_part += f'{" " * 20}raise ItemNotUniqueError("{field.field_name} ' \
                        f'has to be unique among before adding to collection!")\n'
        if item_class.special_is_same_item_checker:
            txt_adders_part += f'{" " * 12}for other_item in self.{collection_member_name}s:\n'
            txt_adders_part += f'{" " * 16}if item.is_same_item(other_item):\n'
            txt_adders_part += f'{" " * 20}# Item has no unique fields and special checker will be used\n'
            txt_adders_part += f'{" " * 20}raise ItemNotUniqueError("Item is not unique ' \
                f'(special uniqueness checker says so). Cannot add to collection!")\n'

        if item_class.additional_validator_before_adding_to_db:
            txt_adders_part += '\n' + ' '*12 + '# additional_validator_before_adding_to_db'
            txt_adders_part += item_class.additional_validator_before_adding_to_db.format(intendation=' '*12)

        note_added = False
        for field in item_class.fields:
            if field.is_item_subtype:
                if field.should_go_into_belongings_of_associated_item:
                    container_field_name = 'belongings'
                else:
                    container_field_name = 'referenced_by'

                if not note_added:
                    txt_adders_part += f'\n{" " * 12}' \
                        f'# This have to be at the end, because of side effect on other items\n'
                    note_added = True
                txt_adders_part += f'{" " * 12}if item.{field.field_name} is not None:\n'
                txt_adders_part += f'{" " * 16}item.{field.field_name}.{container_field_name}.append(item)\n'

        if txt_adders_part or item_class.is_final: # and item_class.is_final:
            collection_adders += f'{" " * 8}if isinstance(item, {item_class.name}):\n'
            collection_adders += txt_adders_part
            if item_class.is_final:
                collection_adders += f'{" " * 12}self.{collection_member_name}s.append(item)\n'
            collection_adders += '\n'

    txt = AUTOMATIC_GENERATION_CLAUSE
    txt += \
f'''from typing import List
from dataclasses import dataclass, field
from common.types.custom_datetime import CustomDateTime
from items_definitions.item_classes import ItemClassDefinition, get_item_class_name_definition
from common import settings
from utils.misc import replace_invalid_plural_forms
{_generate_item_imports_for_collection()}

class ItemNotUniqueError(Exception):
    pass

{generate_all_classes_list()}
{generate_all_final_classes_list()}

@dataclass
class CollectionContent:
    next_item_id: int = 1
{collection_content_members}
    def update_modification_times(self, old_collection_content):
        current_date_time = CustomDateTime.now().to_unix_time()

{_generate_modification_time_updaters()}
@dataclass
class Collection:
    all_items_by_id: dict = field(default_factory=lambda: {{}})
    _next_item_id: int = 1
{_generate_collection_members()}
    def is_empty(self) -> bool:
        return len(self.all_items_by_id) == 0

    def get_items_list_for_class_name(self, class_name: str):
        class_var_name = ItemClassDefinition.get_member_name_for_class_name(class_name)
        member_name = replace_invalid_plural_forms(class_var_name + 's')
        return getattr(self, member_name)        

    def add_item(self, item):
        assert item.item_id is None

        item.item_id = -1  # just to pass validation below, will be changed after the next line...
        try:
            item.serialize()  # this will call validate_fields_type() internally
        except Exception as e:
            item.item_id = None  # if serialization failed, restore previous state
            raise e
        item.item_id = None  # reset it anyway

{collection_adders}
        item.item_id = self._next_item_id
        self._next_item_id += 1
        self.all_items_by_id[item.item_id] = item

    def delete_item(self, item):
        assert item.item_id is not None
        item_class_definition = get_item_class_name_definition(item.__class__.__name__)
        if item_class_definition.has_belongings():
            assert len(item.belongings) == 0
        if item_class_definition.can_be_referenced_by():
            assert len(item.referenced_by) == 0
        item.delete_from_belongings_and_referenced_by()
        del self.all_items_by_id[item.item_id]
        self.get_items_list_for_class_name(item.__class__.__name__).remove(item)
        
    @classmethod
    def from_content(cls, collection_content: CollectionContent, validate_field_types: bool = True):
{generate_converter_from_collection_content()}

    def to_content(self) -> CollectionContent:
{_generate_converter_to_collection_content()}
'''
    with open(file_path, 'w') as file:
        file.write(replace_invalid_plural_forms(txt))


def _generate_converter_to_collection_content():
    """Generate content allowing to convert items `Collection` to `CollectionContent"""
    txt_to_content = ''
    txt_to_content += " "*8 + 'collection_content = CollectionContent()\n'
    txt_to_content += " "*8 + 'collection_content.next_item_id = self._next_item_id\n\n'

    for item_class in ITEM_CLASSES:
        if not item_class.is_final:
            continue
        item_var = item_class.member_name
        txt_to_content += f'{" "*8}for {item_var} in self.{item_var}s:\n'
        txt_to_content += f'{" "*12}{item_var}_content = {item_var}.serialize()\n'
        txt_to_content += f'{" "*12}assert {item_var}.item_id is not None\n'
        txt_to_content += f'{" "*12}collection_content.{item_var}s.append({item_var}_content)\n\n'

        if item_class.additional_validator_before_adding_to_db:
            txt_to_content += f'{" "*12}item = {item_var}'
            txt_to_content += f'{" "*12}# same validation as during adding to db - additional_validator_before_adding_to_db'
            txt_to_content += item_class.additional_validator_before_adding_to_db.format(intendation=' '*12)

    txt_to_content += " "*8 + 'return collection_content\n\n\n'
    return txt_to_content


def generate_converter_from_collection_content():
    """Generate content allowing to convert items `CollectionContent` to `Collection"""
    txt_from_content = ''
    txt_from_content += " "*8 + 'collection: Collection = cls()\n'
    txt_from_content += " "*8 + 'all_items_by_id = {}\n'
    txt_from_content += " "*8 + 'collection.all_items_by_id = all_items_by_id\n'
    txt_from_content += " "*8 + 'collection._next_item_id = collection_content.next_item_id\n\n'

    for item_class in ITEM_CLASSES:
        if not item_class.is_final and item_class.name != 'Item':
            txt_from_content += " " * 8 + f'all_{item_class.member_name}s_content = []\n'
    txt_from_content += '\n'

    for item_class in ITEM_CLASSES:
        if not item_class.is_final:
            continue

        item_var = item_class.member_name
        txt_from_content += f'{" "*8}if hasattr(collection_content, "{item_var}s"):  # to allow for reading of older databases, without new item types\n'
        txt_from_content += f'{" "*12}for {item_var}_content in collection_content.{item_var}s:\n'
        txt_from_content += f'{" "*16}{item_var} = {item_class.name}.deserialize({item_var}_content, validate_field_types)\n'
        txt_from_content += f'{" "*16}assert {item_var}.item_id not in all_items_by_id\n'
        txt_from_content += f'{" "*16}all_items_by_id[{item_var}.item_id] = {item_var}\n'
        txt_from_content += f'{" "*16}collection.{item_var}s.append({item_var})\n'
        if item_class.base_class != 'Item':
            container_var_name = f'all_{ItemClassDefinition.get_member_name_for_class_name(item_class.base_class)}s_content'
            txt_from_content += " " * 16 + f'{container_var_name}.append({item_var}_content)  # only for items with base class different than Item\n'
        txt_from_content += '\n'

    txt_from_content += " "*8 + '# Update items references to other items\n'
    for item_class in ITEM_CLASSES:
        if item_class.name == 'Item':
            continue

        item_var = item_class.member_name
        for field in item_class.fields:
            if field.saved_in_file and field.is_item_subtype:
                if item_class.is_final:
                    txt_from_content += f'{" " * 8}for {item_var}_content in collection_content.{item_var}s:  # final item\n'
                else:
                    container_var_name = f'all_{item_var}s_content'
                    txt_from_content += f'{" " * 8}for {item_var}_content in {container_var_name}:  # not final item\n'

                txt_from_content += f'{" "*12}if {item_var}_content.{field.field_name} is None:\n'
                txt_from_content += f'{" "*16}continue\n'
                txt_from_content += f'{" "*12}associated_{field.field_name}_id = {item_var}_content.{field.field_name}\n'
                txt_from_content += f'{" "*12}associated_{field.field_name} = all_items_by_id[associated_{field.field_name}_id]\n'

                txt_from_content += f'{" "*12}{item_var} = all_items_by_id[{item_var}_content.item_id]\n'
                txt_from_content += f'{" "*12}{item_var}.{field.field_name} = associated_{field.field_name}  # This will also set belonging for associated item\n\n'
                # txt_from_content += f'{" "*12}associated_{field.field_name}.belongings.append({item_var})\n\n'

    txt_from_content += " "*8 + 'return collection'
    return txt_from_content


def generate_all(base_dir: str = None):
    if base_dir is None:
        base_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..', 'src', 'items')

    for item_class in ITEM_CLASSES:
        file_dir = os.path.join(base_dir, item_class.path)
        if not os.path.exists(file_dir):
            os.makedirs(file_dir)
        file_path = os.path.join(file_dir, item_class.get_class_file_name() + '.py')
        create_file_for_item(item_class=item_class, file_path=file_path)

    generate_collection(base_dir)


if __name__ == '__main__':
    generate_all()
    print('Done!')
