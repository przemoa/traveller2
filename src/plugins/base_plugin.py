class BasePlugin:
    def __init__(self):
        pass

    def get_description(self):
        raise NotImplementedError(f'Missing description for plugin "{self.__class__.__name__}"')

    def run(self, engine, parent):
        raise NotImplementedError(f'Missing run function for plugin "{self.__class__.__name__}"')

    def get_name(self):
        return self.__class__.__name__


plugin_class = BasePlugin
