import importlib.util
import logging
import os

from PySide2.QtWidgets import QFileDialog, QMessageBox

EXAMPLE_PLUGINS_DIR = os.path.join(os.path.dirname(__file__), 'examples')


def _select_plugin(parent):
    file_dialog = QFileDialog(
        parent,
        'Select plugin to run...',
        EXAMPLE_PLUGINS_DIR,
        "Python files (*.py);; All (*)")
    if file_dialog.exec_() == QFileDialog.Accepted:
        new_path = file_dialog.selectedFiles()[0]
        return new_path
    return ''


def _execute_plugin(file_path, parent, engine) -> str:
    file_name = os.path.splitext(os.path.basename(file_path))[0]
    try:
        spec = importlib.util.spec_from_file_location(file_name, file_path)
        plugin_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(plugin_module)
        plugin = plugin_module.plugin_class()
    except:
        error_msg = f'Error during plugin ({file_path}) loading!'
        logging.exception(error_msg)
        raise Exception(error_msg)

    try:
        description = plugin.get_description()
        reply = QMessageBox.question(parent, "Plugin execution",
                                     f'Are you sure you want to execute plugin "{plugin.get_name()}"?'
                                     f'\n\nPlugin description:\n'
                                     f'{description}')
        if reply != QMessageBox.StandardButton.Yes:
            return ''
        plugin_message = plugin.run(engine=engine, parent=parent)
    except:
        error_msg = f'Error during plugin ({file_path}) execution!'
        logging.exception(error_msg)
        raise Exception(error_msg)

    return plugin_message


def run_plugin(parent, engine, plugin_path=''):
    if not plugin_path:
        plugin_path = _select_plugin(parent=parent)

    if not plugin_path:
        return

    try:
        plugin_message = _execute_plugin(parent=parent, engine=engine, file_path=plugin_path)
        if plugin_message:
            QMessageBox.information(parent, "Info!", f'Plugin "{os.path.basename(plugin_path)}" executed succesfully!\n\n'
                                    + plugin_message)
    except Exception as e:
        QMessageBox.critical(parent, "Error!", str(e))
