import logging

from common import settings
from plugins.base_plugin import BasePlugin
from utils.misc import get_relative_path_or_none_if_not_subdirectory


class UpdatePhotosRelativePath(BasePlugin):
    def get_description(self):
        return "This plugin updates 'relative file path' for photos without the relative path. " \
               "Relative path is set as common base for absolute photo path and base photos dir from settings."

    def run(self, engine, parent) -> str:
        number_of_modified_photos = 0
        for photo in engine.collection.photos:
            if photo.relative_file_path:
                continue
                
            relative_file_path = get_relative_path_or_none_if_not_subdirectory(
                file_path=photo.original_full_path,
                base_dir=settings.s.files_root_dir_path)
            photo.relative_file_path = relative_file_path
            logging.info(f'Photo "{photo.get_file_name()}" modified')
            number_of_modified_photos += 1

        engine.set_engine_as_modified()
        
        if number_of_modified_photos:
            return f'{number_of_modified_photos} modified!'
        else:
            return f'None of the photos modified!'


plugin_class = UpdatePhotosRelativePath
