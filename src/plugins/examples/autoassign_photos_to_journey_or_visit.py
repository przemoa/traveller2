import logging

from dialogs.progress.progress_dialog import ProgressDialog
from plugins.base_plugin import BasePlugin


class AutoassignPhotosToJourneyOrVisit(BasePlugin):
    def get_description(self):
        return "This plugin tries to assign photos (not attached to any item yet) to journey or visit."

    @staticmethod
    def _get_start_end_time_ranges(items):
        ranges_for_items = {item: (item.start_time.unix_time, item.end_time.unix_time)
                            for item in items
                            if (item.start_time and item.end_time)}
        return ranges_for_items

    @staticmethod
    def _fit_time_point_into_ranges(ranges_for_items, time_point):
        for item, time_range in ranges_for_items.items():
            if time_point >= time_range[0] and time_point <= time_range[1]:
                return item
        return None

    def run(self, engine, parent) -> str:
        progress_dialog = ProgressDialog(parent=parent, title='Processing...', max_value=len(engine.collection.photos))
        progress_dialog.set_message('Trying to attach photos to journeys or visits...')
        msg = self._run(engine=engine, progress_dialog=progress_dialog)
        progress_dialog.cancel()
        return msg

    def _run(self, engine, progress_dialog) -> str:
        journey_ranges = self._get_start_end_time_ranges(engine.collection.journeys)
        visits_ranges = self._get_start_end_time_ranges(engine.collection.visits)

        number_of_modified_photos = 0
        all_fitting_items = set()
        for i, photo in enumerate(engine.collection.photos):
            progress_dialog.set_value(i)
            if photo.attached_to:
                continue

            if not photo.photo_time:
                continue

            time_point = photo.photo_time.unix_time

            fitting_item = self._fit_time_point_into_ranges(ranges_for_items=journey_ranges, time_point=time_point)
            if not fitting_item:
                fitting_item = self._fit_time_point_into_ranges(ranges_for_items=visits_ranges, time_point=time_point)

            if fitting_item:
                all_fitting_items.add(fitting_item)
                photo.attached_to = fitting_item
                logging.info(f'Photo "{photo.get_file_name()}" modified')
                number_of_modified_photos += 1

        if number_of_modified_photos:
            engine.set_engine_as_modified()
        
        if number_of_modified_photos:
            all_fitting_items_str = '\n'.join([item.get_short_description() for item in list(all_fitting_items)[:10]])
            if len(all_fitting_items) > 10:
                all_fitting_items_str += '\n and more...'
            else:
                all_fitting_items_str += '.'
            return f'{number_of_modified_photos} modified! Attached to:\n' + all_fitting_items_str
        else:
            return f'None of the photos modified!'


plugin_class = AutoassignPhotosToJourneyOrVisit
