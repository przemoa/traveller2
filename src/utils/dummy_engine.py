import copy
import os

from common import settings
from common.types.custom_datetime import CustomDateTime
from common.types.enum_types import PlaceType, PersonType
from common.types.image_annotation import ImageAnnotation
from engine.engine import Engine
from items.airport import Airport
from items.attachment.file import File
from items.attachment.photo import Photo
from items.companion import Companion
from items.flight import Flight
from items.journey import Journey
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items.location.place import Place
from items.person import Person
from items.visit import Visit
from items_utils.file_blob_from_path import get_file_from_file_path
from items_utils.photo_from_path import get_photo_from_file_path

_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
FILES_FOR_TESTS_DIR = os.path.join(_SCRIPT_DIR, '..', '..', 'tests', 'files_for_tests')


def get_dummy_engine():
    settings.load_settings(None)

    engine = Engine()
    c = engine.collection
    pc = engine.prompts_collection

    person_0 = Person(name='person_0')
    c.add_item(person_0)
    person_0.type = PersonType.get_readable_name(PersonType.FAMILY)

    person_1 = Person(name='person_1', type=PersonType.get_readable_name(PersonType.FAMILY))
    c.add_item(person_1)

    person_2 = Person(name='person_2', type=PersonType.get_readable_name(PersonType.COWORKER))
    c.add_item(person_2)

    person_3 = Person(name='person_3')
    c.add_item(person_3)

    companion_0 = Companion(title='school friends', person_1=person_1)
    companion_0.person_3 = person_3
    c.add_item(companion_0)

    journey_0 = Journey(
        name='around world')
    journey_0.start_time = CustomDateTime(1234567)
    journey_0.end_time = CustomDateTime(1434567)
    c.add_item(journey_0)

    journey_1 = Journey(
        name='around poland')
    journey_1.start_time = CustomDateTime(12345678)
    journey_1.end_time = CustomDateTime(14345678)
    c.add_item(journey_1)
    journey_1.companion = person_0

    journey_2 = Journey(
        name='around poznan')
    journey_2.start_time = CustomDateTime(123456789)
    journey_2.end_time = CustomDateTime(125456789)
    c.add_item(journey_2)
    journey_2.companion = companion_0

    photo_0 = Photo(original_full_path='/abc/def/b.jpg', relative_file_path='def/')
    photo_0.thumbnail = None
    photo_0.thumbnail = b'000'
    photo_0.photo_time = CustomDateTime(324432423)
    c.add_item(photo_0)

    photo_1_path = os.path.join(FILES_FOR_TESTS_DIR, 'IMG_20170218_102010.jpg')
    photo_3_path = os.path.join(FILES_FOR_TESTS_DIR, 'IMG_20150711_084644.jpg')
    photo_4_path = os.path.join(FILES_FOR_TESTS_DIR, 'IMG_20150711_093126.jpg')
    photo_5_path = os.path.join(FILES_FOR_TESTS_DIR, '100_8666.jpg')
    photo_1 = get_photo_from_file_path(photo_1_path, base_dir='', thumbnail_size=settings.s.thumbnail_size_in_pixels)
    photo_1.note = 'hello note 33'
    photo_1.attached_to = journey_2
    photo_1.annotation = ImageAnnotation({'landmark_annotations': [{'mid': '/m/02rzp8j', 'description': 'Książ Castle', 'score': 0.25295424461364746, 'bounding_poly': {'vertices': [{'x': 975, 'y': 574}, {'x': 1540, 'y': 574}, {'x': 1540, 'y': 671}, {'x': 975, 'y': 671}]}, 'locations': [{'lat_lng': {'latitude': 50.841937, 'longitude': 16.295128}}]}], 'label_annotations': [{'mid': '/m/07pw27b', 'description': 'Atmospheric phenomenon', 'score': 0.8941346406936646, 'topicality': 0.8941346406936646}, {'mid': '/m/05_5t0l', 'description': 'Landmark', 'score': 0.8777039647102356, 'topicality': 0.8777039647102356}, {'mid': '/m/01bqvp', 'description': 'Sky', 'score': 0.862245500087738, 'topicality': 0.862245500087738}, {'mid': '/m/0d5gx', 'description': 'Castle', 'score': 0.7294337153434753, 'topicality': 0.7294337153434753}, {'mid': '/m/07j7r', 'description': 'Tree', 'score': 0.6869218349456787, 'topicality': 0.6869218349456787}, {'mid': '/m/0csby', 'description': 'Cloud', 'score': 0.6644815802574158, 'topicality': 0.6644815802574158}, {'mid': '/m/03nfmq', 'description': 'Architecture', 'score': 0.6545118689537048, 'topicality': 0.6545118689537048}, {'mid': '/m/04_5sg', 'description': 'Hill', 'score': 0.6507007479667664, 'topicality': 0.6507007479667664}, {'mid': '/m/0cgh4', 'description': 'Building', 'score': 0.6230400204658508, 'topicality': 0.6230400204658508}, {'mid': '/m/086mh', 'description': 'Winter', 'score': 0.618801474571228, 'topicality': 0.618801474571228}]})

    photo_2 = Photo(original_full_path='/abc/def/a.jpg', relative_file_path='')
    photo_2.thumbnail = b'222'

    photo_3 = get_photo_from_file_path(photo_3_path, base_dir='', thumbnail_size=settings.s.thumbnail_size_in_pixels)
    photo_4 = get_photo_from_file_path(photo_4_path, base_dir='', thumbnail_size=settings.s.thumbnail_size_in_pixels)
    photo_5 = get_photo_from_file_path(photo_5_path, base_dir='', thumbnail_size=settings.s.thumbnail_size_in_pixels)

    c.add_item(photo_1)
    c.add_item(photo_2)
    c.add_item(photo_3)
    c.add_item(photo_4)
    c.add_item(photo_5)

    country_0 = Country(name='Poland', iso_code='PL')
    country_1 = Country(name='Italy', iso_code='IT')
    country_2 = Country(name='Hungary', iso_code='HU')

    c.add_item(country_0)
    c.add_item(country_1)
    c.add_item(country_2)

    region_major_0 = RegionMajor(name='Greater Poland', geoname_code='gre')
    region_major_0.country = country_0
    c.add_item(region_major_0)

    region_major_1 = RegionMajor(name='Lubusz', geoname_code='lub')
    region_major_1.country = country_1
    c.add_item(region_major_1)
    region_major_1.country = country_0

    region_major_2 = RegionMajor(name='Lombardy', geoname_code='lom')
    region_major_2.country = country_1
    c.add_item(region_major_2)

    region_minor_0 = RegionMinor(name='gmina zielogorska',
                                 non_ascii_name='gmina zielonogórska',
                                 geoname_code='zie')
    region_minor_0.region_major = region_major_1
    c.add_item(region_minor_0)

    region_minor_1 = RegionMinor(name='Milan metropoly', geoname_code='mil')
    region_minor_1.region_major = region_major_1
    c.add_item(region_minor_1)
    region_minor_1.region_major = region_major_2

    city_0 = City(name='Kargowa', geoname_code='kar')
    city_0.region_minor = region_minor_1
    c.add_item(city_0)
    city_0.region_minor = region_minor_0

    city_1 = City(name='Sulechow', non_ascii_name='Sulechów', geoname_code='sul2')
    city_1.region_minor = region_minor_0
    c.add_item(city_1)

    place_0 = Place(name='Big palace')
    place_0.location = country_1
    c.add_item(place_0)

    place_1 = Place(name='Big castle')
    place_1.location = region_major_0
    place_1.type = PlaceType.get_readable_name(PlaceType.CASTLE)
    c.add_item(place_1)


    visit_0 = Visit(location=city_0)
    visit_0.note = 'something important'
    visit_0.start_time = CustomDateTime(1234567)
    visit_0.end_time = CustomDateTime(1239567)
    visit_0.journey = journey_0
    c.add_item(visit_0)

    visit_1 = Visit(location=region_major_2)
    visit_1.start_time = CustomDateTime(12345679)
    visit_1.end_time = CustomDateTime(12349679)
    c.add_item(visit_1)
    visit_1.journey = journey_1

    visit_2 = Visit(location=city_0)
    visit_2.start_time = CustomDateTime(123456733)
    visit_2.end_time = CustomDateTime(123459733)
    visit_2.journey = journey_1
    c.add_item(visit_2)
    visit_2.location = country_0

    visit_3 = Visit(location=city_0)
    visit_3.start_time = CustomDateTime(223456733)
    visit_3.end_time = CustomDateTime(223459733)
    c.add_item(visit_3)
    visit_3.location = place_1
    visit_3.journey = journey_2


    file_0 = File(original_full_path='/file/path')
    c.add_item(file_0)
    file_0.attached_to = visit_2

    file_1_path = os.path.join(FILES_FOR_TESTS_DIR, 'dummy_file_1.txt')
    file_1 = get_file_from_file_path(file_1_path, '', True)
    c.add_item(file_1)

    journey_2.start_location = city_1

    airport_0 = Airport(municipality='zielona gora', name='port lotniczy zg', location=country_0)
    airport_1 = Airport(municipality='mediolan', name='mediolan bergamo', location=country_1)

    c.add_item(airport_0)
    c.add_item(airport_1)

    flight_0 = Flight(starting_airport=airport_0, destination_airport=airport_1,
                      start_time=CustomDateTime(43543543), end_time=CustomDateTime(43573543))
    c.add_item(flight_0)
    flight_0.attached_to = visit_0


    country_0_prompt = copy.copy(country_0); country_0_prompt.item_id = None; country_0_prompt._belongings = []
    country_2_prompt = copy.copy(country_2); country_2_prompt.item_id = None; country_2_prompt._belongings = []
    pc.add_item(country_0_prompt)
    pc.add_item(country_2_prompt)
    country_3_prompt = Country(name='Ukraine', iso_code='UA')
    pc.add_item(country_3_prompt)

    region_major_3_prompt = RegionMajor(name='Silesian', geoname_code='sil')
    region_major_3_prompt.country = country_0_prompt
    pc.add_item(region_major_3_prompt)
    region_major_1_prompt = copy.copy(region_major_1); region_major_1_prompt.item_id = None; region_major_1_prompt._belongings = []
    region_major_1_prompt.country = country_0_prompt
    pc.add_item(region_major_1_prompt)

    region_minor_2_prompt = RegionMinor(name='Sulecinski', geoname_code='sul')
    region_minor_2_prompt.region_major = region_major_1_prompt
    pc.add_item(region_minor_2_prompt)

    region_minor_3_prompt = RegionMinor(name='Nowosolski', geoname_code='now')
    region_minor_3_prompt.region_major = region_major_1_prompt
    pc.add_item(region_minor_3_prompt)
    region_minor_0_prompt = copy.copy(region_minor_0); region_minor_0_prompt.item_id = None; region_minor_0_prompt._belongings = []
    region_minor_0_prompt.region_major = region_major_1_prompt
    pc.add_item(region_minor_0_prompt)

    city_0_prompt = copy.copy(city_0); city_0_prompt.item_id = None; city_0_prompt.region_minor = region_minor_0_prompt
    city_1_prompt = copy.copy(city_1); city_1_prompt.item_id = None; city_1_prompt.region_minor = region_minor_0_prompt
    pc.add_item(city_0_prompt)
    pc.add_item(city_1_prompt)

    city_2_prompt = City(name='Nowogrod Bobrzanski', geoname_code='nwg')
    city_2_prompt.region_minor = region_minor_0_prompt
    pc.add_item(city_2_prompt)

    city_3_prompt = City(name='Torzym', geoname_code='tor')
    city_3_prompt.region_minor = region_minor_2_prompt
    pc.add_item(city_3_prompt)

    for item in pc.all_items_by_id.values():
        item.set_as_from_prompts_database()

    return engine


if __name__ == '__main__':
    get_dummy_engine()
