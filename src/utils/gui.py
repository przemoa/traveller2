import getpass
import os
from typing import Union

from PySide2.QtCore import Signal
from PySide2.QtWidgets import QCheckBox, QGridLayout, QVBoxLayout, QHBoxLayout, QPushButton, QFileDialog, QComboBox, \
    QSpacerItem, QLabel

from common import settings


def set_editability_for_edit(edit_widget, is_editable):
    if not is_editable:
        edit_widget.setStyleSheet("background-color: rgb(211, 211, 211);")
    else:
        edit_widget.setStyleSheet("")
    if isinstance(edit_widget, (QCheckBox, QPushButton, QComboBox)):
        edit_widget.setEnabled(is_editable)
    else:
        edit_widget.setReadOnly(not is_editable)


def set_editability_for_edits(edit_widgets, is_editable):
    for edit_widget in edit_widgets:
        set_editability_for_edit(edit_widget, is_editable)


def add_widget_or_layout_to_ui(parent_layout,
                               widget_or_layout,
                               row: int = None,
                               column: int = None):
    if row is not None and column is not None:
        additional_args = [row, column]
    else:
        additional_args = []

    if isinstance(widget_or_layout, (QGridLayout, QVBoxLayout, QHBoxLayout)):
        parent_layout.addLayout(widget_or_layout, *additional_args)
    elif isinstance(widget_or_layout, QSpacerItem):
        parent_layout.addItem(widget_or_layout, *additional_args)
    else:
        parent_layout.addWidget(widget_or_layout, *additional_args)


def set_splitter_stretch(spliter, percentage_for_first_widget):
    assert percentage_for_first_widget >= 0 and percentage_for_first_widget <= 100
    spliter.setStretchFactor(0, percentage_for_first_widget + 1)
    spliter.setStretchFactor(1, 100 - percentage_for_first_widget + 1)


def set_splitter_division(spliter, percentage_for_first_widget):
    assert percentage_for_first_widget >= 0 and percentage_for_first_widget <= 100
    # See https://stackoverflow.com/questions/43831474/how-to-equally-distribute-the-width-of-qsplitter
    BIG_INT = 9999999
    spliter.setSizes([
        int(BIG_INT * percentage_for_first_widget / 100),
        int(BIG_INT * (100-percentage_for_first_widget) / 100),
    ])


def browse_open_traveller2_database_file(parent, title: str, start_path: Union[str, None] = None):

    file_dialog = QFileDialog(
        parent,
        title,
        start_path or settings.s.common_last_browse_database_dir_path,
        "Pkl files (*.pkl);; All (*)")
    if file_dialog.exec_() == QFileDialog.Accepted:
        new_path = file_dialog.selectedFiles()[0]
        if not start_path:  ## TODO
            settings.s.common_last_browse_database_dir_path = os.path.dirname(new_path)
        return new_path
    return ''


def browse_save_traveller2_database_file(parent,
                                         title: str,
                                         start_file_name: Union[str, None] = None,
                                         start_dir_path: Union[str, None] = None):
    save_file_dialog = QFileDialog(parent)
    save_file_dialog.setWindowTitle(title)
    save_file_dialog.setDefaultSuffix(".pkl")
    save_file_dialog.setAcceptMode(QFileDialog.AcceptSave)
    save_file_dialog.setNameFilter("Pkl files (*.pkl);; All files (*)")

    final_start_path = ''
    if os.path.isdir(start_dir_path):
        final_start_path = start_dir_path
    else:
        final_start_path = settings.s.common_last_browse_database_dir_path

    if start_file_name:
        if not start_file_name.endswith('.pkl'):
            start_file_name += '.pkl'

        final_start_path = os.path.join(final_start_path, start_file_name)
    else:
        final_start_path = os.path.join(final_start_path, f'traveller2_{getpass.getuser()}.pkl')


    save_file_dialog.selectFile(final_start_path)

    if save_file_dialog.exec_() == QFileDialog.Accepted:
        file_path = save_file_dialog.selectedFiles()[0]
        if not start_dir_path:
            settings.s.common_last_browse_database_dir_path = os.path.dirname(file_path)
        return file_path
    return ''


class ClickableLabel(QLabel):
    clicked = Signal(str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setStyleSheet("QLabel { color : rgb(0, 0, 100) }")

    def enterEvent(self, event):
        self.setStyleSheet("QLabel { color : rgb(0, 0, 100); text-decoration: underline }")

    def leaveEvent(self, event):
        self.setStyleSheet("QLabel { color : rgb(0, 0, 100) }")

    def mousePressEvent(self, event):
        self.clicked.emit(self.text())
