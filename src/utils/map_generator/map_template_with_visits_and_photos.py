
map_template = r'''<html>
    <head>
        <style>
        </style>
    </head>


    <body style="margin:0px; padding:0px;" onload="initialize()">

        <div id="map_canvas" style="width: 100%; height: 100%;"></div>



        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js{google_maps_api_key_query}"></script>

        <script type="text/javascript">

            var map = null



            <!-- Part generated automatically by traveller2 from database: START -->
            
            visits = {visits_list_str}
            images = {images_list_str}
            misc_items = {misc_items_list_str}
            workingDirectory = "{working_directory}"
            
            <!-- Part generated automatically by traveller2 from database: END -->



            function initialize()
            {{
                var mapOptions =
                    {{
                        zoom: 5,
                        center: new google.maps.LatLng(49, 10),
                        mapTypeId: google.maps.MapTypeId.ROAD, // ROAD, TERRAIN
                        mapTypeControl: true,
                        scaleControl: true,

                    }};
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

                addAllVisits()
                addAllImages()
                addAllMiscItems()
            }}

            function addAllVisits()
            {{
                for (var i = 0; i < visits.length; i++)
                {{
                    visit = visits[i]
                    description = visit["desc"]
                    content = "<h2>" + description[0] + "</h2><h5>Journey: " + description[1] + "</h5>"
                    
                    previous_visits = description[2] - 1
                    if (previous_visits > 0)
                        content += "<h6>And " + previous_visits + " earlier visit(s) </h6>"
                        
                    markerItem = {{
                        "lat": visit["lat"],
                        "lon": visit["lon"],
                        "item_type": visit["item_type"],
                        "content": content,
                    }}
                    addItemMarker(markerItem)
                }}
            }}
            
            function addAllMiscItems()
            {{
                for (var i = 0; i < misc_items.length; i++)
                {{
                    item = misc_items[i]
                    description = item["desc"]
                    content = "<h2>" + description[0] + "</h2><h5>" + description[1] + "</h5>"
                        
                    markerItem = {{
                        "lat": item["lat"],
                        "lon": item["lon"],
                        "item_type": item["item_type"],
                        "content": content,
                    }}
                    addItemMarker(markerItem)
                }}
            }}

            function addAllImages()
            {{
                for (var i = 0; i < images.length; i++)
                {{
                    image = images[i]
                    description = image["desc"]
                    associate_item_txt = ''
                    image_path = workingDirectory + "/img/" + image['img']
                    
                    if (description[0])
                        associate_item_txt = description[0]
                    content = '<p>' + image["img"] + ' [<b>' + description[1] + '</b>]</p><p>\n' 
                        + associate_item_txt + '</p><img src="' + image_path + '", alt="' + image['img'] + '", style="width:200px;">'
                    markerItem = {{
                        "lat": image["lat"],
                        "lon": image["lon"],
                        "item_type": image["item_type"],
                        "content": content,
                    }}
                    addItemMarker(markerItem)
                }}
            }}

            var yellowCircleSymbol =
            {{
                path: google.maps.SymbolPath.CIRCLE,
                fillOpacity: 0.8,
                scale: 1.2,
                strokeColor: 'darkblue',
                strokeWeight: 8
            }};

            function addItemMarker(markerItem)
            {{
                var marker = new google.maps.Marker(
                    {{
                        position: new google.maps.LatLng(markerItem["lat"], markerItem["lon"]),
                        label: markerItem["item_type"],
                        icon: null
                    }});
                console.log(markerItem)

                if (markerItem['item_type'] == 'x')
                {{
                    marker.label = null
                    marker.icon = yellowCircleSymbol
                }}

                marker.markerItem_ = markerItem
                marker.clickInfoWindow_ = null
                marker.setMap(map)

                marker.addListener('mouseover', showHoverInfoWindow.bind(null, marker))
                marker.addListener('mouseout', hideHoverInfoWindow.bind(null, marker))
                marker.addListener('click', showHideClickInfoWindow.bind(null, marker))
            }}

            function showHoverInfoWindow(marker)
            {{
                var infowindow = new google.maps.InfoWindow(
                    {{
                        content: marker.markerItem_['content']
                    }});
                infowindow.open(map, marker);
                marker.hoverInfoWindow_ = infowindow
            }}

            function hideHoverInfoWindow(marker)
            {{
                marker.hoverInfoWindow_.close()
                marker.hoverInfoWindow_ = null
            }}

            function showHideClickInfoWindow(marker)
            {{
                // google.maps.event.addListener(marker, 'click', showHideClickInfoWindow.bind(null, marker))
                if (marker.clickInfoWindow_)
                {{
                    marker.clickInfoWindow_.close()
                    marker.clickInfoWindow_ = null
                }}
                else
                {{
                    var infowindow = new google.maps.InfoWindow(
                        {{
                            content: marker.markerItem_['content']
                        }});
                    infowindow.open(map, marker);
                    marker.clickInfoWindow_ = infowindow
                }}
            }}

        </script>

    </body>
</html>

'''
