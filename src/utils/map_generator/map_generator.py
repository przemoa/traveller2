import logging
import os
from typing import List

from common import settings
from common.types.coordinates import Coordinates
from items.attachment.photo import Photo
from items.item import Item
from items.location.admin_entity.admin_entity import AdminEntity
from items.location.admin_entity.region_major import RegionMajor
from items.visit import Visit
from items_utils.misc import get_item_coordinates_or_parent_location
from utils.map_generator import map_template_with_single_marker
from utils.map_generator import map_template_with_visits_and_photos


def _get_visits_text(visits: List[Visit]):

    visits_with_count = {}
    for visit in visits:
        if visit.location is None:
            continue
        visit_in_same_item_with_count = visits_with_count.get(visit.location, None)  # type: tuple or None
        if visit_in_same_item_with_count is None:
            visit_in_same_item_with_count = {'last_visit': visit, 'count': 1}  # last visit and count of visits
        else:
            last_visit = visit_in_same_item_with_count['last_visit']  # type: Visit
            if last_visit.start_time < visit.start_time:
                visit_in_same_item_with_count['last_visit'] = visit
            visit_in_same_item_with_count['count'] += 1

        visits_with_count[visit.location] = visit_in_same_item_with_count

    visits_strings = []
    for visited_item_id, visit_with_count in visits_with_count.items():
        last_visit = visit_with_count['last_visit']
        visited_item = last_visit.location

        coordinates = get_item_coordinates_or_parent_location(visited_item)
        if coordinates is None:
            logging.warning(f'Cannot show visited item "{visited_item.get_short_description()}" on map, '
                            f'because it does not contain coordinates!')
            continue

        visit_string = '''{{"lat": {lat}, "lon": {lon}, "item_type": "{item_type}", "desc": ["{name}", "{journey}", {visits_count}]}}''' \
            .format(
            lat=coordinates.lat,
            lon=coordinates.lon,
            item_type=visited_item.__class__.__name__[0].upper(),
            name=last_visit.get_short_description().replace('\n', ' '),
            journey=last_visit.journey.get_short_description().replace('\n', ' ') if last_visit.journey else '-',
            visits_count=visit_with_count['count'])
        visits_strings.append(visit_string)

    visits_str = '[\n' + ',\n'.join(visits_strings) + '\n]'
    return visits_str


def _get_misc_items_text(misc_items: List[Item]):
    misc_items_strings = []
    for misc_item in misc_items:
        if not isinstance(misc_item, AdminEntity):
            raise NotImplementedError(f'Item type "{misc_item.__class__.__name__}" is not supported yet!')

        # coordinates = get_item_coordinates_or_parent_location(visited_item)
        coordinates = misc_item.coordinates
        if coordinates is None:
            logging.warning(f'Cannot show item "{misc_item.get_short_description()}" on map, '
                            f'because it does not contain coordinates!')
            continue

        item_type = misc_item.__class__.__name__[0].lower()
        if isinstance(misc_item, RegionMajor):
            item_type = 'R'

        misc_item_string = '''{{"lat": {lat}, "lon": {lon}, "item_type": "{item_type}", "desc": ["{name}", "{description}"]}}''' \
            .format(
            lat=coordinates.lat,
            lon=coordinates.lon,
            item_type=item_type,
            name=misc_item.name.replace('\n', ' '),
            description=f'{misc_item.__class__.__name__}')
        misc_items_strings.append(misc_item_string)

    misc_items_str = '[\n' + ',\n'.join(misc_items_strings) + '\n]'
    return misc_items_str


def _get_images_text_and_save_thumbnails(images: List[Photo], output_dir):
    # {"lat": 52, "lon": 15, "item_type": 'P', "img": "kot.jpg", "desc": ['kot.jpg', 'Around World', '18.12.2018']},
    img_dir = os.path.join(output_dir, 'img')
    os.makedirs(img_dir, exist_ok=True)

    images_strings = []

    photos_without_coordinates = 0
    for i, image in enumerate(images):
        coordinates = get_item_coordinates_or_parent_location(image)
        if coordinates is None:
            photos_without_coordinates += 1
            continue

        if image.attached_to is not None:
            associated_item_str = image.attached_to.get_short_description()
        else:
            associated_item_str = ''

        if image.annotation and not image.ignore_annotation:
            landmark_name = image.annotation.get_landmark_name()
            if landmark_name:
                if associated_item_str:
                    associated_item_str += '. '
                associated_item_str += landmark_name.replace('"', "'")

        if image.photo_time is not None:
            date_str = image.photo_time.to_chronological_date_time_string()
        else:
            date_str = 'Unknown time'

        item_string = '''{{"lat": {lat}, "lon": {lon}, "item_type": "{item_type}", "img": "{img}", "desc": ["{assoc_item}", "{date}"]}}'''\
            .format(
               lat=coordinates.lat,
               lon=coordinates.lon,
               item_type='x',  # x for special items without big marker
               img=image.get_file_name(),
               assoc_item=associated_item_str.replace('\n', ' '),
               date=date_str)
        images_strings.append(item_string)

        output_img_path = os.path.join(img_dir, image.get_file_name())
        with open(output_img_path, "wb") as file:
            file.write(image.thumbnail)

    images_str = '[\n' + ',\n'.join(images_strings) + '\n]'
    if photos_without_coordinates:
        logging.debug(f'{photos_without_coordinates} photos do not showed on map, because of missing coordinates!')
    return images_str


def generate_map_with_visits_and_photos(
        map_name,
        images: List[Photo] = [],
        visits: List[Visit] = [],
        admin_entities: List[AdminEntity] = [],
) -> str:
    output_dir = os.path.join(settings.TEMPORARY_FILES_DIR, map_name)
    os.makedirs(output_dir, exist_ok=True)
    output_html_file_path = os.path.abspath(os.path.join(output_dir, map_name + '.html'))

    logging.info(f'Generating map at "{output_html_file_path}"...')

    images_list_str = _get_images_text_and_save_thumbnails(images=images, output_dir=output_dir)
    visits_list_str = _get_visits_text(visits=visits)
    misc_items_list_str = _get_misc_items_text(misc_items=admin_entities)
    # working_directory = output_dir  # it will work only offline
    working_directory = '.'  # it allows to work also on server (path relative to html page/server root)

    txt = map_template_with_visits_and_photos.map_template.format(
        google_maps_api_key_query=('?key=' + settings.s.google_maps_api_key) if settings.s.google_maps_api_key else '',
        images_list_str=images_list_str,
        visits_list_str=visits_list_str,
        misc_items_list_str=misc_items_list_str,
        working_directory=working_directory)
    with open(output_html_file_path, "w") as text_file:
        text_file.write(txt)

    logging.info(f'Map generated"')
    return output_html_file_path


def generate_map_with_single_marker(
        coordinates: Coordinates,
):
    map_name = coordinates.get_short_description().replace(' ', '').replace(',', '').\
        replace('.', '').replace('(', '').replace(')', '')
    output_dir = os.path.join(settings.TEMPORARY_FILES_DIR, map_name)
    os.makedirs(output_dir, exist_ok=True)
    output_html_file_path = os.path.abspath(os.path.join(output_dir, map_name + '.html'))

    logging.info(f'Generating map with single marker at "{output_html_file_path}"...')

    # working_directory = output_dir  # it will work only offline
    working_directory = '.'  # it allows to work also on server (path relative to html page/server root)

    marker_position = '{' + f'"lat": {coordinates.lat}, "lon": {coordinates.lon}' + '}'

    txt = map_template_with_single_marker.map_template.format(
        google_maps_api_key_query=('?key=' + settings.s.google_maps_api_key) if settings.s.google_maps_api_key else '',
        working_directory=working_directory,
        marker_text=coordinates.get_short_description(),
        marker_position=marker_position,
    )
    with open(output_html_file_path, "w") as text_file:
        text_file.write(txt)

    logging.info(f'Map generated')
    return output_html_file_path
