
map_template = r'''<html>
    <head>
        <style>
        </style>
    </head>


    <body style="margin:0px; padding:0px;" onload="initialize()">

        <div id="map_canvas" style="width: 100%; height: 100%;"></div>



        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js{google_maps_api_key_query}"></script>

        <script type="text/javascript">

            var map = null



            <!-- Part generated automatically by traveller2 from database: START -->
            
            marker_text = "{marker_text}"
            marker_position = {marker_position}
            workingDirectory = "{working_directory}"

            <!-- Part generated automatically by traveller2 from database: END -->

        var infowindow = new google.maps.InfoWindow({{
          content: marker_text
        }});

            function initialize()
            {{
                var mapOptions =
                    {{
                        zoom: 15,
                        center: new google.maps.LatLng(marker_position['lat'], marker_position['lon']),
                        mapTypeId: google.maps.MapTypeId.ROAD, // ROAD, TERRAIN
                        mapTypeControl: true,
                        scaleControl: true,

                    }};
                map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

                 marker = new google.maps.Marker({{position: new google.maps.LatLng(marker_position['lat'], marker_position['lon']), map: map, title: '-'}});
                 marker.addListener('click', function() {{ infowindow.open(map, marker); }});
            }}

        </script>

    </body>
</html>

'''
