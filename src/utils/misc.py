import logging
import os
import platform
import subprocess
from typing import Union

from common import settings


def get_relative_path_or_none_if_not_subdirectory(file_path, base_dir) -> Union[str, None]:
    file_path_full = os.path.abspath(file_path)
    base_dir_full = os.path.abspath(base_dir)

    if base_dir and file_path_full.startswith(base_dir_full):
        return os.path.relpath(file_path_full, base_dir_full)
    else:
        return None


def get_dir_name_from_file_path(file_path: str) -> str:
    return os.path.basename(os.path.dirname(file_path))


def replace_invalid_plural_forms(txt):
    PLURAL_NAMES_TO_REPLACE = {
        'citys': 'cities',
        'countrys': 'countries',
        'region_majors': 'regions_major',
        'region_minors': 'regions_minor',
    }
    for old_name, new_name in PLURAL_NAMES_TO_REPLACE.items():
        txt = txt.replace(old_name, new_name)
    return txt


def get_full_file_path_for_absolute_or_relative_path(absolute_or_relative_path: str) -> str:
    if os.path.isabs(absolute_or_relative_path):
        absolute_path = absolute_or_relative_path
    else:
        absolute_path = os.path.join(settings.s.files_root_dir_path, absolute_or_relative_path)
    return absolute_path


def open_file_by_absolute_or_relative_path(absolute_or_relative_path: str):
    absolute_path = get_full_file_path_for_absolute_or_relative_path(absolute_or_relative_path)

    logging.info(f'About to open file with absolute path "{absolute_path}"')

    if platform.system() == 'Darwin':  # macOS
        subprocess.call(('open', absolute_path))
    elif platform.system() == 'Windows':  # Windows
        os.startfile(absolute_path)
    else:  # linux variants
        subprocess.call(('xdg-open', absolute_path))


class Namespace:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

