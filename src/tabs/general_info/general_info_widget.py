from common.types.custom_datetime import CustomDateTime
from items.item import Item
from tabs.tab_widget import TabWidget
from utils.gui import set_editability_for_edits


class GeneralInfoWidget(TabWidget):

    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()
        self._setup_ui()
        self.create_ui_connections()

    def _setup_ui(self):
        set_editability_for_edits(
            [
                self.ui.lineEdit_path,
                self.ui.lineEdit_prompts_path,
                self.ui.textEdit_items,
                self.ui.textEdit_prompts_items,
            ],
            is_editable=False)

    def generate_ui(self):
        from tabs.general_info.moc_general_info_widget import Ui_general_info_widget
        ui = Ui_general_info_widget()
        ui.setupUi(self)
        return ui

    def set_engine_modification_info(self, engine_modified_since_reload, last_tabs_reload_time):
        modification_time_txt = CustomDateTime.from_int_or_none(
            self.engine.last_engine_modification_time).to_chronological_date_time_string()
        last_reload_time_txt = CustomDateTime.from_int_or_none(
            last_tabs_reload_time).to_chronological_date_time_string()

        self.ui.label_last_engine_modification_time.setText(modification_time_txt)
        self.ui.label_lat_tabs_reload_time.setText(last_reload_time_txt)
        self.ui.label_is_reload_required.setText(str(engine_modified_since_reload))

    def _reload_after_engine_changed(self):
        self._set_database_info(
            collection=self.engine.collection,
            path=self.engine.collection_file_path,
            label_status=self.ui.label_status,
            line_edit_path=self.ui.lineEdit_path,
            text_edit_items=self.ui.textEdit_items,
        )
        self._set_database_info(
            collection=self.engine.prompts_collection,
            path=self.engine.prompts_collection_file_path,
            label_status=self.ui.label_prompts_status,
            line_edit_path=self.ui.lineEdit_prompts_path,
            text_edit_items=self.ui.textEdit_prompts_items,
        )

    def _set_database_info(self, collection, path, label_status, line_edit_path, text_edit_items):
        if path:
            line_edit_path.setText(path)
            label_status.setText('Loaded')
        else:
            line_edit_path.setText('-')
            label_status.setText('Not loaded')

        total_items_count = 0
        members_count_txt = ''
        for member_name in collection.__dataclass_fields__.keys():
            member_value = getattr(collection, member_name)
            if isinstance(member_value, list) and \
                    len(member_value) > 0 and \
                    isinstance(member_value[0], Item):
                total_items_count += len(member_value)
                members_count_txt += member_name.capitalize() + ': ' + str(len(member_value)) + '\n'
        members_count_txt += f'Total Items: {total_items_count}'
        text_edit_items.setText(members_count_txt)

    def create_ui_connections(self):
        pass


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = GeneralInfoWidget(None, None)
    w.show()
    sys.exit(app.exec_())
