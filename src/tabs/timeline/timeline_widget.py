from PySide2.QtCore import Slot

from items.attachment.photo import Photo
from items.flight import Flight
from items.journey import Journey
from items.visit import Visit
from items_utils.misc import get_closes_item_on_timeline
from items_utils.plot_item import get_plot_points_for_items, get_plot_rectangles_for_items, ITEM_TYPE_TO_Y_VALUE_MAPPING
from tabs.tab_widget import TabWidget
from tabs.timeline.timeline_plot import TimelinePlot
from utils.gui import set_splitter_division


class TimelineWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()

        self.timeline_plot = TimelinePlot(parent=self)
        self.ui.verticalLayout_for_graph.addWidget(self.timeline_plot)
        self.setMinimumHeight(450)
        self._items_for_y_values = {}

        set_splitter_division(self.ui.splitter, 5)

        self.create_ui_connections()

    def generate_ui(self):
        from tabs.timeline.moc_timeline_widget import Ui_timeline_widget
        ui = Ui_timeline_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_generate.clicked.connect(self._generate_and_show)
        self.timeline_plot.signal__clicked_on_plot_near_y_integers.connect(self._mouse_clicked_on_plot_near_y_integers)

    def _reload_after_engine_changed(self):
        if self.ui.checkBox_autorefresh_after_engine_change.isChecked():
            self._generate_and_show()  # may be time consuming
        else:
            pass  # Nothing to refresh

        pass

    def _get_currently_selected_filter_function(self):
        filter_text = self.ui.comboBox_filter_by.currentText()
        for filter_name, filter_definition in self._filters.items():
            if filter_text == filter_name:
                return filter_definition['filter_function']

    @Slot()
    def _generate_and_show(self):
        journeys = self.engine.collection.journeys if self.ui.checkBox_journeys.isChecked() else []
        visits = self.engine.collection.visits if self.ui.checkBox_visits.isChecked() else []
        flights = self.engine.collection.flights if self.ui.checkBox_flights.isChecked() else []
        photos = self.engine.collection.photos if self.ui.checkBox_photos.isChecked() else []

        self._items_for_y_values = {
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Journey]: journeys,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Visit]: visits,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Flight]: flights,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Photo]: photos,
        }

        self.timeline_plot.plot_data(
            rectangle_items=get_plot_rectangles_for_items(journeys=journeys,
                                                          visits=visits,
                                                          flights=flights),
            point_items=get_plot_points_for_items(photos=photos))

    def _mouse_clicked_on_plot_near_y_integers(self, x, y, x_range):
        clicked_item = get_closes_item_on_timeline(items_for_y_values=self._items_for_y_values, x=x, y=y, x_range=x_range)
        if clicked_item:
            self.signal_link_clicked.emit(clicked_item)


def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets
    from utils.dummy_engine import get_dummy_engine

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = TimelineWidget(None, None)
    w.show()
    w.set_engine(get_dummy_engine())
    w._generate_and_show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
