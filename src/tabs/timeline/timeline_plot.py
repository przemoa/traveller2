import datetime
import itertools
import logging
import time
from typing import List, Optional

import pyqtgraph as pg
from PySide2 import QtCore, QtGui
from PySide2.QtCore import Signal

from items_utils.plot_item import PlotItemDefinition, Y_VALUE_TO_ITEM_TYPE_MAPPING


class TimeAxisItem(pg.AxisItem):
    def __init__(self, orientation='bottom', *args, **kwargs):
        super().__init__(orientation=orientation, *args, **kwargs)
        self.setLabel(text="Date and time")

    def tickStrings(self, values, scale, spacing):
        # print(f'values: {values}, scale: {scale}, spacing: {spacing}')

        time_range = spacing
        if time_range > 4*365 *24*3600:
            time_format = '{0:%Y}'
        elif time_range > 180 *24*3600:
            time_format = '{0:%m.%Y}'
        elif time_range > 4 *24*3600:
            time_format = '{0:%d.%m.%Y}'
        else:
            time_format = '{0:%d.%m.%Y\n%H:%M}'

        ticks = [time_format.format(datetime.datetime.fromtimestamp(value)) for value in values]
        return ticks


Y_VALUE_TO_COLOR_MAPPING = {
    1: 'b',
    2: 'g',
    3: 'k',
    4: 'm',
}


class ItemTypeAxisItem(pg.AxisItem):
    def __init__(self, orientation='left', *args, **kwargs):
        super().__init__(orientation=orientation, *args, **kwargs)
        self.setLabel(text="Item type")

    def tickStrings(self, values, scale, spacing):
        tick_classes = [Y_VALUE_TO_ITEM_TYPE_MAPPING.get(value, '') for value in values]
        ticks = [tc.__name__ if tc else '' for tc in tick_classes]
        return ticks


class RectangleItemsPlot(pg.GraphicsObject):
    """ Modified code from pyqtgraph example - 'Custom Grpahics'"""

    def __init__(self, data: List[PlotItemDefinition]):
        pg.GraphicsObject.__init__(self)
        self.data = data
        self.generatePicture()

    def generatePicture(self):
        ## pre-computing a QPicture object allows paint() to run much more quickly,
        ## rather than re-drawing the shapes every time.
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        p.setPen(pg.mkPen('w'))
        for rectangle_item in self.data:
            p.setBrush(pg.mkBrush(Y_VALUE_TO_COLOR_MAPPING[rectangle_item.y]))
            timespan = rectangle_item.x_end - rectangle_item.x_start
            p.drawRect(QtCore.QRectF(rectangle_item.x_start,
                                     rectangle_item.y - rectangle_item.y_width,
                                     timespan,
                                     2*rectangle_item.y_width))
        p.end()

    def paint(self, p, *args):
        p.drawPicture(0, 0, self.picture)

    def boundingRect(self):
        return QtCore.QRectF(self.picture.boundingRect())


class TimelinePlot(pg.GraphicsLayoutWidget):
    signal__clicked_on_plot_near_y_integers = Signal(float, int, float)  # x, y, x_range on plot
    def __init__(self, parent):
        super().__init__(parent=parent, show=True)
        self._plot = self.addPlot(axisItems={
            'bottom': TimeAxisItem(),
            'left': ItemTypeAxisItem(),
        })
        self._plot.showGrid(x=True, y=True)
        view = self._plot.getViewBox()
        view.setLimits(xMin=0,
                       xMax=1000000000,
                       minXRange=300,
                       yMin=0,
                       yMax=5,
                       minYRange=5)
        self._rectangles_plot = None
        self._points_plot = None
        self._vertical_line = None
        self._labels = []
        self._plot.scene().sigMouseClicked.connect(self._mouse_clicked)

    def _mouse_clicked(self, event):
        Y_RANGE_MARGIN = 0.2
        pos = event.scenePos()
        if self._plot.sceneBoundingRect().contains(pos):
            mousePoint = self._plot.vb.mapSceneToView(pos)
            y = mousePoint.y()
            if abs(round(y) - y) < Y_RANGE_MARGIN:
                min_x, max_x = self._plot.viewRange()[0]
                x_range = max_x - min_x
                self.signal__clicked_on_plot_near_y_integers.emit(mousePoint.x(), int(y+Y_RANGE_MARGIN), x_range)


    def plot_data(self,
                  rectangle_items: PlotItemDefinition,
                  point_items:PlotItemDefinition,
                  focus_point: Optional[PlotItemDefinition] = None):
        logging.info('Reploting data on timeline plot')
        self._plot.clear()
        self._labels = []

        if rectangle_items or point_items:
            maximum_x = 0
            minimum_x = time.time()

            if rectangle_items:
                minimum_x = min(minimum_x, min([rectangle_item.x_start for rectangle_item in rectangle_items]))
                maximum_x = max(maximum_x, max([rectangle_item.x_end for rectangle_item in rectangle_items]))

            if point_items:
                minimum_x = min(minimum_x, min([plot_item.x_start for plot_item in point_items]))
                maximum_x = max(maximum_x, max([plot_item.x_start for plot_item in point_items]))

            x_range = maximum_x - minimum_x
            self._plot.getViewBox().setLimits(xMin=minimum_x-x_range/10, xMax=maximum_x+x_range/10)

        self._rectangles_plot = RectangleItemsPlot(rectangle_items)

        if focus_point:
            self._vertical_line = pg.InfiniteLine(angle=90,
                                                  movable=False,
                                                  pen=pg.mkPen(style=QtCore.Qt.DashLine, color='k', width=3))
            self._vertical_line.setPos(focus_point.get_middle_x())
            self._plot.addItem(self._vertical_line)
            X_RANGE_DURING_FOCUS = 14*24*3600
            self._plot.getViewBox().setXRange(min=focus_point.get_middle_x() - X_RANGE_DURING_FOCUS/2,
                                              max=focus_point.get_middle_x() + X_RANGE_DURING_FOCUS/2)

        points_x = []
        points_y = []
        points_colors = []
        for plot_item in itertools.chain(rectangle_items, point_items):
            points_x.append(plot_item.get_middle_x())
            points_y.append(plot_item.y)
            points_colors.append(Y_VALUE_TO_COLOR_MAPPING.get(plot_item.y, 'r'))

            if not plot_item.txt:
                continue
            label = pg.TextItem(text=plot_item.txt, color=(33, 33, 33))
            label.setPos(plot_item.x_start, plot_item.y-plot_item.y_width)
            self._plot.addItem(label, ignoreBounds=True)
            self._labels.append(label)

        self._plot.addItem(self._rectangles_plot)

        self._points_plot = self._plot.plot()
        brush_list = [pg.mkColor(c) for c in points_colors]
        self._points_plot.setData(x=points_x, y=points_y, size=7, pen=pg.mkPen(None), symbol='o', symbolBrush=brush_list)


