from items.attachment.photo import Photo
from items_definitions.item_classes import get_item_class_name_definition
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from tabs.common_ui.display_item.multiple_photos_display.multiple_photos_display_widget import \
    MultiplePhotosDisplayWidget
from tabs.common_ui.filter_item.item_filter_widget import ItemFilterWidget
from tabs.common_ui.filter_item.multiple_photos_filter.multiple_photos_filter_widget import MultiplePhotosFilterWidget
from tabs.common_ui.items_list_display.items_list_display_widget import ItemsListDisplayWidget
from tabs.tab_widget import TabWidget
from utils.dummy_engine import get_dummy_engine
from utils.gui import set_splitter_division

SPECIAL_FILTER_WIDGETS = {
    Photo: MultiplePhotosFilterWidget,
}

SPECIAL_DISPLAY_WIDGETS = {
    Photo: MultiplePhotosDisplayWidget,
}


class ItemEditWidget(TabWidget):
    def __init__(self, parent, tab_id: str, item_class, use_special_filter_and_display_widgets=False):
        super().__init__(parent=parent, tab_id=tab_id)
        self._item_class = item_class
        self.ui = self.generate_ui()
        set_splitter_division(self.ui.splitter_filter_display_and_assoiciated, 75)
        set_splitter_division(self.ui.splitter_display_filter, 25)

        item_filter_widget_class = SPECIAL_FILTER_WIDGETS[item_class] if use_special_filter_and_display_widgets else ItemFilterWidget
        item_display_widget_class = SPECIAL_DISPLAY_WIDGETS[item_class] if use_special_filter_and_display_widgets else ItemDisplayWidget
        self._item_filter_widget = item_filter_widget_class(parent=self,
                                                            item_class=item_class)
        self._item_display_widget = item_display_widget_class(parent=self,
                                                              item_class=item_class,
                                                              is_editable=True,
                                                              clickable_links=True)
        item_class_definition = get_item_class_name_definition(item_class.__name__)
        if item_class_definition.has_belongings():
            self._associated_items_widget = ItemsListDisplayWidget(parent=self)
            self._associated_items_widget.signal_link_clicked.connect(self.signal_link_clicked)
            self.ui.scrollArea_associated_items.setWidget(self._associated_items_widget)
        else:
            self._associated_items_widget = None
            self.ui.widget_associated_items.setVisible(False)

        self.ui.scrollArea_filters.setWidget(self._item_filter_widget)
        self.ui.scrollArea_display.setWidget(self._item_display_widget)
        self.create_ui_connections()

    def generate_ui(self):
        from tabs.edit.moc_item_edit_widget import Ui_item_edit_widget
        ui = Ui_item_edit_widget()
        ui.setupUi(self)
        return ui

    def _reload_after_engine_changed(self):
        self._item_filter_widget.set_engine(self.engine)
        self._item_display_widget.set_engine(self.engine)
        if self._associated_items_widget:
            self._associated_items_widget.set_engine(self.engine)

    def create_ui_connections(self):
        self._item_filter_widget.signal_item_selected.connect(self._item_display_widget.display_item)
        if self._associated_items_widget:
            self._item_filter_widget.signal_item_selected.connect(self._associated_items_widget.set_belongings_of_item)
        self._item_display_widget.signal_displayed_item_changed.connect(self._item_filter_widget.reload_and_select_displayed_item)
        self._item_filter_widget.signal_link_clicked.connect(self.signal_link_clicked)
        self._item_display_widget.signal_link_clicked.connect(self.signal_link_clicked)


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    # w = ItemEditWidget(None, get_dummy_engine(), Journey)
    # w = ItemEditWidget(parent=None, tab_id='x', item_class=Photo, use_special_filter_and_display_widgets=True)
    w = ItemEditWidget(parent=None, tab_id='x', item_class=Photo, use_special_filter_and_display_widgets=True)
    w.show()
    w.set_engine(get_dummy_engine())
    sys.exit(app.exec_())
