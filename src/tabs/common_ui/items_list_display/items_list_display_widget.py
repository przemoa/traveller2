import copy
from typing import Union, List

from PySide2 import QtWidgets
from PySide2.QtCore import Signal, Qt
from PySide2.QtGui import QStandardItem, QStandardItemModel
from PySide2.QtWidgets import QAbstractItemView

from engine.engine import Engine
from items.item import Item
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from utils.dummy_engine import get_dummy_engine


class ItemsListDisplayWidget(QtWidgets.QWidget):
    signal_link_clicked = Signal(object)

    def __init__(self, parent, description_text=''):
        QtWidgets.QWidget.__init__(self, parent)
        self.engine = None  # type: Engine
        self.ui = self.generate_ui()
        self._description_text = description_text

        self._item = None  # if displayed items are belongings of specific item. Required to refresh list on engine change
        self._displayed_items = []
        self.ui.checkBox_recursive_belongings.setVisible(True)

        self._current_display_widget = None
        self._cached_display_widgets = {}

        self._model = QStandardItemModel(self.ui.listView_associated_items)
        self.ui.listView_associated_items.setModel(self._model)
        self.ui.listView_associated_items.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.create_ui_connections()
        self.set_belongings_of_item(None)

    def generate_ui(self):
        from tabs.common_ui.items_list_display.moc_items_list_display_widget import Ui_items_list_display_widget
        ui = Ui_items_list_display_widget()
        ui.setupUi(self)
        return ui

    def set_engine(self, engine):
        """ Load (for the first time) or reload (new one) engine

        Called at startup or if traveller2 database or prompts database changed
        """
        self.engine = engine
        self._reload_after_engine_changed()

    def _reload_after_engine_changed(self):
        if self._item:
            self.set_belongings_of_item(self._item)
        else:
            self.set_items(self._displayed_items)

    def create_ui_connections(self):
        self.ui.listView_associated_items.selectionModel().selectionChanged.connect(self._selected_item_on_list_changed)
        self.ui.listView_associated_items.doubleClicked.connect(self._selected_item_double_clicked)
        self.ui.checkBox_recursive_belongings.stateChanged.connect(self.show_recursive_belongings_state_changed)

    def set_belongings_of_item(self, item: Union[Item, None], show_referenced_by_items: bool = True):
        self._item = item
        self.ui.checkBox_recursive_belongings.setVisible(True)

        if item and item and hasattr(item, 'belongings'):
            if self.ui.checkBox_recursive_belongings.isChecked():
                associated_items = list(item.get_belongings_set_recursively())
            else:
                associated_items = copy.copy(item.belongings)
            if show_referenced_by_items and item and item and hasattr(item, 'referenced_by'):
                associated_items += item.referenced_by
        else:
            associated_items = []
        description_text = self._description_text or 'Item has'
        self.ui.label_number_of_items.setText(f'{description_text} {len(associated_items)} associated items(s):')
        self._displayed_items = associated_items
        self._display_items_on_list(self._displayed_items)

    def show_recursive_belongings_state_changed(self):
        if self._item:
            self.set_belongings_of_item(self._item)

    def set_items(self, items: List[Item]):
        self.ui.checkBox_recursive_belongings.setVisible(False)
        self._item = None
        description_text = self._description_text or ''
        self.ui.label_number_of_items.setText(f'{description_text} {len(items)} item(s)')
        self._displayed_items = copy.copy(items)
        self._display_items_on_list(self._displayed_items)

    def _display_items_on_list(self, items: List[Item]):
        self._model.clear()
        self.ui.listView_associated_items.selectionModel().clear()
        for item in items:
            txt = f'{item.get_short_description()} ({item.__class__.__name__})'
            q_item = QStandardItem(txt)
            q_item.setData(item, Qt.UserRole)
            self._model.appendRow(q_item)

    def _selected_item_on_list_changed(self):
        selection = self.ui.listView_associated_items.selectionModel().selectedIndexes()
        if len(selection) == 1:
            selected_item_row = selection[0].row()
            selected_item = self._displayed_items[selected_item_row]
        else:
            selected_item = None
        self._display_item_details(selected_item)

    def _selected_item_double_clicked(self, q_item):
        item = q_item.data(Qt.UserRole)
        self.signal_link_clicked.emit(item)

    def _display_item_details(self, item: Union[Item, None]):
        if self._current_display_widget:
            self.ui.horizontalLayout_display_widget.removeWidget(self._current_display_widget)
            self._current_display_widget.setVisible(False)
            self._current_display_widget = None

        if item:
            self._current_display_widget = self._cached_display_widgets.get(item.__class__, None)
            if self._current_display_widget is None:
                self._current_display_widget = ItemDisplayWidget(parent=self,
                                                                 item_class=item.__class__,
                                                                 is_editable=False,
                                                                 clickable_links=True)
                self._cached_display_widgets[item.__class__] = self._current_display_widget
                self._current_display_widget.signal_link_clicked.connect(self.signal_link_clicked)

            self._current_display_widget.setVisible(True)

            self.ui.horizontalLayout_display_widget.addWidget(self._current_display_widget)
            self._current_display_widget.display_item(item)


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = ItemsListDisplayWidget(None)
    w.show()
    w.set_belongings_of_item(get_dummy_engine().collection.journeys[1])
    sys.exit(app.exec_())
