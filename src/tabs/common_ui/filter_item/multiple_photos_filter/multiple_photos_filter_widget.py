import logging
import os

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QFileDialog, QMessageBox

from common import settings
from dialogs.progress.progress_dialog import ProgressDialog
from items.attachment.photo import Photo
from items_utils import photo_from_path
from tabs.common_ui.display_item.multiple_photos_display.dirs_photos_table_widget import DirsTableWidget, \
    PhotosTableWidget
from tabs.common_ui.filter_item.item_filter_widget import ItemFilterWidget


class MultiplePhotosFilterWidget(ItemFilterWidget):
    """
    TODO
    ItemFilterWidget and this widget should be change to inherit common interface, instead of one inheriting another
    """

    # signal_item_selected = Signal(object)  # Union[List[Item], None]  # in base class
    # signal_link_clicked = Signal(object)  # in base class

    def __init__(self, parent, item_class):
        ItemFilterWidget.__init__(self, parent=parent, item_class=item_class)
        # self._last_selected_item = None
        # self._last_selected_item used instead of self._last_selected_items to conform to base class interface

        self._dirs_table_widget = DirsTableWidget(parent=self)
        self.ui.verticalLayout_dirs_table.addWidget(self._dirs_table_widget)

        self._photos_table_widget = PhotosTableWidget(parent=self)
        self.ui.verticalLayout_photos_table.addWidget(self._photos_table_widget)

        self.create_additional_ui_connections()

    def create_additional_ui_connections(self):
        self._dirs_table_widget.signal_selected_dirs_changed.connect(self._photos_in_dirs_changed)
        self._photos_table_widget.signal_selected_photos_changed.connect(self._photos_to_display_changed)
        self.ui.pushButton_add.clicked.connect(self._add_photos_clicked)

    def _show_hide_filters(self):
        pass

    @Slot()
    def _photos_in_dirs_changed(self):
        images_in_dirs = self._dirs_table_widget.get_photos_in_selected_dirs()
        filtered_images_in_dirs = self._filter_all_items(images_in_dirs)
        self.ui.label_count_of_items.setText(f'{len(filtered_images_in_dirs)}/{len(images_in_dirs)}')
        self._photos_table_widget.display_photos(filtered_images_in_dirs)
        self._photos_to_display_changed()

    @Slot()
    def _photos_to_display_changed(self):
        photos_to_display = self._photos_table_widget.get_selected_photos()
        self._last_selected_item = photos_to_display
        self.signal_item_selected.emit(photos_to_display)

    def select_photos_to_add_from_dialog(self):
        images_to_add = QFileDialog.getOpenFileNames(self,
                                                 "Select Photos",
                                                 os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop'),
                                                 'Images (*.jpg *.jpeg *.png *.JPG *.JPEG);; '
                                                 'All files (*.*)')
        return images_to_add[0]

    @Slot()
    def _add_photos_clicked(self):
        images_to_add = self.select_photos_to_add_from_dialog()
        items_already_in_database = []
        progress_dialog = ProgressDialog(parent=self, title='Loading photos...', max_value=len(images_to_add))
        added_images_count = 0
        for i, image_path in enumerate(images_to_add):
            progress_dialog.set_value(i)
            progress_dialog.set_message(f'[{i}/{len(images_to_add)}] Loading photo\n{image_path}...')
            if progress_dialog.was_canceled():
                break

            try:
                photo = photo_from_path.get_photo_from_file_path(abs_file_path=image_path,
                                                                 base_dir=settings.s.files_root_dir_path,
                                                                 thumbnail_size=settings.s.thumbnail_size_in_pixels)
            except Exception:
                logging.exception(f'Failed to load photo "{image_path}"!')
                continue

            try:
                self.engine.collection.add_item(photo)
                added_images_count += 1
            except Exception:  # TODO handle duplicated item/error
                items_already_in_database.append(image_path)

        if added_images_count:
            self.engine.set_engine_as_modified()

        progress_dialog.cancel()
        if len(items_already_in_database):
            skipped_photos_txt = '\n'.join(
                ['{}. {}'.format(i, file_path) for i, file_path in enumerate(items_already_in_database[:10])])
            if len(items_already_in_database) > 10:
                skipped_photos_txt += '\n and {} more...'.format(len(items_already_in_database) - 10)
            QMessageBox.warning(self, "Warning!", 'Cannot add some photos to database. '
                                                  # 'because photo with same name and date already exists. '
                                                  'Following photos were not added:\n'
                                                  + skipped_photos_txt)
        if len(images_to_add):
            self._reload_after_engine_changed()

    def _update_list_of_all_dirs_and_photos(self):
        self._dirs_table_widget.display_dirs_for_photos(all_photos=self.engine.collection.photos)
        self._photos_in_dirs_changed()

    def generate_ui(self):
        from tabs.common_ui.filter_item.multiple_photos_filter.moc_multiple_photos_filter_widget  import Ui_filter_multiple_photos_widget
        ui = Ui_filter_multiple_photos_widget()
        ui.setupUi(self)
        return ui

    def reload_and_select_displayed_item(self, item):
        self._photos_table_widget.select_photos(item)

    def _filter_changed(self):
        self._photos_in_dirs_changed()

    def _set_list_of_all_items(self):
        self._update_list_of_all_dirs_and_photos()

    def _selected_item_changed(self):
        raise NotImplementedError()

    def get_last_selected_item(self):
        return self._last_selected_item

    def create_ui_connections(self):
        for field_ui in self._field_uis:
            field_ui.signal_field_modified.connect(self._filter_changed)


def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    from utils.dummy_engine import get_dummy_engine
    settings.DISPLAY_QMESSAGE_AFTER_UNHANDLED_EXCEPTION = False

    w = MultiplePhotosFilterWidget(parent=None, item_class=Photo)
    w.set_engine(get_dummy_engine())
    w.reload_and_select_displayed_item(None)
    w.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
