import copy

from PySide2 import QtWidgets
from PySide2.QtCore import Slot, Signal

from common import settings
from common.types.custom_datetime import CustomDateTime
from common.types.image_annotation import ImageAnnotation
from engine.engine import Engine
from items.attachment.photo import Photo
from items.item import Item
from items_definitions.item_classes import get_item_class_name_definition, \
    get_all_fields_for_item_class_definition_by_name
from items_definitions.item_field_ui import ItemFieldUi, ItemFieldUiPlainTextEdit, ItemFieldUiLineEdit
from items_definitions.item_fields import ItemFieldDateTimeStart, ItemFieldDateTimeEnd, ItemFieldDateTimePoint, \
    ItemFieldDateTimeBefore, ItemFieldDateTimeAfter
from items_utils import filtering
from items_utils.misc import ALL_ITEMS_WITH_HANDLED_PROMPTS
from utils.gui import add_widget_or_layout_to_ui


class ItemFilterWidget(QtWidgets.QWidget):
    signal_item_selected = Signal(Item)
    signal_link_clicked = Signal(object)

    def __init__(self, parent, item_class, allow_for_prompts=False):
        QtWidgets.QWidget.__init__(self, parent)
        self.engine = None  # type: Engine
        self._item_class = item_class
        self._allow_for_prompts = allow_for_prompts and item_class in ALL_ITEMS_WITH_HANDLED_PROMPTS
        self.ui = self.generate_ui()
        self._field_uis = self._generate_field_uis()  # type: List[ItemFieldUi]
        self._show_hide_filters()
        self._last_selected_item = None
        self._all_items = []
        self.create_ui_connections()
        self._can_be_sorted_by_date = self._determine_if_can_be_sorted_by_date()

    def _determine_if_can_be_sorted_by_date(self):
        class_definition = get_item_class_name_definition(self._item_class.__name__)
        can_be_sorted_by_date = class_definition.can_be_time_filtered()
        return can_be_sorted_by_date

    def generate_ui(self):
        from tabs.common_ui.filter_item.moc_item_filter_widget import Ui_item_filter_widget
        ui = Ui_item_filter_widget()
        ui.setupUi(self)
        return ui

    def set_engine(self, engine):
        self.engine = engine
        self._reload_after_engine_changed()

    def _reload_after_engine_changed(self):
        for field_ui in self._field_uis:
            field_ui.set_engine(self.engine)
        self._set_list_of_all_items()

    def reload_and_select_displayed_item(self, item):
        self.ui.comboBox_items.blockSignals(True)
        if item is not None:
            self._clear_filters()
        self._last_selected_item = item
        self._set_list_of_all_items()
        self.ui.comboBox_items.blockSignals(False)

    def _clear_filters(self):
        for field_ui in self._field_uis:
            field_ui.set_value(None)

    def _filter_changed(self):
        self.ui.comboBox_items.blockSignals(True)
        self._set_list_of_all_items()
        self.ui.comboBox_items.blockSignals(False)

    def _filter_all_items(self, all_items):
        items_filtered = []
        for item in all_items:
            for field_ui in self._field_uis:
                value = field_ui.get_value()
                if not value:
                    continue
                if isinstance(value, str):
                    field_value = getattr(item, field_ui.field_name)
                    if field_value is None:
                        break
                    if isinstance(field_value, ImageAnnotation):
                        field_str = field_value.get_long_description().lower() if field_value else ''
                    else:
                        field_str = field_value.lower()
                    if value.lower() not in field_str:
                        break
                elif isinstance(field_ui.field_definition, ItemFieldDateTimeBefore):
                    if not item.is_before_date(value):
                        break
                elif isinstance(field_ui.field_definition, ItemFieldDateTimeAfter):
                    if not item.is_after_date(value):
                        break
                elif isinstance(value, int):
                    field_value = getattr(item, field_ui.field_name)
                    if field_value is None:
                        break
                    if getattr(item, field_ui.field_name) != value:
                        break
                elif isinstance(value, Item):
                    if getattr(item, field_ui.field_name) != value:
                        break
                else:
                    raise Exception(f'Unknown filter field type: {field_ui} [{field_ui.__dict__}]')
            else:
                items_filtered.append(item)
        return items_filtered

    def _set_list_of_all_items(self):
        self.ui.comboBox_items.blockSignals(True)

        if self.ui.checkBox_show_from_prompts_database.isChecked():
            collection = self.engine.prompts_collection
        else:
            collection = self.engine.collection

        all_items_unfiltered = collection.get_items_list_for_class_name(self._item_class.__name__)
        all_items_filtered = self._filter_all_items(all_items_unfiltered)
        if self._can_be_sorted_by_date and settings.s.display_items_sorted_by_date:
            all_items_filtered_sorted = filtering.sort_items_by_time(all_items_filtered)
        else:
            all_items_filtered_sorted = all_items_filtered

        self.ui.label_count_of_items.setText(f'{len(all_items_filtered_sorted)}/{len(all_items_unfiltered)}')

        self.ui.comboBox_items.clear()

        self._all_items = [None]
        combobox_entries = ['']

        for item in all_items_filtered_sorted:
            self._all_items.append(item)
            combobox_entries.append(item.get_short_description())

        self.ui.comboBox_items.addItems(combobox_entries)

        if self._last_selected_item in self._all_items:
            index = self._all_items.index(self._last_selected_item)
            self.ui.comboBox_items.setCurrentIndex(index)
        else:
            self.ui.comboBox_items.blockSignals(False)
            self._last_selected_item = None
            self.ui.comboBox_items.setCurrentIndex(0)  # None

        self.ui.comboBox_items.blockSignals(False)

    def create_ui_connections(self):
        for field_ui in self._field_uis:
            field_ui.signal_field_modified.connect(self._filter_changed)
        self.ui.comboBox_items.currentIndexChanged.connect(self._selected_item_changed)
        self.ui.checkBox_show_filters.stateChanged.connect(self._show_hide_filters)
        self.ui.checkBox_show_from_prompts_database.clicked.connect(self._set_list_of_all_items)

    @Slot()
    def _show_hide_filters(self):
        visible = self.ui.checkBox_show_filters.isChecked()
        self.ui.widget_filters.setVisible(visible)

    def _selected_item_changed(self):
        index = self.ui.comboBox_items.currentIndex()
        selected_item = self._all_items[index]
        if self._last_selected_item != selected_item:
            self._last_selected_item = selected_item
            self.signal_item_selected.emit(selected_item)

    def get_last_selected_item(self):
        return self._last_selected_item

    def _generate_field_uis(self):
        field_uis = []

        if not self._allow_for_prompts:
            try:
                self.ui.checkBox_show_from_prompts_database.setVisible(False)
            except AttributeError:
                pass  # if no checkbox, then ok

        all_fields_definitions = get_all_fields_for_item_class_definition_by_name(self._item_class.__name__)
        item_class_definition = get_item_class_name_definition(self._item_class.__name__)
        if item_class_definition.can_be_time_filtered():
            all_fields_definitions = copy.copy(all_fields_definitions)
            all_fields_definitions += [ItemFieldDateTimeBefore(), ItemFieldDateTimeAfter()]

        for field_definition in all_fields_definitions:
            if not field_definition.display:
                continue
            if not field_definition.filtering_enabled:
                continue

            if isinstance(field_definition, (ItemFieldDateTimeStart, ItemFieldDateTimeEnd, ItemFieldDateTimePoint)):
                continue  # add before/after instead

            item_field_ui = field_definition.item_field_ui
            if item_field_ui == ItemFieldUiPlainTextEdit:
                item_field_ui = ItemFieldUiLineEdit

            field_ui = item_field_ui(
                parent=self,
                field_name=field_definition.field_name,
                field_definition=field_definition,
                engine=self.engine,
                is_editable=True,
                clickable_links=True,
                is_filtering_item=True,
            )

            row = len(field_uis)

            add_widget_or_layout_to_ui(parent_layout=self.ui.gridLayout_filters,
                                       widget_or_layout=field_ui.left_widget,
                                       row=row,
                                       column=0)
            add_widget_or_layout_to_ui(parent_layout=self.ui.gridLayout_filters,
                                       widget_or_layout=field_ui.right_widget_layout,
                                       row=row,
                                       column=1)
            field_ui.signal_link_clicked.connect(self.signal_link_clicked)
            field_uis.append(field_ui)

        return field_uis


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    from utils.dummy_engine import get_dummy_engine

    # w = ItemFilterWidget(parent=None, item_class=Journey)
    w = ItemFilterWidget(parent=None, item_class=Photo)
    w.set_engine(get_dummy_engine())
    w.show()
    sys.exit(app.exec_())
