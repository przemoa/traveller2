from dataclasses import dataclass
from functools import partial
from typing import Union

from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2.QtCore import QByteArray, Signal, Slot
from PySide2.QtGui import QPixmap, QImage
from PySide2.QtWidgets import QGraphicsPixmapItem, QGraphicsScene

from dialogs.blocking_operation_message import BlockingOperationWithMessagebox
from items.item import Item
from utils.misc import open_file_by_absolute_or_relative_path


class DisplaySingleImageWidget(QtWidgets.QWidget):
    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.ui = self.generate_ui()
        self.create_ui_connections()
        self._scene = QGraphicsScene(self)
        self.ui.graphicsView.setScene(self._scene)
        self._item = QGraphicsPixmapItem()
        self._scene.addItem(self._item)

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)

    def display_image(self, image_bytes: Union[bytes, None]):
        if image_bytes is None:
            image_bytes = b''
        qbyte_array = QByteArray(image_bytes)
        qimage = QImage()
        qimage.loadFromData(qbyte_array)
        pixmap = QPixmap(qimage)
        pixmap.convertFromImage(qimage)
        self._item.setPixmap(pixmap)
        self.fit_image()

    def resizeEvent(self, event):
        self.fit_image()

    def showEvent(self, event):
        self.fit_image()

    def fit_image(self):
        self.ui.graphicsView.fitInView(self._item, QtCore.Qt.KeepAspectRatio)

    def generate_ui(self):
        from tabs.common_ui.display_image.moc_display_image_widget import Ui_display_image_widget
        ui = Ui_display_image_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        pass


class GraphicsSceneWithMouse(QGraphicsScene):
    signal_mouse_clicked = Signal(object, object)  # (Tuple[float, float], bool) position and button

    def __init__(self, parent):
        QGraphicsScene.__init__(self, parent=parent)

    def mousePressEvent(self, mouse_event):
        # is_left_button = mouse_event.button() == QtCore.Qt.LeftButton
        # is_right_button = mouse_event.button() == QtCore.Qt.RightButton
        position = (mouse_event.scenePos().x(), mouse_event.scenePos().y())
        self.signal_mouse_clicked.emit(position, mouse_event.button())


@dataclass
class ImagePosition:
    x_start: float
    x_end: float
    y_start: float
    y_end: float

    def contains_point(self, point: tuple) -> bool:
        if self.y_start > point[1] or self.y_end < point[1]:
            return False
        if self.x_start > point[0] or self.x_end < point[0]:
            return False
        return True


class DisplayMultipleImagesWidget(QtWidgets.QWidget):
    signal_link_clicked = Signal(object)
    signal_item_deselected = Signal(Item)

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.ui = self.generate_ui()
        self.create_ui_connections()
        self._scene = GraphicsSceneWithMouse(self)
        self._scene.signal_mouse_clicked.connect(self._mouse_clicked_on_scene)
        self.ui.graphicsView.setScene(self._scene)

        self._photos_with_coordinate = dict()

        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)

    def display_multiple_images(self, photos: list):  # List[Photo]
        vertical_scrollbar_value = self.ui.graphicsView.verticalScrollBar().value()
        horizontal_scrollbar_value = self.ui.graphicsView.horizontalScrollBar().value()
        self._scene.clear()

        if len(photos) > 100:
            bowm = BlockingOperationWithMessagebox(parent=self)
            bowm.perform_operation(message='Loading images to display...',
                                   operation=partial(
                                       self._generate_multiple_images_data_for_display,
                                       photos=photos))
            result = bowm.result
        else:
            result = self._generate_multiple_images_data_for_display(photos=photos)

        self._photos_with_coordinate = result['photos_with_coordinate']
        for scene_item in result['scene_items']:
            self._scene.addItem(scene_item)

        self.fit_image()
        self.ui.graphicsView.verticalScrollBar().setValue(vertical_scrollbar_value)
        self.ui.graphicsView.horizontalScrollBar().setValue(horizontal_scrollbar_value)

    @staticmethod
    def _generate_multiple_images_data_for_display(photos: list):
        NUMBER_OF_COLUMNS = 5
        current_y_offset = 0
        current_x_offset = 0
        current_row_height = 0
        photos_with_coordinate = dict()
        scene_items = []

        for i, photo in enumerate(reversed(photos)):
            item = QGraphicsPixmapItem()
            if i % NUMBER_OF_COLUMNS == 0:
                current_y_offset += current_row_height
                current_x_offset = 0
                current_row_height = 0

            qbyte_array = QByteArray(photo.thumbnail)
            qimage = QImage()
            qimage.loadFromData(qbyte_array)

            item.setOffset(
                current_x_offset,
                current_y_offset)
            photos_with_coordinate[photo] = ImagePosition(x_start=current_x_offset,
                                                                x_end=current_x_offset + qimage.width(),
                                                                y_start=current_y_offset,
                                                                y_end=current_y_offset + qimage.height())
            current_row_height = max(current_row_height, qimage.height())
            current_x_offset += qimage.width() + 5  # 5 px is for small margin between images
            pixmap = QPixmap(qimage)
            pixmap.convertFromImage(qimage)
            item.setPixmap(pixmap)
            scene_items.append(item)

        return {
            'photos_with_coordinate': photos_with_coordinate,
            'scene_items': scene_items,
        }

    @Slot(tuple)
    def _mouse_clicked_on_scene(self, position: tuple, button):
        for photo, image_position in self._photos_with_coordinate.items():
            if image_position.contains_point(position):
                is_left_button = button == QtCore.Qt.LeftButton
                is_right_button = button == QtCore.Qt.RightButton
                is_middle_button = button == QtCore.Qt.MiddleButton

                if is_left_button:
                    open_file_by_absolute_or_relative_path(photo.relative_file_path or photo.original_full_path)
                elif is_middle_button:
                    self.signal_link_clicked.emit(photo)
                elif is_right_button:
                    self.signal_item_deselected.emit(photo)
                break
        else:
            pass

    def resizeEvent(self, event):
        self.fit_image()

    def showEvent(self, event):
        self.fit_image()

    def fit_image(self):
        self.ui.graphicsView.fitInView(self._scene.sceneRect(), QtCore.Qt.KeepAspectRatioByExpanding)

    def generate_ui(self):
        from tabs.common_ui.display_image.moc_display_image_widget import Ui_display_image_widget
        ui = Ui_display_image_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        pass
