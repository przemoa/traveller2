import logging
from typing import Union, List

from PySide2 import QtWidgets
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QMessageBox

from engine.engine import Engine
from items.item import Item
from items.journey import Journey
from items_definitions.item_classes import get_item_class_name_definition, \
    get_all_fields_for_item_class_definition_by_name
from items_definitions.item_field_ui import ItemFieldUi
from items_definitions.item_fields import ItemField
from utils.dummy_engine import get_dummy_engine
from utils.gui import add_widget_or_layout_to_ui


class ItemDisplayWidget(QtWidgets.QWidget):
    """ Class to display items.
    Mainly used to display single items, but also used as base class for displaying multiple items at once
    """

    signal_displayed_item_changed = Signal(object)  # Union[Item, List[Item], None]
    signal_link_clicked = Signal(object)  # Union[int <item_id>, str <item_id_str>, Item <item>]

    def __init__(self, parent, item_class, is_editable: bool, clickable_links: bool = False):
        QtWidgets.QWidget.__init__(self, parent)
        self.engine = None  # type: Engine
        self._clickable_links = clickable_links
        self._item_class = item_class
        self._is_editable = is_editable
        self._item = None  # type: Union[Item, None]  # currently displayed item
        self._is_modified = False  # if fields for currently displayed item were modified
        self.ui = self.generate_ui()
        self.ui.widget_additiona_display.setVisible(False)
        self._field_uis = self._generate_field_uis()  # type: List[ItemFieldUi]
        self._setup_ui()
        self.display_item(None)
        self.create_ui_connections()

    def _generate_field_uis(self):
        return self._generate_field_uis_for_all_fields_definitions(
            get_all_fields_for_item_class_definition_by_name(self._item_class.__name__))

    def _generate_field_uis_for_all_fields_definitions(self, all_fields_definitions: list):
        field_uis = []
        for field_definition in all_fields_definitions:
            if not field_definition.display:
                continue

            field_ui = field_definition.item_field_ui(
                parent=self,
                field_name=field_definition.field_name,
                field_definition=field_definition,
                engine=self.engine,
                is_editable=self._is_editable and field_definition.editable_in_ui,
                clickable_links=self._clickable_links,
            )

            row = len(field_uis)

            add_widget_or_layout_to_ui(parent_layout=self.ui.gridLayout_display,
                                       widget_or_layout=field_ui.left_widget,
                                       row=row,
                                       column=0)
            add_widget_or_layout_to_ui(parent_layout=self.ui.gridLayout_display,
                                       widget_or_layout=field_ui.right_widget_layout,
                                       row=row,
                                       column=1)
            field_ui.signal_link_clicked.connect(self.signal_link_clicked)
            field_uis.append(field_ui)
        return field_uis

    def _item_modified(self):
        self._is_modified = True
        self._set_modify_buttons_states()
        logging.debug(f'{self._item_class.__name__} modified')

    def _setup_ui(self):
        self.ui.widget_modify_buttons.setVisible(self._is_editable)

    def generate_ui(self):
        from tabs.common_ui.display_item.moc_item_display_widget import Ui_item_display_widget
        ui = Ui_item_display_widget()
        ui.setupUi(self)
        return ui

    def set_engine(self, engine):
        """ Load (for the first time) or reload (new one) engine

        Called at startup or if traveller2 database or prompts database changed
        """
        self.engine = engine
        self._reload_after_engine_changed()

    def _reload_after_engine_changed(self):
        for field_ui in self._field_uis:
            field_ui.set_engine(self.engine)

    def create_ui_connections(self):
        for field_ui in self._field_uis:
            field_ui.signal_field_modified.connect(self._item_modified)
        self.ui.pushButton_delete.clicked.connect(self._delete_item)
        self.ui.pushButton_update.clicked.connect(self._update_item)
        self.ui.pushButton_add_as_new.clicked.connect(self._add_as_new)
        self.ui.pushButton_clear.clicked.connect(self._clear)

    def display_item(self, item: Union[Item, None]):
        if self._is_modified:
            reply = QMessageBox.question(self, "Confirm",
                                         'Are you sure you want to display new item? Current changes will be discarded!')
            if reply != QMessageBox.StandardButton.Yes:
                self.signal_displayed_item_changed.emit(self._item)  # to restore current item (displaying canceled)
                return

        self._item = item
        self._is_modified = False
        for field_ui in self._field_uis:
            value = getattr(item, field_ui.field_definition.field_name) if item else None
            field_ui.set_value(value=value, whole_item=item)
        self._set_modify_buttons_states()

    def _set_modify_buttons_states(self):
        is_displayed_item_in_database = self._item is not None and self._item.item_id is not None
        self.ui.pushButton_delete.setEnabled(is_displayed_item_in_database)
        self.ui.pushButton_update.setEnabled(is_displayed_item_in_database and self._is_modified)
        self.ui.pushButton_add_as_new.setEnabled(self._is_modified)

    def _clear(self):
        if self._is_modified:
            reply = QMessageBox.question(self, "Confirm",
                                         'Are you sure you want to clear displayed item? Current changes will be discarded!')
            if reply != QMessageBox.StandardButton.Yes:
                return

        self._is_modified = False
        self.display_item(None)
        self.signal_displayed_item_changed.emit(self._item)

    def _delete_item(self):
        if self._item.item_id is None:
            raise Exception('Error occurred. Unable to delete item without item_id!')
        item_class_definition = get_item_class_name_definition(self._item_class.__name__)
        if item_class_definition.has_belongings_or_can_be_referenced_by():
            associated_items = []
            if item_class_definition.has_belongings():
                associated_items += self._item.belongings
            if item_class_definition.can_be_referenced_by():
                associated_items += self._item.referenced_by

            if len(associated_items):
                warning_msg = 'Cannot delete item. The item has the following associated items:\n'
                MAX_NUMBER_OF_PRINTED_ITEMS = 20
                warning_msg += ',\n'.join([f'{i}: {item.__class__.__name__} - {item.get_short_description()}'
                                                  for i, item in enumerate(associated_items[:MAX_NUMBER_OF_PRINTED_ITEMS])])
                if len(associated_items) > MAX_NUMBER_OF_PRINTED_ITEMS:
                    warning_msg += f'\nand {len(associated_items) - MAX_NUMBER_OF_PRINTED_ITEMS} more...\n'
                else:
                    warning_msg += '\n'
                warning_msg += 'Please delete the associate items first!'
                QMessageBox.warning(self, "Warning!", warning_msg)
                return
        self.engine.collection.delete_item(self._item)
        self._item = None
        self.display_item(None)
        self.signal_displayed_item_changed.emit(self._item)
        self.engine.set_engine_as_modified()

    def _update_item(self):
        # before adding/updating item (after being read from the ui) assert that mandatory fields are not None!
        assert self._item is not None and self._item.item_id
        for field_ui in self._field_uis:
            field_definition = field_ui.field_definition  # type: ItemField
            if field_definition.editable_in_ui:
                logging.debug(f'setting {field_definition.field_name}')
                setattr(self._item, field_ui.field_definition.field_name, field_ui.get_value())
        self._is_modified = False
        logging.debug(f'Item {self._item.get_short_description()} updated.')
        self.signal_displayed_item_changed.emit(self._item)
        self.engine.set_engine_as_modified()
        self._set_modify_buttons_states()

    def _add_as_new(self):
        # TODO before adding/updating item (after being read from the ui) assert that mandatory fields are not None!

        fields_dict = {}
        for field_ui in self._field_uis:
            field_definition = field_ui.field_definition  # type: ItemField
            if field_definition.editable_in_ui:
                fields_dict[field_definition.field_name] = field_ui.get_value()

        hitherto_item = self._item
        try:
            self._item = self._item_class(**fields_dict)
            self.engine.collection.add_item(self._item)
            self._is_modified = False
            self.display_item(self._item)
            logging.debug(f'Item "{self._item.get_short_description()}" added as new.')
            self.signal_displayed_item_changed.emit(self._item)
            self.engine.set_engine_as_modified()
        except Exception as e:
            self._item = hitherto_item
            raise e


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = ItemDisplayWidget(None, Journey, True)
    w.set_engine(get_dummy_engine())
    w.show()
    sys.exit(app.exec_())
