import collections
from typing import List

from PySide2 import QtWidgets
from PySide2.QtCore import Slot, Signal, Qt, QItemSelection, QItemSelectionModel
from PySide2.QtWidgets import QTableWidget, QTableWidgetItem

from items.attachment.photo import Photo
from items_utils import filtering


class DirsTableWidget(QTableWidget):
    signal_selected_dirs_changed = Signal()

    def __init__(self, parent):
        QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setColumnCount(2)
        self.setRowCount(0)
        self.setMinimumWidth(150)
        self.horizontalHeader().setStretchLastSection(True)

        self.setHorizontalHeaderItem(0, QTableWidgetItem('Directory'))
        self.setHorizontalHeaderItem(1, QTableWidgetItem('Count'))
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._dirs_list = []
        self._dirs_counts = []
        self._all_photos = []
        self.create_ui_connections()

    def create_ui_connections(self):
        self.itemSelectionChanged.connect(self._selection_changed)

    @Slot()
    def _selection_changed(self):
        self.signal_selected_dirs_changed.emit()

    def get_photos_in_selected_dirs(self):
        photos_in_selected_dirs = []
        selection_model = self.selectionModel()
        if not selection_model.hasSelection():
            return photos_in_selected_dirs
        required_dirs = []
        required_elements = 0
        for row in selection_model.selectedRows():
            if row.row() >= len(self._dirs_list):
                continue
            dir_name = self._dirs_list[row.row()]
            photos_count_in_dir = self._dirs_counts[row.row()]
            required_elements += photos_count_in_dir
            required_dirs.append(dir_name)

        photos_in_selected_dirs = [None] * required_elements
        i = 0
        for photo in self._all_photos:
            if photo.get_dir_name() in required_dirs:
                assert i < required_elements
                photos_in_selected_dirs[i] = photo
                i += 1
        return photos_in_selected_dirs

    def display_dirs_for_photos(self, all_photos: List[Photo]):
        self._dirs_list.clear()
        self._dirs_counts.clear()
        all_photos_ordered = filtering.sort_items_by_time(all_photos)
        self._all_photos = all_photos_ordered
        dirs_dict = collections.Counter([photo.get_dir_name() for photo in all_photos_ordered])
        self.setRowCount(len(dirs_dict))
        i = 0
        for dir_name, count in dirs_dict.items():
            self.setItem(i, 0, QTableWidgetItem(dir_name))
            self.setItem(i, 1, QTableWidgetItem(str(count)))
            self._dirs_list.append(dir_name)
            self._dirs_counts.append(count)
            i += 1
        self.resizeColumnsToContents()


class PhotosTableWidget(QTableWidget):
    signal_selected_photos_changed = Signal()

    ROW_WITH_PHOTO_ITEM = 1
    COLUMN_COUNT = 3

    def __init__(self, parent):
        QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setColumnCount(3)
        self.setRowCount(0)
        self.setMinimumWidth(300)
        self.horizontalHeader().setStretchLastSection(True)
        self.setSortingEnabled(True)

        self.setHorizontalHeaderItem(0, QTableWidgetItem('Directory'))
        self.setHorizontalHeaderItem(1, QTableWidgetItem('File'))
        self.setHorizontalHeaderItem(2, QTableWidgetItem('Date'))

        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self._all_photos = []
        self.create_ui_connections()

    def create_ui_connections(self):
        self.itemSelectionChanged.connect(self._selection_changed)

    @Slot()
    def _selection_changed(self):
        self.signal_selected_photos_changed.emit()

    def get_selected_photos(self):
        selected_photos = []
        selection_model = self.selectionModel()
        if not selection_model.hasSelection():
            return selected_photos
        if len(selection_model.selectedRows()) == len(self._all_photos):
            return self._all_photos

        for row in selection_model.selectedRows():
            row_number = row.row()
            photo = self.item(row_number, self.ROW_WITH_PHOTO_ITEM).data(Qt.UserRole)
            selected_photos.append(photo)
        return selected_photos

    def display_photos(self, all_photos: List[Photo]):
        if all_photos == self.item:
            return

        self.blockSignals(True)
        old_selection = self.get_selected_photos()
        self.clearSelection()
        self._all_photos = all_photos
        self.setRowCount(len(all_photos))
        for i, photo in enumerate(all_photos):
            self.setItem(i, 0, QTableWidgetItem(photo.get_dir_name()))

            file_name_item = QTableWidgetItem(photo.get_file_name())
            file_name_item.setData(Qt.UserRole, photo)
            self.setItem(i, self.ROW_WITH_PHOTO_ITEM, file_name_item)

            time_str = photo.photo_time.to_chronological_date_time_string() if photo.photo_time else '?'
            self.setItem(i, 2, QTableWidgetItem(time_str))
            assert self.COLUMN_COUNT == 3
        self.resizeColumnsToContents()

        self.select_photos(old_selection)
        self.blockSignals(False)
        self.signal_selected_photos_changed.emit()

    def select_photos(self, photos_to_select):
        self.blockSignals(True)
        selection_model = self.selectionModel()
        selection_model.clear()
        model = self.model()
        for i in range(model.rowCount()):
            photo = model.index(i, self.ROW_WITH_PHOTO_ITEM).data(Qt.UserRole)
            if photo in photos_to_select:
                item_selection = QItemSelection(
                    self.model().index(i, 0),
                    self.model().index(i, self.COLUMN_COUNT-1))
                selection_model.select(item_selection, QItemSelectionModel.Select)
        self.blockSignals(False)
        self._selection_changed()
