import copy
import logging
import os
from functools import partial
from typing import Union, List

from PySide2.QtCore import Slot
from PySide2.QtWidgets import QMessageBox, QPushButton

from common import settings
from dialogs.blocking_operation_message import BlockingOperationWithMessagebox
from items.attachment.photo import Photo
from items_definitions.item_fields import ItemFieldStr, ItemFieldDateTime, ItemFieldAttachmentParent, \
    ItemFieldInt
from items_utils import annotate_photos
from tabs.common_ui.display_image.display_image_widget import DisplayMultipleImagesWidget
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from utils.dummy_engine import get_dummy_engine
from utils.gui import set_splitter_division


class MultiplePhotosDisplayWidget(ItemDisplayWidget):
    """ TODO - do not inherit item display widget! Use common interface instead! """

    def __init__(self, parent, item_class, is_editable: bool, clickable_links: bool = False):
        self._fields_uis_mapped = {}  # will be replaced in _generate_field_uis called in parent class
        self._display_multiple_images_widget = DisplayMultipleImagesWidget(parent=parent)
        self.pushButton_annotate = QPushButton(parent)  # TODO this shouldn't be created here, refactor after applying common interface as described in class docstring
        self._allow_to_reload_displayed_items = True  # hacky flag, to get rid of recursive updating
        ItemDisplayWidget.__init__(self, parent=parent, item_class=item_class, is_editable=is_editable, clickable_links=clickable_links)

        self.pushButton_annotate.setText('&Annotate')
        self.ui.horizontalLayout_buttons.addWidget(self.pushButton_annotate)
        self.pushButton_annotate.clicked.connect(self._annotate_selected_items)
        self._pending_annotations = {}  # pending annotations to be set for selected items after clicking 'update'

        # to conform with base class interface and methods, instead of using self._items, self._item will be used
        # self._items = None
        self._item = None  # type: List[Photo]  # or None

        self.ui.widget_additiona_display.setVisible(True)

        self._single_photo_display_widget = ItemDisplayWidget(parent=self,
                                                              item_class=Photo,
                                                              clickable_links=True,
                                                              is_editable=False)
        self._single_photo_display_widget.signal_link_clicked.connect(self.signal_link_clicked)
        self.ui.gridLayout_spare_display.addWidget(self._single_photo_display_widget)

        self.ui.pushButton_add_as_new.setVisible(False)
        self.ui.pushButton_clear.setVisible(False)
        self._fields_uis_mapped['attached_to_single'].signal_field_modified.connect(self._single_parent_item_modified)
        self.ui.verticalLayout_additional_display.addWidget(self._display_multiple_images_widget)
        set_splitter_division(self.ui.splitter_display_and_addtional_display, 25)

        self._display_multiple_images_widget.signal_link_clicked.connect(self.signal_link_clicked)
        self._display_multiple_images_widget.signal_item_deselected.connect(self._item_deselected)

    def _generate_field_uis(self):
        # after modyfing list, modifiy also self._generate_field_uis_mapping()
        all_fields_definitions = [
            # ItemFieldMultiplePhotos(field_name='thumbnail', editable_in_ui=False),
            ItemFieldInt(field_name='number_of_photos', editable_in_ui=False),
            ItemFieldStr(field_name='original dir(s)', editable_in_ui=False),
            ItemFieldDateTime(field_name='youngest_time', editable_in_ui=False),
            ItemFieldDateTime(field_name='eldest_time', editable_in_ui=False),
            ItemFieldStr(field_name='camera_model(s)', editable_in_ui=False),
            ItemFieldAttachmentParent(field_name='attached_to'),
            ItemFieldStr(field_name='attached_to_items', editable_in_ui=False),
        ]
        field_uis = self._generate_field_uis_for_all_fields_definitions(all_fields_definitions)
        self._fields_uis_mapped = self._generate_field_uis_mapping(field_uis)
        return field_uis

    def _generate_field_uis_mapping(self, field_uis):
        fields_uis_mapped = {
            'number_of_photos': field_uis[0],
            'dir': field_uis[1],
            'youngest_time': field_uis[2],
            'eldest_time': field_uis[3],
            'camera': field_uis[4],
            'attached_to_single': field_uis[5],
            'attached_to_multiple': field_uis[6],
        }
        return fields_uis_mapped

    def _item_modified(self):
        self._is_modified = True
        self._set_modify_buttons_states()
        logging.debug(f'{self._item_class.__name__} modified')

    def display_item(self, item: Union[List[Photo], None]):
        if not self._allow_to_reload_displayed_items:
            return

        if self._is_modified:
            reply = QMessageBox.question(self, "Confirm",
                                         'Are you sure you want to display new item(s)? Current changes will be discarded!')
            if reply != QMessageBox.StandardButton.Yes:
                self._allow_to_reload_displayed_items = False
                self.signal_displayed_item_changed.emit(self._item)  # to restore current item (displaying canceled)
                self._allow_to_reload_displayed_items = True
                return

        self._item = item
        self._pending_annotations = []
        self._is_modified = False

        self._display_photos_details(item if item is not None else [])
        # for field_ui in self._field_uis:
        #     value = getattr(item, field_ui.field_definition.field_name) if item else None
        #     field_ui.set_value(value=value, whole_item=item)
        self._set_modify_buttons_states()

    def _reload_after_engine_changed(self):
        super(MultiplePhotosDisplayWidget, self)._reload_after_engine_changed()
        self._single_photo_display_widget.set_engine(self.engine)

    def _display_single_photo_edit(self, photo: Photo):
        self._single_photo_display_widget.display_item(photo)

    def _display_multiple_photo_edit(self, photos: List[Photo]):
        dirs = set()
        cameras = set()
        parent_items = set()

        eldest_time = None
        youngest_time = None
        for photo in photos:
            # assign any value to photo times
            if photo.photo_time is not None:
                eldest_time = photo.photo_time
                youngest_time = photo.photo_time
                break

        for photo in photos:
            dirs.add(photo.get_dir_name())
            cameras.add(photo.camera_model)
            parent_items.add(photo.attached_to)
            if photo.photo_time is not None:
                youngest_time = min(youngest_time, photo.photo_time)
                eldest_time = max(eldest_time, photo.photo_time)

        if None in cameras:
            cameras.remove(None)
            cameras.add('Unknown')

        parent_items_str = []
        for parent_item in parent_items:
            if parent_item is not None:
                parent_items_str.append(parent_item.get_short_description())
            else:
                parent_items_str.append('Unknown')

        ENTRIES_LIMIT = 5

        def join_strings(entries) -> str:
            txt = ', '.join(list(entries)[:ENTRIES_LIMIT])
            if len(entries) > ENTRIES_LIMIT:
                txt += ', ...'
            return txt

        dirs_txt = join_strings(dirs)
        cameras_txt = join_strings(cameras)
        parent_items_txt = join_strings(parent_items_str)

        self._fields_uis_mapped['dir'].set_value(dirs_txt)
        self._fields_uis_mapped['camera'].set_value(cameras_txt)
        self._fields_uis_mapped['youngest_time'].set_value(youngest_time)
        self._fields_uis_mapped['eldest_time'].set_value(eldest_time)
        self._fields_uis_mapped['number_of_photos'].set_value(len(photos))

        if len(parent_items) <= 1:
            self._fields_uis_mapped['attached_to_single'].set_value(
                list(parent_items)[0] if len(parent_items) == 1 else None)
            self._fields_uis_mapped['attached_to_multiple'].setVisible(False)
        else:
            self._fields_uis_mapped['attached_to_single'].set_value(None)
            self._fields_uis_mapped['attached_to_multiple'].setVisible(True)
            self._fields_uis_mapped['attached_to_multiple'].set_value(parent_items_txt)

    def _display_photos_details(self, photos: List[Photo]):
        self._display_multiple_images_widget.display_multiple_images(photos)

        if len(photos) == 1:
            self._display_single_photo_edit(photos[0])
            self.ui.widget_base_display.setVisible(False)
            self.ui.widget_spare_display.setVisible(True)
            self.ui.widget_modify_buttons.setVisible(False)
        else:
            self.pushButton_annotate.setEnabled(bool(len(photos) >= 2))
            self._display_multiple_photo_edit(photos)
            self.ui.widget_base_display.setVisible(True)
            self.ui.widget_spare_display.setVisible(False)
            self.ui.widget_modify_buttons.setVisible(True)

    @Slot()
    def _annotate_selected_items(self):
        self._pending_annotations = {}
        photos_to_annotate = []
        for photo in self._item:
            absolute_path = photo.get_absolute_path_from_relative()
            if not os.path.isfile(absolute_path):
                logging.warning(f'Cannot annotate photo with path "{absolute_path}" - file does not exist!')
                continue

            if photo.annotation is None or settings.s.force_annotating_already_annotated_photos:
                photos_to_annotate.append(photo)
            else:
                logging.debug(f'Photo with path "{absolute_path}" already annotated')

        self._pending_annotations = annotate_photos.get_photos_annotations_dict_with_progress_dialog(photos_to_annotate, parent=self)

        not_annotated_photos_number = len(self._item) - len(self._pending_annotations)
        msg = f'Annotation for {len(self._pending_annotations)} photos received.'
        if not_annotated_photos_number:
            msg += f'\nBut {not_annotated_photos_number} photos could not be annotated or already annotated!'
        msg += '\nRemember to click "update" to save the results!'
        if len(self._pending_annotations) > 0:
            self._item_modified()
        QMessageBox.information(self, "Info!", msg)

    def _single_parent_item_modified(self):
        self._fields_uis_mapped['attached_to_multiple'].setVisible(False)

    def _set_modify_buttons_states(self):
        is_there_something_to_update = True if self._item else False
        self.ui.pushButton_delete.setEnabled(is_there_something_to_update)
        self.ui.pushButton_update.setEnabled(is_there_something_to_update and self._is_modified)

    def _clear(self):
        raise NotImplementedError('Multiple photos display does not allow for "Clear" button!')

    def _delete_item(self):
        for photo in self._item:
            self.engine.collection.delete_item(photo)
        self.engine.set_engine_as_modified()

    def _update_item__internal(self, attached_to_single_value):
        for item in self._item:
            assert item is not None and item.item_id
            if attached_to_single_value and item.attached_to != attached_to_single_value:
                item.attached_to = attached_to_single_value
                logging.debug(f'setting "attached_to"')
            if item in self._pending_annotations:
                item.annotation = self._pending_annotations[item]
                logging.debug(f'setting annotation"')
            logging.debug(f'Item {item.get_short_description()} updated.')
        self._pending_annotations = {}
        self._is_modified = False

    def _update_item(self):
        bowm = BlockingOperationWithMessagebox(parent=self)
        bowm.perform_operation(message='Updating photos...',
                               operation=partial(
                                   self._update_item__internal,
                                   attached_to_single_value=self._fields_uis_mapped['attached_to_single'].get_value()))
        self.signal_displayed_item_changed.emit(self._item)
        self.engine.set_engine_as_modified()

    def _add_as_new(self):
        raise NotImplementedError('Multiple photos display does not allow for "Add as new" button!')

    def _item_deselected(self, photo):
        remaining_selected_photos = copy.copy(self._item)
        remaining_selected_photos.remove(photo)
        self.signal_displayed_item_changed.emit(remaining_selected_photos)


def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    from common import settings
    settings.DISPLAY_QMESSAGE_AFTER_UNHANDLED_EXCEPTION = False
    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    engine = get_dummy_engine()
    w = MultiplePhotosDisplayWidget(parent=None, item_class=Photo, is_editable=True)
    w.show()
    w.set_engine(engine)
    w.display_item(engine.collection.photos)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
