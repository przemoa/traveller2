from PySide2 import QtWidgets
from PySide2.QtCore import Signal

from engine.engine import Engine


class TabWidget(QtWidgets.QWidget):
    signal_link_clicked = Signal(object)

    def __init__(self, parent, tab_id: str):
        QtWidgets.QWidget.__init__(self, parent)
        self.tab_id = tab_id
        self.engine = Engine()  # type: Engine  # empty engine at start
        self._last_engine_modification_time = 0

    def set_engine(self, engine):
        """ Load (for the first time) or reload (new one) engine

        Called at startup or if traveller2 database or prompts database changed
        """
        self.engine = engine
        if self._last_engine_modification_time != engine.last_engine_modification_time:
            self._last_engine_modification_time = engine.last_engine_modification_time
            self._reload_after_engine_changed()

    def _reload_after_engine_changed(self):
        pass

    def tear_down(self):
        pass


class InvalidTabWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent, tab_id)
        self.generate_ui()

    def generate_ui(self):
        layout = QtWidgets.QVBoxLayout(self)
        label = QtWidgets.QLabel(self)
        label.setText('ERROR! Failed to setup "{}" tab!'.format(self.tab_id))
        layout.addWidget(label)

    def _reload_after_engine_changed(self):
        pass
