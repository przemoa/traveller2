import enum
import logging
import random
from typing import Union

from PySide2.QtCore import Slot, QEvent, Qt
from PySide2.QtGui import QGuiApplication

from items.attachment.photo import Photo
from items.item import Item
from items.journey import Journey
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from tabs.common_ui.items_list_display.items_list_display_widget import ItemsListDisplayWidget
from tabs.edit.item_edit_widget import ItemEditWidget
from tabs.recent.recent_items_widget import RecentItemsWidget
from tabs.tab_widget import TabWidget
from utils.dummy_engine import get_dummy_engine
from utils.gui import set_splitter_division


class SpecialTabItem(enum.Enum):
    RECENT_ITEMS = enum.auto()
    PHOTOS = enum.auto()
    JOURNEYS = enum.auto()


class ItemBrowserWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        super().__init__(parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()
        set_splitter_division(self.ui.splitter_navigation_view, 1)
        set_splitter_division(self.ui.splitter_details_belongings, 70)

        self._history = []  # history of displayed items. None is 'Recent items' tab
        self._current_history_index = -1

        self._special_widgets = {
            SpecialTabItem.RECENT_ITEMS: RecentItemsWidget(parent=self, tab_id=None),
            SpecialTabItem.PHOTOS: ItemEditWidget(parent=self, item_class=Photo, tab_id=None, use_special_filter_and_display_widgets=True),
            SpecialTabItem.JOURNEYS: ItemEditWidget(parent=self, item_class=Journey, tab_id=None),
        }
        for special_widget in self._special_widgets.values():
            special_widget.setVisible(False)
            self.ui.horizontalLayout_browsed_widget.addWidget(special_widget)

        self._current_widget = self._special_widgets[SpecialTabItem.PHOTOS]
        self.show_item(SpecialTabItem.PHOTOS)

        self._display_widgets = dict()
        self._associated_items_widget = ItemsListDisplayWidget(parent=self)
        self.ui.horizontalLayout_for_item_belongings.addWidget(self._associated_items_widget)

        self.installEventFilter(self)
        self.create_ui_connections()

    def generate_ui(self):
        from tabs.browser.moc_item_browser_widget import Ui_item_browser_widget
        ui = Ui_item_browser_widget()
        ui.setupUi(self)
        return ui

    def _reload_after_engine_changed(self):
        self._current_widget.set_engine(self.engine)

    def create_ui_connections(self):
        self.ui.pushButton_random_item.clicked.connect(self._show_random_item)
        self.ui.pushButton_recent_items.clicked.connect(self._show_recent_items_clicked)
        self.ui.pushButton_photos.clicked.connect(self._show_photos_items_clicked)
        self.ui.pushButton_journeys.clicked.connect(self._show_journeys_items_clicked)
        self.ui.pushButton_back.clicked.connect(self._back_to_previous_item_in_history)
        self.ui.pushButton_forward.clicked.connect(self._forward_to_next_item_in_history)

        self._associated_items_widget.signal_link_clicked.connect(self.link_clicked)
        for special_widget in self._special_widgets.values():
            special_widget.signal_link_clicked.connect(self.link_clicked)

    @Slot()
    def _show_random_item(self):
        random_item = random.choice(list(self.engine.collection.all_items_by_id.values()))
        self.show_item(random_item)

    @Slot()
    def _show_recent_items_clicked(self):
        self.show_item(SpecialTabItem.RECENT_ITEMS)

    @Slot()
    def _show_photos_items_clicked(self):
        self.show_item(SpecialTabItem.PHOTOS)

    @Slot()
    def _show_journeys_items_clicked(self):
        self.show_item(SpecialTabItem.JOURNEYS)

    def _get_item_or_special_widget_description(self, item: Union[Item, SpecialTabItem]):
        if isinstance(item, SpecialTabItem):
            return item.name
        else:
            return f'{item.__class__.__name__}: {item.get_short_description()}'

    def _add_as_new_to_history(self, item):
        self._history = self._history[:self._current_history_index+1]
        self._history.append(item)
        self._current_history_index += 1

    def _back_to_previous_item_in_history(self):
        assert self._current_history_index > 0
        self._current_history_index -= 1
        self._display_item(self._history[self._current_history_index])
        self._set_back_forward_buttons_state()

    def _forward_to_next_item_in_history(self):
        assert (self._current_history_index + 1) < len(self._history)
        self._current_history_index += 1
        self._display_item(self._history[self._current_history_index])
        self._set_back_forward_buttons_state()

    def show_item(self, item):
        self._add_as_new_to_history(item)
        self._display_item(item)
        self._set_back_forward_buttons_state()

    def _show_special_item(self, special_item):
        self._current_widget = self._special_widgets[special_item]
        self._current_widget.setVisible(True)
        self._current_widget.set_engine(self.engine)
        self.ui.widget_for_item_belongings.setVisible(False)

        # if special_item == SpecialTabItem.RECENTLY_ADDED:
        #     self._show_recent_items()
        # elif special_item == SpecialTabItem.PHOTOS:
        #     self._show_photos_items()
        # else:
        #     raise NotImplementedError()

    def _display_item(self, item: Item):
        logging.debug(f'Displaying item {item}')
        self.ui.label_currently_browsed.setText(self._get_item_or_special_widget_description(item))
        self._current_widget.setVisible(False)
        if isinstance(item, SpecialTabItem):
            self._show_special_item(item)
            return

        display_widget = self._display_widgets.get(item.__class__, None)
        if display_widget is None:
            display_widget = ItemDisplayWidget(parent=self,
                                               item_class=item.__class__,
                                               is_editable=False,
                                               clickable_links=True)
            self._display_widgets[item.__class__] = display_widget
            display_widget.signal_link_clicked.connect(self.link_clicked)
            self.ui.horizontalLayout_browsed_widget.addWidget(display_widget)

        display_widget.display_item(item)
        display_widget.set_engine(self.engine)
        self._associated_items_widget.set_belongings_of_item(item)
        self.ui.widget_for_item_belongings.setVisible(True)

        self._current_widget = display_widget
        self._current_widget.setVisible(True)

    def _set_back_forward_buttons_state(self):
        self.ui.pushButton_back.setEnabled(self._current_history_index > 0)
        self.ui.pushButton_forward.setEnabled((self._current_history_index + 1) < len(self._history))

        MAX_NUMBER_OF_HISTORY_ON_TOOLTIP = 20

        back_history_items = self._history[-MAX_NUMBER_OF_HISTORY_ON_TOOLTIP-1:self._current_history_index]
        back_tooltip_str = '\n'.join([self._get_item_or_special_widget_description(item) for item in back_history_items])
        self.ui.pushButton_back.setToolTip(back_tooltip_str)

        forward_history_items = self._history[self._current_history_index+1:self._current_history_index+1+MAX_NUMBER_OF_HISTORY_ON_TOOLTIP]
        forward_tooltip_str = '\n'.join([self._get_item_or_special_widget_description(item) for item in forward_history_items])
        self.ui.pushButton_forward.setToolTip(forward_tooltip_str)

    @Slot()
    def link_clicked(self, clicked_value):

        if clicked_value is None:
            logging.info('None object requested to open in browser. Ignoring.')
        elif isinstance(clicked_value, Item):
            logging.info(f'Link clicked, signal with value "{clicked_value.get_short_description()}" received.')
            self.show_item(clicked_value)
        elif isinstance(clicked_value, int):
            logging.info(f'Link clicked, signal with value "{clicked_value}" received.')
            if clicked_value > 0:
                self.show_item(self.engine.collection.all_items_by_id[clicked_value])
        elif isinstance(clicked_value, str):
            logging.info(f'Link clicked, signal with value "{clicked_value}" received.')
            item_id = int(clicked_value)
            if item_id > 0:
                self.show_item(self.engine.collection.all_items_by_id[item_id])
        else:
            raise Exception(f'Unhandled value type "{type(clicked_value)}" in clicked link.')
        self.raise_()

    def eventFilter(self, object, event):
        if event.KeyPress:
            if event.type() == QEvent.ShortcutOverride:
                if bool(QGuiApplication.queryKeyboardModifiers() & Qt.AltModifier):
                    if event.key() == Qt.Key_Left:
                        if self.ui.pushButton_back.isEnabled():
                            self.ui.pushButton_back.click()
                        return True
                    if event.key() == Qt.Key_Right:
                        if self.ui.pushButton_forward.isEnabled():
                            self.ui.pushButton_forward.click()
                        return True
        return False
    # def keyPressEvent(self, f_event):
    #     if bool(f_event.modifiers() & Qt.AltModifier):
    #         print('key_press_alt')
    #         print(f_event.key())
    #         if f_event.key() == Qt.Key_Left:
    #             print('hello')


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    # w = ItemEditWidget(None, get_dummy_engine(), Journey)
    w = ItemBrowserWidget(parent=None, tab_id='x')
    w.show()
    w.set_engine(get_dummy_engine())
    sys.exit(app.exec_())
