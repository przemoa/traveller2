import logging
import logging.handlers

from PySide2.QtCore import Slot, Signal
from PySide2.QtGui import QFont
from PySide2.QtWidgets import QTextBrowser, QVBoxLayout

from common import settings
from tabs.tab_widget import TabWidget


class GuiLogHandler(logging.StreamHandler):
    """ Have to be separate from LogWidget, because both QObject and StreamHandler implement emit method..."""
    LEVEL_TO_COLOR = {
        logging.DEBUG: 'Blue',
        logging.INFO: 'Black',
        logging.WARNING: 'Orange',
        logging.ERROR: 'OrangeRed',
        logging.CRITICAL: 'Red',
    }

    def __init__(self, write_function):
        super().__init__()
        self._write_function = write_function

    def emit(self, record):
        msg = self.format(record).replace('\n', '<br>')
        color = self.LEVEL_TO_COLOR.get(record.levelno, 'Black')
        formatted_msg = f'<font color="{color}">{msg}</font><'
        if record.levelno in [logging.ERROR, logging.CRITICAL]:
            formatted_msg = '<b>' + formatted_msg + '</b>'
        self._write_function(formatted_msg)


class LogWidget(TabWidget):
    signal_append_to_text_browser = Signal(str)
    signal_trim_text_in_text_browser = Signal()
    PERCENTAGE_OF_LOG_ENTRIES_TO_REMOVE_DURING_TRIMMING = 20
    MAX_NUMBER_OF_LOG_ENTRIES_IN_GUI_CONSOLE = 10000

    def __init__(self, parent, tab_id: str):
        super().__init__(parent=parent, tab_id=tab_id)
        self._create_ui()
        self._log_level = settings.s.console_log_level

        self._approximate_number_of_log_entries = 0
        self.signal_append_to_text_browser.connect(self._text_browser.append)
        self.signal_trim_text_in_text_browser.connect(self._trim_text_in_text_browser)

        self._log_handler = self._setup_logger()
        self._print_startup_info()

    def tear_down(self):
        logging.getLogger().removeHandler(self._log_handler)

    def _create_ui(self):
        self._verticalLayout = QVBoxLayout(self)
        # self._pushButton_clear_logs = QPushButton(self)
        # self._pushButton_clear_logs.setText('C&lear Logs')
        # self._pushButton_clear_logs.setMaximumSize(QtCore.QSize(100, 16777215))
        # self._pushButton_clear_logs.clicked.connect(self._clear_logs)
        # self._verticalLayout.addWidget(self._pushButton_clear_logs)
        self._text_browser = QTextBrowser(self)
        self._verticalLayout.addWidget(self._text_browser)
        self._font = QFont('Monospace')
        self._font.setPointSize(8)
        self._text_browser.setFont(self._font)

    def _setup_logger(self):
        log_handler = GuiLogHandler(self.write_function)
        log_formatter = logging.Formatter(
            "&nbsp;%(asctime)s.%(msecs)03d:  %(message)s", datefmt="%H:%M:%S")
        log_handler.setFormatter(log_formatter)

        log_handler.setLevel(self._log_level)
        logging.getLogger().addHandler(log_handler)
        return log_handler

    def _print_startup_info(self):
        self._text_browser.append('Logging level: {} ({})'.format(
            logging.getLevelName(self._log_level), self._log_level))

        log_colors_txt = 'Levels: '
        for level, color in self._log_handler.LEVEL_TO_COLOR.items():
            log_colors_txt += f'<b><font color="{color}">{logging.getLevelName(level)}</font></b>  '
        self._text_browser.append(log_colors_txt)
        self._text_browser.append('')

    # def _clear_logs(self):
    #     self._text_browser.clear()
    #     self._print_startup_info()

    def write_function(self, formatted_msg):
        self._approximate_number_of_log_entries += 1
        if self._approximate_number_of_log_entries > self.MAX_NUMBER_OF_LOG_ENTRIES_IN_GUI_CONSOLE:
            self._approximate_number_of_log_entries = int(
                self._approximate_number_of_log_entries * (1 - self.PERCENTAGE_OF_LOG_ENTRIES_TO_REMOVE_DURING_TRIMMING/100))
            self.signal_trim_text_in_text_browser.emit()
        self.signal_append_to_text_browser.emit(formatted_msg)

    @Slot()
    def _trim_text_in_text_browser(self):
        text = self._text_browser.toPlainText()
        index = int(len(text) * (self.PERCENTAGE_OF_LOG_ENTRIES_TO_REMOVE_DURING_TRIMMING/100))
        assert index < len(text) and index > 0
        self._text_browser.setText(text[index:])
        scroll_bar = self._text_browser.verticalScrollBar()
        scroll_bar.setValue(scroll_bar.maximum())
