import collections
from functools import partial

from items.airport import Airport
from items.attachment.file import File
from items.attachment.photo import Photo
from items.companion import Companion
from items.flight import Flight
from items.journey import Journey
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items.location.place import Place
from items.person import Person
from items.visit import Visit
from tabs.browser.item_browser_widget import ItemBrowserWidget
from tabs.edit.item_edit_widget import ItemEditWidget
from tabs.general_info.general_info_widget import GeneralInfoWidget
from tabs.log.log_widget import LogWidget
from tabs.map.map_widget import MapWidget
from tabs.timeline.timeline_widget import TimelineWidget


class TabId:
    GENERAL_INFO = 'General Info'
    LOG = 'Log'

    JOURNEYS = 'Journeys'
    VISITS = 'Visits'
    PLACES = 'Places'
    SINGLE_PHOTO = 'Photo'
    PHOTOS = 'Photos'
    FILES = 'Files'
    COUNTRIES = 'Countries'
    REGIONS_MAJOR = 'Regions Major'
    REGIONS_MINOR = 'Regions Minor'
    CITIES = 'Cities'
    AIRPORTS = 'Airports'
    FLIGHTS = 'Flights'
    PERSONS = 'Persons'
    COMPANIONS = 'Companions'

    MAP = 'Map'
    TIMELINE = 'Timeline'
    RECENT = 'Recent'
    BROWSER = 'Browser'


ALL_TABS_LIST = collections.OrderedDict(
    [
        (TabId.GENERAL_INFO, GeneralInfoWidget),
        (TabId.LOG, LogWidget),

        (TabId.JOURNEYS, partial(ItemEditWidget, item_class=Journey)),
        (TabId.VISITS, partial(ItemEditWidget, item_class=Visit)),
        (TabId.PLACES, partial(ItemEditWidget, item_class=Place)),
        (TabId.COUNTRIES, partial(ItemEditWidget, item_class=Country)),
        (TabId.REGIONS_MAJOR, partial(ItemEditWidget, item_class=RegionMajor)),
        (TabId.REGIONS_MINOR, partial(ItemEditWidget, item_class=RegionMinor)),
        (TabId.CITIES, partial(ItemEditWidget, item_class=City)),
        (TabId.SINGLE_PHOTO, partial(ItemEditWidget, item_class=Photo)),
        (TabId.PHOTOS, partial(ItemEditWidget, item_class=Photo, use_special_filter_and_display_widgets=True)),
        (TabId.FILES, partial(ItemEditWidget, item_class=File)),
        (TabId.AIRPORTS, partial(ItemEditWidget, item_class=Airport)),
        (TabId.FLIGHTS, partial(ItemEditWidget, item_class=Flight)),
        (TabId.PERSONS, partial(ItemEditWidget, item_class=Person)),
        (TabId.COMPANIONS, partial(ItemEditWidget, item_class=Companion)),

        (TabId.MAP, MapWidget),
        (TabId.TIMELINE, TimelineWidget),
        (TabId.BROWSER, ItemBrowserWidget),
    ]
)

