from items_utils.filtering import get_recently_added_items, get_recently_modified_items
from tabs.common_ui.items_list_display.items_list_display_widget import ItemsListDisplayWidget
from tabs.tab_widget import TabWidget
from utils.dummy_engine import get_dummy_engine
from utils.gui import set_splitter_division


class RecentItemsWidget(TabWidget):
    def __init__(self, parent, tab_id: str):
        super().__init__(parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()
        set_splitter_division(self.ui.splitter, 50)

        self._recently_added_items_list_widget = ItemsListDisplayWidget(parent=self,
                                                                        description_text='Recently added items -')
        self._recently_modified_items_list_widget = ItemsListDisplayWidget(parent=self,
                                                                           description_text='Recently modified items -')

        self.ui.verticalLayout_recently_added.addWidget(self._recently_added_items_list_widget)
        self.ui.verticalLayout_recently_modified.addWidget(self._recently_modified_items_list_widget)
        self.create_ui_connections()

    def generate_ui(self):
        from tabs.recent.moc_recent_items_widget import Ui_recent_items_widget
        ui = Ui_recent_items_widget()
        ui.setupUi(self)
        return ui

    def _reload_after_engine_changed(self):
        recently_added_items = get_recently_added_items(collection=self.engine.collection)
        self._recently_added_items_list_widget.set_items(recently_added_items)

        recently_modified_items = get_recently_modified_items(collection=self.engine.collection)
        self._recently_modified_items_list_widget.set_items(recently_modified_items)

        # self._recently_added_items_list_widget.set_engine(self.engine)
        # self._recently_modified_items_list_widget.set_engine(self.engine)

    def create_ui_connections(self):
        self._recently_added_items_list_widget.signal_link_clicked.connect(self.signal_link_clicked)
        self._recently_modified_items_list_widget.signal_link_clicked.connect(self.signal_link_clicked)


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    # w = ItemEditWidget(None, get_dummy_engine(), Journey)
    w = RecentItemsWidget(parent=None, tab_id='x')
    w.show()
    w.set_engine(get_dummy_engine())
    sys.exit(app.exec_())
