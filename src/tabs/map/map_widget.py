# use google java script api
# with one free visit for api key
# generate map to file for selected config (show places, cites, images (with icon whole time or after hovering pin)
import logging
import os
import time
from functools import partial

from PySide2.QtCore import Slot, QUrl
from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import QMessageBox

from common import settings
from common.types.misc import PromptSourceType
from dialogs.blocking_operation_message import BlockingOperationWithMessagebox
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_definitions.item_fields import ItemFieldJourney, ItemFieldDateTime, ItemFieldAdminEntity
from items_utils import filtering
from items_utils.admin_entity_utils import get_city_prompts, get_region_minor_prompts, get_region_major_prompts
from items_utils.filtering import filter_items__no_filter, filter_items__for_journey, filter_items__for_time
from tabs.tab_widget import TabWidget
from utils.gui import set_splitter_division, set_editability_for_edit, add_widget_or_layout_to_ui
from utils.map_generator import map_generator


class MapWidget(TabWidget):

    def __init__(self, parent, tab_id: str):
        TabWidget.__init__(self, parent=parent, tab_id=tab_id)
        self.ui = self.generate_ui()

        self._web_view = QWebEngineView(self)
        self.ui.verticalLayout_fow_web_view.addWidget(self._web_view)
        self._web_view.show()
        self._web_view.setUrl('')
        set_splitter_division(self.ui.splitter, 5)
        set_editability_for_edit(self.ui.lineEdit_map_path, False)
        self.ui.lineEdit_map_path.setEnabled(False)
        self._filters = self._create_filters()
        self._selected_filter_changed()
        self.create_ui_connections()

    def generate_ui(self):
        from tabs.map.moc_map_widget import Ui_map_widget
        ui = Ui_map_widget()
        ui.setupUi(self)
        return ui

    def create_ui_connections(self):
        self.ui.pushButton_generate.clicked.connect(self._generate_and_show_map)
        self.ui.comboBox_filter_by.currentTextChanged.connect(self._selected_filter_changed)

    def _reload_after_engine_changed(self):
        if self.ui.checkBox_autorefresh_after_engine_change.isChecked():
            self._generate_and_show_map()  # may be time consuming
        else:
            pass  # Nothing to refresh
        for filter_name, filter_definition in self._filters.items():
            for ui_field in filter_definition['ui_fields']:
                ui_field.set_engine(self.engine)

    def _get_currently_selected_filter_function_for_own_items(self):
        filter_text = self.ui.comboBox_filter_by.currentText()
        for filter_name, filter_definition in self._filters.items():
            if filter_text == filter_name:
                return filter_definition['filter_function']

    def _get_filtered_admin_entites(self):
        filtered_admin_entities = []
        parent = self._filters['For parent']['ui_fields'][0].get_value()

        if self.ui.checkBox_cities.isChecked() and isinstance(parent, (Country, RegionMajor, RegionMinor)):
            filtered_admin_entities += get_city_prompts(engine=self.engine,
                                                        prompt_type=PromptSourceType.ALL_AVAILABLE,
                                                        parent=parent)
        if self.ui.checkBox_regions.isChecked() and isinstance(parent, (Country, RegionMajor)):
            filtered_admin_entities += get_region_minor_prompts(engine=self.engine,
                                                                prompt_type=PromptSourceType.ALL_AVAILABLE,
                                                                parent=parent)
        if self.ui.checkBox_regions.isChecked() and isinstance(parent, Country):
            filtered_admin_entities += get_region_major_prompts(engine=self.engine,
                                                                prompt_type=PromptSourceType.ALL_AVAILABLE,
                                                                parent=parent)
        MAX_NUMBER_OF_ADMIN_ENTITES_ON_MAP = 20000
        if len(filtered_admin_entities) > MAX_NUMBER_OF_ADMIN_ENTITES_ON_MAP:
            QMessageBox.warning(self, "Warning!", f'Too many admin entites selected to be displayed ({len(filtered_admin_entities)})!')
            # filtered_admin_entities = filtered_admin_entities[:MAX_NUMBER_OF_ADMIN_ENTITES_ON_MAP]
        return filtered_admin_entities

    @Slot()
    def _generate_and_show_map(self):
        bowm = BlockingOperationWithMessagebox(parent=self)
        bowm.perform_operation(message='Generating map...',
                               operation=partial(
                                   self._generate_map,
                                   own_items_filter_function=self._get_currently_selected_filter_function_for_own_items(),
                                   use_visits=self.ui.checkBox_visits.isChecked(),
                                   use_photos=self.ui.groupBox_photos.isChecked(),
                                   use_all_photos=self.ui.radioButton_photos_all.isChecked(),
                                   use_decimated_photos=self.ui.radioButton_photos_decimated.isChecked())
                               )
        absolute_html_file_path = bowm.result
        self._show_map(absolute_html_file_path=absolute_html_file_path)

    def _generate_map(self,
                      own_items_filter_function,
                      use_visits, use_photos,
                      use_all_photos,
                      use_decimated_photos):
        map_name = 'traveller2_map_' + str(int(time.time()))

        visits = []
        photos = []

        if use_visits:
            visits = self.engine.collection.visits
        if use_photos:
            all_photos = self.engine.collection.photos
            if use_decimated_photos:
                logging.info('About to decimate photos in space...')
                photos = filtering.decimate_photos_in_space(photos=all_photos, radius=settings.s.radius_of_photos_decimation_in_space_in_meters)
                logging.info(f'Photos decimated (in space) form {len(all_photos)} to {len(photos)}')
            elif use_all_photos:
                photos = all_photos
            else:
                raise NotImplementedError('Unknown photos filtering option')

        absolute_html_file_path = map_generator.generate_map_with_visits_and_photos(
            visits=own_items_filter_function(visits),
            images=own_items_filter_function(photos),
            admin_entities=self._get_filtered_admin_entites(),
            map_name=map_name)
        return absolute_html_file_path

    def _show_map(self, absolute_html_file_path):
        self.ui.lineEdit_map_path.setText('')

        self.ui.lineEdit_map_path.setEnabled(True)
        self.ui.lineEdit_map_path.setText(os.path.dirname(absolute_html_file_path))

        url = QUrl.fromLocalFile(absolute_html_file_path)
        self._web_view.setUrl(url)


    def _create_filters(self):
        filters = {
            '': {
                'allowed_for_normal_items': True,
                'field_definitions': [],
                'ui_fields': [],
                'filter_function': filter_items__no_filter,
                'layout': None,
                'widget': None,
            },
            'For journey':
                {
                    'allowed_for_normal_items': True,
                    'field_definitions': [ItemFieldJourney(field_name='belongs_to')],
                    'ui_fields': [],
                    'filter_function': self._filter_items__for_journey,
                    'layout': self.ui.gridLayout_filter_by_journey,
                    'widget': self.ui.widget_filter_by_journey,
                },
            'Between dates': {
                'allowed_for_normal_items': True,
                'field_definitions': [
                    ItemFieldDateTime(field_name='after'),
                    ItemFieldDateTime(field_name='before')
                ],
                'filter_function': self._filter_items__for_time,
                'ui_fields': [],
                'layout': self.ui.gridLayout_filter_by_date,
                'widget': self.ui.widget_filter_by_date,
            },
            'For parent': {
                'allowed_for_normal_items': False,
                'field_definitions': [
                    ItemFieldAdminEntity(field_name='parent'),
                ],
                'filter_function': None,
                'ui_fields': [],
                'layout': self.ui.gridLayout_filter_by_parent,
                'widget': None,
            }
        }
        for filter_name, filter_definition in filters.items():
            if filter_definition['allowed_for_normal_items']:
                self.ui.comboBox_filter_by.addItem(filter_name)

            for field_definition in filter_definition['field_definitions']:
                field_ui = field_definition.item_field_ui(
                    parent=self,
                    field_name=field_definition.field_name,
                    field_definition=field_definition,
                    engine=self.engine,
                    is_editable=True,
                )

                row = len(filter_definition['ui_fields'])

                add_widget_or_layout_to_ui(parent_layout=filter_definition['layout'],
                                           widget_or_layout=field_ui.left_widget,
                                           row=row,
                                           column=0)
                add_widget_or_layout_to_ui(parent_layout=filter_definition['layout'],
                                           widget_or_layout=field_ui.right_widget_layout,
                                           row=row,
                                           column=1)
                filter_definition['ui_fields'].append(field_ui)
        return filters

    def _filter_items__for_journey(self, items):
        selected_journey = self._filters['For journey']['ui_fields'][0].get_value()
        return filter_items__for_journey(items=items, journey=selected_journey)

    def _filter_items__for_time(self, items):
        ui_fields = self._filters['Between dates']['ui_fields']
        after_time = ui_fields[0].get_value()
        before_time = ui_fields[1].get_value()
        return filter_items__for_time(items=items, before_time=before_time, after_time=after_time)

    def _selected_filter_changed(self):
        filter_text = self.ui.comboBox_filter_by.currentText()
        for filter_name, filter_definition in self._filters.items():
            if not filter_definition['widget']:
                continue
            filter_definition['widget'].setVisible(filter_text == filter_name)

def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets
    from utils.dummy_engine import get_dummy_engine

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    w = MapWidget(None, None)
    w.show()
    w.set_engine(get_dummy_engine())
    w._generate_and_show_map()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
