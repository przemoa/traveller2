import collections
import glob
import logging
import os
import threading
import time
from functools import partial
from typing import Union, Callable
import datetime
import logging
import ntpath
import shutil

from PySide2 import QtWidgets, QtGui
from PySide2.QtCore import Qt, Slot, QTimer, QEvent, Signal
from PySide2.QtGui import QGuiApplication
from PySide2.QtWidgets import QMessageBox

from dialogs.about.about_dialog import AboutDialog
from dialogs.blocking_operation_message import BlockingOperationWithMessagebox
from dialogs.nonmodal_info.all_info_dialog_classes import ALL_INFO_DIALOG_CLASSES
from dialogs.select_item.select_item_dialog import select_item_from_dialog
from dialogs.settings.settings_dialog import SettingsDialog
from engine.engine_loader import EngineLoader
from items.location.admin_entity.admin_entity import AdminEntity
from items_utils.port_items import select_and_export_item, select_and_import_item
from main_window.docked_tab import DockedTab
from plugins import plugin_runner
from resources.img import img_paths
from tabs.tab_widget import InvalidTabWidget
from tabs.tabs_list import ALL_TABS_LIST, TabId
from engine.engine import Engine
from common import settings
from utils.gui import browse_open_traveller2_database_file, browse_save_traveller2_database_file


class MainWindow(QtWidgets.QMainWindow):
    signal_invoke_after_creation = Signal()

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        self.engine = Engine()  # type: Engine
        self._last_traveller_db_save_time = self.engine.last_engine_modification_time

        self.ui = self.generate_ui()
        self.setWindowIcon(QtGui.QIcon(img_paths.get_img_path(img_paths.traveller2)))

        # self._ask_before_close = not settings.IS_DEBUG
        self._ask_before_close = False
        self._docked_tabs = self._create_and_dock_all_docked_tabs()  # type: List[DockedTab]
        self._clip_all_docked_tabs()
        self._show_tabs(ALL_TABS_LIST)
        self._engine_loader = None  # type: EngineLoader

        self._restore_gui_state_and_geometry()

        self._recently_opened_files_actions = {}
        self._create_recently_opened_files_menu()
        self._plugins_menu_actions = self._create_plugins_menu_actions()

        self.create_ui_connections()

        self._last_tabs_reload_time = self.engine.last_engine_modification_time

        self.installEventFilter(self)

        self._reload_tabs_and_status_timer = self._create_reload_tabs_and_status_timer()
        self._reload_tabs_and_status_timer_timeout()

        self.signal_invoke_after_creation.connect(self._post_creation, Qt.QueuedConnection)  # to call from event loop, after all setup is done
        self.signal_invoke_after_creation.emit()

    def generate_ui(self):
        from main_window.moc_main_window import Ui_main_window
        ui = Ui_main_window()
        ui.setupUi(self)
        return ui

    def _post_creation(self):
        self.start_opening_databases(database_path=settings.s.last_database_path,
                                     prompts_database_path=settings.s.prompts_database_path)

    def create_ui_connections(self):
        # Help ->
        self.ui.actionAbout.triggered.connect(self._show_about)
        self.ui.actionAbout_Qt.triggered.connect(self._show_about_qt)

        # File ->
        self.ui.actionNew.triggered.connect(self._new_database)
        self.ui.actionOpen.triggered.connect(self._select_and_open_new_database)
        self.ui.actionSave.triggered.connect(self._save_database)
        self.ui.actionSave_as.triggered.connect(self._select_and_save_as_new_database)
        self.ui.actionSave_and_create_backup.triggered.connect(self._save_database_and_create_backup)
        self.ui.actionSettings.triggered.connect(self._show_settings)
        self.ui.actionRestore_default_settings.triggered.connect(self._restore_default_settings)
        self.ui.actionExit.triggered.connect(self._close_app)

        # View ->
        self.ui.actionRestore_clip_all_tabs.triggered.connect(self._clip_all_tabs)

        # Tools ->
        self.ui.actionReload_engine_in_all_tabs.triggered.connect(self._force_reload_engine)
        self.ui.actionManage_admin_entities.triggered.connect(self._copy_admin_entity_from_prompts_db)
        self.ui.action_Import_Journey_Visit.triggered.connect(self._import_item)
        self.ui.action_Export_Journey_Visit.triggered.connect(self._export_item)
        self.ui.actionBrowse_and_run_plugin.triggered.connect(self._browse_and_run_plugin)

        for docked_tab in self._docked_tabs.values():
            docked_tab.widget.signal_link_clicked.connect(self._link_clicked)

    @Slot(object)
    def _link_clicked(self, item):
        browser_tab = self._docked_tabs[TabId.BROWSER]
        browser_tab.widget.link_clicked(item)
        self._docked_tabs[TabId.BROWSER].raise_()

    # Create and setup tabs - START #
    def _create_and_dock_all_docked_tabs(self):
        docked_tabs = collections.OrderedDict()
        self.setDockNestingEnabled(True)
        for tab_id, tab_class in ALL_TABS_LIST.items():
            docked_tabs[tab_id] = self._create_docked_tab(tab_id=tab_id, tab_class=tab_class)
            if tab_id != TabId.LOG:
                self.addDockWidget(Qt.TopDockWidgetArea, docked_tabs[tab_id])
            else:
                self.addDockWidget(Qt.BottomDockWidgetArea, docked_tabs[tab_id])

        self.setTabPosition(Qt.TopDockWidgetArea, QtWidgets.QTabWidget.TabPosition.North)
        return docked_tabs

    def _set_engine(self, engine: Engine):
        self.engine = engine
        self._update_window_title()
        try:
            for docked_tab in self._docked_tabs.values():
                docked_tab.widget.set_engine(engine=engine)
        except:
            msg = 'Error during setting engine for tabs! Application will be closed!'
            logging.exception("msg")
            QMessageBox.critical(self, "Error!", msg)
            self._close_app(ask_before_close=False)

    def _update_window_title(self):
        title = 'Traveller2' + '  -  '
        if self.engine.collection_file_path:
            title += self.engine.collection_file_path
        else:
            title += 'new database...'

        if self.engine.last_engine_modification_time != self._last_traveller_db_save_time:
            title += ' *'
        self.setWindowTitle(title)

    def _clip_all_docked_tabs(self):
        first_tab = None
        for docked_tab in self._docked_tabs.values():
            if docked_tab.tab_id == TabId.LOG:
                continue

            if not first_tab:
                first_tab = docked_tab
                docked_tab.setFloating(False)
                continue
            self.tabifyDockWidget(first_tab, docked_tab)

    def _create_docked_tab(self, tab_id, tab_class):
        try:
            docked_tab = DockedTab(parent=self, tab_id=tab_id)
            tab_widget = tab_class(parent=docked_tab, tab_id=tab_id)
            docked_tab.set_widget(widget=tab_widget)
        except Exception:
            docked_tab = DockedTab(parent=self, tab_id=tab_id)
            logging.exception('Error during setup of tab "{}"'.format(tab_id))
            tab_widget = InvalidTabWidget(parent=docked_tab, tab_id=tab_id)
            docked_tab.set_widget(widget=tab_widget)
        self.ui.menuTabs.addAction(docked_tab.view_action)
        return docked_tab

    def _show_tabs(self, ids_to_show: Union[list, dict]):
        for tab_id in ALL_TABS_LIST:
            state = bool(tab_id in ids_to_show)
            self._docked_tabs[tab_id].view_action.setChecked(state)

    # Create and setup tabs - END #

    def _show_about(self):
        about_dialog = AboutDialog(parent=self)
        about_dialog.exec_()

    def _show_settings(self):
        settings_dialog = SettingsDialog()
        settings_dialog.exec_()

    def _restore_default_settings(self):
        reply = QMessageBox.question(self, "Confirm?", "Are you sure you want to reset settings?\n"
                                                       "Changes will be visible only after application restart.")
        if reply == QMessageBox.StandardButton.Yes:
            logging.info('Settings marked as "to be restarted" during saving.')
            settings.reset_before_saving = True

    def _show_about_qt(self):
        QMessageBox.aboutQt(self,"About Qt")

    def closeEvent(self, event):
        if not self._ask_if_user_sure_about_closing_current_database():
            event.ignore()
            return

        if self._ask_before_close:
            reply = QMessageBox.question(self, "Quit?", "Are you sure you want to quit?")
            if reply != QMessageBox.StandardButton.Yes:
                event.ignore()
                return
        settings.s.last_database_path = self.engine.collection_file_path

        self._save_gui_state_and_geometry()

        for docked_tab in self._docked_tabs.values():
            docked_tab.widget.tear_down()
        for info_dialog_class in ALL_INFO_DIALOG_CLASSES:
            info_dialog_class.close_dialogs()

        event.accept()

    @Slot()
    def _close_app(self, ask_before_close = True):
        self._ask_before_close = ask_before_close
        self.close()

    @Slot()
    def _copy_admin_entity_from_prompts_db(self):
        select_item_from_dialog(engine=self.engine, parent=self, allowed_item_types=[AdminEntity])

    def _import_item(self):
        select_and_import_item(parent=self, engine=self.engine)

    def _export_item(self):
        select_and_export_item(parent=self, engine=self.engine)

    def _browse_and_run_plugin(self):
        plugin_runner.run_plugin(parent=self, engine=self.engine)

    def _create_plugins_menu_actions(self):
        plugins_menu_actions = []
        file_paths = [f for f in glob.glob(plugin_runner.EXAMPLE_PLUGINS_DIR + "**/*.py",
                                      recursive=True)]
        for file_path in file_paths:
            plugin_base_name = os.path.splitext(os.path.basename(file_path))[0]
            plugin_pretty_name = plugin_base_name.replace('_', ' ').title()
            action = QtWidgets.QAction(self)
            action.setObjectName("action_plugin_" + plugin_base_name)
            action.setText(plugin_pretty_name)
            action.plugin_path = file_path
            action.triggered.connect(self._run_plugin_from_menu)
            plugins_menu_actions.append(action)
            self.ui.menuPlugins.addAction(action)
        return plugins_menu_actions

    # @Slot() - sender() returns None with this decorator ?!?
    def _run_plugin_from_menu(self):
        sender = self.sender()
        plugin_runner.run_plugin(parent=self, engine=self.engine, plugin_path=sender.plugin_path)

    @Slot()
    def _force_reload_engine(self):
        logging.info('Forced to reload engine in all tabs')
        self.engine.set_engine_as_modified()
        self._reload_engine_in_all_tabs()

    def _reload_engine_in_all_tabs(self):
        logging.info('Reloading engine in all tabs')
        self._last_tabs_reload_time = self.engine.last_engine_modification_time
        for docked_tab in self._docked_tabs.values():
            docked_tab.widget.set_engine(engine=self.engine)

    def _create_reload_tabs_and_status_timer(self):
        reload_tabs_and_status_timer = QTimer()
        reload_tabs_and_status_timer.setSingleShot(True)
        reload_tabs_and_status_timer.timeout.connect(self._reload_tabs_and_status_timer_timeout)
        return reload_tabs_and_status_timer

    @Slot()
    def _clip_all_tabs(self):
        self.setTabPosition(Qt.TopDockWidgetArea, QtWidgets.QTabWidget.TabPosition.North)
        for docked_tab in self._docked_tabs.values():
            docked_tab.setFloating(False)
        first_tab = None
        for docked_tab in self._docked_tabs.values():
            if not first_tab:
                first_tab = docked_tab
            else:
                self.tabifyDockWidget(first_tab, docked_tab)

    @Slot()
    def _reload_tabs_and_status_timer_timeout(self):
        # TODO consider removing timer and conecting signal to engine
        engine_modified_since_reload = (self._last_tabs_reload_time != self.engine.last_engine_modification_time)
        if engine_modified_since_reload:
            if settings.s.automatically_refresh_tabs_after_engine_change:
                self._reload_engine_in_all_tabs()

        self._docked_tabs[TabId.GENERAL_INFO].widget.set_engine_modification_info(
            engine_modified_since_reload=engine_modified_since_reload,
            last_tabs_reload_time=self._last_tabs_reload_time)

        # TODO For the time beeing it is fine to refresh it every time, but consider better solution
        self._reload_tabs_and_status_timer.start(settings.s.automatic_engine_refresh_time_ms)
        self._update_window_title()

    def _restore_gui_state_and_geometry(self):
        if settings.s.main_window_geometry:
            self.restoreGeometry(bytes.fromhex(settings.s.main_window_geometry))
        if settings.s.main_window_state:
            self.restoreState(bytes.fromhex(settings.s.main_window_state))

        for docked_tab in self._docked_tabs.values():
            if docked_tab.isHidden():
                docked_tab.set_action_state(False)

    def _save_gui_state_and_geometry(self):
        settings.s.main_window_geometry = self.saveGeometry().data().hex()
        settings.s.main_window_state = self.saveState().data().hex()

    def _create_recently_opened_files_menu(self):
        for action in self._recently_opened_files_actions:
            self.ui.menuOpen_Recent.removeAction(action)
        self._recently_opened_files_actions = []

        for recent_file in reversed(settings.s.recently_opened_databases):
            action = QtWidgets.QAction(self)
            action.setObjectName("action_file_" + os.path.basename(recent_file))
            action.setText(recent_file)
            self.ui.menuOpen_Recent.addAction(action)
            self._recently_opened_files_actions.append(action)
            action.triggered.connect(self._open_recent_file)
        self.ui.menuOpen_Recent.setEnabled(len(self._recently_opened_files_actions) > 0)

    def _open_recent_file(self):
        self.start_opening_databases(self.sender().text(), prompts_database_path=settings.s.prompts_database_path)

    def _add_recently_opened_file(self, file_path):
        if not file_path:
            return
        if file_path not in settings.s.recently_opened_databases:
            settings.s.recently_opened_databases.append(file_path)
            if len(settings.s.recently_opened_databases) > 10:
                settings.s.recently_opened_databases = settings.s.recently_opened_databases[:10]
            self._create_recently_opened_files_menu()

    def eventFilter(self, object, event):
        if event.KeyPress:
            if event.type() == QEvent.ShortcutOverride:
                if bool(QGuiApplication.queryKeyboardModifiers() & Qt.ControlModifier):
                    if event.key() == Qt.Key_S:
                        self._save_database()
                        return True
        return False

    #### Engine management section ####
    def _ask_if_user_sure_about_closing_current_database(self):
        """ Checks if allowed to continue closing database,
        e.g. during program closing or loading other database.
        If database is not save, question (to continue) will be generated"""
        engine_modified_since_saved = (self._last_traveller_db_save_time != self.engine.last_engine_modification_time)
        if engine_modified_since_saved:
            reply = QMessageBox.question(self, "Continue?",
                                         'There are probably unsaved changes in traveller database.\n '
                                         'Are you sure you want to continue and discard current changes?')
            if reply != QMessageBox.StandardButton.Yes:
                return False
        return True

    def set_engine(engine):
        raise NotImplementedError

    def start_opening_databases(self, database_path: str, prompts_database_path: str):
        """ Open main database from path

        :param file_path: Path to database to open. If empty, new database will be created
        """
        if self.engine.collection_file_path or not self.engine.collection.is_empty():
            if not self._ask_if_user_sure_about_closing_current_database():
                return

        if self._engine_loader:
            self._engine_loader.ignore_result()
            self._engine_loader = None
        self._engine_loader = EngineLoader(parent=self,
                                           database_path=database_path,
                                           prompts_database_path=prompts_database_path)
        self._engine_loader.signal_engine_loaded.connect(self._opening_database_finished)
        self._engine_loader.start()

    @Slot(object, object)
    def _opening_database_finished(self, engine: Engine, error_message):
        if error_message:
            QMessageBox.warning(self, 'Warning!', error_message)

        if engine is not None:
            self._last_traveller_db_save_time = engine.last_engine_modification_time
            self._set_engine(engine)
            self._add_recently_opened_file(engine.collection_file_path)

            if engine.prompts_collection_file_path:
                logging.info(f'Prompts database "{engine.prompts_collection_file_path}" opened')

            if engine.collection_file_path:
                logging.info(f'Database "{engine.collection_file_path}" opened')
            else:
                logging.info(f'New database created')

        self._engine_loader = None

    def _new_database(self):
        logging.info(f'New database created')
        self.start_opening_databases(database_path='', prompts_database_path=settings.s.prompts_database_path)

    def _select_and_open_new_database(self):
        new_path = self._select_file_to_open()
        if new_path:
            self.start_opening_databases(database_path=new_path, prompts_database_path=settings.s.prompts_database_path)

    def _select_file_to_open(self):
        start_path = self.engine.collection_file_path if self.engine.collection_file_path else settings.s.last_database_path
        title = 'Select database to open...'
        return browse_open_traveller2_database_file(parent=self, title=title, start_path=start_path)

    def _select_and_save_as_new_database(self):
        file_path = self._select_file_to_save()
        if file_path:
            self._save_database(file_path)
        else:
            logging.debug('Saving database aborted')

    def _save_database_and_create_backup(self):
        file_path = self._save_database()
        if file_path:
            output_file_path = self._create_database_backup(file_path)
            msg = f'Database backup saved as "{output_file_path}".'
            logging.info(msg)
            QMessageBox.information(self, "Info!", msg)
        else:
            logging.debug('Saving database backup aborted')

    def _select_file_to_save(self):
        start_path = self.engine.collection_file_path or ''
        title = "Select output pkl file for database"
        return browse_save_traveller2_database_file(parent=self,
                                                    title=title,
                                                    start_file_name=os.path.basename(start_path),
                                                    start_dir_path=ntpath.dirname(start_path),
                                                    )

    def _save_database(self, file_path: str = ''):
        if not file_path:
            file_path = self.engine.collection_file_path
        if not file_path:
            self._select_and_save_as_new_database()
            return
        try:
            bowm = BlockingOperationWithMessagebox(parent=self)
            bowm.perform_operation(message='Saving database',
                                   operation=partial(self.engine.save_collection, file_path))

            self._last_traveller_db_save_time = self.engine.last_engine_modification_time
            logging.info(f'Database saved as {file_path}')
            # self._set_engine(self.engine)  # Reloading of engine disabled - this is handled differently since task #33
            self._add_recently_opened_file(file_path)
            return file_path
        except Exception:
            msg = f'Failed to save database as "{file_path}"'
            logging.exception(msg)
            QMessageBox.warning(self, 'Warning!', msg)

    def _create_database_backup(self, database_file_path):
        backup_suffix = '_backup_' + '{0:%Y%m%d_%H%M%S}'.format(datetime.datetime.now())
        output_dir = os.path.dirname(database_file_path)
        base_name = os.path.splitext(os.path.basename(database_file_path))[0]
        output_file_path = os.path.join(output_dir, base_name + backup_suffix + '.pkl')
        shutil.copy(database_file_path, output_file_path)
        logging.info(f'Database backup saved as {output_file_path}')
        return output_file_path

    def _save_database_copy(self, file_path):
        assert file_path
    #### end of Engine management section ####



if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
