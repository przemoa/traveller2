import argparse
import logging
import os
import signal
import subprocess
import sys
import time

import pyqtgraph as pg
from PySide2.QtWidgets import QApplication, QMessageBox

import _version
from common import settings
from common.package_config import package_config_devel, ProjectConfig
from main_window.main_window import MainWindow


def debug_setup(create_mocs=True):
    load_gui_config(package_config=package_config_devel)
    setup_additional_packages()
    setup_logger()
    if create_mocs:
        moc_ui_files()


def _moc_ui_file(ui_file_path: str, moc_file_path: str, force_moc: bool = False):
    if not force_moc and os.path.isfile(moc_file_path):
        ui_file_modification_time = os.path.getmtime(ui_file_path)
        moc_file_modification_time = os.path.getmtime(moc_file_path)
        if moc_file_modification_time > ui_file_modification_time:
            logging.debug('Skipping mocking of file {}'.format(ui_file_path))
            return

    logging.debug('Mocking file {} to {}'.format(ui_file_path, moc_file_path))
    try:
        os.remove(moc_file_path)
    except OSError:
        pass
    moc_command = 'pyside2-uic "{0}" -o "{1}" -x'.format(ui_file_path, moc_file_path)

    process = subprocess.Popen(moc_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = process.communicate(timeout=10)
    return_code = process.returncode
    if return_code is not 0:
        exception_message = 'Mocing file {0} failed. stdout: {1}, stderr: {2}'.format(
            ui_file_path, str(stdout), str(stderr))
        raise Exception(exception_message)


def get_moc_module_name_for_ui(ui_name: str) -> str:
    return 'moc_' + ui_name


def moc_ui_files(force_moc=False):
    logging.debug('Mocking started...')
    gui_dir_path = os.path.dirname(os.path.realpath(__file__))
    for (dir_path, dir_names, file_names) in os.walk(gui_dir_path):
        for file_name in file_names:
            if file_name.endswith('.ui'):
                ui_file_path = os.path.join(dir_path, file_name)
                moc_module_name = get_moc_module_name_for_ui(ui_name=file_name[:-3])
                moc_file_path = os.path.join(dir_path, moc_module_name + '.py')
                _moc_ui_file(ui_file_path=ui_file_path, moc_file_path=moc_file_path, force_moc=force_moc)
    logging.debug('Mocking finished')
    create_file_with_build_date()


def create_file_with_build_date():
    time_str = 'build_time = ' + time.strftime("'%d.%m.%Y %H:%M:%S'")
    file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '_build_time.py')
    with open(file_path, "w") as text_file:
        text_file.write(time_str)


def load_gui_config(package_config: ProjectConfig):
    settings.load_settings(package_config=package_config)


def save_gui_config(package_config: ProjectConfig):
    settings.save_settings(package_config=package_config)


def setup_additional_packages():
    pg.setConfigOptions(antialias=True, background='w')
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = settings.s.google_application_credentials_file_path


def run_traveller2_gui():
    app = QApplication(sys.argv)
    setup_additional_packages()
    main_window = MainWindow()
    main_window.show()
    app.exec_()


def logger_exception_hook(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logging.fatal("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    # exc_info = (exc_type, exc_value, exc_traceback)
    # exception_stack_text = "".join(traceback.format_exception(*exc_info, limit=None, chain=True))
    # logging.error(short_msg + '\n' + exception_stack_text)

    if settings.DISPLAY_QMESSAGE_AFTER_UNHANDLED_EXCEPTION:
        short_msg = f'{exc_type.__name__}: {str(exc_value)}.\n'
        QMessageBox.critical(None, "Unhandled excption!", short_msg)


def setup_logger(log_file_path=None):
    logging.getLogger().handlers.clear()
    console_formatter = logging.Formatter("%(asctime)s.%(msecs)03d [%(levelname)-7s]:  "
                                          "%(message)s [%(module)s: %(funcName)s]", datefmt="%H:%M:%S")

    if log_file_path:
        file_formatter = logging.Formatter("%(asctime)s.%(msecs)03d [%(levelname)-7s]:  "
                                           "%(message)s [%(module)s: %(funcName)s]", datefmt="%Y-%m-%d %H:%M:%S")
        file_handler = logging.FileHandler(log_file_path)
        file_handler.setFormatter(file_formatter)
        file_handler.setLevel(settings.s.file_log_level)
        logging.getLogger().addHandler(file_handler)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(console_formatter)
    stream_handler.setLevel(settings.s.console_log_level)
    logging.getLogger().addHandler(stream_handler)

    logging.getLogger().setLevel(logging.DEBUG)
    if not settings.IS_DEBUG:
        sys.excepthook = logger_exception_hook
    logging.info('Logger started')


def sig_int_handler(*args):
    if sig_int_handler.already_called:
        print('\nCtrl+c received twice. Exiting application immediately.')
        os._exit(1)
    else:
        sig_int_handler.already_called = True
        print('\nCtrl+c received for the first time. '
              'Please wait for graceful application exit (or click ctrl+c again to force exit)...')

        # remove all loggers (including gui logger) except for the root logger
        logging.getLogger().handlers = logging.getLogger().handlers[:1]
        QApplication.quit()


sig_int_handler.already_called = False
signal.signal(signal.SIGINT, sig_int_handler)


def parse_and_apply_command_line_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('-V', '--version', action='version', version='traveller2 version {}'.format(
        _version.__version__))

    parser.add_argument('database_path', nargs='?', metavar='FILE', type=str,
                        help='Path to file with traveller2 database (e.g "$HOME/Desktop/file.pkl").')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Run as debug version.')
    parser.add_argument('-r', '--read_only', action='store_true',
                        help='Run database as read only. Allows also for faster usage. Disables also prompts database.')
    parser.add_argument('-x', '--delete_settings', action='store_true',
                        help='Delete settings file before loading it.')


    args = parser.parse_args()
    if args.debug and not settings.IS_DEBUG:
        settings.IS_DEBUG = True
        settings.print_warning_about_debug_version_or_development_config()
    if args.read_only:
        raise NotImplementedError('Read only mode is not implemented yet!')

    return args


if __name__ == '__main__':
    moc_ui_files()
