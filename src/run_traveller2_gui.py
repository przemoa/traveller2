import logging

import gui_setup
from common import settings
from common.package_config import package_config_release, package_config_devel


def main():
    logging.basicConfig(format='%(message)s')  # just for a moment, will be overwritten soon
    logging.getLogger().setLevel(logging.INFO)

    if settings.IS_DEVELOPMENT:
        package_config = package_config_devel
    else:
        package_config = package_config_release

    args = gui_setup.parse_and_apply_command_line_arguments()
    if args.delete_settings:
        settings.delete_settings_file(package_config=package_config)

    gui_setup.load_gui_config(package_config=package_config)

    gui_setup.setup_logger()

    if settings.IS_DEBUG:
        gui_setup.moc_ui_files()
        from items_definitions.items_generator import generate_all
        generate_all()

    gui_setup.run_traveller2_gui()
    gui_setup.save_gui_config(package_config=package_config)


if __name__ == '__main__':
    main()
