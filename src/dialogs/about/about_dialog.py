from PySide2 import QtWidgets

import _build_time
from _version import __version__ as gui_version


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = self.generate_ui()
        self._display_info()

    def generate_ui(self):
        from dialogs.about.moc_about_dialog import Ui_about_dialog
        ui = Ui_about_dialog()
        ui.setupUi(self)
        return ui

    def _display_info(self):
        html_source = '<p><b>Traveller2 GUI</b> {}</p>'.format(gui_version)
        self.ui.textBrowser_about.append(html_source)
        self.ui.textBrowser_about.append(f'Build time: {_build_time.build_time}')
        self.ui.textBrowser_about.append('Author: przemyslaw.aszkowski@gmail.com')
        self.ui.textBrowser_about.append("""
        <p>Tips:</p>
        - uncheck 'validate_field_types_during_load_engine' in config 
        to ignore some safety checks and speed-up database loading <br>
        - in 'Photos' tab, click left/right/middle mouse button on image thumbnail 
        to open, show or deselect a photo <br>
        - left click on item in timeline widget/dialog to open it in browser <br>
        """)
        self.ui.textBrowser_about.append('')
        self.ui.textBrowser_about.append('')
        self.ui.textBrowser_about.append("""
        <small>
        <p>Credits:</p>
        <div>
        &bull; Application icon made by <a href="https://www.flaticon.com/authors/vectors-market" 
        title="Vectors Market">Vectors Market</a>  
        from <a href="https://www.flaticon.com/" 			    
        title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    
        title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
        </small></div>
        """)


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = AboutDialog(None)
    w.show()
    sys.exit(app.exec_())
