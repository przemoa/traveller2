from dataclasses import dataclass

from PySide2 import QtCore, QtWidgets
from PySide2.QtWidgets import QMessageBox

from common import settings
from dialogs.settings.settings_field import SettingField, get_setting_field_class


@dataclass
class SettingsField:
    name_in_settings: str
    settings_field_class: type = None
    displayed_name: str = ''  # if empty, same text as variable in settings will be used
    parameters: dict = None

    def __post_init__(self):
        if not self.parameters:
            self.parameters = {}
        if not self.displayed_name:
            self.displayed_name = self.name_in_settings.capitalize().replace('_', ' ')
        if not self.settings_field_class:
            variable_value_from_settings = getattr(settings.Settings, self.name_in_settings)
            self.settings_field_class = get_setting_field_class(
                self.name_in_settings,
                variable_value_from_settings)


SettingsDivision = {
    'General': [
        SettingsField(name_in_settings='console_log_level'),
        SettingsField(name_in_settings='file_log_level'),

    ],
    'Database': [
        SettingsField(name_in_settings='prompts_database_path'),
        SettingsField(name_in_settings='last_database_path',
                      parameters={'read_only': True}),
        SettingsField(name_in_settings='files_root_dir_path'),
    ],
    'UX': [
        SettingsField(name_in_settings='ask_before_copying_prompt_to_traveller_db'),
        SettingsField(name_in_settings='automatically_refresh_tabs_after_engine_change'),
        SettingsField(name_in_settings='automatic_engine_refresh_time_ms',
                      parameters={'min': 50, 'max': 500000, 'step': 100}),
        SettingsField(name_in_settings='use_single_dialog_for_all_tabs_for_map_and_wiki'),
    ],
    'Dialogs': [
        SettingsField(name_in_settings='first_language_for_wikipedia'),
        SettingsField(name_in_settings='second_language_for_wikipedia'),
    ],
    'Credentials': [
        SettingsField(name_in_settings='google_maps_api_key', displayed_name='Google maps API key'),
        SettingsField(name_in_settings='google_application_credentials_file_path'),
    ],
    'Items': [
        SettingsField(name_in_settings='display_items_sorted_by_date'),
        SettingsField(name_in_settings='validate_field_types_during_load_engine'),
        SettingsField(name_in_settings='ask_before_copying_prompt_to_traveller_db'),
        SettingsField(name_in_settings='thumbnail_size_in_pixels',
                      parameters={'min': 16, 'max': 512}),
        SettingsField(name_in_settings='radius_of_photos_decimation_in_space_in_meters',
                      displayed_name='Radius of photos decimation in space [m]',
                      parameters={'min': -1, 'max': 10000}),
    ],
}


class SettingsDialog(QtWidgets.QDialog):
    """ Generate dialog with settings for settings class.
    Special attributes (allowed characters/ranges/types) can be set additionally.
    Some attributes (e.g. field type) can be determined automatically.
    See settings_field file for details
    """

    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.ui = self.generate_ui()
        self._fields = self._generate_fields()  # type: List[SettingField]
        self.ui.buttonBox.accepted.connect(self._ok_clicked)

    def generate_ui(self):
        from dialogs.settings.moc_settings_dialog import Ui_settings_dialog
        ui = Ui_settings_dialog()
        ui.setupUi(self)
        return ui

    def _generate_fields(self):
        fields = {}

        for tab_name, tab_fields_definitions in SettingsDivision.items():
            current_tab = QtWidgets.QWidget()
            current_tab.setObjectName("tab_" + tab_name)
            current_tab.horizontalLayout = QtWidgets.QHBoxLayout(current_tab)
            current_tab.horizontalLayout.setObjectName("horizontalLayout_" + tab_name)
            current_tab.horizontalLayout.setContentsMargins(0, 0, 0, 0)
            current_tab.scrollArea = QtWidgets.QScrollArea(current_tab)
            current_tab.scrollArea.setWidgetResizable(True)
            current_tab.scrollArea.setObjectName("scrollArea_" + tab_name)
            current_tab.scrollAreaWidgetContents = QtWidgets.QWidget()
            current_tab.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 636, 294))
            current_tab.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")

            current_tab.verticalLayout_2 = QtWidgets.QVBoxLayout(current_tab.scrollAreaWidgetContents)
            current_tab.verticalLayout_2.setObjectName("verticalLayout_2_" + tab_name)
            current_tab.gridLayout_settings = QtWidgets.QGridLayout()
            current_tab.gridLayout_settings.setObjectName("gridLayout_settings_" + tab_name)
            current_tab.verticalLayout_2.addLayout(current_tab.gridLayout_settings)
            spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
            current_tab.verticalLayout_2.addItem(spacerItem)
            current_tab.scrollArea.setWidget(current_tab.scrollAreaWidgetContents)
            current_tab.horizontalLayout.addWidget(current_tab.scrollArea)

            self.ui.tabWidget.addTab(current_tab, tab_name)

            for i, field_definition in enumerate(tab_fields_definitions):

                kwargs = {
                    'parent': self,
                    'name': field_definition.name_in_settings,
                    'value_from_settings': settings.s.__getattribute__(field_definition.name_in_settings),
                    'displayed_name': field_definition.displayed_name,
                    'parameters': field_definition.parameters
                }

                field = field_definition.settings_field_class(**kwargs)

                current_tab.gridLayout_settings.addWidget(field.label, i, 0)
                current_tab.gridLayout_settings.addWidget(field.ui_edit, i, 1)
                if field.additional_ui_element is not None:
                    current_tab.gridLayout_settings.addWidget(field.additional_ui_element, i, 2)

                fields[field_definition.name_in_settings] = field

        return fields

    def _ok_clicked(self):
        something_changed = False
        for field_name, field in self._fields.items():
            settings_value = settings.s.__getattribute__(field_name)
            ui_value = field.get_value_from_ui_edit()
            if type(settings_value) != type(ui_value):
                raise Exception(f'Type of value in settings is different than expected (for field {field_name})')

            if ui_value != settings_value:
                setattr(settings.s, field_name, ui_value)
                something_changed = True

        if something_changed:
            QMessageBox.information(self, "Info!", 'Some changes may be not visible until restart.')


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    w = SettingsDialog()
    w.show()
    sys.exit(app.exec_())
