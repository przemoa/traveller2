import logging

from PySide2 import QtWidgets
from PySide2.QtCore import QObject, Slot
from PySide2.QtWidgets import QLabel, QFileDialog

from utils.gui import set_editability_for_edit


class SettingField(QObject):
    def __init__(self,
                 parent,
                 name: str,
                 value_from_settings,
                 displayed_name: str,
                 parameters: dict):
        super().__init__(parent=parent)
        self.parameters = parameters
        self.parent = parent
        self.name = name
        self.value_from_settings = value_from_settings
        self.displayed_name = displayed_name
        self.label = self._create_label()  # label to put in first column
        self.ui_edit = self._create_ui_edit()  # field edit to put in second column
        self.additional_ui_element = self._create_additional_ui_element()  # Additional ui element to put in third column

        if parameters.get('read_only', False):
            set_editability_for_edit(self.ui_edit, False)
            if self.additional_ui_element:
                set_editability_for_edit(self.additional_ui_element, False)

    def _create_label(self):
        label = QLabel(self.parent)
        label.setText(self.displayed_name + ':')
        return label

    def _create_ui_edit(self):
        raise NotImplementedError(f'Method not implemented in class "{self.__class__}"')

    def _create_additional_ui_element(self):
        return None

    def get_value_from_ui_edit(self):
        raise NotImplementedError(f'Method not implemented in class "{self.__class__}"')


class IntSettingField(SettingField):
    def _create_ui_edit(self):
        ui_edit = QtWidgets.QSpinBox(self.parent)
        ui_edit.setMinimum(self.parameters.get('min', -1000000))
        ui_edit.setMaximum(self.parameters.get('max', 1000000))
        ui_edit.setSingleStep(self.parameters.get('step', 1))
        ui_edit.setValue(self.value_from_settings)
        return ui_edit
    
    def get_value_from_ui_edit(self):
        return self.ui_edit.value()


class FloatSettingField(SettingField):
    def _create_ui_edit(self):
        ui_edit = QtWidgets.QDoubleSpinBox(self.parent)
        ui_edit.setMinimum(self.parameters.get('min', -1000000))
        ui_edit.setMaximum(self.parameters.get('max', 1000000))
        ui_edit.setSingleStep(self.parameters.get('step', 1))
        ui_edit.setValue(self.value_from_settings)
        return ui_edit

    def get_value_from_ui_edit(self):
        return self.ui_edit.value()
    
    
class BoolSettingField(SettingField):
    def _create_ui_edit(self):
        ui_edit = QtWidgets.QCheckBox(self.parent)
        ui_edit.setChecked(self.value_from_settings)
        return ui_edit

    def get_value_from_ui_edit(self):
        return self.ui_edit.isChecked()


class StringSettingField(SettingField):
    def _create_ui_edit(self):
        ui_edit = QtWidgets.QLineEdit(self.parent)
        ui_edit.setText(self.value_from_settings)
        return ui_edit

    def get_value_from_ui_edit(self):
        return self.ui_edit.text()


class ComboBoxSettingField(SettingField):
    def _create_ui_edit(self):
        ui_edit = QtWidgets.QComboBox(self.parent)

        for item in self._get_items_list():
            ui_edit.addItem(str(item))

        default_value = self._get_default_element_text()
        try:
            ui_edit.setCurrentText(default_value)
        except:
            logging.exception(f'Cannot set value "{default_value} in combobox. Leaving first available.')

        return ui_edit

    def _get_default_element_text(self):
        raise NotImplementedError(f'Method not implemented in class "{self.__class__}"')

    def get_value_from_ui_edit(self):
        raise NotImplementedError(f'Method not implemented in class "{self.__class__}"')

    def _get_items_list(self) -> list:
        raise NotImplementedError(f'Method not implemented in class "{self.__class__}"')


class LogLevelSettingField(ComboBoxSettingField):
    LOG_LEVELS = {
        'Debug': logging.DEBUG,
        'Info': logging.INFO,
        'Warning': logging.WARNING,
        'Error': logging.ERROR,
        'Fatal': logging.FATAL,
    }

    def _get_items_list(self):
        return list(self.LOG_LEVELS.keys())

    def get_value_from_ui_edit(self):
        return self.LOG_LEVELS[self.ui_edit.currentText()]

    def _get_default_element_text(self):
        for txt, value in self.LOG_LEVELS.items():
            if value == self.value_from_settings:
                return txt
        else:
            logging.warning(f'Default "{self.name}" field value "{self.value_from_settings}" is unknown. '
                            f'Using first available.')
            return list(self.LOG_LEVELS.keys())[0]


class PathSettingField(StringSettingField):
    def _create_additional_ui_element(self):
        browse_button = QtWidgets.QPushButton(self.parent)
        browse_button.setObjectName(f'push_button_settings_browse_{self.name}')
        browse_button.setText('Browse')
        browse_button.clicked.connect(self._browse_clicked)
        return browse_button

    @Slot()
    def _browse_clicked(self):
        start_path = self.get_value_from_ui_edit()
        select_title = f'Select {self.displayed_name}...'
        new_path = None

        if self.name.endswith('_dir_path'):
            new_path = QFileDialog.getExistingDirectory(
                self.parent,
                select_title,
                start_path,
                QFileDialog.ShowDirsOnly)
        else:
            file_dialog = QFileDialog(
                self.parent,
                select_title,
                start_path,
                "All (*)")
            if file_dialog.exec_():
                new_path = file_dialog.selectedFiles()[0]

        if new_path:
            self.ui_edit.setText(new_path)


def get_setting_field_class(variable_name: str, variable_value_from_settings) -> SettingField:
    """ Determine appropriate SettingsField class. Most generic at the bottom of if-else list """

    if isinstance(variable_value_from_settings, int) and variable_name.endswith('log_level'):
        return LogLevelSettingField
    elif isinstance(variable_value_from_settings, str) and variable_name.endswith('_path'):
        return PathSettingField
    elif isinstance(variable_value_from_settings, bool):
        return BoolSettingField
    elif isinstance(variable_value_from_settings, int):
        return IntSettingField
    elif isinstance(variable_value_from_settings, float):
        return FloatSettingField
    elif isinstance(variable_value_from_settings, str):
        return StringSettingField
    else:
        raise NotImplementedError(f'SettingsField- not implemented type "{type(variable_value_from_settings)}"')


