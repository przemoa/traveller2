from PySide2 import QtWidgets
from PySide2.QtCore import QCoreApplication
from PySide2.QtCore import Qt


class ProgressDialog:
    def __init__(self, parent, title, max_value=100):
        self.dialog = QtWidgets.QProgressDialog(parent)
        self.dialog.setWindowModality(Qt.WindowModal)
        self.dialog.setWindowTitle(title)
        self.dialog.setMaximum(max_value)
        self.dialog.show()
        QCoreApplication.processEvents()

    def set_message(self, text):
        self.dialog.setLabelText(text)

    def set_value(self, value):
        self.dialog.setValue(value)
        QCoreApplication.processEvents()

    def was_canceled(self) -> bool:
        return self.dialog.wasCanceled()

    def cancel(self):
        self.dialog.cancel()
        QCoreApplication.processEvents()
