import threading
import time
from typing import Callable

from PySide2 import QtWidgets
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QMessageBox


class BlockingOperationWithMessagebox(QtWidgets.QWidget):
    """ may be called only from main Qt thread """

    signal_operation_finished = Signal()  # result, error_message
    id_counter = 0

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.id_counter += 1
        self._id = self.id_counter

        self.result = None
        self._error = None
        self._operation = None
        self._parent = parent
        self._dialog = None

    def perform_operation(self, message: str, operation: Callable):
        """
        Be carefull to not do anything on ui in 'operation'
        :param message:
        :param operation:
        :return:
        """
        self._operation = operation

        self._dialog = QMessageBox(self)
        self._dialog.setWindowTitle('Please wait...')
        self._dialog.setText(message)
        self._dialog.setIcon(QMessageBox.Information)
        self._dialog.setModal(True)
        self._dialog.setStandardButtons(0)

        self.signal_operation_finished.connect(self._operation_finished)
        self._thread = threading.Thread(target=self._run,
                                        name=f'Blocking_operation_{self.id_counter}')
        self._thread.start()

        self._dialog.exec_()

        self._thread.join()

        if self._error:
            raise Exception(f'Error occurred during blocking operation: {self._error}')

    def _run(self):
        time.sleep(0.3)  # give some time for loading_dialog to load (so QT GUI thread will have some time for processing)

        try:
            self.result = self._operation()
        except Exception as e:
            self._error = str(e)
        self.signal_operation_finished.emit()

    def _operation_finished(self):
        self._dialog.reject()

