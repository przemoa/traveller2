# use google java script api
# with one free visit for api key
# generate map to file for selected config (show places, cites, images (with icon whole time or after hovering pin)


from PySide2.QtCore import QUrl
from PySide2.QtWebEngineWidgets import QWebEngineView
from PySide2.QtWidgets import QVBoxLayout

from dialogs.nonmodal_info.nonmodal_info_dialog import NonmodalInfoDialog
from items.item import Item


class SingleMarkerMapDialog(NonmodalInfoDialog):
    _all_dialogs_by_parents = dict()

    def __init__(self, parent):
        super().__init__(parent=parent)

        self.ui = self.generate_ui()

        self.ui.web_view.show()
        self.ui.web_view.setUrl('')

    def generate_ui(self):
        class SingleMarkerMapDialogUi:
            def __init__(self, parent):
                self.verticalLayout = QVBoxLayout(parent)
                self.verticalLayout.setContentsMargins(0, 0, 0, 0)

                self.web_view = QWebEngineView(parent)
                self.verticalLayout.addWidget(self.web_view)
        ui = SingleMarkerMapDialogUi(parent=self)
        self.resize(600, 550)
        return ui

    def generate_and_show_map(self, item_or_coordinates):
        from utils.map_generator import map_generator  # hidden to not propagate dependencies for items_generator

        coordinates = item_or_coordinates.coordinates if isinstance(item_or_coordinates, Item) else item_or_coordinates
        self.setWindowTitle(item_or_coordinates.get_short_description())
        absolute_html_file_path = map_generator.generate_map_with_single_marker(
            coordinates=coordinates)
        url = QUrl.fromLocalFile(absolute_html_file_path)
        self.ui.web_view.setUrl(url)

    def _display_info(self, info, engine=None):
        self.generate_and_show_map(item_or_coordinates=info)

    # @classmethod
    # def show_on_map(cls, parent, item_or_coordinates: Union[Coordinates, Item]):
    #     if settings.s.use_single_dialog_for_all_tabs_for_map_and_wiki:
    #         parent = None
    #
    #     dialog = cls._all_dialogs_by_parents.get(parent, None)
    #     if dialog is None:
    #         dialog = cls(parent=parent)
    #         cls._all_dialogs_by_parents[parent] = dialog
    #     dialog.show()
    #     dialog.display_info(item_or_coordinates)


def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets
    from utils.dummy_engine import get_dummy_engine

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    # w = SingleMarkerMapDialog(parent=None)
    # w.show()
    # w.generate_and_show_map(get_dummy_engine().collection.photos[3])
    SingleMarkerMapDialog.display_info_in_dialog(None, get_dummy_engine().collection.photos[3].coordinates)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
