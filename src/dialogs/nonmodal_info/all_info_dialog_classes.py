from dialogs.nonmodal_info.single_marker_map.single_marker_map_dialog import SingleMarkerMapDialog
from dialogs.nonmodal_info.wikipedia_info.wikipedia_info_dialog import WikipediaInfoDialog


ALL_INFO_DIALOG_CLASSES = [
    WikipediaInfoDialog,
    SingleMarkerMapDialog,
]
