from dataclasses import dataclass
from typing import List

import requests
from PySide2.QtCore import QTimer
from PySide2.QtWebEngineWidgets import QWebEngineView

from common import settings
from dialogs.nonmodal_info.nonmodal_info_dialog import NonmodalInfoDialog
from utils.gui import set_splitter_division


@dataclass
class SearchResult:
    name: str
    url: str
    description: str


class WikipediaInfoDialog(NonmodalInfoDialog):
    WIKIPEDIA_MAIN_PAGE = "https://{language}.wikipedia.org/"
    WIKIPEDIA_API_BASE_URL = "https://{language}.wikipedia.org/w/api.php"

    _all_dialogs_by_parents = {}

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.ui = self.generate_ui()

        self._requests_session = requests.Session()
        self._web_view = self._generate_web_view()
        self._current_search_results = []  # type: List[SearchResult]  # actually List[Union[SearchResult, None]], None is for separator
        set_splitter_division(spliter=self.ui.splitter, percentage_for_first_widget=1)
        self._search_delay_timer = self._create_search_delay_timer()
        self.create_connections()

    def generate_ui(self):
        from dialogs.nonmodal_info.wikipedia_info.moc_wikipedia_info_dialog import Ui_wikipedia_info_dialog
        ui = Ui_wikipedia_info_dialog()
        ui.setupUi(self)
        return ui

    def _generate_web_view(self):
        web_view = QWebEngineView(self)
        web_view.show()
        web_view.setUrl('')
        self.ui.verticalLayout_for_web_engine.addWidget(web_view)
        return web_view

    def _go_to_url(self, url):
        self._web_view.setUrl(url)

    def _create_search_delay_timer(self):
        search_delay_timer = QTimer()
        search_delay_timer.setSingleShot(True)
        search_delay_timer.timeout.connect(self._search_and_go_to_text_in_line_edit)
        return search_delay_timer

    def _line_edit_with_search_modified(self):
        self._search_delay_timer.start(500)

    def _search_and_go_to_text_in_line_edit(self):
        self._search_and_go(phrase=self.ui.lineEdit_search.text())

    def type_search_and_go(self, phrase):
        self._search_delay_timer.stop()
        self.ui.lineEdit_search.blockSignals(True)
        self.ui.lineEdit_search.setText(phrase)
        self.ui.lineEdit_search.blockSignals(False)
        self._search_and_go_to_text_in_line_edit()

    def _search_and_go(self, phrase: str):
        params = {
            'action': "opensearch",
            'search': phrase,
            'limit': 10,
            'namespace': 0,
            'format': "json",
        }

        number_of_results = 0
        self._current_search_results = []

        for language in [settings.s.first_language_for_wikipedia, settings.s.second_language_for_wikipedia]:
            response = self._requests_session.get(url=self.WIKIPEDIA_API_BASE_URL.format(language=language), params=params)
            response_data = response.json()

            number_of_results_for_single_language = len(response_data[1]) if isinstance(response_data, list) else 0  # in case of error dict is received
            number_of_results += number_of_results_for_single_language
            for i in range(number_of_results_for_single_language):
                name = response_data[1][i]
                url = response_data[3][i]
                self._current_search_results.append(SearchResult(name=name, url=url, description=response_data[2][i]))
            if number_of_results_for_single_language:
                self._current_search_results.append(None)

        self._display_search_results_in_combobx()

        if number_of_results > 0:
            self._go_to_url(self._current_search_results[0].url)
        else:
            self._go_to_url(self.WIKIPEDIA_MAIN_PAGE.format(language=settings.s.first_language_for_wikipedia))

    def _display_search_results_in_combobx(self):
        self.ui.comboBox_search_results.blockSignals(True)
        self.ui.comboBox_search_results.clear()
        MAX_DESCRIPTION_LENGTH = 90
        for i, search_result in enumerate(self._current_search_results):
            if search_result is None:
                self.ui.comboBox_search_results.insertSeparator(i)
            else:
                short_description = search_result.description[:MAX_DESCRIPTION_LENGTH]
                if len(search_result.description) > MAX_DESCRIPTION_LENGTH:
                    short_description += '...'
                if len(short_description) == 0:  # some items does not have any description...
                    short_description = search_result.name
                self.ui.comboBox_search_results.addItem(short_description)

        self.ui.comboBox_search_results.blockSignals(False)

    def create_connections(self):
        self.ui.lineEdit_search.textChanged.connect(self._line_edit_with_search_modified)
        self.ui.lineEdit_search.returnPressed.connect(self._search_and_go_to_text_in_line_edit)
        self.ui.pushButton_go.clicked.connect(self._search_and_go_to_text_in_line_edit)
        self.ui.comboBox_search_results.currentIndexChanged.connect(self._selected_search_result_changed)

    def _selected_search_result_changed(self, index):
        self._go_to_url(self._current_search_results[index].url)

    def _display_info(self, info, engine=None):
        self.type_search_and_go(phrase=info)



if __name__ == '__main__':
    import sys


    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    WikipediaInfoDialog.display_info_in_dialog(None, 'poznan')
    # w = WikipediaInfoDialog(None)
    # w.show()
    # w.type_search_and_go('zielona gora')
    sys.exit(app.exec_())
