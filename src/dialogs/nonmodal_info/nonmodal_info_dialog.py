from PySide2 import QtCore
from PySide2.QtCore import Signal
from PySide2.QtWidgets import QDialog

from common import settings


class NonmodalInfoDialog(QDialog):
    """ Base class for unmodal dialogs with info to display.

    After adding new dialog, add it to the list of ALL_INFO_DIALOG_CLASSES
    """

    signal_link_clicked = Signal(object)

    def __init__(self, parent):
        QDialog.__init__(self, parent)

        self.hide_instead_of_close = True
        self.setWindowModality(QtCore.Qt.WindowModal)
        # self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)

    def generate_ui(self):
        raise NotImplementedError()

    def closeEvent(self, event):
        if self.hide_instead_of_close:
            event.ignore()
            self.hide()
        else:
            event.accept()

    def _display_info(self, info):
        raise NotImplementedError()

    @classmethod
    def display_info_in_dialog(cls, parent, info, engine=None):
        if settings.s.use_single_dialog_for_all_tabs_for_map_and_wiki:
            parent_or_none = None
        else:
            parent_or_none = parent

        dialog = cls._all_dialogs_by_parents.get(parent_or_none, None)
        if dialog is None:
            dialog = cls(parent=parent_or_none)
            cls._all_dialogs_by_parents[parent_or_none] = dialog
        dialog.signal_link_clicked.connect(parent.signal_link_clicked)
        dialog.show()
        dialog._display_info(info, engine=engine)

    @classmethod
    def close_dialogs(cls):
        for dialog in cls._all_dialogs_by_parents.values():
            dialog.hide_instead_of_close = False
            dialog.close()
