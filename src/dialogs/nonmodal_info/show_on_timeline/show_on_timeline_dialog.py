# use google java script api
# with one free visit for api key
# generate map to file for selected config (show places, cites, images (with icon whole time or after hovering pin)


from PySide2.QtCore import QUrl
from PySide2.QtWidgets import QVBoxLayout

from common.types.custom_datetime import CustomDateTime
from dialogs.nonmodal_info.nonmodal_info_dialog import NonmodalInfoDialog
from items.attachment.photo import Photo
from items.flight import Flight
from items.item import Item
from items.journey import Journey
from items.visit import Visit
from items_utils.misc import get_closes_item_on_timeline
from items_utils.plot_item import get_plot_rectangles_for_items, PlotItemDefinition, get_plot_points_for_items, \
    ITEM_TYPE_TO_Y_VALUE_MAPPING
from tabs.timeline.timeline_plot import TimelinePlot


class ShowOnTimelineDialog(NonmodalInfoDialog):
    _all_dialogs_by_parents = dict()

    def __init__(self, parent):
        super().__init__(parent=parent)

        self.ui = self.generate_ui()

        self.timeline_plot = TimelinePlot(parent=self)
        self.ui.verticalLayout.addWidget(self.timeline_plot)
        self._items_for_y_values = None
        self.timeline_plot.signal__clicked_on_plot_near_y_integers.connect(self._mouse_clicked_on_plot_near_y_integers)

    def generate_ui(self):
        class ShowOnTimelineDialogUi:
            def __init__(self, parent):
                self.verticalLayout = QVBoxLayout(parent)
                self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        ui = ShowOnTimelineDialogUi(parent=self)
        self.setWindowTitle('')
        self.resize(700, 300)
        self.setMinimumHeight(450)
        return ui

    def generate_and_show_map(self, item_or_coordinates):
        from utils.map_generator import map_generator  # hidden to not propagate dependencies for items_generator

        coordinates = item_or_coordinates.coordinates if isinstance(item_or_coordinates, Item) else item_or_coordinates
        self.setWindowTitle(item_or_coordinates.get_short_description())
        absolute_html_file_path = map_generator.generate_map_with_single_marker(
            coordinates=coordinates)
        url = QUrl.fromLocalFile(absolute_html_file_path)
        self.ui.web_view.setUrl(url)

    def _mouse_clicked_on_plot_near_y_integers(self, x, y, x_range):
        clicked_item = get_closes_item_on_timeline(items_for_y_values=self._items_for_y_values, x=x, y=y, x_range=x_range)
        if clicked_item:
            self.signal_link_clicked.emit(clicked_item)

    def _display_info(self, info: CustomDateTime, engine=None, show_photos=True):
        c = engine.collection

        focus_point = PlotItemDefinition(x_start=info.to_unix_time(),
                                     y=0.5,
                                     txt=f'Timepoint: {info.to_chronological_date_time_string()}')
        self._items_for_y_values = {
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Journey]: c.journeys,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Visit]: c.visits,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Flight]: c.flights,
            ITEM_TYPE_TO_Y_VALUE_MAPPING[Photo]: c.photos,
        }
        self.timeline_plot.plot_data(
            rectangle_items=get_plot_rectangles_for_items(journeys=c.journeys, visits=c.visits, flights=c.flights),
            point_items=get_plot_points_for_items(photos=c.photos) if show_photos else [],
            focus_point=focus_point)


def main():
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets
    from utils.dummy_engine import get_dummy_engine

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)

    engine = get_dummy_engine()
    ShowOnTimelineDialog.display_info_in_dialog(parent=None, info=engine.collection.journeys[0].start_time, engine=engine)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
