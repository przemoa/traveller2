from typing import Union

from PySide2 import QtCore
from PySide2 import QtWidgets
from PySide2.QtCore import Slot, Signal
from PySide2.QtGui import QPalette

from common import colors
from common.types.misc import PromptSourceType
from items.item import Item
from items.location.admin_entity.admin_entity import AdminEntity
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_utils.admin_entity_utils import get_region_minor_prompts, get_region_major_prompts, \
    get_country_prompts, get_city_prompts
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from utils.dummy_engine import get_dummy_engine

"""
actions for each combobox selection:
A) text_changed_by_user
B) update_list_for_existing_parent (or None parent, if none) [remember last update state, skip if not necessary]
C) validate current text in list
D) set_from_child (called if child is valid) = enter text and validate

    Country     |   Admin 1     |   Admin 2     |   City
    ___________________________________________________________     
    1) A
    2) C            
                    3) B & C
                                    4) B & C
                                                    5) B & C        
    ___________________________________________________________
                    1) A
                    2) C
    3) D     
                    4) B            
                                    5) B & C
                                                    6) B & C
    ___________________________________________________________     
                                    1) A
                                    2) C
    3) D
                    4) B & D
                                    5) B
                                                    6) B & C
    ___________________________________________________________
                                                    1) A
                                                    2) C
    3) D
                    4) B & D
                                    5) B & D 
                                                    6) B    
    ___________________________________________________________     


"""



class SelectAdminEntityWidget(QtWidgets.QWidget):
    signal_item_selected = Signal(Item)

    INFINITE_NUMBER_OF_PROMPTS = 999999

    def __init__(self, parent, item_class):
        QtWidgets.QWidget.__init__(self, parent)
        self.engine = None  # type: Engine
        self._item_class = item_class
        self.ui = self.generate_ui()
        self._display_details_widgets = self._create_and_add_display_details_widgets()

        self._prompt_type = self._determine_selected_prompt_type()
        self._last_selected_city = ''

        self._country_selection = None
        self._admin_region_major_selection = None
        self._admin_region_minor_selection = None
        self._city_selection = None
        self._selection_created = False
        self.ui.label_city_population.setVisible(False)
        self._last_selected_item = None

        self.create_ui_connections()

    def generate_ui(self):
        from dialogs.select_item.select_admin_entity.moc_select_admin_entity_widget \
            import Ui_select_admin_entity_widget
        ui = Ui_select_admin_entity_widget()
        ui.setupUi(self)
        return ui

    def set_engine(self, engine):
        self.engine = engine
        self._reload_after_engine_changed()

    def select_item(self, item: Union[Item, None]):
        country = None
        region_major = None
        region_minor = None
        city = None

        self._city_selection['combobox'].lineEdit().setText('')
        self._admin_region_minor_selection['combobox'].lineEdit().setText('')
        self._admin_region_major_selection['combobox'].lineEdit().setText('')
        self._country_selection['combobox'].lineEdit().setText('')

        if item is not None:
            if item.is_from_prompts_database():
                self.ui.radioButton_prompt_database.setChecked(True)
            else:
                self.ui.radioButton_own_database.setChecked(True)
            self.update_all()

            if isinstance(item, City):
                city = item
                region_minor = city.region_minor
                region_major = region_minor.region_major
                country = region_major.country
            elif isinstance(item, RegionMinor):
                region_minor = item
                region_major = region_minor.region_major
                country = region_major.country
            elif isinstance(item, RegionMajor):
                region_major = item
                country = region_major.country

            elif isinstance(item, Country):
                country = item
            else:
                raise Exception(f'Unexpected item type "{item.__class__.__name__}"!')
        self._last_selected_item = item
        self._display_detials_for_item(item)

        if country:
            self._country_selection['combobox'].lineEdit().setText(country.name)
        if region_major:
            self._admin_region_major_selection['combobox'].lineEdit().setText(region_major.name)
        if region_minor:
            self._admin_region_minor_selection['combobox'].lineEdit().setText(region_minor.name)
        if city:
            self._city_selection['combobox'].lineEdit().setText(city.name)

    def _reload_after_engine_changed(self):
        # TODO handle engine reload correctly
        if not self._selection_created:
            self._country_selection = self._prepare_country_selection()
            self._admin_region_major_selection = self._prepare_admin_region_major_selection()
            self._admin_region_minor_selection = self._prepare_admin_region_minor_selection()
            self._city_selection = self._prepare_city_selection()
            self._selection_created = True
        else:
            # don't know if it is necessary, but shouldn't harm
            self._country_selection['selected'] = None
            self._admin_region_major_selection['selected'] = None
            self._admin_region_minor_selection['selected'] = None
            self._city_selection['selected'] = None
            self.update_all()

    def create_ui_connections(self):
        self.ui.comboBox_countries.editTextChanged.connect(self._country_text_changed)
        self.ui.comboBox_admin_regions_major.editTextChanged.connect(self._admin_region_major_text_changed)
        self.ui.comboBox_admin_regions_minor.editTextChanged.connect(self._admin_region_minor_text_changed)
        self.ui.comboBox_cities.editTextChanged.connect(self._city_text_changed)

        self.ui.radioButton_prompt_database.clicked.connect(self.update_all)
        self.ui.radioButton_own_database.clicked.connect(self.update_all)
        self.ui.radioButton_both_databases.clicked.connect(self.update_all)
        self.ui.pushButton_clear.clicked.connect(self._clear_inputs)

    @staticmethod
    def _set_items_for_combobox_and_completer(selection, items):
        selection['items'] = items
        combobox = selection['combobox']
        txt_entries = [item.name for item in items]

        selection['txt_entries'] = txt_entries
        combobox.clear()
        combobox.addItems([''])
        combobox.addItems(txt_entries)
        completer = QtWidgets.QCompleter(txt_entries)
        selection['completer'] = completer
        combobox.setCompleter(completer)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)

    @staticmethod
    def _create_common_selection_and_setup(combobox, items, info_label):
        selection = {
            'items': None,
            'txt_entries': [],
            'completer': None,
            'combobox': combobox,
            'selected': None,
            'info_label': info_label,
            'last_update_config': (None, None),
        }
        SelectAdminEntityWidget._set_items_for_combobox_and_completer(selection, items)
        combobox.setStyleSheet("QComboBox {background: white;}")
        return selection

    @staticmethod
    def _validate_selection_text(selection):
        text = selection['combobox'].lineEdit().text()
        palette = QPalette()
        try:
            item_index = selection['txt_entries'].index(text)
            selected_item = selection['items'][item_index]
            if isinstance(selected_item, Country):
                additional_info = ''
                # if selected_item.continent_code:
                #     additional_info = 'continent ' + selected_item.continent_code
                # else:
                #     additional_info = ''
            else:
                non_ascii_name = selected_item.non_ascii_name
                if non_ascii_name != text:
                    additional_info = selected_item.non_ascii_name
                else:
                    additional_info = ''
            selection['info_label'].setText(additional_info)
            selection['selected'] = selected_item
            try:
                if not selected_item.is_from_prompts_database():
                    palette.setColor(QPalette.Base, colors.LineEditColors.EXISTING_ITEM_COLOR)
                else:
                    palette.setColor(QPalette.Base, colors.LineEditColors.ITEM_FROM_PROMPT_COLOR)
            except:
                pass
        except ValueError:
            if text != '':
                selection['info_label'].setText('New item...')
            else:
                selection['info_label'].setText('')
            selection['selected'] = None
            palette.setColor(QPalette.Base, colors.LineEditColors.NEW_ITEM_COLOR)
        selection['combobox'].setPalette(palette)

    def get_youngest_parent_for_type(self, location_type):
        parent_is_new = False
        parent = None
        if self._country_selection['selected'] is not None:
            parent_is_new = False
            parent = self._country_selection['selected']
        elif self._country_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        if location_type == RegionMajor:
            return parent_is_new, parent

        if self._admin_region_major_selection['selected'] is not None:
            parent_is_new = False
            parent = self._admin_region_major_selection['selected']
        elif self._admin_region_major_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        if location_type == RegionMinor:
            return parent_is_new, parent

        if self._admin_region_minor_selection['selected'] is not None:
            parent_is_new = False
            parent = self._admin_region_minor_selection['selected']
        elif self._admin_region_minor_selection['combobox'].lineEdit().text() != '':
            parent_is_new = True
            parent = None
        return parent_is_new, parent

    def _get_youngest_selected_item(self):
        if self._city_selection['selected']:
            return self._city_selection['selected']
        elif self._admin_region_minor_selection['selected']:
            return self._admin_region_minor_selection['selected']
        elif self._admin_region_major_selection['selected']:
            return self._admin_region_major_selection['selected']
        elif self._country_selection['selected']:
            return self._country_selection['selected']
        else:
            return None

    def _selected_location_changed(self):
        youngest_item = self._get_youngest_selected_item()
        self._last_selected_item = youngest_item
        self._display_detials_for_item(self._last_selected_item)
        self.signal_item_selected.emit(self._last_selected_item)

    def _display_detials_for_item(self, item):
        for item_class, display_details_widget in self._display_details_widgets.items():
            show = (isinstance(item, item_class))
            display_details_widget.setVisible(show)
            if show:
                display_details_widget.display_item(item)

    def get_last_selected_item(self):
        return self._last_selected_item

    #country
    def _prepare_country_selection(self):
        combobox = self.ui.comboBox_countries
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=get_country_prompts(engine=self.engine, prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_country_info)
        return selection

    @Slot(str)
    def _country_text_changed(self, text):
        selection = self._country_selection
        self._validate_selection_text(selection=selection)

        self._update_admin_regions_major()
        self._validate_selection_text(self._admin_region_major_selection)
        self._update_admin_regions_minor()
        self._validate_selection_text(self._admin_region_minor_selection)
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_country_from_region_major(self, region_major):
        selection = self._country_selection
        selection['combobox'].blockSignals(True)
        for i, country in enumerate(selection['items']):
            if country == region_major.country:
                selection['combobox'].setCurrentIndex(i + 1)  # add 1 for empty line
                self._validate_selection_text(selection=selection)
                break
        selection['combobox'].blockSignals(False)

    #admin major
    def _prepare_admin_region_major_selection(self):
        combobox = self.ui.comboBox_admin_regions_major
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=get_region_major_prompts(engine=self.engine, prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_admin_major_info)
        return selection

    def _update_admin_regions_major(self):
        selection = self._admin_region_major_selection
        selection['combobox'].blockSignals(True)
        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(RegionMajor)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new:
                items = []
            else:
                items = get_region_major_prompts(engine=self.engine, prompt_type=self._prompt_type, parent=parent)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _admin_region_major_text_changed(self):
        selection = self._admin_region_major_selection
        self._validate_selection_text(selection=selection)
        if selection['selected'] is not None:
            self._set_country_from_region_major(selection['selected'])
            self._update_admin_regions_major()
        self._update_admin_regions_minor()
        self._validate_selection_text(self._admin_region_minor_selection)
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_admin_region_major_from_region_minor(self, region_minor: RegionMinor):
        region_major = region_minor.region_major
        selection = self._admin_region_major_selection
        selection['combobox'].blockSignals(True)
        selection['combobox'].lineEdit().setText(region_major.name)
        self._validate_selection_text(selection=selection)
        selection['combobox'].blockSignals(False)
        self._set_country_from_region_major(region_major)

    # admin region minor
    def _prepare_admin_region_minor_selection(self):
        combobox = self.ui.comboBox_admin_regions_minor
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=get_region_minor_prompts(engine=self.engine, prompt_type=self._prompt_type),
            info_label=self.ui.label_selected_admin_minor_info)
        return selection

    def _update_admin_regions_minor(self):
        selection = self._admin_region_minor_selection
        selection['combobox'].blockSignals(True)

        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(RegionMinor)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new:
                items = []
            else:
                items = get_region_minor_prompts(engine=self.engine, prompt_type=self._prompt_type, parent=parent)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _admin_region_minor_text_changed(self):
        selection = self._admin_region_minor_selection
        text = selection['combobox'].lineEdit().text()
        self._validate_selection_text(selection=selection)
        if selection['selected'] is not None:
            self._set_admin_region_major_from_region_minor(selection['selected'])
            self._update_admin_regions_minor()
        self._update_city()
        self._validate_selection_text(self._city_selection)
        self._selected_location_changed()

    def _set_admin_region_minor_from_city(self, city: City):
        region_minor = city.region_minor
        selection = self._admin_region_minor_selection
        selection['combobox'].blockSignals(True)
        selection['combobox'].lineEdit().setText(region_minor.name)
        self._validate_selection_text(selection=selection)
        selection['combobox'].blockSignals(False)
        self._set_admin_region_major_from_region_minor(region_minor)

    # city
    def _prepare_city_selection(self):
        combobox = self.ui.comboBox_cities
        items = []
        selection = self._create_common_selection_and_setup(
            combobox=combobox,
            items=items,
            info_label=self.ui.label_selected_city_info)
        return selection

    def _update_city(self):
        selection = self._city_selection
        selection['combobox'].blockSignals(True)
        text = selection['combobox'].lineEdit().text()
        parent_is_new, parent = self.get_youngest_parent_for_type(City)
        if selection['last_update_config'] != (parent_is_new, parent):
            if parent_is_new or not parent:
                items = []
            else:
                items = get_city_prompts(engine=self.engine, prompt_type=self._prompt_type, parent=parent)
            selection['last_update_config'] = parent_is_new, parent
            self._set_items_for_combobox_and_completer(selection, items)
        selection['combobox'].lineEdit().setText(text)
        selection['combobox'].blockSignals(False)

    @Slot()
    def _city_text_changed(self):
        selection = self._city_selection
        self._validate_selection_text(selection=selection)
        text = selection['combobox'].lineEdit().text()
        if selection['selected'] is not None:
            self._set_admin_region_minor_from_city(selection['selected'])
            self._update_city()
        self._selected_location_changed()

    def _determine_selected_prompt_type(self):
        if self.ui.radioButton_own_database.isChecked():
            return PromptSourceType.TRAVELLER_DATABASE
        elif self.ui.radioButton_prompt_database.isChecked():
            return PromptSourceType.PROMPT_DATABASE
        else:
            return PromptSourceType.ALL_AVAILABLE

    @Slot()
    def _clear_inputs(self):
        self.select_item(None)

    def update_all(self):
        self._prompt_type = self._determine_selected_prompt_type()

        self._admin_region_major_selection['last_update_config'] = None, None
        self._admin_region_minor_selection['last_update_config'] = None, None
        self._city_selection['last_update_config'] = None, None

        current_country_text = self.ui.comboBox_countries.lineEdit().text()

        # to reload after changing prompt type
        self._country_selection['combobox'].blockSignals(True)
        self._country_selection = self._prepare_country_selection()
        self._country_selection['combobox'].blockSignals(False)
        self.ui.comboBox_countries.lineEdit().setText('')
        self.ui.comboBox_countries.lineEdit().setText(current_country_text)

        self._country_text_changed(current_country_text)

        # TODO wouldn't it be called in country_text_changed? Delete later
        # self._admin_region_major_text_changed()
        # self._admin_region_minor_text_changed()
        # self._city_text_changed()

    def _create_and_add_display_details_widgets(self):
        classes = [Country, RegionMajor, RegionMinor, City]
        display_details_widgets = {
            item_class: ItemDisplayWidget(parent=self, item_class=item_class, is_editable=False) for item_class in classes
        }
        for display_details_widget in display_details_widgets.values():
            display_details_widget.setVisible(False)
            self.ui.horizontalLayout_item_details.addWidget(display_details_widget)
        return display_details_widgets


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    engine = get_dummy_engine()

    w = SelectAdminEntityWidget(parent=None, item_class=AdminEntity)
    w.set_engine(engine=engine)
    w.show()


    sys.exit(app.exec_())

