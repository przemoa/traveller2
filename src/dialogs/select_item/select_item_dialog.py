import logging
import time
from typing import List, Union

from PySide2 import QtWidgets, QtCore
from PySide2.QtCore import Slot, QCoreApplication, Qt
from PySide2.QtWidgets import QMessageBox

from dialogs.select_item.select_admin_entity.select_admin_entity_widget import SelectAdminEntityWidget
from engine.engine import Engine
from items.airport import Airport
from items.item import Item
from items.journey import Journey
from items.location.admin_entity.admin_entity import AdminEntity
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items.visit import Visit
from items_utils.copy_items import copy_prompt_item_with_parents_and_belongings
from tabs.common_ui.display_item.item_display_widget import ItemDisplayWidget
from tabs.common_ui.filter_item.item_filter_widget import ItemFilterWidget
from utils.dummy_engine import get_dummy_engine

# TODO there is a problem with memory leak after deleting widget. Instead of deleting, widgets are cached

ITEM_TYPES_WITH_SPECIAL_FILTER_AND_DISPLAY_WIDGETS = {
    AdminEntity: SelectAdminEntityWidget,
}


class SelectItemDialog(QtWidgets.QDialog):
    """ Allows to select item from one or multiple normal classes (Journey, Visit, Place, ...),
    but also from special classes, e.g. AdminEntity

    """

    def __init__(self,
                 parent,
                 allowed_item_types: List):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = self.generate_ui()
        self.was_canceled = None
        self._selected_item = None
        self.engine = None  # type: Engine
        self._prepared_at_least_once = False

        self._allowed_item_types = allowed_item_types
        self._check_allowed_item_types(allowed_item_types)

        self._cached_filter_widgets = {}
        self._cached_display_widgets = {}
        self._cached_special_filter_and_display_widgets = {}

        # for item_type in allowed_item_types:
        #     assert item_type in ALL_ALLOWED_TYPES_WIDGETS
        self._currently_selected_item_type = self._allowed_item_types[0]

        self._current_filter_widget = None  # type: ItemFilterWidget
        self._current_display_widget = None  # type: ItemDisplayWidget
        self._current_special_filter_and_display_widget = None  # type ...

        self._items_in_item_type_combobox = self._prepare_allowed_type_combobox()
        self._select_item_type_in_combobox(self._currently_selected_item_type)
        self._set_title()
        self.create_ui_connections()
        self._last_engine_modification_time = None

    def _check_allowed_item_types(self, allowed_item_types):
        if AdminEntity in allowed_item_types:
            if len({Country, RegionMajor, RegionMinor, City}.intersection(set(allowed_item_types))) != 0:
                raise Exception('Cannot select both AdmiEntity and subtype of AdminEntity in one dialog! '
                                '(E.g. selected country could be interpreted both as Country and AdminEntity')

        for i, item_type_1 in enumerate(allowed_item_types):
            for j, item_type_2 in enumerate(allowed_item_types):
                if i == j:
                    continue
                if issubclass(item_type_1, item_type_2) or issubclass(item_type_2, item_type_1):
                    raise Exception(f'Cannot select at once two classes on same inheritance line, '
                                    f'because selected item cannot be classified to only one category. '
                                    f'Conflict for: {item_type_1.__name__} and {item_type_2.__name__}')

    def set_engine(self, engine: Engine):
        if self.engine is None or self.engine.last_engine_modification_time != self._last_engine_modification_time:
            if self.engine is not None:
                logging.info('Reloading engine in SelectItemDialog')
            self._last_engine_modification_time = engine.last_engine_modification_time
            self.engine = engine
            self._reload_after_engine_changed()
        # else engine is same and not modified, do nothing

    def _reload_after_engine_changed(self):
        if self._current_filter_widget:
            self._current_filter_widget.set_engine(self.engine)
        if self._current_display_widget:
            self._current_display_widget.set_engine(self.engine)
        if self._current_special_filter_and_display_widget:
            self._current_special_filter_and_display_widget.set_engine(self.engine)

    def prepare_dialog_for_selection(self,
                                     selected_item: Union[Item, None]):
        self.was_canceled = False

        if not self._prepared_at_least_once:
            self._prepared_at_least_once = True
            if selected_item is None:
                self._set_new_allowed_item_type(self._currently_selected_item_type)

        # new_selected_item_type = self._currently_selected_item_type

        if selected_item is not None:
            self._selected_item = selected_item
            # assert selected_at_start_item.__class__ in ALL_ALLOWED_TYPES_WIDGETS
            if selected_item.__class__ in self._allowed_item_types:
                new_selected_item_type = selected_item.__class__
            else:
                for allowed_item_type in self._allowed_item_types:
                    if issubclass(selected_item.__class__, allowed_item_type):
                        new_selected_item_type = allowed_item_type
                        break  # not expected to encounter second hit, because of check in _check_allowed_item_types
                else:
                    raise Exception(f'Cannot match item class ({selected_item.__class__.__name__}) '
                                    f'for selected item with allowed item classes!')
            self.ui.comboBox_item_type.blockSignals(True)
            self._select_item_type_in_combobox(new_selected_item_type)
            self.ui.comboBox_item_type.blockSignals(False)
            self._set_new_allowed_item_type(new_selected_item_type)  # this will be called in slot after changing item_type in combobox

            if self._currently_selected_item_type in ITEM_TYPES_WITH_SPECIAL_FILTER_AND_DISPLAY_WIDGETS:
                self._current_special_filter_and_display_widget.select_item(selected_item)
            else:
                self._current_filter_widget.reload_and_select_displayed_item(selected_item)
                self._current_display_widget.display_item(selected_item)
        else:
            if self._current_special_filter_and_display_widget:
                self._selected_item = self._current_special_filter_and_display_widget.get_last_selected_item()
            if self._current_filter_widget:  # TODO - is it necessary? I don't think so - update = apparentyl it is (#120)
                self._selected_item = self._current_filter_widget.get_last_selected_item()

        self._set_button_state()

    def _prepare_allowed_type_combobox(self):
        if len(self._allowed_item_types) == 1:
            self.ui.widget_select_item_type.setVisible(False)
            self.ui.line_upper.setVisible(False)
            return
        items_in_item_type_combobox = [None] + self._allowed_item_types
        self.ui.comboBox_item_type.clear()
        self.ui.comboBox_item_type.addItems([''])
        txt_entries = [item_type.__name__ for item_type in self._allowed_item_types]
        self.ui.comboBox_item_type.addItems(txt_entries)
        return items_in_item_type_combobox

    def _set_button_state(self):
        self.ui.pushButton_select.setEnabled(self._selected_item is not None)

    def _select_item_type_in_combobox(self, selected_item_type):
        if len(self._allowed_item_types) == 1:
            return

        if selected_item_type is not None:
            index = self._items_in_item_type_combobox.index(selected_item_type)
            self.ui.comboBox_item_type.setCurrentIndex(index)

    def _set_new_allowed_item_type(self, item_type):
        """ update widgets and show, remove old ones """

        if self._currently_selected_item_type in ITEM_TYPES_WITH_SPECIAL_FILTER_AND_DISPLAY_WIDGETS:
            self.ui.horizontalLayout_for_special_widget.removeWidget(
                self._current_special_filter_and_display_widget)
            if self._current_special_filter_and_display_widget is not None:
                self._current_special_filter_and_display_widget.setVisible(False)
            self._current_special_filter_and_display_widget = None
            assert self._current_filter_widget is None
        else:
            if self._current_filter_widget is not None:
                self.ui.verticalLayout_filter_widget.removeWidget(self._current_filter_widget)
                self._current_filter_widget.setVisible(False)
                self._current_filter_widget = None
            if self._current_display_widget is not None:
                self.ui.verticalLayout_display_widget.removeWidget(self._current_display_widget)
                self._current_display_widget.setVisible(False)
                self._current_display_widget = None

        self._currently_selected_item_type = item_type

        if item_type is not None:
            if item_type in ITEM_TYPES_WITH_SPECIAL_FILTER_AND_DISPLAY_WIDGETS:
                self.ui.widget_splitter_filter_details.setVisible(False)
                # self.ui.label_details.setVisible(False)
                # self.ui.label_filters.setVisible(False)
                self._current_special_filter_and_display_widget = \
                    self._get_cached_or_create_special_filter_and_display_widget(item_type)
                self._current_special_filter_and_display_widget.set_engine(self.engine)
                self.ui.horizontalLayout_for_special_widget.addWidget(
                    self._current_special_filter_and_display_widget)
            else:
                self.ui.widget_splitter_filter_details.setVisible(True)
                # self.ui.label_details.setVisible(True)
                # self.ui.label_filters.setVisible(True)
                self._current_filter_widget = self._get_cached_or_create_filter_widget(item_type)
                self._current_filter_widget.set_engine(self.engine)

                self._current_display_widget = self._get_cached_or_create_display_widget(item_type)
                self._current_display_widget.set_engine(self.engine)

                if self._current_filter_widget:
                    self.ui.verticalLayout_filter_widget.addWidget(self._current_filter_widget)
                if self._current_display_widget:
                    self.ui.verticalLayout_display_widget.addWidget(self._current_display_widget)

    def _get_cached_or_create_filter_widget(self, item_type):
        widget = self._cached_filter_widgets.get(item_type, None)
        if widget is None:
            widget = ItemFilterWidget(parent=self, item_class=item_type, allow_for_prompts=True)
            self._cached_filter_widgets[item_type] = widget
            widget.signal_item_selected.connect(self._selected_item_changed)
        else:
            widget.setVisible(True)
            self._selected_item_changed(widget.get_last_selected_item())
        return widget

    def _get_cached_or_create_special_filter_and_display_widget(self, item_type):
        widget = self._cached_special_filter_and_display_widgets.get(item_type, None)
        if widget is None:
            widget_class = ITEM_TYPES_WITH_SPECIAL_FILTER_AND_DISPLAY_WIDGETS[item_type]
            widget = widget_class(parent=self, item_class=item_type)
            self._cached_special_filter_and_display_widgets[item_type] = widget
            widget.signal_item_selected.connect(self._selected_item_changed)
        else:
            widget.setVisible(True)
            self._selected_item_changed(widget.get_last_selected_item())
        return widget

    def _get_cached_or_create_display_widget(self, item_type):
        widget = self._cached_display_widgets.get(item_type, None)
        if widget is None:
            widget = ItemDisplayWidget(parent=self, item_class=item_type, is_editable=False)
            self._cached_display_widgets[item_type] = widget
        else:
            widget.setVisible(True)
        return widget

    def generate_ui(self):
        from dialogs.select_item.moc_select_item_dialog import Ui_select_item_dialog
        ui = Ui_select_item_dialog()
        ui.setupUi(self)
        return ui

    def _set_title(self):
        if len(self._allowed_item_types) == 1:
            txt = 'Select {}...'.format(self._allowed_item_types[0].__name__)
        else:
            txt = 'Select Item...'
        self.setWindowTitle(txt)

    @Slot(Item)
    def _selected_item_changed(self, item):
        self._selected_item = item
        if self._current_display_widget is not None:
            self._current_display_widget.display_item(item)
        self._set_button_state()

    def get_selected_item(self) -> Item:
        return self._selected_item

    @Slot(int)
    def _selected_index_in_combobox_changed(self, index):
        new_item_type = self._items_in_item_type_combobox[index]
        self._selected_item = None
        self._set_new_allowed_item_type(new_item_type)

    def create_ui_connections(self):
        self.ui.pushButton_select.clicked.connect(self._select_clicked)
        self.ui.pushButton_deselect.clicked.connect(self._deselect_clicked)
        self.ui.pushButton_cancel.clicked.connect(self._cancel_clicked)
        self.ui.comboBox_item_type.currentIndexChanged.connect(self._selected_index_in_combobox_changed)

    def _select_clicked(self):
        self.hide()

    def _deselect_clicked(self):
        self._selected_item = None
        self.hide()

    def _cancel_clicked(self):
        self.was_canceled = True
        self.hide()

    def closeEvent(self, event):
        event.ignore()
        self._cancel_clicked()

    def keyPressEvent(self, event):
        if event.key() != Qt.Key_Escape:
            super().keyPressEvent(event)
        else:
            self._cancel_clicked()


def select_item_from_dialog(parent,
                            engine: Engine,
                            allowed_item_types: List[Item],
                            selected_at_start_item: Union[Item, None] = None,
                            allow_for_prompts_selection: bool = False):
    assert len(allowed_item_types) >= 1
    # key = (parent, tuple(allowed_item_types))
    key = tuple(allowed_item_types)

    widget = select_item_from_dialog.cached_dialogs.get(key, None)
    if widget is None:
        widget = SelectItemDialog(parent=parent,
                                  allowed_item_types=allowed_item_types)
        select_item_from_dialog.cached_dialogs[key] = widget
        widget.setWindowModality(QtCore.Qt.ApplicationModal)
    widget.set_engine(engine)
    widget.prepare_dialog_for_selection(selected_item=selected_at_start_item)
    widget.show()
    while not widget.isHidden():
        QCoreApplication.processEvents()
        time.sleep(0.05)  # without sleep, 100% of the CPU would be used

    if widget.was_canceled:
        return selected_at_start_item
    else:
        selected_item = widget.get_selected_item()
        if selected_item and not allow_for_prompts_selection and selected_item.is_from_prompts_database():
            logging.info('Prompt selected from SelectItemDialog')
            reply = QMessageBox.question(parent, "Confirm",
                                         'Cannot select prompt. Would you like to copy item(s) '
                                         'to traveller database first?\n'
                                         '[To avoid this message set ask_before_copying_prompt_to_traveller_db in settings]')
            if reply != QMessageBox.StandardButton.Yes:
                return None

            new_selected_item = copy_prompt_item_with_parents_and_belongings(collection=engine.collection,
                                                                             item=selected_item)
            engine.set_engine_as_modified()
            return new_selected_item
        else:
            return selected_item


select_item_from_dialog.cached_dialogs = {}  # dialog is cached for item type. Key = tuple(item_types)


if __name__ == '__main__':
    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    engine = get_dummy_engine()

    # w1 = SelectItemDialog(parent=None, allowed_item_types=[Journey, Visit])
    # w1.setWindowModality(QtCore.Qt.ApplicationModal)
    # w2 = SelectItemDialog(parent=None, allowed_item_types=[Journey, Visit])
    # w1.show()
    # w2.show()

    # w.prepare_dialog_for_selection(engine=engine, selected_item=None)
    print(select_item_from_dialog(parent=None,
                                  engine=engine,
                                  allowed_item_types=[Journey, Visit, AdminEntity, Airport],
                                  selected_at_start_item=engine.collection.visits[0],
                                  # selected_at_start_item=engine.collection.regions_minor[0],
                                  ))
    # select_item_from_dialog(parent=None, engine=engine, allowed_item_types=[Journey, Visit])
    sys.exit(app.exec_())
