# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.custom_datetime import CustomDateTime


@dataclass
class PersonContent(ItemContent):
    name: str = None
    type: Union[str, None] = None

    def validate_fields_type(self):
        super(PersonContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=PersonContent.__annotations__["name"])
        typeguard.check_type(argname="field \"type\"",
                             value=self.type,
                             expected_type=PersonContent.__annotations__["type"])


class Person(Item):
    def __init__(
            self, 
            name: str,
            type: Union[str, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.type = type  # type: str  # 
        if referenced_by is None:
            referenced_by = []
        self._referenced_by = referenced_by  # type: List[Item]  # 

    @property
    def referenced_by(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._referenced_by

    def serialize(self) -> PersonContent:
        data_class = PersonContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return self.name + (" (" + self.type + ")" if self.type else "")

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None)  # None just for a moment, will be overwritten below
        super(Person, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.type = copy.deepcopy(self.type)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.name == item.name)

    def _write_to_data_class(self, data_class):
        super(Person, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"Person\"")
        data_class.type = self.type

    def _read_from_data_class(self, data_class):
        super(Person, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.type = data_class.type
