# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items.location.admin_entity.admin_entity import AdminEntity


@dataclass
class PlaceContent(ItemContent):
    name: str = None
    type: Union[str, None] = None
    coordinates: Union[Tuple[float, float], None] = None
    location: Union[int, None] = None

    def validate_fields_type(self):
        super(PlaceContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=PlaceContent.__annotations__["name"])
        typeguard.check_type(argname="field \"type\"",
                             value=self.type,
                             expected_type=PlaceContent.__annotations__["type"])
        typeguard.check_type(argname="field \"coordinates\"",
                             value=self.coordinates,
                             expected_type=PlaceContent.__annotations__["coordinates"])
        typeguard.check_type(argname="field \"location\"",
                             value=self.location,
                             expected_type=PlaceContent.__annotations__["location"])


class Place(Item):
    def __init__(
            self, 
            name: str,
            type: Union[str, None] = None,
            coordinates: Union[Coordinates, None] = None,
            location: Union[AdminEntity, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.type = type  # type: str  # 
        self.coordinates = coordinates  # type: Coordinates  # 
        self._location = location  # type: AdminEntity  # 
        if referenced_by is None:
            referenced_by = []
        self._referenced_by = referenced_by  # type: List[Item]  # 

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, value: Union[Item, None]):
        if self._location is not None and self.item_id:
            assert isinstance(self._location, Item)
            self._location.referenced_by.remove(self)

        self._location = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def referenced_by(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._referenced_by

    def serialize(self) -> PlaceContent:
        data_class = PlaceContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.name}"

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None)  # None just for a moment, will be overwritten below
        super(Place, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.type = copy.deepcopy(self.type)
        new_item.coordinates = copy.deepcopy(self.coordinates)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.name == item.name)

    def delete_from_belongings_and_referenced_by(self):
        self.location = None

    def _write_to_data_class(self, data_class):
        super(Place, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"Place\"")
        data_class.type = self.type
        data_class.coordinates = Coordinates.to_tuple_from_instance(self.coordinates)
        data_class.location = Item.get_id_for_item(self._location)

    def _read_from_data_class(self, data_class):
        super(Place, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.type = data_class.type
        self.coordinates = Coordinates.from_tuple(data_class.coordinates)
