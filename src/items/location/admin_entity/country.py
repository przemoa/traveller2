# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime


@dataclass
class CountryContent(AdminEntityContent):
    name: str = None
    iso_code: str = None

    def validate_fields_type(self):
        super(CountryContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=CountryContent.__annotations__["name"])
        typeguard.check_type(argname="field \"iso_code\"",
                             value=self.iso_code,
                             expected_type=CountryContent.__annotations__["iso_code"])


class Country(AdminEntity):
    def __init__(
            self, 
            name: str,
            iso_code: str,
            belongings: Union[List[Item], None] = None,
            coordinates: Union[Coordinates, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                coordinates=coordinates,
                referenced_by=referenced_by,
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.iso_code = iso_code  # type: str  # 
        if belongings is None:
            belongings = []
        self._belongings = belongings  # type: List[Item]  # List of Regions Major

    @property
    def belongings(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._belongings

    def serialize(self) -> CountryContent:
        data_class = CountryContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None,
            iso_code=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.name} ({self.iso_code})"

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None,
                iso_code=None)  # None just for a moment, will be overwritten below
        super(Country, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.iso_code = copy.deepcopy(self.iso_code)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.name == item.name)

    def _write_to_data_class(self, data_class):
        super(Country, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"Country\"")
        data_class.iso_code = self.iso_code
        if self.iso_code is None:
            raise ValueError("Cannot save item, field \"iso_code\" is required for class \"Country\"")

    def _read_from_data_class(self, data_class):
        super(Country, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.iso_code = data_class.iso_code
