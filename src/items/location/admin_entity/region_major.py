# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items.location.admin_entity.country import Country


@dataclass
class RegionMajorContent(AdminEntityContent):
    name: str = None
    non_ascii_name: Union[str, None] = None
    geoname_code: str = None
    country: Union[int, None] = None

    def validate_fields_type(self):
        super(RegionMajorContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=RegionMajorContent.__annotations__["name"])
        typeguard.check_type(argname="field \"non_ascii_name\"",
                             value=self.non_ascii_name,
                             expected_type=RegionMajorContent.__annotations__["non_ascii_name"])
        typeguard.check_type(argname="field \"geoname_code\"",
                             value=self.geoname_code,
                             expected_type=RegionMajorContent.__annotations__["geoname_code"])
        typeguard.check_type(argname="field \"country\"",
                             value=self.country,
                             expected_type=RegionMajorContent.__annotations__["country"])


class RegionMajor(AdminEntity):
    def __init__(
            self, 
            name: str,
            geoname_code: str,
            non_ascii_name: Union[str, None] = None,
            country: Union[Country, None] = None,
            belongings: Union[List[Item], None] = None,
            coordinates: Union[Coordinates, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                coordinates=coordinates,
                referenced_by=referenced_by,
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.non_ascii_name = non_ascii_name  # type: str  # 
        self.geoname_code = geoname_code  # type: str  # 
        self._country = country  # type: Country  # 
        if belongings is None:
            belongings = []
        self._belongings = belongings  # type: List[Item]  # List of Regions Minor

    @property
    def country(self):
        return self._country

    @country.setter
    def country(self, value: Union[Item, None]):
        if self._country is not None and self.item_id:
            assert isinstance(self._country, Item)
            self._country.belongings.remove(self)

        self._country = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.belongings.append(self)

    @property
    def belongings(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._belongings

    def serialize(self) -> RegionMajorContent:
        data_class = RegionMajorContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None,
            geoname_code=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.name} ({self.country.iso_code})"

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None,
                geoname_code=None)  # None just for a moment, will be overwritten below
        super(RegionMajor, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.non_ascii_name = copy.deepcopy(self.non_ascii_name)
        new_item.geoname_code = copy.deepcopy(self.geoname_code)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.geoname_code == item.geoname_code)

    def delete_from_belongings_and_referenced_by(self):
        self.country = None

    def _write_to_data_class(self, data_class):
        super(RegionMajor, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"RegionMajor\"")
        data_class.non_ascii_name = self.non_ascii_name
        data_class.geoname_code = self.geoname_code
        if self.geoname_code is None:
            raise ValueError("Cannot save item, field \"geoname_code\" is required for class \"RegionMajor\"")
        data_class.country = Item.get_id_for_item(self._country)

    def _read_from_data_class(self, data_class):
        super(RegionMajor, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.non_ascii_name = data_class.non_ascii_name
        self.geoname_code = data_class.geoname_code

    def get_parent_location(self):
        return self.country
            
    def set_parent_location(self, parent_location):
        self.country = parent_location
