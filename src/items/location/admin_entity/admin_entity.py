# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime


@dataclass
class AdminEntityContent(ItemContent):
    coordinates: Union[Tuple[float, float], None] = None

    def validate_fields_type(self):
        super(AdminEntityContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"coordinates\"",
                             value=self.coordinates,
                             expected_type=AdminEntityContent.__annotations__["coordinates"])


class AdminEntity(Item):
    def __init__(
            self, 
            coordinates: Union[Coordinates, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.coordinates = coordinates  # type: Coordinates  # 
        if referenced_by is None:
            referenced_by = []
        self._referenced_by = referenced_by  # type: List[Item]  # Items other than AdminEntity items referencing it...

    @property
    def referenced_by(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._referenced_by

    def serialize(self) -> AdminEntityContent:
        data_class = AdminEntityContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls()
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return str(self)

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__()
        super(AdminEntity, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.coordinates = copy.deepcopy(self.coordinates)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        raise Exception("Cannot compare abstract (not final) class AdminEntity")

    def _write_to_data_class(self, data_class):
        super(AdminEntity, self)._write_to_data_class(data_class)
        data_class.coordinates = Coordinates.to_tuple_from_instance(self.coordinates)

    def _read_from_data_class(self, data_class):
        super(AdminEntity, self)._read_from_data_class(data_class)
        self.coordinates = Coordinates.from_tuple(data_class.coordinates)
