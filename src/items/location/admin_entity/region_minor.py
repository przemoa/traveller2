# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items.location.admin_entity.region_major import RegionMajor


@dataclass
class RegionMinorContent(AdminEntityContent):
    name: str = None
    non_ascii_name: Union[str, None] = None
    geoname_code: str = None
    region_major: Union[int, None] = None

    def validate_fields_type(self):
        super(RegionMinorContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=RegionMinorContent.__annotations__["name"])
        typeguard.check_type(argname="field \"non_ascii_name\"",
                             value=self.non_ascii_name,
                             expected_type=RegionMinorContent.__annotations__["non_ascii_name"])
        typeguard.check_type(argname="field \"geoname_code\"",
                             value=self.geoname_code,
                             expected_type=RegionMinorContent.__annotations__["geoname_code"])
        typeguard.check_type(argname="field \"region_major\"",
                             value=self.region_major,
                             expected_type=RegionMinorContent.__annotations__["region_major"])


class RegionMinor(AdminEntity):
    def __init__(
            self, 
            name: str,
            geoname_code: str,
            non_ascii_name: Union[str, None] = None,
            region_major: Union[RegionMajor, None] = None,
            belongings: Union[List[Item], None] = None,
            coordinates: Union[Coordinates, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                coordinates=coordinates,
                referenced_by=referenced_by,
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.non_ascii_name = non_ascii_name  # type: str  # 
        self.geoname_code = geoname_code  # type: str  # 
        self._region_major = region_major  # type: RegionMajor  # 
        if belongings is None:
            belongings = []
        self._belongings = belongings  # type: List[Item]  # List of Cities

    @property
    def region_major(self):
        return self._region_major

    @region_major.setter
    def region_major(self, value: Union[Item, None]):
        if self._region_major is not None and self.item_id:
            assert isinstance(self._region_major, Item)
            self._region_major.belongings.remove(self)

        self._region_major = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.belongings.append(self)

    @property
    def belongings(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._belongings

    def serialize(self) -> RegionMinorContent:
        data_class = RegionMinorContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None,
            geoname_code=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.name} ({self.region_major.country.iso_code})"

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None,
                geoname_code=None)  # None just for a moment, will be overwritten below
        super(RegionMinor, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.non_ascii_name = copy.deepcopy(self.non_ascii_name)
        new_item.geoname_code = copy.deepcopy(self.geoname_code)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.geoname_code == item.geoname_code)

    def delete_from_belongings_and_referenced_by(self):
        self.region_major = None

    def _write_to_data_class(self, data_class):
        super(RegionMinor, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"RegionMinor\"")
        data_class.non_ascii_name = self.non_ascii_name
        data_class.geoname_code = self.geoname_code
        if self.geoname_code is None:
            raise ValueError("Cannot save item, field \"geoname_code\" is required for class \"RegionMinor\"")
        data_class.region_major = Item.get_id_for_item(self._region_major)

    def _read_from_data_class(self, data_class):
        super(RegionMinor, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.non_ascii_name = data_class.non_ascii_name
        self.geoname_code = data_class.geoname_code

    def get_parent_location(self):
        return self.region_major
            
    def set_parent_location(self, parent_location):
        self.region_major = parent_location
