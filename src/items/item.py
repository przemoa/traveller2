# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy
import itertools
import typeguard

from common.types.custom_datetime import CustomDateTime


@dataclass
class ItemContent:
    item_id: int = None
    item_creation_time: Union[int, None] = None
    item_modification_time: Union[int, None] = None
    note: Union[str, None] = None

    def validate_fields_type(self):
        typeguard.check_type(argname="field \"item_id\"",
                             value=self.item_id,
                             expected_type=ItemContent.__annotations__["item_id"])
        typeguard.check_type(argname="field \"item_creation_time\"",
                             value=self.item_creation_time,
                             expected_type=ItemContent.__annotations__["item_creation_time"])
        typeguard.check_type(argname="field \"item_modification_time\"",
                             value=self.item_modification_time,
                             expected_type=ItemContent.__annotations__["item_modification_time"])
        typeguard.check_type(argname="field \"note\"",
                             value=self.note,
                             expected_type=ItemContent.__annotations__["note"])


class Item:
    def __init__(
            self, 
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        self.item_id = item_id  # type: int  # 
        self.item_creation_time = item_creation_time  # type: CustomDateTime  # 
        self.item_modification_time = item_modification_time  # type: CustomDateTime  # 
        self.note = note  # type: str  # 

    def serialize(self) -> ItemContent:
        data_class = ItemContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls()
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return str(self)

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__()
        new_item.note = copy.deepcopy(self.note)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        raise Exception("Cannot compare abstract (not final) class Item")

    def _write_to_data_class(self, data_class):
        data_class.item_id = self.item_id
        if self.item_id is None:
            raise ValueError("Cannot save item, field \"item_id\" is required for class \"Item\"")
        data_class.item_creation_time = CustomDateTime.to_unix_time_from_instance(self.item_creation_time)
        data_class.item_modification_time = CustomDateTime.to_unix_time_from_instance(self.item_modification_time)
        data_class.note = self.note

    def _read_from_data_class(self, data_class):
        self.item_id = data_class.item_id
        self.item_creation_time = CustomDateTime.from_int_or_none(data_class.item_creation_time)
        self.item_modification_time = CustomDateTime.from_int_or_none(data_class.item_modification_time)
        self.note = data_class.note

    @staticmethod
    def get_id_for_item(item):
        if not item:
            return None
        assert item.item_id is not None
        return item.item_id
    
    def is_from_prompts_database(self):
        return hasattr(self, '_is_from_prompts_database') and self._is_from_prompts_database
        
    def set_as_from_prompts_database(self):
        self._is_from_prompts_database = True
    
    def get_belongings_set_recursively(self):
        belongings_belongings = [self.belongings]
        for item in self.belongings:
            if hasattr(item, 'belongings') and item.belongings:
                belongings_belongings.append(item.get_belongings_set_recursively())
        return set(itertools.chain.from_iterable(belongings_belongings))
