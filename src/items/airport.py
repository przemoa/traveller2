# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items.location.admin_entity.admin_entity import AdminEntity


@dataclass
class AirportContent(ItemContent):
    municipality: str = None
    name: str = None
    coordinates: Union[Tuple[float, float], None] = None
    location: Union[int, None] = None

    def validate_fields_type(self):
        super(AirportContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"municipality\"",
                             value=self.municipality,
                             expected_type=AirportContent.__annotations__["municipality"])
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=AirportContent.__annotations__["name"])
        typeguard.check_type(argname="field \"coordinates\"",
                             value=self.coordinates,
                             expected_type=AirportContent.__annotations__["coordinates"])
        typeguard.check_type(argname="field \"location\"",
                             value=self.location,
                             expected_type=AirportContent.__annotations__["location"])


class Airport(Item):
    def __init__(
            self, 
            municipality: str,
            name: str,
            coordinates: Union[Coordinates, None] = None,
            location: Union[AdminEntity, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.municipality = municipality  # type: str  # 
        self.name = name  # type: str  # 
        self.coordinates = coordinates  # type: Coordinates  # 
        self._location = location  # type: AdminEntity  # 
        if referenced_by is None:
            referenced_by = []
        self._referenced_by = referenced_by  # type: List[Item]  # 

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, value: Union[Item, None]):
        if self._location is not None and self.item_id:
            assert isinstance(self._location, Item)
            self._location.referenced_by.remove(self)

        self._location = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def referenced_by(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._referenced_by

    def serialize(self) -> AirportContent:
        data_class = AirportContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            municipality=None,
            name=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return self._get_name_for_display()

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                municipality=None,
                name=None)  # None just for a moment, will be overwritten below
        super(Airport, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.municipality = copy.deepcopy(self.municipality)
        new_item.name = copy.deepcopy(self.name)
        new_item.coordinates = copy.deepcopy(self.coordinates)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.name == item.name)

    def delete_from_belongings_and_referenced_by(self):
        self.location = None

    def _write_to_data_class(self, data_class):
        super(Airport, self)._write_to_data_class(data_class)
        data_class.municipality = self.municipality
        if self.municipality is None:
            raise ValueError("Cannot save item, field \"municipality\" is required for class \"Airport\"")
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"Airport\"")
        data_class.coordinates = Coordinates.to_tuple_from_instance(self.coordinates)
        data_class.location = Item.get_id_for_item(self._location)

    def _read_from_data_class(self, data_class):
        super(Airport, self)._read_from_data_class(data_class)
        self.municipality = data_class.municipality
        self.name = data_class.name
        self.coordinates = Coordinates.from_tuple(data_class.coordinates)

    def _get_name_for_display(self):
        location_str = f" - {self.location.get_short_description()}" if self.location else ""
        name_str = self.name.replace(self.municipality, "").replace("Airport", "").strip(" ").strip("-")  
        name_str_with_braces = (" [" + name_str + "]") if name_str else ''
        return self.municipality + name_str_with_braces + location_str
        