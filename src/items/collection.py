# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from typing import List
from dataclasses import dataclass, field
from common.types.custom_datetime import CustomDateTime
from items_definitions.item_classes import ItemClassDefinition, get_item_class_name_definition
from common import settings
from utils.misc import replace_invalid_plural_forms
from items.item import Item, ItemContent
from items.journey import Journey, JourneyContent
from items.visit import Visit, VisitContent
from items.attachment.attachment import Attachment, AttachmentContent
from items.attachment.attachment import Attachment, AttachmentContent
from items.attachment.photo import Photo, PhotoContent
from items.attachment.attachment import Attachment, AttachmentContent
from items.attachment.file import File, FileContent
from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.location.admin_entity.country import Country, CountryContent
from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.location.admin_entity.region_major import RegionMajor, RegionMajorContent
from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.location.admin_entity.region_minor import RegionMinor, RegionMinorContent
from items.location.admin_entity.admin_entity import AdminEntity, AdminEntityContent
from items.location.admin_entity.city import City, CityContent
from items.airport import Airport, AirportContent
from items.flight import Flight, FlightContent
from items.location.place import Place, PlaceContent
from items.person import Person, PersonContent
from items.companion import Companion, CompanionContent


class ItemNotUniqueError(Exception):
    pass

ALL_ITEM_CLASSES = [
    Item,
    Journey,
    Visit,
    Attachment,
    Photo,
    File,
    AdminEntity,
    Country,
    RegionMajor,
    RegionMinor,
    City,
    Airport,
    Flight,
    Place,
    Person,
    Companion,
]

ALL_FINAL_ITEM_CLASSES = [
    Journey,
    Visit,
    Photo,
    File,
    Country,
    RegionMajor,
    RegionMinor,
    City,
    Airport,
    Flight,
    Place,
    Person,
    Companion,
]


@dataclass
class CollectionContent:
    next_item_id: int = 1
    journeys: List[JourneyContent] = field(default_factory=lambda: [])
    visits: List[VisitContent] = field(default_factory=lambda: [])
    photos: List[PhotoContent] = field(default_factory=lambda: [])
    files: List[FileContent] = field(default_factory=lambda: [])
    countries: List[CountryContent] = field(default_factory=lambda: [])
    regions_major: List[RegionMajorContent] = field(default_factory=lambda: [])
    regions_minor: List[RegionMinorContent] = field(default_factory=lambda: [])
    cities: List[CityContent] = field(default_factory=lambda: [])
    airports: List[AirportContent] = field(default_factory=lambda: [])
    flights: List[FlightContent] = field(default_factory=lambda: [])
    places: List[PlaceContent] = field(default_factory=lambda: [])
    persons: List[PersonContent] = field(default_factory=lambda: [])
    companions: List[CompanionContent] = field(default_factory=lambda: [])

    def update_modification_times(self, old_collection_content):
        current_date_time = CustomDateTime.now().to_unix_time()

        old_journey_collection_content_dict = {}
        for journey in old_collection_content.journeys:
            old_journey_collection_content_dict[journey.item_id] = journey
        for journey in self.journeys:
            if journey.item_creation_time is None:
                journey.item_creation_time = current_date_time
                journey.item_modification_time = current_date_time
            old_item = old_journey_collection_content_dict.get(journey.item_id, None)
            if old_item is not None and journey != old_item:
                journey.item_modification_time = current_date_time

        old_visit_collection_content_dict = {}
        for visit in old_collection_content.visits:
            old_visit_collection_content_dict[visit.item_id] = visit
        for visit in self.visits:
            if visit.item_creation_time is None:
                visit.item_creation_time = current_date_time
                visit.item_modification_time = current_date_time
            old_item = old_visit_collection_content_dict.get(visit.item_id, None)
            if old_item is not None and visit != old_item:
                visit.item_modification_time = current_date_time

        old_photo_collection_content_dict = {}
        for photo in old_collection_content.photos:
            old_photo_collection_content_dict[photo.item_id] = photo
        for photo in self.photos:
            if photo.item_creation_time is None:
                photo.item_creation_time = current_date_time
                photo.item_modification_time = current_date_time
            old_item = old_photo_collection_content_dict.get(photo.item_id, None)
            if old_item is not None and photo != old_item:
                photo.item_modification_time = current_date_time

        old_file_collection_content_dict = {}
        for file in old_collection_content.files:
            old_file_collection_content_dict[file.item_id] = file
        for file in self.files:
            if file.item_creation_time is None:
                file.item_creation_time = current_date_time
                file.item_modification_time = current_date_time
            old_item = old_file_collection_content_dict.get(file.item_id, None)
            if old_item is not None and file != old_item:
                file.item_modification_time = current_date_time

        old_country_collection_content_dict = {}
        for country in old_collection_content.countries:
            old_country_collection_content_dict[country.item_id] = country
        for country in self.countries:
            if country.item_creation_time is None:
                country.item_creation_time = current_date_time
                country.item_modification_time = current_date_time
            old_item = old_country_collection_content_dict.get(country.item_id, None)
            if old_item is not None and country != old_item:
                country.item_modification_time = current_date_time

        old_region_major_collection_content_dict = {}
        for region_major in old_collection_content.regions_major:
            old_region_major_collection_content_dict[region_major.item_id] = region_major
        for region_major in self.regions_major:
            if region_major.item_creation_time is None:
                region_major.item_creation_time = current_date_time
                region_major.item_modification_time = current_date_time
            old_item = old_region_major_collection_content_dict.get(region_major.item_id, None)
            if old_item is not None and region_major != old_item:
                region_major.item_modification_time = current_date_time

        old_region_minor_collection_content_dict = {}
        for region_minor in old_collection_content.regions_minor:
            old_region_minor_collection_content_dict[region_minor.item_id] = region_minor
        for region_minor in self.regions_minor:
            if region_minor.item_creation_time is None:
                region_minor.item_creation_time = current_date_time
                region_minor.item_modification_time = current_date_time
            old_item = old_region_minor_collection_content_dict.get(region_minor.item_id, None)
            if old_item is not None and region_minor != old_item:
                region_minor.item_modification_time = current_date_time

        old_city_collection_content_dict = {}
        for city in old_collection_content.cities:
            old_city_collection_content_dict[city.item_id] = city
        for city in self.cities:
            if city.item_creation_time is None:
                city.item_creation_time = current_date_time
                city.item_modification_time = current_date_time
            old_item = old_city_collection_content_dict.get(city.item_id, None)
            if old_item is not None and city != old_item:
                city.item_modification_time = current_date_time

        old_airport_collection_content_dict = {}
        for airport in old_collection_content.airports:
            old_airport_collection_content_dict[airport.item_id] = airport
        for airport in self.airports:
            if airport.item_creation_time is None:
                airport.item_creation_time = current_date_time
                airport.item_modification_time = current_date_time
            old_item = old_airport_collection_content_dict.get(airport.item_id, None)
            if old_item is not None and airport != old_item:
                airport.item_modification_time = current_date_time

        old_flight_collection_content_dict = {}
        for flight in old_collection_content.flights:
            old_flight_collection_content_dict[flight.item_id] = flight
        for flight in self.flights:
            if flight.item_creation_time is None:
                flight.item_creation_time = current_date_time
                flight.item_modification_time = current_date_time
            old_item = old_flight_collection_content_dict.get(flight.item_id, None)
            if old_item is not None and flight != old_item:
                flight.item_modification_time = current_date_time

        old_place_collection_content_dict = {}
        for place in old_collection_content.places:
            old_place_collection_content_dict[place.item_id] = place
        for place in self.places:
            if place.item_creation_time is None:
                place.item_creation_time = current_date_time
                place.item_modification_time = current_date_time
            old_item = old_place_collection_content_dict.get(place.item_id, None)
            if old_item is not None and place != old_item:
                place.item_modification_time = current_date_time

        old_person_collection_content_dict = {}
        for person in old_collection_content.persons:
            old_person_collection_content_dict[person.item_id] = person
        for person in self.persons:
            if person.item_creation_time is None:
                person.item_creation_time = current_date_time
                person.item_modification_time = current_date_time
            old_item = old_person_collection_content_dict.get(person.item_id, None)
            if old_item is not None and person != old_item:
                person.item_modification_time = current_date_time

        old_companion_collection_content_dict = {}
        for companion in old_collection_content.companions:
            old_companion_collection_content_dict[companion.item_id] = companion
        for companion in self.companions:
            if companion.item_creation_time is None:
                companion.item_creation_time = current_date_time
                companion.item_modification_time = current_date_time
            old_item = old_companion_collection_content_dict.get(companion.item_id, None)
            if old_item is not None and companion != old_item:
                companion.item_modification_time = current_date_time


@dataclass
class Collection:
    all_items_by_id: dict = field(default_factory=lambda: {})
    _next_item_id: int = 1
    journeys: List[Journey] = field(default_factory=lambda: [])
    visits: List[Visit] = field(default_factory=lambda: [])
    photos: List[Photo] = field(default_factory=lambda: [])
    files: List[File] = field(default_factory=lambda: [])
    countries: List[Country] = field(default_factory=lambda: [])
    regions_major: List[RegionMajor] = field(default_factory=lambda: [])
    regions_minor: List[RegionMinor] = field(default_factory=lambda: [])
    cities: List[City] = field(default_factory=lambda: [])
    airports: List[Airport] = field(default_factory=lambda: [])
    flights: List[Flight] = field(default_factory=lambda: [])
    places: List[Place] = field(default_factory=lambda: [])
    persons: List[Person] = field(default_factory=lambda: [])
    companions: List[Companion] = field(default_factory=lambda: [])

    def is_empty(self) -> bool:
        return len(self.all_items_by_id) == 0

    def get_items_list_for_class_name(self, class_name: str):
        class_var_name = ItemClassDefinition.get_member_name_for_class_name(class_name)
        member_name = replace_invalid_plural_forms(class_var_name + 's')
        return getattr(self, member_name)        

    def add_item(self, item):
        assert item.item_id is None

        item.item_id = -1  # just to pass validation below, will be changed after the next line...
        try:
            item.serialize()  # this will call validate_fields_type() internally
        except Exception as e:
            item.item_id = None  # if serialization failed, restore previous state
            raise e
        item.item_id = None  # reset it anyway

        if isinstance(item, Journey):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.belongings is None:
                raise ValueError("Cannot add item, field \"belongings\" is required")
            for other_item in self.journeys:
                if item.name == other_item.name:
                    raise ItemNotUniqueError("name has to be unique among before adding to collection!")

            # This have to be at the end, because of side effect on other items
            if item.start_location is not None:
                item.start_location.referenced_by.append(item)
            if item.companion is not None:
                item.companion.referenced_by.append(item)
            self.journeys.append(item)

        if isinstance(item, Visit):
            if item.location is None:
                raise ValueError("Cannot add item, field \"location\" is required")
            if item.belongings is None:
                raise ValueError("Cannot add item, field \"belongings\" is required")
            for other_item in self.visits:
                if item.is_same_item(other_item):
                    # Item has no unique fields and special checker will be used
                    raise ItemNotUniqueError("Item is not unique (special uniqueness checker says so). Cannot add to collection!")

            # This have to be at the end, because of side effect on other items
            if item.location is not None:
                item.location.referenced_by.append(item)
            if item.journey is not None:
                item.journey.belongings.append(item)
            if item.companion is not None:
                item.companion.referenced_by.append(item)
            self.visits.append(item)

        if isinstance(item, Attachment):
            if item.original_full_path is None:
                raise ValueError("Cannot add item, field \"original_full_path\" is required")

            # This have to be at the end, because of side effect on other items
            if item.attached_to is not None:
                item.attached_to.belongings.append(item)

        if isinstance(item, Photo):
            for other_item in self.photos:
                if item.original_full_path == other_item.original_full_path:
                    raise ItemNotUniqueError("original_full_path has to be unique among before adding to collection!")
            self.photos.append(item)

        if isinstance(item, File):
            for other_item in self.files:
                if item.original_full_path == other_item.original_full_path:
                    raise ItemNotUniqueError("original_full_path has to be unique among before adding to collection!")
            self.files.append(item)

        if isinstance(item, AdminEntity):
            if item.referenced_by is None:
                raise ValueError("Cannot add item, field \"referenced_by\" is required")

        if isinstance(item, Country):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.iso_code is None:
                raise ValueError("Cannot add item, field \"iso_code\" is required")
            if item.belongings is None:
                raise ValueError("Cannot add item, field \"belongings\" is required")
            for other_item in self.countries:
                if item.name == other_item.name:
                    raise ItemNotUniqueError("name has to be unique among before adding to collection!")
            for other_item in self.countries:
                if item.iso_code == other_item.iso_code:
                    raise ItemNotUniqueError("iso_code has to be unique among before adding to collection!")
            self.countries.append(item)

        if isinstance(item, RegionMajor):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.geoname_code is None:
                raise ValueError("Cannot add item, field \"geoname_code\" is required")
            if item.belongings is None:
                raise ValueError("Cannot add item, field \"belongings\" is required")
            for other_item in self.regions_major:
                if item.geoname_code == other_item.geoname_code:
                    raise ItemNotUniqueError("geoname_code has to be unique among before adding to collection!")

            # additional_validator_before_adding_to_db
            if not item.country:
                raise Exception('country field is required for region major!')
            for other_region_major in item.country.belongings:
                if item != other_region_major and other_region_major.name == item.name:
                    raise ItemNotUniqueError('Region major name is not unique among country!')

            # This have to be at the end, because of side effect on other items
            if item.country is not None:
                item.country.belongings.append(item)
            self.regions_major.append(item)

        if isinstance(item, RegionMinor):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.geoname_code is None:
                raise ValueError("Cannot add item, field \"geoname_code\" is required")
            if item.belongings is None:
                raise ValueError("Cannot add item, field \"belongings\" is required")
            for other_item in self.regions_minor:
                if item.geoname_code == other_item.geoname_code:
                    raise ItemNotUniqueError("geoname_code has to be unique among before adding to collection!")

            # additional_validator_before_adding_to_db
            if not item.region_major:
                raise Exception('region_major field is required for region minor!')
            for other_region_minor in item.region_major.belongings:
                if item != other_region_minor and other_region_minor.name == item.name:
                    raise ItemNotUniqueError('Region minor name is not unique among region major!')

            # This have to be at the end, because of side effect on other items
            if item.region_major is not None:
                item.region_major.belongings.append(item)
            self.regions_minor.append(item)

        if isinstance(item, City):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.geoname_code is None:
                raise ValueError("Cannot add item, field \"geoname_code\" is required")
            for other_item in self.cities:
                if item.geoname_code == other_item.geoname_code:
                    raise ItemNotUniqueError("geoname_code has to be unique among before adding to collection!")

            # additional_validator_before_adding_to_db
            if not item.region_minor:
                raise Exception('region_minor field is required for city!')
            for other_city in item.region_minor.belongings:
                if item != other_city and other_city.name == item.name:
                    raise ItemNotUniqueError('City name is not unique among region minor!')

            # This have to be at the end, because of side effect on other items
            if item.region_minor is not None:
                item.region_minor.belongings.append(item)
            self.cities.append(item)

        if isinstance(item, Airport):
            if item.municipality is None:
                raise ValueError("Cannot add item, field \"municipality\" is required")
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.referenced_by is None:
                raise ValueError("Cannot add item, field \"referenced_by\" is required")
            for other_item in self.airports:
                if item.name == other_item.name:
                    raise ItemNotUniqueError("name has to be unique among before adding to collection!")

            # This have to be at the end, because of side effect on other items
            if item.location is not None:
                item.location.referenced_by.append(item)
            self.airports.append(item)

        if isinstance(item, Flight):
            if item.starting_airport is None:
                raise ValueError("Cannot add item, field \"starting_airport\" is required")
            if item.destination_airport is None:
                raise ValueError("Cannot add item, field \"destination_airport\" is required")
            for other_item in self.flights:
                if item.is_same_item(other_item):
                    # Item has no unique fields and special checker will be used
                    raise ItemNotUniqueError("Item is not unique (special uniqueness checker says so). Cannot add to collection!")

            # This have to be at the end, because of side effect on other items
            if item.attached_to is not None:
                item.attached_to.belongings.append(item)
            if item.starting_airport is not None:
                item.starting_airport.referenced_by.append(item)
            if item.destination_airport is not None:
                item.destination_airport.referenced_by.append(item)
            self.flights.append(item)

        if isinstance(item, Place):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.referenced_by is None:
                raise ValueError("Cannot add item, field \"referenced_by\" is required")
            for other_item in self.places:
                if item.name == other_item.name:
                    raise ItemNotUniqueError("name has to be unique among before adding to collection!")

            # This have to be at the end, because of side effect on other items
            if item.location is not None:
                item.location.referenced_by.append(item)
            self.places.append(item)

        if isinstance(item, Person):
            if item.name is None:
                raise ValueError("Cannot add item, field \"name\" is required")
            if item.referenced_by is None:
                raise ValueError("Cannot add item, field \"referenced_by\" is required")
            for other_item in self.persons:
                if item.name == other_item.name:
                    raise ItemNotUniqueError("name has to be unique among before adding to collection!")
            self.persons.append(item)

        if isinstance(item, Companion):
            if item.title is None:
                raise ValueError("Cannot add item, field \"title\" is required")
            if item.person_1 is None:
                raise ValueError("Cannot add item, field \"person_1\" is required")
            if item.referenced_by is None:
                raise ValueError("Cannot add item, field \"referenced_by\" is required")
            for other_item in self.companions:
                if item.title == other_item.title:
                    raise ItemNotUniqueError("title has to be unique among before adding to collection!")

            # This have to be at the end, because of side effect on other items
            if item.person_1 is not None:
                item.person_1.referenced_by.append(item)
            if item.person_2 is not None:
                item.person_2.referenced_by.append(item)
            if item.person_3 is not None:
                item.person_3.referenced_by.append(item)
            if item.person_4 is not None:
                item.person_4.referenced_by.append(item)
            if item.person_5 is not None:
                item.person_5.referenced_by.append(item)
            self.companions.append(item)


        item.item_id = self._next_item_id
        self._next_item_id += 1
        self.all_items_by_id[item.item_id] = item

    def delete_item(self, item):
        assert item.item_id is not None
        item_class_definition = get_item_class_name_definition(item.__class__.__name__)
        if item_class_definition.has_belongings():
            assert len(item.belongings) == 0
        if item_class_definition.can_be_referenced_by():
            assert len(item.referenced_by) == 0
        item.delete_from_belongings_and_referenced_by()
        del self.all_items_by_id[item.item_id]
        self.get_items_list_for_class_name(item.__class__.__name__).remove(item)
        
    @classmethod
    def from_content(cls, collection_content: CollectionContent, validate_field_types: bool = True):
        collection: Collection = cls()
        all_items_by_id = {}
        collection.all_items_by_id = all_items_by_id
        collection._next_item_id = collection_content.next_item_id

        all_attachments_content = []
        all_admin_entitys_content = []

        if hasattr(collection_content, "journeys"):  # to allow for reading of older databases, without new item types
            for journey_content in collection_content.journeys:
                journey = Journey.deserialize(journey_content, validate_field_types)
                assert journey.item_id not in all_items_by_id
                all_items_by_id[journey.item_id] = journey
                collection.journeys.append(journey)

        if hasattr(collection_content, "visits"):  # to allow for reading of older databases, without new item types
            for visit_content in collection_content.visits:
                visit = Visit.deserialize(visit_content, validate_field_types)
                assert visit.item_id not in all_items_by_id
                all_items_by_id[visit.item_id] = visit
                collection.visits.append(visit)

        if hasattr(collection_content, "photos"):  # to allow for reading of older databases, without new item types
            for photo_content in collection_content.photos:
                photo = Photo.deserialize(photo_content, validate_field_types)
                assert photo.item_id not in all_items_by_id
                all_items_by_id[photo.item_id] = photo
                collection.photos.append(photo)
                all_attachments_content.append(photo_content)  # only for items with base class different than Item

        if hasattr(collection_content, "files"):  # to allow for reading of older databases, without new item types
            for file_content in collection_content.files:
                file = File.deserialize(file_content, validate_field_types)
                assert file.item_id not in all_items_by_id
                all_items_by_id[file.item_id] = file
                collection.files.append(file)
                all_attachments_content.append(file_content)  # only for items with base class different than Item

        if hasattr(collection_content, "countries"):  # to allow for reading of older databases, without new item types
            for country_content in collection_content.countries:
                country = Country.deserialize(country_content, validate_field_types)
                assert country.item_id not in all_items_by_id
                all_items_by_id[country.item_id] = country
                collection.countries.append(country)
                all_admin_entitys_content.append(country_content)  # only for items with base class different than Item

        if hasattr(collection_content, "regions_major"):  # to allow for reading of older databases, without new item types
            for region_major_content in collection_content.regions_major:
                region_major = RegionMajor.deserialize(region_major_content, validate_field_types)
                assert region_major.item_id not in all_items_by_id
                all_items_by_id[region_major.item_id] = region_major
                collection.regions_major.append(region_major)
                all_admin_entitys_content.append(region_major_content)  # only for items with base class different than Item

        if hasattr(collection_content, "regions_minor"):  # to allow for reading of older databases, without new item types
            for region_minor_content in collection_content.regions_minor:
                region_minor = RegionMinor.deserialize(region_minor_content, validate_field_types)
                assert region_minor.item_id not in all_items_by_id
                all_items_by_id[region_minor.item_id] = region_minor
                collection.regions_minor.append(region_minor)
                all_admin_entitys_content.append(region_minor_content)  # only for items with base class different than Item

        if hasattr(collection_content, "cities"):  # to allow for reading of older databases, without new item types
            for city_content in collection_content.cities:
                city = City.deserialize(city_content, validate_field_types)
                assert city.item_id not in all_items_by_id
                all_items_by_id[city.item_id] = city
                collection.cities.append(city)
                all_admin_entitys_content.append(city_content)  # only for items with base class different than Item

        if hasattr(collection_content, "airports"):  # to allow for reading of older databases, without new item types
            for airport_content in collection_content.airports:
                airport = Airport.deserialize(airport_content, validate_field_types)
                assert airport.item_id not in all_items_by_id
                all_items_by_id[airport.item_id] = airport
                collection.airports.append(airport)

        if hasattr(collection_content, "flights"):  # to allow for reading of older databases, without new item types
            for flight_content in collection_content.flights:
                flight = Flight.deserialize(flight_content, validate_field_types)
                assert flight.item_id not in all_items_by_id
                all_items_by_id[flight.item_id] = flight
                collection.flights.append(flight)

        if hasattr(collection_content, "places"):  # to allow for reading of older databases, without new item types
            for place_content in collection_content.places:
                place = Place.deserialize(place_content, validate_field_types)
                assert place.item_id not in all_items_by_id
                all_items_by_id[place.item_id] = place
                collection.places.append(place)

        if hasattr(collection_content, "persons"):  # to allow for reading of older databases, without new item types
            for person_content in collection_content.persons:
                person = Person.deserialize(person_content, validate_field_types)
                assert person.item_id not in all_items_by_id
                all_items_by_id[person.item_id] = person
                collection.persons.append(person)

        if hasattr(collection_content, "companions"):  # to allow for reading of older databases, without new item types
            for companion_content in collection_content.companions:
                companion = Companion.deserialize(companion_content, validate_field_types)
                assert companion.item_id not in all_items_by_id
                all_items_by_id[companion.item_id] = companion
                collection.companions.append(companion)

        # Update items references to other items
        for journey_content in collection_content.journeys:  # final item
            if journey_content.start_location is None:
                continue
            associated_start_location_id = journey_content.start_location
            associated_start_location = all_items_by_id[associated_start_location_id]
            journey = all_items_by_id[journey_content.item_id]
            journey.start_location = associated_start_location  # This will also set belonging for associated item

        for journey_content in collection_content.journeys:  # final item
            if journey_content.companion is None:
                continue
            associated_companion_id = journey_content.companion
            associated_companion = all_items_by_id[associated_companion_id]
            journey = all_items_by_id[journey_content.item_id]
            journey.companion = associated_companion  # This will also set belonging for associated item

        for visit_content in collection_content.visits:  # final item
            if visit_content.location is None:
                continue
            associated_location_id = visit_content.location
            associated_location = all_items_by_id[associated_location_id]
            visit = all_items_by_id[visit_content.item_id]
            visit.location = associated_location  # This will also set belonging for associated item

        for visit_content in collection_content.visits:  # final item
            if visit_content.journey is None:
                continue
            associated_journey_id = visit_content.journey
            associated_journey = all_items_by_id[associated_journey_id]
            visit = all_items_by_id[visit_content.item_id]
            visit.journey = associated_journey  # This will also set belonging for associated item

        for visit_content in collection_content.visits:  # final item
            if visit_content.companion is None:
                continue
            associated_companion_id = visit_content.companion
            associated_companion = all_items_by_id[associated_companion_id]
            visit = all_items_by_id[visit_content.item_id]
            visit.companion = associated_companion  # This will also set belonging for associated item

        for attachment_content in all_attachments_content:  # not final item
            if attachment_content.attached_to is None:
                continue
            associated_attached_to_id = attachment_content.attached_to
            associated_attached_to = all_items_by_id[associated_attached_to_id]
            attachment = all_items_by_id[attachment_content.item_id]
            attachment.attached_to = associated_attached_to  # This will also set belonging for associated item

        for region_major_content in collection_content.regions_major:  # final item
            if region_major_content.country is None:
                continue
            associated_country_id = region_major_content.country
            associated_country = all_items_by_id[associated_country_id]
            region_major = all_items_by_id[region_major_content.item_id]
            region_major.country = associated_country  # This will also set belonging for associated item

        for region_minor_content in collection_content.regions_minor:  # final item
            if region_minor_content.region_major is None:
                continue
            associated_region_major_id = region_minor_content.region_major
            associated_region_major = all_items_by_id[associated_region_major_id]
            region_minor = all_items_by_id[region_minor_content.item_id]
            region_minor.region_major = associated_region_major  # This will also set belonging for associated item

        for city_content in collection_content.cities:  # final item
            if city_content.region_minor is None:
                continue
            associated_region_minor_id = city_content.region_minor
            associated_region_minor = all_items_by_id[associated_region_minor_id]
            city = all_items_by_id[city_content.item_id]
            city.region_minor = associated_region_minor  # This will also set belonging for associated item

        for airport_content in collection_content.airports:  # final item
            if airport_content.location is None:
                continue
            associated_location_id = airport_content.location
            associated_location = all_items_by_id[associated_location_id]
            airport = all_items_by_id[airport_content.item_id]
            airport.location = associated_location  # This will also set belonging for associated item

        for flight_content in collection_content.flights:  # final item
            if flight_content.attached_to is None:
                continue
            associated_attached_to_id = flight_content.attached_to
            associated_attached_to = all_items_by_id[associated_attached_to_id]
            flight = all_items_by_id[flight_content.item_id]
            flight.attached_to = associated_attached_to  # This will also set belonging for associated item

        for flight_content in collection_content.flights:  # final item
            if flight_content.starting_airport is None:
                continue
            associated_starting_airport_id = flight_content.starting_airport
            associated_starting_airport = all_items_by_id[associated_starting_airport_id]
            flight = all_items_by_id[flight_content.item_id]
            flight.starting_airport = associated_starting_airport  # This will also set belonging for associated item

        for flight_content in collection_content.flights:  # final item
            if flight_content.destination_airport is None:
                continue
            associated_destination_airport_id = flight_content.destination_airport
            associated_destination_airport = all_items_by_id[associated_destination_airport_id]
            flight = all_items_by_id[flight_content.item_id]
            flight.destination_airport = associated_destination_airport  # This will also set belonging for associated item

        for place_content in collection_content.places:  # final item
            if place_content.location is None:
                continue
            associated_location_id = place_content.location
            associated_location = all_items_by_id[associated_location_id]
            place = all_items_by_id[place_content.item_id]
            place.location = associated_location  # This will also set belonging for associated item

        for companion_content in collection_content.companions:  # final item
            if companion_content.person_1 is None:
                continue
            associated_person_1_id = companion_content.person_1
            associated_person_1 = all_items_by_id[associated_person_1_id]
            companion = all_items_by_id[companion_content.item_id]
            companion.person_1 = associated_person_1  # This will also set belonging for associated item

        for companion_content in collection_content.companions:  # final item
            if companion_content.person_2 is None:
                continue
            associated_person_2_id = companion_content.person_2
            associated_person_2 = all_items_by_id[associated_person_2_id]
            companion = all_items_by_id[companion_content.item_id]
            companion.person_2 = associated_person_2  # This will also set belonging for associated item

        for companion_content in collection_content.companions:  # final item
            if companion_content.person_3 is None:
                continue
            associated_person_3_id = companion_content.person_3
            associated_person_3 = all_items_by_id[associated_person_3_id]
            companion = all_items_by_id[companion_content.item_id]
            companion.person_3 = associated_person_3  # This will also set belonging for associated item

        for companion_content in collection_content.companions:  # final item
            if companion_content.person_4 is None:
                continue
            associated_person_4_id = companion_content.person_4
            associated_person_4 = all_items_by_id[associated_person_4_id]
            companion = all_items_by_id[companion_content.item_id]
            companion.person_4 = associated_person_4  # This will also set belonging for associated item

        for companion_content in collection_content.companions:  # final item
            if companion_content.person_5 is None:
                continue
            associated_person_5_id = companion_content.person_5
            associated_person_5 = all_items_by_id[associated_person_5_id]
            companion = all_items_by_id[companion_content.item_id]
            companion.person_5 = associated_person_5  # This will also set belonging for associated item

        return collection

    def to_content(self) -> CollectionContent:
        collection_content = CollectionContent()
        collection_content.next_item_id = self._next_item_id

        for journey in self.journeys:
            journey_content = journey.serialize()
            assert journey.item_id is not None
            collection_content.journeys.append(journey_content)

        for visit in self.visits:
            visit_content = visit.serialize()
            assert visit.item_id is not None
            collection_content.visits.append(visit_content)

        for photo in self.photos:
            photo_content = photo.serialize()
            assert photo.item_id is not None
            collection_content.photos.append(photo_content)

        for file in self.files:
            file_content = file.serialize()
            assert file.item_id is not None
            collection_content.files.append(file_content)

        for country in self.countries:
            country_content = country.serialize()
            assert country.item_id is not None
            collection_content.countries.append(country_content)

        for region_major in self.regions_major:
            region_major_content = region_major.serialize()
            assert region_major.item_id is not None
            collection_content.regions_major.append(region_major_content)

            item = region_major            # same validation as during adding to db - additional_validator_before_adding_to_db
            if not item.country:
                raise Exception('country field is required for region major!')
            for other_region_major in item.country.belongings:
                if item != other_region_major and other_region_major.name == item.name:
                    raise ItemNotUniqueError('Region major name is not unique among country!')
        for region_minor in self.regions_minor:
            region_minor_content = region_minor.serialize()
            assert region_minor.item_id is not None
            collection_content.regions_minor.append(region_minor_content)

            item = region_minor            # same validation as during adding to db - additional_validator_before_adding_to_db
            if not item.region_major:
                raise Exception('region_major field is required for region minor!')
            for other_region_minor in item.region_major.belongings:
                if item != other_region_minor and other_region_minor.name == item.name:
                    raise ItemNotUniqueError('Region minor name is not unique among region major!')
        for city in self.cities:
            city_content = city.serialize()
            assert city.item_id is not None
            collection_content.cities.append(city_content)

            item = city            # same validation as during adding to db - additional_validator_before_adding_to_db
            if not item.region_minor:
                raise Exception('region_minor field is required for city!')
            for other_city in item.region_minor.belongings:
                if item != other_city and other_city.name == item.name:
                    raise ItemNotUniqueError('City name is not unique among region minor!')
        for airport in self.airports:
            airport_content = airport.serialize()
            assert airport.item_id is not None
            collection_content.airports.append(airport_content)

        for flight in self.flights:
            flight_content = flight.serialize()
            assert flight.item_id is not None
            collection_content.flights.append(flight_content)

        for place in self.places:
            place_content = place.serialize()
            assert place.item_id is not None
            collection_content.places.append(place_content)

        for person in self.persons:
            person_content = person.serialize()
            assert person.item_id is not None
            collection_content.persons.append(person_content)

        for companion in self.companions:
            companion_content = companion.serialize()
            assert companion.item_id is not None
            collection_content.companions.append(companion_content)

        return collection_content



