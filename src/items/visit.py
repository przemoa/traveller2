# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent

from common.types.custom_datetime import CustomDateTime
from items.journey import Journey


@dataclass
class VisitContent(ItemContent):
    start_time: Union[int, None] = None
    end_time: Union[int, None] = None
    location: int = None
    journey: Union[int, None] = None
    companion: Union[int, None] = None
    name_suffix: Union[str, None] = None

    def validate_fields_type(self):
        super(VisitContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"start_time\"",
                             value=self.start_time,
                             expected_type=VisitContent.__annotations__["start_time"])
        typeguard.check_type(argname="field \"end_time\"",
                             value=self.end_time,
                             expected_type=VisitContent.__annotations__["end_time"])
        typeguard.check_type(argname="field \"location\"",
                             value=self.location,
                             expected_type=VisitContent.__annotations__["location"])
        typeguard.check_type(argname="field \"journey\"",
                             value=self.journey,
                             expected_type=VisitContent.__annotations__["journey"])
        typeguard.check_type(argname="field \"companion\"",
                             value=self.companion,
                             expected_type=VisitContent.__annotations__["companion"])
        typeguard.check_type(argname="field \"name_suffix\"",
                             value=self.name_suffix,
                             expected_type=VisitContent.__annotations__["name_suffix"])


class Visit(Item):
    def __init__(
            self, 
            location: Item,
            start_time: Union[CustomDateTime, None] = None,
            end_time: Union[CustomDateTime, None] = None,
            journey: Union[Journey, None] = None,
            companion: Union[Item, None] = None,
            belongings: Union[List[Item], None] = None,
            name_suffix: Union[str, None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.start_time = start_time  # type: CustomDateTime  # 
        self.end_time = end_time  # type: CustomDateTime  # 
        self._location = location  # type: Item  # 
        self._journey = journey  # type: Journey  # 
        self._companion = companion  # type: Item  # 
        if belongings is None:
            belongings = []
        self._belongings = belongings  # type: List[Item]  # 
        self.name_suffix = name_suffix  # type: str  # 

    @property
    def location(self):
        return self._location

    @location.setter
    def location(self, value: Item):
        if self._location is not None and self.item_id:
            assert isinstance(self._location, Item)
            self._location.referenced_by.remove(self)

        self._location = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def journey(self):
        return self._journey

    @journey.setter
    def journey(self, value: Union[Item, None]):
        if self._journey is not None and self.item_id:
            assert isinstance(self._journey, Item)
            self._journey.belongings.remove(self)

        self._journey = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.belongings.append(self)

    @property
    def companion(self):
        return self._companion

    @companion.setter
    def companion(self, value: Union[Item, None]):
        if self._companion is not None and self.item_id:
            assert isinstance(self._companion, Item)
            self._companion.referenced_by.remove(self)

        self._companion = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def belongings(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._belongings

    def serialize(self) -> VisitContent:
        data_class = VisitContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            location=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return (self.location.get_short_description() + (self.name_suffix or "") if self.location else "???") + (" (" + self.start_time.to_month_year_string() + ")" if self.start_time else "")

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                location=None)  # None just for a moment, will be overwritten below
        super(Visit, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.start_time = copy.deepcopy(self.start_time)
        new_item.end_time = copy.deepcopy(self.end_time)
        new_item.name_suffix = copy.deepcopy(self.name_suffix)
        return new_item

    def get_date_for_sorting(self):
        return self.start_time or CustomDateTime(0)

    def is_before_date(self, date) -> bool:
        start_date = self.start_time
        return start_date and start_date < date

    def is_after_date(self, date) -> bool:
        end_date = self.end_time
        return end_date and end_date > date

    def is_same_item(self, item) -> bool:
        # if visit cannot be distinct basing on its description, it is not a unique visit!
        return (self.get_short_description() == item.get_short_description())

    def delete_from_belongings_and_referenced_by(self):
        self.location = None
        self.journey = None
        self.companion = None

    def _write_to_data_class(self, data_class):
        super(Visit, self)._write_to_data_class(data_class)
        data_class.start_time = CustomDateTime.to_unix_time_from_instance(self.start_time)
        data_class.end_time = CustomDateTime.to_unix_time_from_instance(self.end_time)
        data_class.location = Item.get_id_for_item(self._location)
        if Item.get_id_for_item(self._location) is None:
            raise ValueError("Cannot save item, field \"location\" is required for class \"Visit\"")
        data_class.journey = Item.get_id_for_item(self._journey)
        data_class.companion = Item.get_id_for_item(self._companion)
        data_class.name_suffix = self.name_suffix

    def _read_from_data_class(self, data_class):
        super(Visit, self)._read_from_data_class(data_class)
        self.start_time = CustomDateTime.from_int_or_none(data_class.start_time)
        self.end_time = CustomDateTime.from_int_or_none(data_class.end_time)
        self.name_suffix = data_class.name_suffix
