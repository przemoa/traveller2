# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.custom_datetime import CustomDateTime


@dataclass
class FlightContent(ItemContent):
    start_time: Union[int, None] = None
    end_time: Union[int, None] = None
    attached_to: Union[int, None] = None
    starting_airport: int = None
    destination_airport: int = None

    def validate_fields_type(self):
        super(FlightContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"start_time\"",
                             value=self.start_time,
                             expected_type=FlightContent.__annotations__["start_time"])
        typeguard.check_type(argname="field \"end_time\"",
                             value=self.end_time,
                             expected_type=FlightContent.__annotations__["end_time"])
        typeguard.check_type(argname="field \"attached_to\"",
                             value=self.attached_to,
                             expected_type=FlightContent.__annotations__["attached_to"])
        typeguard.check_type(argname="field \"starting_airport\"",
                             value=self.starting_airport,
                             expected_type=FlightContent.__annotations__["starting_airport"])
        typeguard.check_type(argname="field \"destination_airport\"",
                             value=self.destination_airport,
                             expected_type=FlightContent.__annotations__["destination_airport"])


class Flight(Item):
    def __init__(
            self, 
            starting_airport: Item,
            destination_airport: Item,
            start_time: Union[CustomDateTime, None] = None,
            end_time: Union[CustomDateTime, None] = None,
            attached_to: Union[Item, None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.start_time = start_time  # type: CustomDateTime  # 
        self.end_time = end_time  # type: CustomDateTime  # 
        self._attached_to = attached_to  # type: Item  # 
        self._starting_airport = starting_airport  # type: Item  # 
        self._destination_airport = destination_airport  # type: Item  # 

    @property
    def attached_to(self):
        return self._attached_to

    @attached_to.setter
    def attached_to(self, value: Union[Item, None]):
        if self._attached_to is not None and self.item_id:
            assert isinstance(self._attached_to, Item)
            self._attached_to.belongings.remove(self)

        self._attached_to = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.belongings.append(self)

    @property
    def starting_airport(self):
        return self._starting_airport

    @starting_airport.setter
    def starting_airport(self, value: Item):
        if self._starting_airport is not None and self.item_id:
            assert isinstance(self._starting_airport, Item)
            self._starting_airport.referenced_by.remove(self)

        self._starting_airport = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def destination_airport(self):
        return self._destination_airport

    @destination_airport.setter
    def destination_airport(self, value: Item):
        if self._destination_airport is not None and self.item_id:
            assert isinstance(self._destination_airport, Item)
            self._destination_airport.referenced_by.remove(self)

        self._destination_airport = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    def serialize(self) -> FlightContent:
        data_class = FlightContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            starting_airport=None,
            destination_airport=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.starting_airport.municipality} - {self.destination_airport.municipality}" + (f" ({self.start_time.to_month_year_string()} )" if self.start_time else "")

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                starting_airport=None,
                destination_airport=None)  # None just for a moment, will be overwritten below
        super(Flight, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.start_time = copy.deepcopy(self.start_time)
        new_item.end_time = copy.deepcopy(self.end_time)
        return new_item

    def get_date_for_sorting(self):
        return self.start_time or CustomDateTime(0)

    def is_before_date(self, date) -> bool:
        start_date = self.start_time
        return start_date and start_date < date

    def is_after_date(self, date) -> bool:
        end_date = self.end_time
        return end_date and end_date > date

    def is_same_item(self, item) -> bool:
        # if flight cannot be distinct basing on its description (or date), it is not a unique flight!
        return (self.get_short_description() == item.get_short_description() and self.start_time == item.start_time)

    def delete_from_belongings_and_referenced_by(self):
        self.attached_to = None
        self.starting_airport = None
        self.destination_airport = None

    def _write_to_data_class(self, data_class):
        super(Flight, self)._write_to_data_class(data_class)
        data_class.start_time = CustomDateTime.to_unix_time_from_instance(self.start_time)
        data_class.end_time = CustomDateTime.to_unix_time_from_instance(self.end_time)
        data_class.attached_to = Item.get_id_for_item(self._attached_to)
        data_class.starting_airport = Item.get_id_for_item(self._starting_airport)
        if Item.get_id_for_item(self._starting_airport) is None:
            raise ValueError("Cannot save item, field \"starting_airport\" is required for class \"Flight\"")
        data_class.destination_airport = Item.get_id_for_item(self._destination_airport)
        if Item.get_id_for_item(self._destination_airport) is None:
            raise ValueError("Cannot save item, field \"destination_airport\" is required for class \"Flight\"")

    def _read_from_data_class(self, data_class):
        super(Flight, self)._read_from_data_class(data_class)
        self.start_time = CustomDateTime.from_int_or_none(data_class.start_time)
        self.end_time = CustomDateTime.from_int_or_none(data_class.end_time)
