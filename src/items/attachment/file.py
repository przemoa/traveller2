# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy
import ntpath
import typeguard

from items.attachment.attachment import Attachment, AttachmentContent
from items.item import Item, ItemContent
from common.types.custom_datetime import CustomDateTime


@dataclass
class FileContent(AttachmentContent):
    file_checksum_md5: Union[str, None] = None
    file_blob: Union[bytes, None] = None

    def validate_fields_type(self):
        super(FileContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"file_checksum_md5\"",
                             value=self.file_checksum_md5,
                             expected_type=FileContent.__annotations__["file_checksum_md5"])
        typeguard.check_type(argname="field \"file_blob\"",
                             value=self.file_blob,
                             expected_type=FileContent.__annotations__["file_blob"])


class File(Attachment):
    def __init__(
            self, 
            original_full_path: str,
            file_checksum_md5: Union[str, None] = None,
            file_blob: Union[bytes, None] = None,
            relative_file_path: Union[str, None] = None,
            attached_to: Union[Item, None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                original_full_path=original_full_path,
                relative_file_path=relative_file_path,
                attached_to=attached_to,
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.file_checksum_md5 = file_checksum_md5  # type: str  # 
        self.file_blob = file_blob  # type: bytes  # 

    def serialize(self) -> FileContent:
        data_class = FileContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            original_full_path=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return ntpath.basename(self.original_full_path)

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                original_full_path=None)  # None just for a moment, will be overwritten below
        super(File, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.file_checksum_md5 = copy.deepcopy(self.file_checksum_md5)
        new_item.file_blob = copy.deepcopy(self.file_blob)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.original_full_path == item.original_full_path)

    def delete_from_belongings_and_referenced_by(self):
        self.attached_to = None

    def _write_to_data_class(self, data_class):
        super(File, self)._write_to_data_class(data_class)
        data_class.file_checksum_md5 = self.file_checksum_md5
        data_class.file_blob = self.file_blob

    def _read_from_data_class(self, data_class):
        super(File, self)._read_from_data_class(data_class)
        self.file_checksum_md5 = data_class.file_checksum_md5
        self.file_blob = data_class.file_blob
