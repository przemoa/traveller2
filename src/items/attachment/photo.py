# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy
import ntpath
import os
from utils.misc import get_dir_name_from_file_path
import typeguard

from items.attachment.attachment import Attachment, AttachmentContent
from items.item import Item, ItemContent
from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from common.types.image_annotation import ImageAnnotation


@dataclass
class PhotoContent(AttachmentContent):
    thumbnail: Union[bytes, None] = None
    photo_time: Union[int, None] = None
    coordinates: Union[Tuple[float, float], None] = None
    camera_model: Union[str, None] = None
    annotation: Union[dict, None] = None
    ignore_annotation: Union[bool, None] = None

    def validate_fields_type(self):
        super(PhotoContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"thumbnail\"",
                             value=self.thumbnail,
                             expected_type=PhotoContent.__annotations__["thumbnail"])
        typeguard.check_type(argname="field \"photo_time\"",
                             value=self.photo_time,
                             expected_type=PhotoContent.__annotations__["photo_time"])
        typeguard.check_type(argname="field \"coordinates\"",
                             value=self.coordinates,
                             expected_type=PhotoContent.__annotations__["coordinates"])
        typeguard.check_type(argname="field \"camera_model\"",
                             value=self.camera_model,
                             expected_type=PhotoContent.__annotations__["camera_model"])
        typeguard.check_type(argname="field \"annotation\"",
                             value=self.annotation,
                             expected_type=PhotoContent.__annotations__["annotation"])
        typeguard.check_type(argname="field \"ignore_annotation\"",
                             value=self.ignore_annotation,
                             expected_type=PhotoContent.__annotations__["ignore_annotation"])


class Photo(Attachment):
    def __init__(
            self, 
            original_full_path: str,
            thumbnail: Union[bytes, None] = None,
            photo_time: Union[CustomDateTime, None] = None,
            coordinates: Union[Coordinates, None] = None,
            camera_model: Union[str, None] = None,
            annotation: Union[ImageAnnotation, None] = None,
            ignore_annotation: Union[bool, None] = None,
            relative_file_path: Union[str, None] = None,
            attached_to: Union[Item, None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                original_full_path=original_full_path,
                relative_file_path=relative_file_path,
                attached_to=attached_to,
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.thumbnail = thumbnail  # type: bytes  # 
        self.photo_time = photo_time  # type: CustomDateTime  # 
        self.coordinates = coordinates  # type: Coordinates  # 
        self.camera_model = camera_model  # type: str  # 
        self.annotation = annotation  # type: ImageAnnotation  # 
        self.ignore_annotation = ignore_annotation  # type: bool  # 

    def serialize(self) -> PhotoContent:
        data_class = PhotoContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            original_full_path=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return ntpath.basename(self.original_full_path)

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                original_full_path=None)  # None just for a moment, will be overwritten below
        super(Photo, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.thumbnail = copy.deepcopy(self.thumbnail)
        new_item.photo_time = copy.deepcopy(self.photo_time)
        new_item.coordinates = copy.deepcopy(self.coordinates)
        new_item.camera_model = copy.deepcopy(self.camera_model)
        new_item.annotation = copy.deepcopy(self.annotation)
        new_item.ignore_annotation = copy.deepcopy(self.ignore_annotation)
        return new_item

    def get_date_for_sorting(self):
        return self.photo_time or CustomDateTime(0)

    def is_before_date(self, date) -> bool:
        start_date = self.photo_time
        return start_date and start_date < date

    def is_after_date(self, date) -> bool:
        end_date = self.photo_time
        return end_date and end_date > date

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.original_full_path == item.original_full_path)

    def delete_from_belongings_and_referenced_by(self):
        self.attached_to = None

    def _write_to_data_class(self, data_class):
        super(Photo, self)._write_to_data_class(data_class)
        data_class.thumbnail = self.thumbnail
        data_class.photo_time = CustomDateTime.to_unix_time_from_instance(self.photo_time)
        data_class.coordinates = Coordinates.to_tuple_from_instance(self.coordinates)
        data_class.camera_model = self.camera_model
        data_class.annotation = ImageAnnotation.to_dict_from_instance(self.annotation)
        data_class.ignore_annotation = self.ignore_annotation

    def _read_from_data_class(self, data_class):
        super(Photo, self)._read_from_data_class(data_class)
        self.thumbnail = data_class.thumbnail
        self.photo_time = CustomDateTime.from_int_or_none(data_class.photo_time)
        self.coordinates = Coordinates.from_tuple(data_class.coordinates)
        self.camera_model = data_class.camera_model
        self.annotation = ImageAnnotation.from_dict(data_class.annotation)
        self.ignore_annotation = data_class.ignore_annotation

    def get_dir_name(self):
        return get_dir_name_from_file_path(self.original_full_path)
    
    def get_file_name(self):
        return os.path.basename(self.original_full_path)
