# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy
from utils.misc import get_full_file_path_for_absolute_or_relative_path
import typeguard

from items.item import Item, ItemContent
from common.types.custom_datetime import CustomDateTime


@dataclass
class AttachmentContent(ItemContent):
    original_full_path: str = None
    relative_file_path: Union[str, None] = None
    attached_to: Union[int, None] = None

    def validate_fields_type(self):
        super(AttachmentContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"original_full_path\"",
                             value=self.original_full_path,
                             expected_type=AttachmentContent.__annotations__["original_full_path"])
        typeguard.check_type(argname="field \"relative_file_path\"",
                             value=self.relative_file_path,
                             expected_type=AttachmentContent.__annotations__["relative_file_path"])
        typeguard.check_type(argname="field \"attached_to\"",
                             value=self.attached_to,
                             expected_type=AttachmentContent.__annotations__["attached_to"])


class Attachment(Item):
    def __init__(
            self, 
            original_full_path: str,
            relative_file_path: Union[str, None] = None,
            attached_to: Union[Item, None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.original_full_path = original_full_path  # type: str  # 
        self.relative_file_path = relative_file_path  # type: str  # 
        self._attached_to = attached_to  # type: Item  # 

    @property
    def attached_to(self):
        return self._attached_to

    @attached_to.setter
    def attached_to(self, value: Union[Item, None]):
        if self._attached_to is not None and self.item_id:
            assert isinstance(self._attached_to, Item)
            self._attached_to.belongings.remove(self)

        self._attached_to = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.belongings.append(self)

    def serialize(self) -> AttachmentContent:
        data_class = AttachmentContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            original_full_path=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return str(self)

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                original_full_path=None)  # None just for a moment, will be overwritten below
        super(Attachment, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.original_full_path = copy.deepcopy(self.original_full_path)
        new_item.relative_file_path = copy.deepcopy(self.relative_file_path)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        raise Exception("Cannot compare abstract (not final) class Attachment")

    def delete_from_belongings_and_referenced_by(self):
        self.attached_to = None

    def _write_to_data_class(self, data_class):
        super(Attachment, self)._write_to_data_class(data_class)
        data_class.original_full_path = self.original_full_path
        if self.original_full_path is None:
            raise ValueError("Cannot save item, field \"original_full_path\" is required for class \"Attachment\"")
        data_class.relative_file_path = self.relative_file_path
        data_class.attached_to = Item.get_id_for_item(self._attached_to)

    def _read_from_data_class(self, data_class):
        super(Attachment, self)._read_from_data_class(data_class)
        self.original_full_path = data_class.original_full_path
        self.relative_file_path = data_class.relative_file_path

    def get_absolute_path_from_relative(self):
        return get_full_file_path_for_absolute_or_relative_path(self.relative_file_path or self.original_full_path)        
