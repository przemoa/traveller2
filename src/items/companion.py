# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent
from common.types.custom_datetime import CustomDateTime
from items.person import Person


@dataclass
class CompanionContent(ItemContent):
    title: str = None
    person_1: int = None
    person_2: Union[int, None] = None
    person_3: Union[int, None] = None
    person_4: Union[int, None] = None
    person_5: Union[int, None] = None

    def validate_fields_type(self):
        super(CompanionContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"title\"",
                             value=self.title,
                             expected_type=CompanionContent.__annotations__["title"])
        typeguard.check_type(argname="field \"person_1\"",
                             value=self.person_1,
                             expected_type=CompanionContent.__annotations__["person_1"])
        typeguard.check_type(argname="field \"person_2\"",
                             value=self.person_2,
                             expected_type=CompanionContent.__annotations__["person_2"])
        typeguard.check_type(argname="field \"person_3\"",
                             value=self.person_3,
                             expected_type=CompanionContent.__annotations__["person_3"])
        typeguard.check_type(argname="field \"person_4\"",
                             value=self.person_4,
                             expected_type=CompanionContent.__annotations__["person_4"])
        typeguard.check_type(argname="field \"person_5\"",
                             value=self.person_5,
                             expected_type=CompanionContent.__annotations__["person_5"])


class Companion(Item):
    def __init__(
            self, 
            title: str,
            person_1: Person,
            person_2: Union[Person, None] = None,
            person_3: Union[Person, None] = None,
            person_4: Union[Person, None] = None,
            person_5: Union[Person, None] = None,
            referenced_by: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.title = title  # type: str  # 
        self._person_1 = person_1  # type: Person  # 
        self._person_2 = person_2  # type: Person  # 
        self._person_3 = person_3  # type: Person  # 
        self._person_4 = person_4  # type: Person  # 
        self._person_5 = person_5  # type: Person  # 
        if referenced_by is None:
            referenced_by = []
        self._referenced_by = referenced_by  # type: List[Item]  # 

    @property
    def person_1(self):
        return self._person_1

    @person_1.setter
    def person_1(self, value: Item):
        if self._person_1 is not None and self.item_id:
            assert isinstance(self._person_1, Item)
            self._person_1.referenced_by.remove(self)

        self._person_1 = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def person_2(self):
        return self._person_2

    @person_2.setter
    def person_2(self, value: Union[Item, None]):
        if self._person_2 is not None and self.item_id:
            assert isinstance(self._person_2, Item)
            self._person_2.referenced_by.remove(self)

        self._person_2 = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def person_3(self):
        return self._person_3

    @person_3.setter
    def person_3(self, value: Union[Item, None]):
        if self._person_3 is not None and self.item_id:
            assert isinstance(self._person_3, Item)
            self._person_3.referenced_by.remove(self)

        self._person_3 = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def person_4(self):
        return self._person_4

    @person_4.setter
    def person_4(self, value: Union[Item, None]):
        if self._person_4 is not None and self.item_id:
            assert isinstance(self._person_4, Item)
            self._person_4.referenced_by.remove(self)

        self._person_4 = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def person_5(self):
        return self._person_5

    @person_5.setter
    def person_5(self, value: Union[Item, None]):
        if self._person_5 is not None and self.item_id:
            assert isinstance(self._person_5, Item)
            self._person_5.referenced_by.remove(self)

        self._person_5 = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def referenced_by(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._referenced_by

    def serialize(self) -> CompanionContent:
        data_class = CompanionContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            title=None,
            person_1=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return self.title + " [" + ", ".join([c.get_short_description() for c in self.get_persons()]) + "]"

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                title=None,
                person_1=None)  # None just for a moment, will be overwritten below
        super(Companion, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.title = copy.deepcopy(self.title)
        return new_item

    def get_date_for_sorting(self):
        raise Exception("Item cannot be time sorted!")

    def is_before_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_after_date(self, date) -> bool:
        raise Exception("Item cannot be time filtered!")

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.title == item.title)

    def delete_from_belongings_and_referenced_by(self):
        self.person_1 = None
        self.person_2 = None
        self.person_3 = None
        self.person_4 = None
        self.person_5 = None

    def _write_to_data_class(self, data_class):
        super(Companion, self)._write_to_data_class(data_class)
        data_class.title = self.title
        if self.title is None:
            raise ValueError("Cannot save item, field \"title\" is required for class \"Companion\"")
        data_class.person_1 = Item.get_id_for_item(self._person_1)
        if Item.get_id_for_item(self._person_1) is None:
            raise ValueError("Cannot save item, field \"person_1\" is required for class \"Companion\"")
        data_class.person_2 = Item.get_id_for_item(self._person_2)
        data_class.person_3 = Item.get_id_for_item(self._person_3)
        data_class.person_4 = Item.get_id_for_item(self._person_4)
        data_class.person_5 = Item.get_id_for_item(self._person_5)

    def _read_from_data_class(self, data_class):
        super(Companion, self)._read_from_data_class(data_class)
        self.title = data_class.title

    def get_persons(self):
        all_persons = []
        if self.person_1:
            all_persons.append(self.person_1)
        if self.person_2:
            all_persons.append(self.person_2)
        if self.person_3:
            all_persons.append(self.person_3)
        if self.person_4:
            all_persons.append(self.person_4)
        if self.person_5:
            all_persons.append(self.person_5)
        return all_persons
