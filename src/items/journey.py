# Warning! This file was generated automatically with "misc/items_generator/items_generator.py".
# Edit is inadvisable!

from dataclasses import dataclass
from typing import Tuple, Union, List
import copy

import typeguard

from items.item import Item, ItemContent

from common.types.custom_datetime import CustomDateTime


@dataclass
class JourneyContent(ItemContent):
    name: str = None
    start_time: Union[int, None] = None
    end_time: Union[int, None] = None
    start_location: Union[int, None] = None
    type: Union[str, None] = None
    companion: Union[int, None] = None

    def validate_fields_type(self):
        super(JourneyContent, self).validate_fields_type()
        typeguard.check_type(argname="field \"name\"",
                             value=self.name,
                             expected_type=JourneyContent.__annotations__["name"])
        typeguard.check_type(argname="field \"start_time\"",
                             value=self.start_time,
                             expected_type=JourneyContent.__annotations__["start_time"])
        typeguard.check_type(argname="field \"end_time\"",
                             value=self.end_time,
                             expected_type=JourneyContent.__annotations__["end_time"])
        typeguard.check_type(argname="field \"start_location\"",
                             value=self.start_location,
                             expected_type=JourneyContent.__annotations__["start_location"])
        typeguard.check_type(argname="field \"type\"",
                             value=self.type,
                             expected_type=JourneyContent.__annotations__["type"])
        typeguard.check_type(argname="field \"companion\"",
                             value=self.companion,
                             expected_type=JourneyContent.__annotations__["companion"])


class Journey(Item):
    def __init__(
            self, 
            name: str,
            start_time: Union[CustomDateTime, None] = None,
            end_time: Union[CustomDateTime, None] = None,
            start_location: Union[Item, None] = None,
            type: Union[str, None] = None,
            companion: Union[Item, None] = None,
            belongings: Union[List[Item], None] = None,
            item_id: Union[int, None] = None,
            item_creation_time: Union[CustomDateTime, None] = None,
            item_modification_time: Union[CustomDateTime, None] = None,
            note: Union[str, None] = None):
        super().__init__(
                item_id=item_id,
                item_creation_time=item_creation_time,
                item_modification_time=item_modification_time,
                note=note)
        self.name = name  # type: str  # 
        self.start_time = start_time  # type: CustomDateTime  # 
        self.end_time = end_time  # type: CustomDateTime  # 
        self._start_location = start_location  # type: Item  # 
        self.type = type  # type: str  # 
        self._companion = companion  # type: Item  # 
        if belongings is None:
            belongings = []
        self._belongings = belongings  # type: List[Item]  # 

    @property
    def start_location(self):
        return self._start_location

    @start_location.setter
    def start_location(self, value: Union[Item, None]):
        if self._start_location is not None and self.item_id:
            assert isinstance(self._start_location, Item)
            self._start_location.referenced_by.remove(self)

        self._start_location = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def companion(self):
        return self._companion

    @companion.setter
    def companion(self, value: Union[Item, None]):
        if self._companion is not None and self.item_id:
            assert isinstance(self._companion, Item)
            self._companion.referenced_by.remove(self)

        self._companion = value
        if value is not None and self.item_id:
            if value.item_id is None:
                raise ValueError("Associated item have to be already in database!")
            value.referenced_by.append(self)

    @property
    def belongings(self):
        """Do not modify this list directly. Update associated item instead"""
        return self._belongings

    def serialize(self) -> JourneyContent:
        data_class = JourneyContent()
        self._write_to_data_class(data_class)
        data_class.validate_fields_type()
        return data_class

    @classmethod
    def deserialize(cls, data_class, validate_field_types=True):
        if validate_field_types:
            data_class.validate_fields_type()
        instance = cls(
            name=None)  # None just for a moment, will be overwritten below
        instance._read_from_data_class(data_class)
        return instance

    def get_short_description(self):
        return f"{self.name}" + (f" ({self.start_time.to_month_year_string()})" if self.start_time else "")

    def copy_fields_without_associated_items(self, new_item=None):
        """Copy only non-item fields, that are stored in db"""
        if new_item is None:
            new_item = self.__class__(
                name=None)  # None just for a moment, will be overwritten below
        super(Journey, self).copy_fields_without_associated_items(new_item=new_item)
        new_item.name = copy.deepcopy(self.name)
        new_item.start_time = copy.deepcopy(self.start_time)
        new_item.end_time = copy.deepcopy(self.end_time)
        new_item.type = copy.deepcopy(self.type)
        return new_item

    def get_date_for_sorting(self):
        return self.start_time or CustomDateTime(0)

    def is_before_date(self, date) -> bool:
        start_date = self.start_time
        return start_date and start_date < date

    def is_after_date(self, date) -> bool:
        end_date = self.end_time
        return end_date and end_date > date

    def is_same_item(self, item) -> bool:
        # if unique field is same, then it is definitely same object
        return (self.name == item.name)

    def delete_from_belongings_and_referenced_by(self):
        self.start_location = None
        self.companion = None

    def _write_to_data_class(self, data_class):
        super(Journey, self)._write_to_data_class(data_class)
        data_class.name = self.name
        if self.name is None:
            raise ValueError("Cannot save item, field \"name\" is required for class \"Journey\"")
        data_class.start_time = CustomDateTime.to_unix_time_from_instance(self.start_time)
        data_class.end_time = CustomDateTime.to_unix_time_from_instance(self.end_time)
        data_class.start_location = Item.get_id_for_item(self._start_location)
        data_class.type = self.type
        data_class.companion = Item.get_id_for_item(self._companion)

    def _read_from_data_class(self, data_class):
        super(Journey, self)._read_from_data_class(data_class)
        self.name = data_class.name
        self.start_time = CustomDateTime.from_int_or_none(data_class.start_time)
        self.end_time = CustomDateTime.from_int_or_none(data_class.end_time)
        self.type = data_class.type
