from typing import List

from common.types.coordinates import Coordinates, may_be_coordinates_distance_smaller
from common.types.custom_datetime import CustomDateTime
from items.attachment.photo import Photo
from items.collection import Collection
from items.item import Item
from items_utils.misc import get_item_coordinates_or_parent_location, get_item_coordinates_or_parent_location__cached


def filter_items__no_filter(items, parameters=None):
    return items


def filter_items__for_journey(items, journey):
    return filter_items__for_parent(items=items, parent=journey)


def filter_items__for_parent(items, parent):
    if not parent:
        return items
    filtered_items = []
    belongings_set = parent.get_belongings_set_recursively()
    for item in items:
        if item in belongings_set:
            filtered_items.append(item)
    return filtered_items


def filter_items__for_time(items, before_time: CustomDateTime, after_time: CustomDateTime):
    filtered_items = []
    for item in items:
        if before_time and not item.is_before_date(before_time):
            continue
        if after_time and not item.is_after_date(after_time):
            continue
        filtered_items.append(item)
    return filtered_items


def get_biggest_item_by_field(items, field_name):
    biggest_value = None
    biggest_item = None

    for item in items:
        item_value = getattr(item, field_name)
        if (biggest_value is None) or \
                (item_value is not None and item_value > biggest_value):
            biggest_value = item_value
            biggest_item = item
    return biggest_item


def _get_biggest_items_by_field(collection: Collection, field_name, number_of_items: int):
    all_items_set = set(collection.all_items_by_id.values())
    biggest_items = []

    for i in range(min(number_of_items, len(all_items_set))):
        biggest_item = get_biggest_item_by_field(items=all_items_set, field_name=field_name)
        all_items_set.remove(biggest_item)
        assert biggest_item is not None
        biggest_items.append(biggest_item)

    return biggest_items


def get_recently_added_items(collection: Collection, number_of_items: int = 60):
    return _get_biggest_items_by_field(collection=collection,
                                       number_of_items=number_of_items,
                                       field_name='item_creation_time')


def get_recently_modified_items(collection: Collection, number_of_items: int = 60):
    return _get_biggest_items_by_field(collection=collection,
                                       number_of_items=number_of_items,
                                       field_name='item_modification_time')


def sort_items_by_time(items: List[Item]):
    sorted_list = sorted(items, key=lambda x: x.get_date_for_sorting(), reverse=True)
    # for item in sorted_list:
    #     print(item.get_date_for_sorting())
    return sorted_list


def _check_if_photo_within_radius_from_another_one(checked_photo: Photo, all_photos: List[Photo], radius: float):
    coordinates = get_item_coordinates_or_parent_location__cached(checked_photo)
    for other_photo in reversed(all_photos):
        other_photo_coordinates = get_item_coordinates_or_parent_location__cached(other_photo)

        may_be_distance_smaller = may_be_coordinates_distance_smaller(coordinates, other_photo_coordinates, distance=radius)
        distance_cannot_be_smaller = not may_be_distance_smaller

        # validation of the may_be_coordinates_distance_smaller(...)
        # real_distance = coordinates.get_distance_to_other_coordinates(other_photo_coordinates)
        # if distance_cannot_be_smaller and (real_distance < radius):
        #     may_be_coordinates_distance_smaller(coordinates, other_photo_coordinates, distance=radius)
        #     raise Exception('invalid logic!')

        if distance_cannot_be_smaller:
            continue

        if coordinates.get_distance_to_other_coordinates(other_photo_coordinates) < radius:
            return True

    return False


def decimate_photos_in_space(photos: List[Photo], radius):
    decimated_photos = []
    for photo in photos:
        coordinates = get_item_coordinates_or_parent_location__cached(photo)
        if coordinates is None:
            continue

        if not _check_if_photo_within_radius_from_another_one(checked_photo=photo, all_photos=decimated_photos, radius=radius):
            decimated_photos.append(photo)
    return decimated_photos
