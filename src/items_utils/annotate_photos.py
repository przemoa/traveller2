import logging
from typing import List, Dict

from PySide2.QtWidgets import QMessageBox
from google.auth.exceptions import DefaultCredentialsError
from google.cloud import vision
from google.cloud.vision import types
from google.protobuf.json_format import MessageToDict

from common.types.image_annotation import ImageAnnotation
from dialogs.progress.progress_dialog import ProgressDialog
from items.attachment.photo import Photo
from items_utils.photo_from_path import create_thumbnail


def get_photos_annotations_dict_with_progress_dialog(photos: List[Photo], parent) -> Dict[Photo, ImageAnnotation]:
    for photo in photos:
        try:
            full_path = photo.get_absolute_path_from_relative()

            # Loads the image into memory
            with open(full_path, 'rb') as image_file:
                pass  # just confirm that file can be opened
        except Exception as e:
            raise Exception(
                f'Failed to open file for photo "{photo.get_file_name()}" (id {photo.item_id}).') from e

    NUMBER_OF_PHOTOS_TO_ANNOTATE_IN_ONE_REQUEST = 7  # TODO - maximum request size is 10 MB - think about lowering the resolution!
    progress_dialog = ProgressDialog(parent=parent, title='Annotating photos...', max_value=len(photos))
    progress_dialog.set_message(f'{len(photos)} Photos to annotate. Photos will be annotated in batches with '
                                f'{NUMBER_OF_PHOTOS_TO_ANNOTATE_IN_ONE_REQUEST} photos each.'
                                f'\nOperation can be canceled between batches.')
    progress_dialog.set_value(0)
    photos_with_annotations = {}

    photos_to_annotate_in_current_iteration = []
    for i, photo in enumerate(photos):
        photos_to_annotate_in_current_iteration.append(photo)
        if (len(photos_to_annotate_in_current_iteration) == NUMBER_OF_PHOTOS_TO_ANNOTATE_IN_ONE_REQUEST) \
                or (i == len(photos)-1):
            progress_dialog.set_value(i)
            progress_dialog.set_message(f'[{i}/{len(photos)}] Annotating photo:'
                                        f'\n{photos_to_annotate_in_current_iteration[0].get_absolute_path_from_relative()}...')
            try:
                annotations = get_photos_annotations(photos=photos_to_annotate_in_current_iteration)
            except Exception as e:
                QMessageBox.critical(parent, "Error!", str(e))
                break

            for photo, annotation in zip(photos_to_annotate_in_current_iteration, annotations):
                photos_with_annotations[photo] = annotation
            photos_to_annotate_in_current_iteration = []

            if progress_dialog.was_canceled():
                break

    progress_dialog.cancel()
    return photos_with_annotations


def get_photos_annotations(photos: List[Photo]) -> List[ImageAnnotation]:
    """ Check if file exist before requesting annotation! """

    MAX_NUMBER_OF_PHOTOS_TO_ANNOTATE_AT_ONCE = 100
    if len(photos) > MAX_NUMBER_OF_PHOTOS_TO_ANNOTATE_AT_ONCE:
        raise Exception('Too many photos requested to be annotated at once!'
                        '\nMax {MAX_NUMBER_OF_PHOTOS_TO_ANNOTATE_AT_ONCE} photos allowed at once!')

    image_requests = []

    for photo in photos:
        full_path = photo.get_absolute_path_from_relative()

        image_data = create_thumbnail(file_path=full_path, thumbnail_size=2048)
        # with open(full_path, 'rb') as image_file:
        #     content = image_file.read()

        image = types.Image(content=image_data)
        image_request = types.AnnotateImageRequest(
            image=image,
            features=[
                {'type': vision.enums.Feature.Type.OBJECT_LOCALIZATION},
                {'type': vision.enums.Feature.Type.LANDMARK_DETECTION},
                {'type': vision.enums.Feature.Type.LABEL_DETECTION},
            ])
        image_requests.append(image_request)

    try:
        client = vision.ImageAnnotatorClient()
        global_response = client.batch_annotate_images(image_requests)
    except DefaultCredentialsError as e:
        msg = f'Missing Google Vision API credentials file! Please set path to credentials file in settings!\n({e})'
        logging.exception(msg)
        raise Exception(msg)
    except Exception as e:
        msg = f'Error during image annotation!\n({e})'
        logging.exception(msg)
        raise Exception(msg)

    if len(global_response.responses) != len(image_requests):
        raise Exception('Invalid response data count in Vision API!')

    image_annotations = []
    for i, response in enumerate(global_response.responses):
        response_dict = MessageToDict(response, preserving_proto_field_name=True)
        image_annotations.append(ImageAnnotation(response_dict=response_dict))
    return image_annotations


def main():
    from utils.dummy_engine import get_dummy_engine
    from gui_setup import debug_setup
    # from PySide2 import QtWidgets

    debug_setup()
    # app = QtWidgets.QApplication(sys.argv)
    engine = get_dummy_engine()
    r = get_photos_annotations(photos=engine.collection.photos)


if __name__ == '__main__':
    main()
