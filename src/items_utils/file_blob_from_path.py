import hashlib
import os

from items.attachment.file import File
from utils.misc import get_relative_path_or_none_if_not_subdirectory


def get_file_from_file_path(abs_file_path, base_dir, save_blob: bool = True) -> File:
    """ create File Item from file in filesystem

    :param abs_file_path: absolute file path
    :param base_dir: base dir. If it overlaps with file_path, path for the photo will be se as relative
    :param save_blob: if True, file content will be save to the file
    :return:
    """
    file_blob = None

    with open(abs_file_path, 'rb') as f:
        file_content = f.read()
        if save_blob:
            file_blob = file_content
        checksum = hashlib.md5(file_content).hexdigest()

    return File(
        original_full_path=os.path.abspath(abs_file_path),
        relative_file_path=get_relative_path_or_none_if_not_subdirectory(abs_file_path, base_dir),
        file_blob=file_blob,
        file_checksum_md5=checksum)
