from typing import Union

from items.collection import Collection
from items.item import Item


def find_same_item_in_collection(item: Item, collection: Collection) -> Union[Item, None]:
    """ TODO - move to collection after refactorization of collection generation """
    all_items = collection.get_items_list_for_class_name(class_name=item.__class__.__name__)
    for other_item in all_items:
        if item.is_same_item(other_item):
            return other_item
    return None

