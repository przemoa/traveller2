from PySide2.QtWidgets import QMessageBox

from dialogs.select_item.select_item_dialog import select_item_from_dialog
from engine.collection_utils import save_collection_content_to_file, load_collection_from_file_path
from engine.engine import Engine
from items.collection import Collection
from items.item import Item
from items.journey import Journey
from items.visit import Visit
from items_utils.copy_items import copy_prompt_item_with_parents_and_belongings
from items_utils.item_search import find_same_item_in_collection
from utils.gui import browse_save_traveller2_database_file, browse_open_traveller2_database_file

ALLOWED_BASE_ITEM_TYPES_TO_EXPORT_IMPORT = [Journey, Visit]


def select_and_export_item(parent, engine):
    item = select_item_from_dialog(engine=engine,
                                   parent=parent,
                                   allowed_item_types=ALLOWED_BASE_ITEM_TYPES_TO_EXPORT_IMPORT)
    if not item:
        return
    file_path = browse_save_traveller2_database_file(parent=parent,
                                                     title='Select path for output file...',
                                                     start_dir_path='',
                                                     start_file_name=item.get_short_description().replace(' ', '_'))
    if not file_path:
        return
    _export_item_to_new_database(item=item, file_path=file_path)
    QMessageBox.information(parent, "Info!", f'Item exported succesfully!\nFile path: "{file_path}"')


def select_and_import_item(parent, engine):
    file_path = browse_open_traveller2_database_file(parent=parent, title='Select path with item to import...')
    if not file_path:
        return
    collection, _ = load_collection_from_file_path(file_path=file_path, is_prompts_database=True)
    if len(collection.visits) == 1:
        item_to_import = collection.visits[0]
    elif len(collection.journeys) == 1:
        item_to_import = collection.journeys[0]
    else:
        # select item manually
        engine_with_item_to_select = Engine()
        engine_with_item_to_select.collection = collection
        item_to_import = select_item_from_dialog(engine=engine_with_item_to_select,
                                                 parent=parent,
                                                 allowed_item_types=ALLOWED_BASE_ITEM_TYPES_TO_EXPORT_IMPORT,
                                                 allow_for_prompts_selection=True)
        if not item_to_import:
            return

    initial_number_of_items = len(engine.collection.all_items_by_id)
    _import_item_from_prompt_database_to_collection(collection=engine.collection, item_to_import=item_to_import)
    final_number_of_items = len(engine.collection.all_items_by_id)
    number_of_imported_associated_items = final_number_of_items - initial_number_of_items - 1  # -1 because of the main imported item
    msg = f'Item "{item_to_import.get_short_description()} ({item_to_import.__class__.__name__})" ' \
        f'with {number_of_imported_associated_items} additional associated items imported successfully!'
    engine.set_engine_as_modified()
    QMessageBox.information(parent, "Info!", msg)


def _export_item_to_new_database(item: Item, file_path: str):
    collection_to_export = Collection()
    copy_prompt_item_with_parents_and_belongings(collection=collection_to_export, item=item)
    save_collection_content_to_file(file_path=file_path,
                                    collection_content=collection_to_export.to_content())


def _import_item_from_prompt_database_to_collection(collection: Collection, item_to_import: Item):
    if find_same_item_in_collection(item=item_to_import, collection=collection):
        raise Exception(f'Cannot import item "{item_to_import.get_short_description()} '
                        f'({item_to_import.__class__.__name__})", same already exist in database!')
    copy_prompt_item_with_parents_and_belongings(collection=collection, item=item_to_import)
