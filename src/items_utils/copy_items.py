from items.collection import Collection
from items.item import Item
from items.location.admin_entity.admin_entity import AdminEntity
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_definitions.item_classes import get_item_class_name_definition
from items_definitions.item_fields import ItemFieldReferencedByItems, ItemFieldDirectlyAssociatedItems, ItemFieldItem
from items_utils.item_search import find_same_item_in_collection


def copy_prompt_item_with_parents_and_belongings(collection: Collection, item: Item) -> Item:
    if isinstance(item, AdminEntity):
        return _copy_admin_entity_prompt_with_parents(collection=collection, item=item)
    else:
        existing_item = find_same_item_in_collection(item=item, collection=collection)
        if existing_item:
            return existing_item

        new_item = item.copy_fields_without_associated_items()
        item_class_definition = get_item_class_name_definition(item.__class__.__name__)
        for field in item_class_definition.get_all_fields_with_base_classes():
            if isinstance(field, ItemFieldReferencedByItems):
                continue  # not interesting with copying items that reference our one
            if isinstance(field, ItemFieldDirectlyAssociatedItems):
                continue  # will be handled later
            if isinstance(field, ItemFieldItem):
                # first we need to copy fields that are necessary part of copied item (e.g. location for visit)
                if field.not_none_value_required_in_constructor:
                    additional_item_to_copy = getattr(item, field.field_name)
                    if additional_item_to_copy:
                        copied_additional_item = copy_prompt_item_with_parents_and_belongings(
                            collection=collection, item=additional_item_to_copy)
                        setattr(new_item, field.field_name, copied_additional_item)
                else:
                    pass  # will be handled later
        collection.add_item(new_item)
        for field in item_class_definition.get_all_fields_with_base_classes():
            if isinstance(field, ItemFieldReferencedByItems):
                continue  # not interesting with copying items that reference our one
            if isinstance(field, ItemFieldDirectlyAssociatedItems):
                for belonging in item.belongings:
                    copy_prompt_item_with_parents_and_belongings(collection=collection, item=belonging)
            if isinstance(field, ItemFieldItem):
                # first we need to copy fields that are necessary part of copied item (e.g. location for visit)
                if field.not_none_value_required_in_constructor:
                    pass  # already copied above
                else:
                    additional_item_to_copy = getattr(item, field.field_name)
                    if additional_item_to_copy:
                        copied_additional_item = copy_prompt_item_with_parents_and_belongings(
                            collection=collection, item=additional_item_to_copy)
                        setattr(new_item, field.field_name, copied_additional_item)
        return new_item


def _copy_admin_entity_prompt_with_parents(collection: Collection, item: AdminEntity) -> AdminEntity:
    existing_item = find_same_item_in_collection(item=item, collection=collection)
    if existing_item:
        return existing_item

    if isinstance(item, Country):
        new_item = item.copy_fields_without_associated_items()
        collection.add_item(new_item)
        return new_item
    elif isinstance(item, (RegionMajor, RegionMinor, City)):
        new_item = item.copy_fields_without_associated_items()

        # copy parent location
        item_parent = item.get_parent_location()
        new_item_parent = _copy_admin_entity_prompt_with_parents(collection=collection, item=item_parent)
        new_item.set_parent_location(new_item_parent)
        collection.add_item(new_item)

        return new_item
    else:
        raise Exception(f'Unhandled item prompt type during copying to traveller db: {item.__class__.__name__}')
