import itertools
import logging
from dataclasses import dataclass
from typing import Union

from items.attachment.photo import Photo
from items.flight import Flight
from items.journey import Journey
from items.visit import Visit

Y_VALUE_TO_ITEM_TYPE_MAPPING = {
    1: Journey,
    2: Visit,
    3: Photo,
    4: Flight,
}

ITEM_TYPE_TO_Y_VALUE_MAPPING = {value: key for key, value in Y_VALUE_TO_ITEM_TYPE_MAPPING.items()}


@dataclass
class PlotItemDefinition:
    x_start: float
    y: float
    txt: str
    x_end: Union[float, None] = None
    y_width: float = 0.1

    def __post_init__(self):
        if self.x_start is None and self.x_end is None:
            raise Exception(f'Cannot plot item ("{self.txt}") without start and end time!')
        if self.x_end is None:
            self.x_end = self.x_start
        elif self.x_start is None:
            self.x_start = self.x_end

    def get_middle_x(self):
        if self.x_end is not None:
            return (self.x_start + self.x_end) / 2
        else:
            return self.x_start


def get_plot_points_for_items(photos):
    point_items = []
    y_for_photo = ITEM_TYPE_TO_Y_VALUE_MAPPING[Photo]
    for photo in photos:
        photo_time = photo.photo_time
        if photo_time is None:
            logging.warning(f'Skipping photo "{photo.get_short_description()}" on timeline, '
                            f'because of missing photo time!')
            continue
        point_items.append(PlotItemDefinition(x_start=photo_time.to_unix_time(),
                                              y=y_for_photo,
                                              # txt=photo.get_file_name() + f' [id {photo.item_id}]',  # too slow to plot 10000 text items
                                              txt='',
                                              ))
    return point_items


def get_plot_rectangles_for_items(journeys, visits, flights):
    rectangle_items = []
    for item in itertools.chain(journeys, visits, flights):
        start_time = item.start_time
        end_time = item.end_time

        if start_time is None and end_time is None:
            logging.warning(f'Skipping {item.__class__.__name__} "{item.get_short_description()}" '
                            f'on timeline, because of missing start and end time!')
            continue

        rectangle_items.append(PlotItemDefinition(x_start=start_time.to_unix_time() if start_time else None,
                                                  x_end=end_time.to_unix_time() if end_time else None,
                                                  y=ITEM_TYPE_TO_Y_VALUE_MAPPING[item.__class__],
                                                  txt=item.get_short_description() + f' [id {item.item_id}]',
                                                  ))
    return rectangle_items
