from functools import lru_cache

from items.airport import Airport
from items.attachment.photo import Photo
from items.collection import ALL_ITEM_CLASSES
from items.item import Item
from items.journey import Journey
from items.location.admin_entity.admin_entity import AdminEntity
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items.location.place import Place
from items.visit import Visit

ALL_ITEM_CLASSES_BY_NAMES = {item_type.__name__: item_type for item_type in ALL_ITEM_CLASSES}

ALL_ITEMS_WITH_HANDLED_PROMPTS = {AdminEntity, Country, RegionMajor, RegionMinor, City, Airport}


def get_item_class_by_name_string(name: str):
    item = ALL_ITEM_CLASSES_BY_NAMES[name]
    return item


def get_name_for_dummy_region_major(region_major_code):
    return f'{region_major_code} - Unknown region major'


def get_name_for_dummy_region_minor(region_minor_code):
    return f'{region_minor_code} - Unknown region minor'


@lru_cache(maxsize=1000000)
def get_item_coordinates_or_parent_location__cached(item):
    return get_item_coordinates_or_parent_location(item)


def get_item_coordinates_or_parent_location(item: Item):
    if hasattr(item, 'coordinates') and item.coordinates:
        return item.coordinates

    if isinstance(item, AdminEntity):
        return item.coordinates
    elif isinstance(item, Photo):
        if item.annotation and not item.ignore_annotation:
            return item.annotation.get_coordinates()
        elif item.attached_to:
            return get_item_coordinates_or_parent_location(item.attached_to)
        else:
            return None
    elif isinstance(item, Visit):
        if item.location:
            return get_item_coordinates_or_parent_location(item.location)
        else:
            raise Exception('Cannot obtain coordinates for Visit without location!')
    elif isinstance(item, Journey):
        return None
    elif isinstance(item, Place):
        if item.coordinates:
            return item.coordinates
        elif item.location:
            return get_item_coordinates_or_parent_location(item.location)
    else:
        raise NotImplementedError(f'Cannot get coordinates for item of type "{item.__class__.__name__}"')


def get_closes_item_on_timeline(items_for_y_values, x, y, x_range):
    possible_items = items_for_y_values.get(y, [])
    closest_distance = 999999999999
    closest_item = None

    if not len(possible_items):
        return

    for item in possible_items:
        if isinstance(item, Photo):
            if not item.photo_time:
                continue
            distance_to_item = abs(x - item.photo_time.unix_time)
        else:
            if item.start_time and item.end_time:
                middle_point = (item.start_time.unix_time + item.end_time.unix_time) / 2
            else:
                middle_point_datetime = item.start_time or item.end_time
                if middle_point_datetime:
                    middle_point = middle_point_datetime.unix_time
                else:
                    continue
            distance_to_item = abs(x - middle_point)
        if distance_to_item < closest_distance:
            closest_item = item
            closest_distance = distance_to_item

    if closest_item:
        X_RANGE_MARGIN_PERCENTAGE = 0.02
        if closest_distance < x_range * X_RANGE_MARGIN_PERCENTAGE:
            return closest_item

    return None
