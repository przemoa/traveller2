import datetime
import logging
import os

import exifread

exifread.logger.setLevel(logging.WARNING)
import io
from PIL import ExifTags

from PIL import Image

from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True  # Because there was PIL error for some files, and it helps


from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items.attachment.photo import Photo
from utils.misc import get_relative_path_or_none_if_not_subdirectory


def get_photo_from_file_path(abs_file_path, base_dir, thumbnail_size: int) -> Photo:
    """ create Photo Item from file in filesystem

    :param abs_file_path: absolute file path
    :param base_dir: base dir. If it overlaps with file_path, path for the photo will be se as relative
    :return:
    """
    with open(abs_file_path, 'rb') as f:
        tags = exifread.process_file(f)

    coordinates = _extract_coordinates_from_exti_tags(tags)
    if 'Image Model' in tags:
        camera_model = tags['Image Model'].values
    else:
        camera_model = ''
    photo_time_from_metadata = _extract_datetime_from_exti_tags(tags)
    if not photo_time_from_metadata:
        logging.warning(f'Failed to read photo time for photo "{abs_file_path}"')

    thumbnail = create_thumbnail(abs_file_path, thumbnail_size)
    return Photo(
        coordinates=coordinates,
        original_full_path=os.path.abspath(abs_file_path),
        relative_file_path=get_relative_path_or_none_if_not_subdirectory(abs_file_path, base_dir),
        camera_model=camera_model,
        thumbnail=thumbnail,
        photo_time=photo_time_from_metadata)


def create_thumbnail(file_path, thumbnail_size: int) -> bytes:
    im = get_rotated_image_for_path(file_path)
    im.thumbnail((thumbnail_size, thumbnail_size))
    bytes_io = io.BytesIO()
    im.save(bytes_io, format='JPEG')
    bytes_array = bytes_io.getvalue()
    return bytes_array


def get_rotated_image_for_path(abs_file_path):
    image = Image.open(abs_file_path)
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation':
                break
        exif = dict(image._getexif().items())

        if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image = image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
    except (AttributeError, KeyError, IndexError):
        # cases: image don't have getexif
        pass
    return image


def _extract_coordinates_from_exti_tags(tags) -> Coordinates:
    def deg_min_sec_to_float(vals: list) -> float:
        deg = vals[0].num / vals[0].den
        min = vals[1].num / vals[1].den
        sec = vals[2].num / vals[2].den
        return deg + min / 60 + sec / 3600

    if 'GPS GPSLongitude' in tags and 'GPS GPSLatitude' in tags:
        coordinates = Coordinates(0, 0)

        value = deg_min_sec_to_float(tags['GPS GPSLongitude'].values)
        if tags['GPS GPSLongitudeRef'].values == 'W':
            value = -value
        coordinates.lon = value

        value = deg_min_sec_to_float(tags['GPS GPSLatitude'].values)
        if tags['GPS GPSLatitudeRef'].values == 'S':
            value = -value
        coordinates.lat = value
        return coordinates
    else:
        return None


def _extract_datetime_from_exti_tags(tags) -> CustomDateTime:
    final_date_time_tag_name = None
    date_time_tag_names = ['Image DateTime', 'EXIF DateTimeOriginal']
    for date_time_tag_name in date_time_tag_names:
        if date_time_tag_name in tags:
            final_date_time_tag_name = date_time_tag_name
            break
    if not final_date_time_tag_name:
        return None
    txt = tags[final_date_time_tag_name].values

    # some photos contain hour as 24 instead of 0...
    add_1_hour = False
    if ' 24:' in txt:
        logging.warning(f'Photo with invalid time format ({txt}) encountered.')
        txt = txt.replace(' 24:', ' 23:')
        add_1_hour = True

    date_time = datetime.datetime.strptime(txt, '%Y:%m:%d %H:%M:%S')
    if add_1_hour:
        date_time += datetime.timedelta(hours=1)

    return CustomDateTime.from_datetime(date_time)


if __name__ == '__main__':
    # p1 = get_photo_from_file_path('/media/przemek/dane/samba_ro/Zdjecia/z_telefonu/11_18-19/IMG_20190217_153455.jpg', '', 55)
    # print(p1.coordinates.get_short_description())
    p2 = get_photo_from_file_path("/media/przemek/dane/samba_ro/Zdjecia/z_telefonu/8_15-16/IMG_20151116_002134.jpg", '', 55)
