import itertools
from typing import Union, List

from common.types.misc import PromptSourceType
from engine.engine import Engine
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor


def get_country_prompts(engine, prompt_type: PromptSourceType):
    items_prompt_db = []
    items_traveller_db = []
    if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        items_traveller_db = engine.collection.countries

    if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        items_prompt_db_with_duplicates = engine.prompts_collection.countries
        items_traveller_db_by_iso_code = {c.iso_code: c for c in items_traveller_db
                                          if c.iso_code is not None}
        items_prompt_db = [c for c in items_prompt_db_with_duplicates
                           if c.iso_code not in items_traveller_db_by_iso_code]

    return items_traveller_db + items_prompt_db


def get_region_major_prompts(engine, prompt_type: PromptSourceType, parent: Union[Country, None] = None):
    if parent is None:
        items_prompt_db = []
        items_traveller_db = []

        if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_traveller_db = engine.collection.regions_major

        if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_prompt_db_with_duplicates = engine.prompts_collection.regions_major
            items_traveller_db_by_genome_code = {r.geoname_code: r for r in items_traveller_db
                                                 if r.geoname_code is not None}
            items_prompt_db = [r for r in items_prompt_db_with_duplicates
                               if r.geoname_code not in items_traveller_db_by_genome_code]
        return items_traveller_db + items_prompt_db
    else:
        return _get_regions_major_for_country(
            engine=engine,
            prompt_type=prompt_type,
            country=parent)


def get_region_minor_prompts(engine, prompt_type: PromptSourceType, parent: Union[Country, RegionMajor, None] = None):
    if parent is None:
        items_prompt_db = []
        items_traveller_db = []

        if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_traveller_db = engine.collection.regions_minor

        if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_prompt_db_with_duplicates = engine.prompts_collection.regions_minor
            items_traveller_db_by_genome_code = {r.geoname_code: r for r in items_traveller_db
                                                 if r.geoname_code is not None}
            items_prompt_db = [r for r in items_prompt_db_with_duplicates
                               if r.geoname_code not in items_traveller_db_by_genome_code]
        return items_traveller_db + items_prompt_db
    else:
        if isinstance(parent, Country):
            return _get_regions_minor_for_country(
                engine=engine,
                prompt_type=prompt_type,
                country=parent)
        if isinstance(parent, RegionMajor):
            return _get_regions_minor_for_region_major(
                engine=engine,
                prompt_type=prompt_type,
                region_major=parent)


def get_city_prompts(engine,
                     prompt_type: PromptSourceType,
                     parent: Union[Country, RegionMajor, RegionMinor, None] = None) -> List[City]:
    if parent is None:
        items_prompt_db = []
        items_traveller_db = []

        if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_traveller_db = engine.collection.cities

        if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
            items_prompt_db_with_duplicates = engine.prompts_collection.cities
            items_traveller_db_by_genome_code = {c.geoname_code: c for c in items_traveller_db
                                                 if c.geoname_code is not None}
            items_prompt_db = [c for c in items_prompt_db_with_duplicates
                               if c.geoname_code not in items_traveller_db_by_genome_code]
        return items_traveller_db + items_prompt_db
    else:
        if isinstance(parent, Country):
            return _get_cities_for_country(
                engine=engine,
                prompt_type=prompt_type,
                country=parent)
        if isinstance(parent, RegionMajor):
            return _get_cities_for_region_major(
                engine=engine,
                prompt_type=prompt_type,
                region_major=parent)
        if isinstance(parent, RegionMinor):
            return _get_cities_for_region_minor(
                engine=engine,
                prompt_type=prompt_type,
                region_minor=parent)


# TODO how about removing duplicates from result? (if prompt same (same geoname id) as item from traveller db? - DONE

def _get_cities_for_region_minor(engine: Engine,
                                 prompt_type: PromptSourceType,
                                 region_minor: RegionMinor):
    items_traveller_db = []
    items_prompt_db = []

    if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        if region_minor.is_from_prompts_database():
            if region_minor.geoname_code is not None:
                matching_items = [r for r in engine.collection.regions_minor
                                  if r.geoname_code == region_minor.geoname_code]
                assert len(matching_items) <= 1
                if len(matching_items) == 1:
                    items_traveller_db = matching_items[0].belongings
                else:
                    pass  # matching item not found
            else:
                pass  # no way to find item in traveller database basing on item in prompts database
        else:
            items_traveller_db = region_minor.belongings

    if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        if region_minor.geoname_code is not None:
            items_prompt_db_with_duplicates = []
            if region_minor.is_from_prompts_database():
                items_prompt_db_with_duplicates = region_minor.belongings
            else:
                matching_items = [r for r in engine.prompts_collection.regions_minor
                                  if r.geoname_code == region_minor.geoname_code]
                assert len(matching_items) <= 1
                if len(matching_items) == 1:
                    items_prompt_db_with_duplicates = matching_items[0].belongings
                else:
                    pass  # matching item not found
            items_traveller_db_by_genome_code = {c.geoname_code: c for c in items_traveller_db
                                                 if c.geoname_code is not None}
            items_prompt_db = [c for c in items_prompt_db_with_duplicates
                               if c.geoname_code not in items_traveller_db_by_genome_code]
        else:
            # no way to find item in prompts database basing on item from traveller database
            pass

    return items_traveller_db + items_prompt_db


def _get_regions_minor_for_region_major(engine: Engine,
                                        prompt_type: PromptSourceType,
                                        region_major: RegionMajor):
    items_traveller_db = []
    items_prompt_db = []

    if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        if region_major.is_from_prompts_database():
            if region_major.geoname_code is not None:
                matching_items = [r for r in engine.collection.regions_major
                                  if r.geoname_code == region_major.geoname_code]
                assert len(matching_items) <= 1
                if len(matching_items) == 1:
                    items_traveller_db = matching_items[0].belongings
                else:
                    pass  # matching item not found
            else:
                pass  # no way to find item in traveller database basing on item in prompts database
        else:
            items_traveller_db = region_major.belongings

    if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        if region_major.geoname_code is not None:
            items_prompt_db_with_duplicates = []
            if region_major.is_from_prompts_database():
                items_prompt_db_with_duplicates = region_major.belongings
            else:
                matching_items = [r for r in engine.prompts_collection.regions_major
                                  if r.geoname_code == region_major.geoname_code]
                assert len(matching_items) <= 1
                if len(matching_items) == 1:
                    items_prompt_db_with_duplicates = matching_items[0].belongings
                else:
                    pass  # matching item not found
            items_traveller_db_by_genome_code = {r.geoname_code: r for r in items_traveller_db
                                                 if r.geoname_code is not None}
            items_prompt_db = [r for r in items_prompt_db_with_duplicates
                               if r.geoname_code not in items_traveller_db_by_genome_code]
        else:
            # no way to find item in prompts database basing on item from traveller database
            pass

    return items_traveller_db + items_prompt_db


def _get_regions_major_for_country(engine: Engine,
                                   prompt_type: PromptSourceType,
                                   country: Country):
    items_traveller_db = []
    items_prompt_db = []

    if prompt_type in [PromptSourceType.TRAVELLER_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        matching_items = [c for c in engine.collection.countries
                          if c.iso_code == country.iso_code]
        assert len(matching_items) <= 1
        if len(matching_items) == 1:
            items_traveller_db = matching_items[0].belongings
        else:
            pass  # matching item not found

    if prompt_type in [PromptSourceType.PROMPT_DATABASE, PromptSourceType.ALL_AVAILABLE]:
        matching_items = [c for c in engine.prompts_collection.countries
                          if c.iso_code == country.iso_code]
        assert len(matching_items) <= 1
        if len(matching_items) == 1:
            items_prompt_db_with_duplicates = matching_items[0].belongings
            items_traveller_db_by_genome_code = {r.geoname_code: r for r in items_traveller_db
                                                 if r.geoname_code is not None}
            items_prompt_db = [r for r in items_prompt_db_with_duplicates
                               if r.geoname_code not in items_traveller_db_by_genome_code]
        else:
            pass  # matching item not found

    return items_traveller_db + items_prompt_db


def _get_cities_for_region_major(engine: Engine,
                                 prompt_type: PromptSourceType,
                                 region_major: RegionMajor):
    regions_minor = _get_regions_minor_for_region_major(engine=engine,
                                                        prompt_type=prompt_type,
                                                        region_major=region_major)
    all_cities_sublists = []
    for regions_minor in regions_minor:
        all_cities_sublists.append(_get_cities_for_region_minor(
            engine=engine,
            prompt_type=prompt_type,
            region_minor=regions_minor))
    return list(itertools.chain(*all_cities_sublists))


def _get_cities_for_country(engine: Engine,
                            prompt_type: PromptSourceType,
                            country: Country):
    regions_major = _get_regions_major_for_country(engine=engine,
                                                   prompt_type=prompt_type,
                                                   country=country)
    all_cities_sublists = []
    for region_major in regions_major:
        all_cities_sublists.append(_get_cities_for_region_major(
            engine=engine,
            prompt_type=prompt_type,
            region_major=region_major))
    return list(itertools.chain(*all_cities_sublists))


def _get_regions_minor_for_country(engine: Engine,
                                   prompt_type: PromptSourceType,
                                   country: Country):
    regions_major = _get_regions_major_for_country(engine=engine,
                                                   prompt_type=prompt_type,
                                                   country=country)
    all_regions_minor_sublists = []
    for region_major in regions_major:
        all_regions_minor_sublists.append(_get_regions_minor_for_region_major(
            engine=engine,
            prompt_type=prompt_type,
            region_major=region_major))
    return list(itertools.chain(*all_regions_minor_sublists))
