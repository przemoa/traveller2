#!/usr/bin/env bash

# Does not modify source directory, copies everything first

set -e
set -x


OPTION_BUILD_PROMPTS_DB=false
OPTION_RUN_UNIT_TESTS=false
OPTION_BUILD_DEBIAN_PACKAGE=false


PROJECT_SOURCE_DIR="$(dirname "$0")/../"
PROJECT_BINARY_DIR=`pwd`
DEBIAN_PACKAGE_FILES_BASE_DIR="build/linux/"
CODE_INSTALLATION_DIR="/opt/traveller2"


showHelp()
{
    printf "Usage: $0 [OPTIONS]\n"
    printf "\n"
    printf "  -h Show this help\n"
    printf "  -p Build prompts database\n"
    printf "  -t Run unit tests and coverage report\n"
    printf "  -d Build debian package\n"
}

parse_input_options()
{
    while getopts "hptd" arg; do
        printf "parsing arg: '${arg}' \n"
        case "${arg}" in
            h) printf "Build/test traveller2\n"
                showHelp
                exit 0
            ;;
            p) OPTION_BUILD_PROMPTS_DB=true
            ;;
            t) OPTION_RUN_UNIT_TESTS=true
            ;;
            d) OPTION_BUILD_DEBIAN_PACKAGE=true
            ;;
            *) echo "Invalid option -$OPTARG" >&2
                showHelp
                exit 1
            ;;
        esac
    done
    shift $((OPTIND-1))

    printf "OPTION_BUILD_PROMPTS_DB=$OPTION_BUILD_PROMPTS_DB \n"
    printf "OPTION_RUN_UNIT_TESTS=$OPTION_RUN_UNIT_TESTS \n"
    printf "OPTION_BUILD_DEBIAN_PACKAGE=$OPTION_BUILD_DEBIAN_PACKAGE \n"
}

delete_temporary_files()
{
    find . -name "*.pyc" -type f -delete
    find . -name "*.pyo" -type f -delete
    find . -name "*.json" -type f -delete
}

copy_project_code()
{
    if [[ "${PROJECT_SOURCE_DIR}" -ef "`pwd`" ]]
    then
        echo "Same directory, copying of the code skipped!"
    else
        echo "Different build directory, copying code...!"
        rm -rf build
        rm -rf dist
        mkdir dist
        rsync --delete --exclude="venv" --exclude=".git" --recursive $PROJECT_SOURCE_DIR .
    fi

    delete_temporary_files
}

setup_and_activate_virtual_env()
{
    ENV_CONFIGURED__PATH="venv/bin/.env_configured"
    if [ -f "$ENV_CONFIGURED__PATH" ]
    then
        printf "Activating virtual environment...\n"
        source ./venv/bin/activate
    else
        printf "Creating virtual environment...\n"
        python3.7 -m venv venv
        source venv/bin/activate

        printf "Installing requirements...\n"
        pip install --upgrade pip
        pip install -r requirements.txt

        # not in pip packages - update also it during download
        pip install git+https://github.com/pyqtgraph/pyqtgraph

        touch "$ENV_CONFIGURED__PATH"
    fi

    export PYTHONPATH=`pwd`/src:`pwd`/tests:`pwd`/misc/global_components_creator:$PYTHONPATH
}

generate_items()
{
    printf "Generating items...\n"
    python3 src/items_definitions/items_generator.py
    delete_temporary_files
}

generate_prompts_database()
{
    printf "Generating prompts database... \n"
    GLOBAL_COMPONENS_CREATOR_DIR=misc/global_components_creator
    ABSOLUTE_OUTPUT_PROMPTS_DATABASE_PATH=`python3 -c "from common.package_config import package_config_release as pcr; print(pcr.prompts_database_path)"`
    echo $ABSOLUTE_OUTPUT_PROMPTS_DATABASE_PATH
    python3 $GLOBAL_COMPONENS_CREATOR_DIR/generate_prompts_database.py --output_file_path "${DEBIAN_PACKAGE_FILES_BASE_DIR}${ABSOLUTE_OUTPUT_PROMPTS_DATABASE_PATH}" --continent ALL
}

run_unit_tests()
{
    printf "Running tests... \n"
    pytest --cov-report=html --cov=src tests/
    printf "Test coverage report in htmlcov directory.\n"
}

create_mocs()
{
    pushd src
    printf "Creating mocs...\n"
    python3 -c "import gui_setup; gui_setup.moc_ui_files(True)"
    popd
}

copy_source_code_to_debian_package()
{
    mkdir -p "${DEBIAN_PACKAGE_FILES_BASE_DIR}${CODE_INSTALLATION_DIR}"
    rsync --delete --recursive  src/ "${DEBIAN_PACKAGE_FILES_BASE_DIR}${CODE_INSTALLATION_DIR}"
}

build_debian_package()
{
    PACKAGE_VERSION=`python3 -c "import _version; print(_version.version_for_deb_package)"`
    sed -i "s/<<<VERSION>>>/$PACKAGE_VERSION/g" ${DEBIAN_PACKAGE_FILES_BASE_DIR}/DEBIAN/control

    dpkg-deb --build build/linux
    mkdir dist
    mv build/linux.deb dist/traveller2_$PACKAGE_VERSION.deb

    printf "Output traveller2 deb package in dist directory.\n"
}

download_requirements()
{
    pushd ${DEBIAN_PACKAGE_FILES_BASE_DIR}
    mkdir -p tmp/traveller2/requirements
    cd tmp/traveller2/requirements
    pip download -r ${PROJECT_BINARY_DIR}/requirements.txt

    # not in pip packages
    pip download git+https://github.com/pyqtgraph/pyqtgraph

    popd
}


parse_input_options $@

copy_project_code

setup_and_activate_virtual_env

generate_items

create_mocs

[[ $OPTION_BUILD_DEBIAN_PACKAGE == true ]] && copy_source_code_to_debian_package

[[ $OPTION_BUILD_DEBIAN_PACKAGE == true ]] && download_requirements

[[ $OPTION_RUN_UNIT_TESTS == true ]] && run_unit_tests

[[ $OPTION_BUILD_PROMPTS_DB == true ]] && generate_prompts_database

[[ $OPTION_BUILD_DEBIAN_PACKAGE == true ]] && build_debian_package


printf "Done with all!\n"
