# -*- mode: python -*-
import os

block_cipher = None

spec_root = os.path.abspath(SPECPATH)


images_to_add = [
    'traveller2.png',
    'traveller2.ico',
    ]

a = Analysis(['run_traveller2_gui.py'],
             pathex=['/home/przemek/Projects/traveller2/src'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

for img_name in images_to_add:
    image_path = os.path.join(spec_root, 'resources', 'img', img_name)
    a.datas += [(img_name, image_path, 'DATA')]

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='traveller2_gui',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False,
          icon='resources/img/traveller2.ico',
          )
