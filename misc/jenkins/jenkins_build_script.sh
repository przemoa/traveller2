#!/usr/bin/env bash

set -e
set -x

git log -n 1


if [ -z "$BRANCH_TO_BUILD" ]
then
    echo "Buildng latest branch"
else
    git checkout ${BRANCH_TO_BUILD}
    echo "Building ${BRANCH_TO_BUILD}"
fi

OPTIONS_STRING=

[[ $OPTION_RUN_UNIT_TESTS == true ]] && OPTIONS_STRING=${OPTIONS_STRING}" -t"
[[ $OPTION_BUILD_PROMPTS_DB == true ]] && OPTIONS_STRING=${OPTIONS_STRING}" -p"
[[ $OPTION_BUILD_DEBIAN_PACKAGE == true ]] && OPTIONS_STRING=${OPTIONS_STRING}" -d"

echo "Options string: '${OPTIONS_STRING}'"

./build/build_all_linux.sh $OPTIONS_STRING
