#!/usr/bin/env bash


set -x  # print commands
set -e  # any commands which fail will cause the shell script to exit immediately
set -u  # treat unset variables and parameters other than the special parameters "@" and "*" as an error

printf "About to generate items..."


SCRIPT_DIR="$(dirname "$0")"
SRC_DIR=$SCRIPT_DIR/../src

if [ -z ${PYTHONPATH+x} ]; then PYTHONPATH=; fi
if [ -z ${PYTHONPATH+x} ]; then echo "PYTHONPATH is unset"; else echo "PYTHONPATH is set to '$PYTHONPATH'"; fi

export PYTHONPATH=$SRC_DIR:$PYTHONPATH

python3 $SRC_DIR/items_definitions/items_generator.py

printf "Finished generating items!"
