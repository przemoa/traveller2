"""
Gather data about cities:
- name
- location
- coordinates
- population
"""

import csv
import logging
import os
import urllib.request

import config
from items.collection import Collection, ItemNotUniqueError
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_utils.misc import get_name_for_dummy_region_major


def _download_region_minor_info_file():
    logging.info('Downloading region_minor info file...')
    region_minor_info_file_path = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'admin2Codes.txt')
    if not os.path.isfile(region_minor_info_file_path):
        url = 'http://download.geonames.org/export/dump/admin2Codes.txt'
        urllib.request.urlretrieve(url, region_minor_info_file_path)
    logging.info('region_minor info file downloaded to {}'.format(region_minor_info_file_path))
    return region_minor_info_file_path


def _parse_region_minor_info_file(file_path,
                                  collection: Collection):
    geonameid_fileds = [
        'country_admin1_and_2_code', 'name',	'ascii_name'
    ]
    column_with_country_admin1_and_2_code = geonameid_fileds.index('country_admin1_and_2_code')
    column_with_name = geonameid_fileds.index('name')
    column_with_ascii_name = geonameid_fileds.index('ascii_name')

    with open(file_path, 'rt') as csv_file:
        cities_reader = csv.reader(csv_file, delimiter='\t')
        for row in cities_reader:
            geoname_code = row[column_with_country_admin1_and_2_code]
            # print(geoname_code)
            country_code, admin1_code_str, region_minor_code_str = geoname_code.split('.')

            country = collection.countries_by_iso_code.get(country_code, None)
            if country is None:
                continue  # not in requested countries

            region_major_code = f'{country_code}.{admin1_code_str}'
            region_major = collection.regions_major_by_geoname_code.get(region_major_code, None)
            if region_major is None:
                # region minor has not parent region major.
                # We need to create dummy region major for that country
                # raise Exception('Unknown region major!')
                #print(f'Adding dummy region major: {region_major_code}')
                dummy_region_major_name = get_name_for_dummy_region_major(region_major_code)
                region_major = RegionMajor(
                    name=dummy_region_major_name,
                    non_ascii_name=dummy_region_major_name,
                    country=country,
                    geoname_code=region_major_code
                )
                collection.add_item(region_major)
                collection.regions_major_by_geoname_code[region_major_code] = region_major

            region_minor = RegionMinor(
                name=row[column_with_ascii_name],
                non_ascii_name=row[column_with_name],
                geoname_code=geoname_code,
                region_major=region_major
            )


            region_added = False
            index = 1
            original_name = ''
            while not region_added:
                try:
                    collection.add_item(region_minor)
                    region_added = True
                except ItemNotUniqueError:
                    if index == 1:
                        original_name = region_minor.name
                    index += 1
                    if index >= 3:
                        print(f'!!!!!!!!!! {index} regions minor with same name?!?')
                    region_minor.name = original_name + f' ({index})'
                    # print(f'Not unique region minor name in specific region major ({region_minor.name}): '
                    #       f'changing to {region_minor.name}')

    collection.regions_minor_by_geoname_code = {r.geoname_code: r for r in collection.regions_minor}


def add_regions_minor(collection) -> list:
    region_minor_file_path = _download_region_minor_info_file()
    _parse_region_minor_info_file(region_minor_file_path,
                                  collection=collection)
