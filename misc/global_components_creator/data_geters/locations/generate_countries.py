"""
Gather data about countries:
- name
- location
- capital
- population
"""

import csv
import logging
import os
import urllib.request

import config
from items.collection import Collection
from items.location.admin_entity.country import Country


def _download_country_info_file() -> str:
    logging.info('Downloading country info file...')
    countries_file_path = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'countryInfo.txt')
    if not os.path.isfile(countries_file_path):
        url = 'http://download.geonames.org/export/dump/countryInfo.txt'
        urllib.request.urlretrieve(url, countries_file_path)
    logging.info('Country info file downloaded to {}'.format(countries_file_path))
    return countries_file_path


def _parse_country_info_file(file_path,
                             searched_continent: str,
                             allowed_iso_codes: list,
                             excluded_iso_codes: list,
                             collection: Collection):
    logging.info('Parsing location info file...')
    with open(file_path, 'rt') as csv_file:
        country_reader = csv.reader(csv_file, delimiter='\t')
        last_row_with_hash = None

        headers_row_analysed = False
        column_with_iso_code = None
        column_with_name = None
        column_with_continent = None

        for row in country_reader:
            if row[0][0] == '#':
                last_row_with_hash = row
            else:
                if not headers_row_analysed:
                    column_with_iso_code = last_row_with_hash.index('#ISO')
                    column_with_name = last_row_with_hash.index('Country')
                    column_with_continent = last_row_with_hash.index('Continent')
                    headers_row_analysed = True
                if row[column_with_continent] != searched_continent:
                    continue
                if allowed_iso_codes is not None:
                    if row[column_with_iso_code] not in allowed_iso_codes:
                        continue
                if excluded_iso_codes is not None:
                    if row[column_with_name] in excluded_iso_codes:
                        continue

                country = Country(
                    name=row[column_with_name],
                    # continent_code=row[column_with_continent],
                    iso_code=row[column_with_iso_code]
                )
                collection.add_item(country)

    collection.countries_by_iso_code = {c.iso_code: c for c in collection.countries}
    logging.info('Country info file parsing finished, found {} countries'.format(len(collection.countries)))


def add_countries_on_continent(collection, allowed_iso_codes: list = None, excluded_iso_codes: list = None, continent='EU'):
    countries_file_path = _download_country_info_file()
    _parse_country_info_file(file_path=countries_file_path,
                             searched_continent=continent,
                             allowed_iso_codes=allowed_iso_codes,
                             excluded_iso_codes=excluded_iso_codes,
                             collection=collection)

