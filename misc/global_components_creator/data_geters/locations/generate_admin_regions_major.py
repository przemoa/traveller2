"""
Gather data about cities:
- name
- location
- coordinates
- population
"""

import csv
import logging
import os
import urllib.request

import config
from items.collection import Collection
from items.location.admin_entity.region_major import RegionMajor


def _download_region_major_info_file():
    logging.info('Downloading region_major info file...')
    region_major_info_file_path = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'admin1CodesASCII.txt')
    if not os.path.isfile(region_major_info_file_path):
        url = 'http://download.geonames.org/export/dump/admin1CodesASCII.txt'
        urllib.request.urlretrieve(url, region_major_info_file_path)
    logging.info('region_major info file downloaded to {}'.format(region_major_info_file_path))
    return region_major_info_file_path


def _parse_region_major_regions_info_file(file_path, collection: Collection):
    geonameid_fileds = [
        'country_and_region_major_code', 'name',	'ascii_name'
    ]
    column_with_country_and_region_major_code = geonameid_fileds.index('country_and_region_major_code')
    column_with_name = geonameid_fileds.index('name')
    column_with_ascii_name = geonameid_fileds.index('ascii_name')

    with open(file_path, 'rt') as csv_file:
        cities_reader = csv.reader(csv_file, delimiter='\t')
        for row in cities_reader:
            geoname_code = row[column_with_country_and_region_major_code]
            country_code, region_major_code_str = geoname_code.split('.')

            country = collection.countries_by_iso_code.get(country_code, None)
            if country is None:
                continue

            region_major = RegionMajor(
                name=row[column_with_ascii_name],
                non_ascii_name=row[column_with_name],
                geoname_code=geoname_code,
                country=country,
            )
            collection.add_item(region_major)
    collection.regions_major_by_geoname_code = {r.geoname_code: r for r in collection.regions_major}


def add_regions_major(collection) -> list:
    region_major_file_path = _download_region_major_info_file()
    _parse_region_major_regions_info_file(
        region_major_file_path, collection=collection)
