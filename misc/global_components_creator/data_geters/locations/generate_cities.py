"""
Gather data about cities:
- name
- location
- coordinates
- population
"""

import csv
import logging
import os
import urllib.request
import zipfile

import config
from common.types.coordinates import Coordinates
from items.collection import Collection, ItemNotUniqueError
from items.location.admin_entity.city import City
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_utils.misc import get_name_for_dummy_region_major, get_name_for_dummy_region_minor


def _download_city_info_file():
    logging.info('Downloading city info file...')
    final_txt_file = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'cities1000.txt')
    if not os.path.isfile(final_txt_file):
        zip_file_path = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'cities1000.zip')
        if not os.path.isfile(zip_file_path):
            url = 'http://download.geonames.org/export/dump/cities1000.zip'
            urllib.request.urlretrieve(url, zip_file_path)
        logging.info('Unzipping city info file...')
        zip_ref = zipfile.ZipFile(zip_file_path, 'r')
        zip_ref.extractall(config.DOWNLOADED_FILES_DIRECTORY)
        zip_ref.close()
    logging.info('City info file downloaded: {}'.format(final_txt_file))
    return final_txt_file


def _parse_city_info_file(file_path, collection: Collection) :
    geonameid_fileds = [
        'geoname_id', 'name',	'ascii_name',
        'alternative_names', 'latitude', 'longitude',
        'feature_class', 'feature_code', 'country_code',
        'cc2', 'admin1_code', 'admin2_code',
        'admin3_code', 'admin4_code', 'population',
        'elevation'
    ]
    column_with_geoname_id = geonameid_fileds.index('geoname_id')
    column_with_name = geonameid_fileds.index('name')
    column_with_ascii_name = geonameid_fileds.index('ascii_name')
    column_with_latitude = geonameid_fileds.index('latitude')
    column_with_longitude = geonameid_fileds.index('longitude')
    column_with_population = geonameid_fileds.index('population')
    column_with_country_code = geonameid_fileds.index('country_code')
    column_with_admin1_code = geonameid_fileds.index('admin1_code')
    column_with_admin2_code = geonameid_fileds.index('admin2_code')

    with open(file_path, 'rt') as csv_file:
        cities_reader = csv.reader(csv_file, delimiter='\t')
        for row in cities_reader:
            country_code = row[column_with_country_code]
            admin1_code_str = row[column_with_admin1_code]
            admin2_code_str = row[column_with_admin2_code]
            city_code_str = row[column_with_geoname_id]

            geoname_code = f'{country_code}.{admin1_code_str}.{admin2_code_str}.{city_code_str}'
            # print(geoname_code)
            region_minor_code = f'{country_code}.{admin1_code_str}.{admin2_code_str}'
            region_major_code = f'{country_code}.{admin1_code_str}'

            country = collection.countries_by_iso_code.get(country_code, None)
            if country is None:
                continue  # not in requested countries

            region_minor = collection.regions_minor_by_geoname_code.get(region_minor_code, None)
            if region_minor is None:
                # city has not parent region minor.
                # We need to create dummy region minor for that country
                # raise Exception('Unknown region minor!')

                region_major = collection.regions_major_by_geoname_code.get(region_major_code, None)
                if region_major is None:
                    # region minor has not parent region major.
                    # We need to create dummy region major for that country
                    # raise Exception('Unknown region major!')
                    #print(f'Adding dummy region major: {region_major_code}')
                    dummy_region_major_name = get_name_for_dummy_region_major(region_major_code)
                    region_major = RegionMajor(
                        name=dummy_region_major_name,
                        non_ascii_name=dummy_region_major_name,
                        country=country,
                        geoname_code=region_major_code,
                    )
                    collection.add_item(region_major)
                    collection.regions_major_by_geoname_code[region_major_code] = region_major

                #print(f'Adding dummy region minor: {region_minor_code}')
                dummy_region_minor_name = get_name_for_dummy_region_minor(region_minor_code)
                region_minor = RegionMinor(
                    name=dummy_region_minor_name,
                    non_ascii_name=dummy_region_minor_name,
                    region_major=region_major,
                    geoname_code=region_minor_code,
                )
                collection.add_item(region_minor)
                collection.regions_minor_by_geoname_code[region_minor_code] = region_minor

            city = City(
                name=row[column_with_ascii_name],
                non_ascii_name=row[column_with_name],
                geoname_code=geoname_code,
                region_minor=region_minor,
                population=int(row[column_with_population]),
                # coordinates=Coordinates(lat=float(row[column_with_latitude]),
                #                         lon=float(row[column_with_longitude]),),
                coordinates=Coordinates.from_tuple([
                    float(row[column_with_latitude]),
                    float(row[column_with_longitude])]),
            )

            city_added = False
            city_index = 1
            original_city_name = ''
            while not city_added:
                try:
                    collection.add_item(city)
                    city_added = True
                except ItemNotUniqueError:
                    if city_index == 1:
                        original_city_name = city.name
                    city_index += 1
                    if city_index >= 3:
                        print(f'!!!!!!!!!! {city_index} cities with same name?!?')
                    city.name = original_city_name + f' ({city_index})'
                    # print(f'Not unique city name in specific region minor ({region_minor.name}): '
                    #       f'changing to {city.name}')


def add_cities(collection) -> list:
    cities_file_path = _download_city_info_file()
    _parse_city_info_file(cities_file_path, collection=collection)

