"""
Gather data about countries:
- name
- location
- capital
- population
"""

import csv
import logging
import os
import urllib.request

import config
from common.types.coordinates import Coordinates
from items.airport import Airport
from items.collection import Collection, ItemNotUniqueError


def _download_airports_info_file() -> str:
    logging.info('Downloading airports info file...')
    airports_file_path = os.path.join(config.DOWNLOADED_FILES_DIRECTORY, 'airports.csv')
    if not os.path.isfile(airports_file_path):
        url = 'http://ourairports.com/data/airports.csv'
        urllib.request.urlretrieve(url, airports_file_path)
    logging.info('Airports info file downloaded to {}'.format(airports_file_path))
    return airports_file_path


def _parse_airports_info_file(file_path,
                              collection: Collection):
    logging.info('Parsing airports info file...')
    with open(file_path, 'rt') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        for i, row in enumerate(csv_reader):
            if i == 0:
                continue
            column_with_type = 2
            column_with_name = 3
            column_with_latitude = 4
            column_with_longitude = 5
            column_with_country_iso_code = 8
            column_with_municipality = 10

            if row[column_with_type] not in ['medium_airport', 'large_airport']:
                continue

            country = collection.countries_by_iso_code.get(row[column_with_country_iso_code], None)
            if country is None:
                continue

            name = row[column_with_name]
            if name in ['Airport', 'airport']:
                name = row[column_with_municipality] + ' Airport'
                print(f'changing airport name to: {name}')

            airport = Airport(
                name=name,
                location=country,
                municipality=row[column_with_municipality],
                coordinates=Coordinates.from_tuple([
                    float(row[column_with_latitude]),
                    float(row[column_with_longitude])]),
            )
            try:
                collection.add_item(airport)
            except ItemNotUniqueError:
                airport.name += f' ({airport.municipality})'
                print(f'Airport name changed to "{airport.name}"')
                try:
                    collection.add_item(airport)
                except ItemNotUniqueError:
                    print(f'Ignoring probablyh duplicated airport "{airport.name}"')
                    pass

    logging.info('Airports info file parsing finished, found {} airports'.format(len(collection.airports)))


def add_airports_for_countries(collection):
    airports_file_path = _download_airports_info_file()
    _parse_airports_info_file(file_path=airports_file_path,
                              collection=collection)

