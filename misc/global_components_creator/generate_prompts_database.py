import argparse
import os

# from traveller.prompts.locations.city_prompt import CityPrompt
# from traveller.prompts.locations.admin_region_major_prompt import AdminRegionMajorPrompt
# from traveller.prompts.locations.admin_region_minor_prompt import AdminRegionMinorPrompt
# from traveller.prompts.locations.country_prompt import CountryPrompt
from common.types.coordinates import Coordinates
from data_geters.airports.generate_airports import add_airports_for_countries
from data_geters.locations.generate_admin_regions_major import add_regions_major
from data_geters.locations.generate_admin_regions_minor import add_regions_minor
from data_geters.locations.generate_cities import add_cities
from data_geters.locations.generate_countries import add_countries_on_continent
from engine.engine import Engine
from items.collection import Collection


def generate_prompts_database(output_file_path: str, continent):
    collection = Collection()

    add_countries_on_continent(collection=collection, excluded_iso_codes=['CS'], continent=continent)
    # add_countries_on_continent(collection=collection, allowed_iso_codes=['PL', 'IT'])
    add_regions_major(collection)
    add_regions_minor(collection)
    add_cities(collection)
    print('Finished adding cities!')

    print('countries:', len(collection.countries))
    print('regions major:', len(collection.regions_major))
    print('regions minor:', len(collection.regions_minor))
    print('cities:', len(collection.cities))

    calculate_coordinates(collection)

    add_airports_for_countries(collection=collection)
    print('airports:', len(collection.airports))

    try:
        os.remove(output_file_path)
    except OSError:
        pass

    if os.path.dirname(output_file_path):
        os.makedirs(os.path.dirname(output_file_path), exist_ok=True)

    engine = Engine()
    engine.collection = collection
    engine.save_collection(output_file_path)
    print(f'Collection saved to "{output_file_path}", which is "{os.path.abspath(output_file_path)}"')


def calculate_coordinates(collection: Collection):
    for group in [collection.regions_minor, collection.regions_major, collection.countries]:
        for item in group:
            count = 0
            lat_sum = 0
            lon_sum = 0
            for belonging in item.belongings:
                if belonging.coordinates is None:
                    pass
                    # print(f'No coordinates for entity {belonging.__class__.__name__}: {belonging.name}')
                else:
                    count += 1
                    lat_sum += belonging.coordinates.lat
                    lon_sum += belonging.coordinates.lon
            if count > 0:
                item.coordinates = Coordinates(lat=lat_sum/count,
                                               lon=lon_sum/count)
            else:
                pass
                # print(f'No coordinates for entity {item.__class__.__name__}: {item.name}')
    print('Finished calculating coordinates!')


def main():
    parser = argparse.ArgumentParser(description='Generates prompts database for traveller2.')
    parser.add_argument('--output_file_path', metavar='PATH', type=str, default='traveller2_prompts.pkl',
                        help='File path for output path')
    parser.add_argument('--continent', metavar='CODE', type=str, default='EU',
                        help='Code of continent for which to generate prompts database. '
                             'Use "ALL" for to generate databases for each continents')
    args = parser.parse_args()
    if args.continent == 'ALL':
        if not args.output_file_path.endswith('.pkl'):
            raise Exception('output file path have to end with ".pkl" for generation of '
                            'prompt databases for "ALL" continents')
        for code in ['EU', 'AS', 'NA', 'OC', 'SA', 'AN', 'AF']:
            print(f'Generating prompts database for continent with code: {code}')
            file_path = args.output_file_path[:-len('_EU.pkl')] + f'_{code}.pkl'
            generate_prompts_database(output_file_path=file_path, continent=code)
    else:
        generate_prompts_database(output_file_path=args.output_file_path, continent=args.continent)


if __name__ == '__main__':
    main()
