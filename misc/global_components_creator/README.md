city has to belong to region minor.
region minor has to belong to region major.
region major to country.

regions and cities have to be identified by geoname code

# General info
This package (global_data_creator) is de facto not required for Traveller package.

It is only used to gather global data: 
 - countries, 
 - cities, 
 - locations 
 
 and save them to database, which will be used by Traveller.
 
 Collected data are stored as part of Traveller package, therefore there is no need to use this package again. 
 
 After some time this package will probably stop working, nevertheless if data update is required, it will be good starting point.
 
 # Data source
 data downloaded from:
 http://download.geonames.org/export/dump/
 
 # How to use
