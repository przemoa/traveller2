import io
import os

TRAVELLER_2_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../..')
API_KEY_FILE_PATH = os.path.abspath(os.path.join(TRAVELLER_2_DIR, 'misc/google_cloud_api_key.json'))

print(API_KEY_FILE_PATH)

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = API_KEY_FILE_PATH

# Imports the Google Cloud client library
from google.cloud import vision
from google.cloud.vision import types

# Instantiates a client
client = vision.ImageAnnotatorClient()

# The name of the image file to annotate
file_name = '/home/przemek/Desktop/zd2/IMG_20170809_104916.jpg'

# Loads the image into memory
with io.open(file_name, 'rb') as image_file:
    content = image_file.read()

image = types.Image(content=content)

if 0:
    # Performs label detection on the image file
    response = client.label_detection(image=image)
    labels = response.label_annotations

    print('Labels:')
    for label in labels:
        print(label.description)

p1 = types.AnnotateImageRequest(
    image=image,
    features=[
        {'type': vision.enums.Feature.Type.OBJECT_LOCALIZATION},
        {'type': vision.enums.Feature.Type.LANDMARK_DETECTION},
        {'type': vision.enums.Feature.Type.LABEL_DETECTION},
        # vision.enums.Feature.Type.LANDMARK_DETECTION,
        # vision.enums.Feature.Type.LABEL_DETECTION,
    ])
requests = [p1]
response = client.batch_annotate_images(requests)
print(p1)


"""
r = client.annotate_image({'image': image})
r.label_annotations
r.landmark_annotations

"""
