#!/usr/bin/env bash

set -x  # print commands
set -e  # any commands which fail will cause the shell script to exit immediately
set -u  # treat unset variables and parameters other than the special parameters "@" and "*" as an error

printf "About to generate prompts database.."


SCRIPT_DIR="$(dirname "$0")"
GLOBAL_COMPONENS_CREATOR_DIR=$SCRIPT_DIR/global_components_creator
TRAVELLER2_SRC_DIR=$SCRIPT_DIR/../src

echo "SCRIPT_DIR: $SCRIPT_DIR"
echo "GLOBAL_COMPONENS_CREATOR_DIR: GLOBAL_COMPONENS_CREATOR_DIR"
echo "TRAVELLER2_SRC_DIR: TRAVELLER2_SRC_DIR"

if [ -z ${PYTHONPATH+x} ]; then PYTHONPATH=; fi
if [ -z ${PYTHONPATH+x} ]; then echo "PYTHONPATH is unset"; else echo "PYTHONPATH is set to '$PYTHONPATH'"; fi

export PYTHONPATH=$TRAVELLER2_SRC_DIR:$GLOBAL_COMPONENS_CREATOR_DIR:$PYTHONPATH


python3 $GLOBAL_COMPONENS_CREATOR_DIR/generate_prompts_database.py --output_file_path "$GLOBAL_COMPONENS_CREATOR_DIR/traveller2_prompts.pkl"

printf "Finished generating prompts database!"
