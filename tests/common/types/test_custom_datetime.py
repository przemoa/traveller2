import pytest

from common.types.custom_datetime import CustomDateTime


def test_basic_usage_datetime():
    with pytest.raises(Exception):
        CustomDateTime('daw')

    cd_1 = CustomDateTime(123456789)
    d_2 = cd_1.to_datetime()
    cd_3 = CustomDateTime.from_datetime(d_2)

    assert cd_1 == cd_3

    cd_4 = CustomDateTime.now()
    assert cd_4 == CustomDateTime.from_datetime(cd_4.to_datetime())

    cd_4.to_date_string()
    cd_4.to_time_string()
    cd_4.to_date_time_string()
    cd_4.to_chronological_date_time_string()
