import math

import pytest

from common.types.coordinates import Coordinates


def test_basic_usage_coordinates():
    c = Coordinates(lat=-10.5, lon=15.5)
    assert c.lat == -10.5
    assert c.lon == 15.5
    assert c.to_tuple() == (-10.5, 15.5)
    assert Coordinates.from_tuple((-10.5, 15.5)) == c
    assert Coordinates.from_tuple(c.to_tuple()) == c
    with pytest.raises(Exception):
        c = Coordinates(lat=324, lon=45)

    c_poznan = Coordinates(lat=52.415741, lon=16.932143)
    c_crete = Coordinates(lat=35.367097, lon=24.477894)
    distance = c_poznan.get_distance_to_other_coordinates(c_crete)
    assert math.isclose(distance, 1987000, abs_tol=1000)


if __name__ == '__main__':
    test_basic_usage_coordinates()
