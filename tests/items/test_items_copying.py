from items.attachment.photo import Photo
from utils.dummy_engine import get_dummy_engine


def test_item_copying():
    engine = get_dummy_engine()

    photo_1 = engine.collection.photos[1]
    photo_1.attached_to = engine.collection.journeys[0]

    journey_0_belongings_initial_count = len(engine.collection.journeys[0].belongings)

    photo_1_copy = photo_1.copy_fields_without_associated_items()  # type: Photo
    assert photo_1.coordinates == photo_1_copy.coordinates
    assert photo_1.relative_file_path == photo_1_copy.relative_file_path
    assert photo_1.thumbnail == photo_1_copy.thumbnail
    assert photo_1.attached_to != photo_1_copy.attached_to
    assert photo_1_copy.attached_to is None
    assert photo_1_copy.item_id is None

    photo_1.original_full_path = '/ad/awda/dwad'
    assert photo_1.original_full_path != photo_1_copy.original_full_path
    photo_1_copy.thumbnail = b'wdaawdaw'
    assert photo_1.thumbnail != photo_1_copy.thumbnail

    engine.collection.add_item(photo_1_copy)
    assert journey_0_belongings_initial_count == len(engine.collection.journeys[0].belongings)
    photo_1_copy.attached_to = engine.collection.journeys[0]
    assert journey_0_belongings_initial_count == len(engine.collection.journeys[0].belongings) - 1



if __name__ == '__main__':
    test_item_copying()
