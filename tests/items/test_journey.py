from items.journey import JourneyContent, Journey


def test_journey_basic():
    journey_1_content = JourneyContent(name='Around the world', note='hello', item_id=5)
    expected_fields = ['name', 'start_time', 'end_time', 'start_location',
                       'item_id', 'item_creation_time', 'item_modification_time', 'note']

    journey_dir = dir(journey_1_content)
    for expected_field in expected_fields:
        assert expected_field in journey_dir

    journey_1 = Journey.deserialize(journey_1_content)
    assert journey_1.note == 'hello'
    assert journey_1.name == 'Around the world'

    journey_1_content.item_id = 3
    journey_3 = Journey.deserialize(journey_1_content)

    journey_2_content = journey_3.serialize()

    assert journey_2_content == journey_1_content


if __name__ == '__main__':
    test_journey_basic()
