import filecmp
import os
import os.path
import shutil

from items_definitions.items_generator import generate_all
from paths import TEST_TEMPORARY_DIR
from root_path import SRC_DIR


def assert_dir_trees_equal(dir1, dir2):
    """
    Compare two directories recursively. Files in each directory are
    assumed to be equal if their names and contents are equal.

    @param dir1: First directory path
    @param dir2: Second directory path

    @return: True if the directory trees are the same and
        there were no errors while accessing the directories or files,
        False otherwise.

   Code from https://stackoverflow.com/questions/4187564/recursive-dircmp-compare-two-directories-to-ensure-they-have-the-same-files-and
   """

    dirs_cmp = filecmp.dircmp(dir1, dir2)

    excluded = ['__init__.py', '__pycache__']
    for name in excluded:
        if name in dirs_cmp.left_only:
            dirs_cmp.left_only.remove(name)
        if name in dirs_cmp.right_only:
            dirs_cmp.right_only.remove(name)
        if name in dirs_cmp.common:
            dirs_cmp.common.remove(name)

    if len(dirs_cmp.left_only)>0:
        raise Exception(f'Only in {dir1}: {dirs_cmp.left_only}')
    elif len(dirs_cmp.right_only)>0:
        raise Exception(f'Only in {dir2}: {dirs_cmp.right_only}')
    elif len(dirs_cmp.funny_files)>0:
        raise Exception(f'Funny files: {dirs_cmp.funny_files}')
    (_, mismatch, errors) = filecmp.cmpfiles(
        dir1, dir2, dirs_cmp.common_files, shallow=False)
    if len(mismatch)>0 or len(errors)>0:
        raise Exception(f'Different files in {dir1} and {dir2}: {mismatch}, {errors}')
    for common_dir in dirs_cmp.common_dirs:
        new_dir1 = os.path.join(dir1, common_dir)
        new_dir2 = os.path.join(dir2, common_dir)
        assert_dir_trees_equal(new_dir1, new_dir2)


def test_generate_all():
    generated_base_dir = os.path.join(TEST_TEMPORARY_DIR, 'generated_items')
    src_items_dir = os.path.join(SRC_DIR, 'items')

    if os.path.exists(generated_base_dir):
        shutil.rmtree(generated_base_dir)

    generate_all(base_dir=generated_base_dir)
    assert_dir_trees_equal(src_items_dir, generated_base_dir)

    if os.path.exists(generated_base_dir):
        shutil.rmtree(generated_base_dir)


if __name__ == '__main__':
    test_generate_all()
