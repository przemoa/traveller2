import time

import math


def simple_calculation_1(lat1, lon1, lat2, lon2):
    # 1.26 s for 1M
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c
    return d


def simple_calculation_2(lat1, lon1, lat2, lon2):
    # 1.15 s for 1M
    radius = 6371  # km
    pi = 3.14159265359
    dlat = (lat2 - lat1) * pi / 180
    dlon = (lon2 - lon1) * pi / 180
    a = (math.sin(dlat / 2) * math.sin(dlat / 2) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon / 2) * math.sin(dlon / 2))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c
    return d


def simple_calculation_3(lat1, lon1, lat2, lon2):
    # 1.03 s for 1M
    radius = 6371  # km
    dlat = (lat2 - lat1) * 0.008726646259972222
    dlon = (lon2 - lon1) * 0.008726646259972222
    a = (math.sin(dlat) * math.sin(dlat) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon) * math.sin(dlon))
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c
    return d


def simple_calculation_3(lat1, lon1, lat2, lon2):
    # 1 s for 1M
    dlat = (lat2 - lat1) * 0.008726646259972222
    dlon = (lon2 - lon1) * 0.008726646259972222
    a = (math.sin(dlat) * math.sin(dlat) +
         math.cos(math.radians(lat1)) * math.cos(math.radians(lat2)) *
         math.sin(dlon) * math.sin(dlon))
    return 12742 * math.atan2(math.sqrt(a), math.sqrt(1 - a))


def simple_calculation__performance(f):
    lat_1, lon_1 = 23.123, 12.12
    lat_2, lon_2 = 13.123, 2.12
    t1 = time.time()
    x = f(lat_1, lon_1, lat_2, lon_2)
    assert math.isclose(x, 1532.71, abs_tol=0.02)
    for i in range(1000000):
        f(lat_1, lon_1, lat_2, lon_2)
    print(f'{time.time()-t1} s')


if __name__ == '__main__':
    simple_calculation__performance(f=simple_calculation_3)
