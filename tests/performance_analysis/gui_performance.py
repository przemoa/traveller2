import time

from engine.engine import Engine
from items.attachment.photo import Photo
from tabs.common_ui.display_item.multiple_photos_display.multiple_photos_display_widget import \
    MultiplePhotosDisplayWidget

"""
export PYTHONPATH=/home/przemek/Projects/traveller2/src:$PYTHONPATH
python -m cProfile -o program.prof gui_performance.py 
snakeviz program.prof
"""


def multiple_photos_widget__performance():
    db_path = '/media/przemek/dane/samba_ro/Dokumenty/traveller/traveller2/traveller2_przemek_backup_20190720_123924.pkl'

    import sys
    from gui_setup import debug_setup
    from PySide2 import QtWidgets

    from common import settings
    settings.DISPLAY_QMESSAGE_AFTER_UNHANDLED_EXCEPTION = False
    debug_setup()
    app = QtWidgets.QApplication(sys.argv)
    engine = Engine()
    engine.load_collection(db_path)
    w = MultiplePhotosDisplayWidget(parent=None, item_class=Photo, is_editable=True)
    w.show()
    w.set_engine(engine)
    w.display_item(engine.collection.photos)
    sys.exit(app.exec_())


if __name__ == '__main__':
    multiple_photos_widget__performance()
