import time

from common.types.coordinates import haversine_distance
from engine.collection_utils import load_collection_from_file_path
from items_utils.filtering import decimate_photos_in_space


"""
export PYTHONPATH=/home/przemek/Projects/traveller2/src:$PYTHONPATH
python -m cProfile -o program.prof filtering_preformance.py 
snakeviz program.prof
"""


def decimate_photos_in_space__performance():
    """
    Reversed list: speed up 70%
    @lru_cache(maxsize=1000000) for get_coordinate - speed up 20% more
    may_be_coordinates_distance_smaller - speed up 75 %
    """
    db_path = '/media/przemek/dane/samba_ro/Dokumenty/traveller/traveller2/traveller2_przemek_backup_20190720_123924.pkl'
    c, _ = load_collection_from_file_path(file_path=db_path, validate_field_types=False)
    t1 = time.time()
    decimated_photos = decimate_photos_in_space(c.photos, radius=200)
    print(f'Photos decimated from {len(c.photos)} to {len(decimated_photos)}')
    print(f'{time.time()-t1} s')


if __name__ == '__main__':
    decimate_photos_in_space__performance()
