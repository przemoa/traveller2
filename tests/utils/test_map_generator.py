import os
import time

from common import settings
from common.types.coordinates import Coordinates
from paths import TEST_TEMPORARY_DIR
from utils.dummy_engine import get_dummy_engine
from utils.map_generator.map_generator import generate_map_with_visits_and_photos, generate_map_with_single_marker


def test_generate_map_with_visits_and_photos():
    settings.TEMPORARY_FILES_DIR = TEST_TEMPORARY_DIR
    settings.load_settings()
    engine = get_dummy_engine()

    full_path = generate_map_with_visits_and_photos(
        map_name='tmp_test' + str(time.time()),
        images=engine.collection.photos,
        visits=engine.collection.visits,
    )

    assert os.path.exists(full_path)


def test_generate_map_with_single_marker():
    settings.TEMPORARY_FILES_DIR = TEST_TEMPORARY_DIR
    settings.load_settings()
    full_path = generate_map_with_single_marker(coordinates=Coordinates(lat=23.45, lon=-32.12))

    try:
        os.remove(full_path)
    except:
        pass

    generate_map_with_single_marker(coordinates=Coordinates(lat=23.45, lon=-32.12))

    assert os.path.exists(full_path)


if __name__ == '__main__':
    test_generate_map_with_single_marker()
    test_generate_map_with_visits_and_photos()
