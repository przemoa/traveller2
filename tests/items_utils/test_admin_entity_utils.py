from common.types.misc import PromptSourceType
from items_utils import admin_entity_utils
from utils.dummy_engine import get_dummy_engine


def test_get_country_prompts():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_country_prompts(
        engine=engine,
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_country_prompts(
        engine=engine,
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_country_prompts(
        engine=engine,
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 3
    assert len(items_prompts) == 3


def test_get_region_major_prompts():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 3
    assert len(items_prompts) == 2


def test_get_region_major_prompts_for_country():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 3
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 2


def test_get_region_major_prompts_for_country_prompt():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_major_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)


def test_get_region_minor_prompts():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 3


def test_get_region_minor_prompts_for_country():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 3
    assert len(items_traveller_db) == 1
    assert len(items_prompts) == 3


def test_get_region_minor_prompts_for_country_prompt():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 3
    assert len(items_traveller_db) == 1
    assert len(items_prompts) == 3


def test_get_region_minor_prompts_for_region_major_1():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 0
    assert len(items_traveller_db) == 0
    assert len(items_prompts) == 0


def test_get_region_minor_prompts_for_region_major_2():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 3
    assert len(items_traveller_db) == 1
    assert len(items_prompts) == 3


def test_get_region_minor_prompts_for_region_major_prompt():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_region_minor_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 0
    assert len(items_traveller_db) == 0
    assert len(items_prompts) == 0


def test_get_city_prompts():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 4


def test_get_city_prompts_for_country():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 4


def test_get_city_prompts_for_country_prompt():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.countries[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 4


def test_get_city_prompts_for_region_major_1():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 0
    assert len(items_traveller_db) == 0
    assert len(items_prompts) == 0


def test_get_city_prompts_for_region_major_2():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_major[1],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 4


def test_get_city_prompts_for_region_major_prompt_1():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 0
    assert len(items_traveller_db) == 0
    assert len(items_prompts) == 0


def test_get_city_prompts_for_region_major_prompt_2():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[1],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[1],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_major[1],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 4
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 4

def test_get_city_prompts_for_region_minor():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_minor[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_minor[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.collection.regions_minor[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 3
    assert len(items_traveller_db) == 2
    assert len(items_prompts) == 3


def test_get_city_prompts_for_region_minor_prompt():
    engine = get_dummy_engine()
    items_prompts = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_minor[0],
        prompt_type=PromptSourceType.PROMPT_DATABASE)

    items_traveller_db = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_minor[0],
        prompt_type=PromptSourceType.TRAVELLER_DATABASE)

    items_all = admin_entity_utils.get_city_prompts(
        engine=engine,
        parent=engine.prompts_collection.regions_minor[0],
        prompt_type=PromptSourceType.ALL_AVAILABLE)

    assert len(items_all) <= len(items_prompts) + len(items_traveller_db)
    assert len(items_all) >= len(items_traveller_db)
    assert len(items_all) >= len(items_prompts)

    assert len(items_all) == 1
    assert len(items_traveller_db) == 0
    assert len(items_prompts) == 1


if __name__ == '__main__':
    test_get_country_prompts()
    test_get_region_major_prompts()
    test_get_region_major_prompts_for_country()
    test_get_region_minor_prompts()
    test_get_region_minor_prompts_for_country()
    test_get_region_minor_prompts_for_region_major_1()
    test_get_region_minor_prompts_for_region_major_2()
    test_get_city_prompts()
    test_get_city_prompts_for_country()
    test_get_city_prompts_for_region_major_1()
    test_get_city_prompts_for_region_major_2()
    test_get_city_prompts_for_region_minor()

    test_get_region_major_prompts_for_country_prompt()
    test_get_region_minor_prompts_for_country_prompt()
    test_get_region_minor_prompts_for_region_major_prompt()
    test_get_city_prompts_for_country_prompt()
    test_get_city_prompts_for_region_major_prompt_1()
    test_get_city_prompts_for_region_major_prompt_2()
    test_get_city_prompts_for_region_minor_prompt()
