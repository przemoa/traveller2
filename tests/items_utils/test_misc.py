from items_utils.misc import get_item_class_by_name_string


def test_get_item_class_by_name_string():
    assert get_item_class_by_name_string('Journey').__name__ == 'Journey'


if __name__ == '__main__':
    test_get_item_class_by_name_string()
