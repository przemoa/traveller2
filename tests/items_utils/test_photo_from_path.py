import os

from common.types.coordinates import Coordinates
from common.types.custom_datetime import CustomDateTime
from items_utils.photo_from_path import get_photo_from_file_path
from paths import FILES_FOR_TESTS_DIR


def test_get_photo_from_file_path():
    photo_path = os.path.join(FILES_FOR_TESTS_DIR, 'IMG_20170218_102010.jpg')

    photo_1 = get_photo_from_file_path(
        abs_file_path=photo_path,
        base_dir='',
        thumbnail_size=200)

    assert photo_1.coordinates == Coordinates(lat=50.80726111111111, lon=16.293894444444447)
    assert photo_1.camera_model == 'Nexus 5'
    assert photo_1.photo_time == CustomDateTime(1487409611)
    assert photo_1.relative_file_path is None
    assert photo_1.original_full_path.endswith(os.path.join('tests', 'files_for_tests', 'IMG_20170218_102010.jpg'))
    assert len(photo_1.thumbnail) > 1000


if __name__ == '__main__':
    test_get_photo_from_file_path()
