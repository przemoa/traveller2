import os

from common.types.coordinates import Coordinates
from engine.engine import Engine
from items.attachment.photo import Photo
from items_utils.filtering import filter_items__for_journey, filter_items__for_time, get_recently_added_items, \
    get_recently_modified_items, sort_items_by_time, decimate_photos_in_space
from paths import TEST_TEMPORARY_DIR
from utils.dummy_engine import get_dummy_engine


def test_filter_items__for_journey():
    engine = get_dummy_engine()

    visits_for_journey_1 = filter_items__for_journey(items=engine.collection.visits, journey=engine.collection.journeys[1])
    assert len(visits_for_journey_1) == 2

    photos_for_journey_1 = filter_items__for_journey(items=engine.collection.photos, journey=engine.collection.journeys[1])
    assert len(photos_for_journey_1) == 0

    files_for_journey_1 = filter_items__for_journey(items=engine.collection.files, journey=engine.collection.journeys[1])
    assert len(files_for_journey_1) == 1

    visits_for_journey_2 = filter_items__for_journey(items=engine.collection.visits, journey=engine.collection.journeys[2])
    assert len(visits_for_journey_2) == 1

    photos_for_journey_2 = filter_items__for_journey(items=engine.collection.photos, journey=engine.collection.journeys[2])
    assert len(photos_for_journey_2) == 1


def test_filter_items__for_time():
    engine = get_dummy_engine()

    visit_1_time = engine.collection.visits[1].start_time
    visits = filter_items__for_time(items=engine.collection.visits,
                                    before_time=visit_1_time+1,
                                    after_time=visit_1_time-1,
                                    )
    assert len(visits) == 1
    assert visits[0] == engine.collection.visits[1]


def test_get_recently_added_items():
    engine_1 = get_dummy_engine()
    file_path = os.path.join(TEST_TEMPORARY_DIR, 'test_engine__recent_items_test.pkl')
    engine_1.save_collection(file_path)

    engine_2 = Engine()
    engine_2.load_collection(file_path)

    recently_added_1 = get_recently_added_items(collection=engine_2.collection, number_of_items=2)
    recently_added_2 = get_recently_added_items(collection=engine_2.collection, number_of_items=1000)

    assert recently_added_1[0] == recently_added_2[0]
    assert len(recently_added_1) == 2
    assert len(recently_added_2) < 1000


def test_get_recently_modifed_items():
    engine_1 = get_dummy_engine()
    file_path = os.path.join(TEST_TEMPORARY_DIR, 'test_engine__recent_items_test.pkl')
    engine_1.save_collection(file_path)

    engine_2 = Engine()
    engine_2.load_collection(file_path)

    recently_updated_1 = get_recently_modified_items(collection=engine_2.collection, number_of_items=2)
    recently_updated_2 = get_recently_modified_items(collection=engine_2.collection, number_of_items=1000)

    assert recently_updated_1[0] == recently_updated_2[0]
    assert len(recently_updated_1) == 2
    assert len(recently_updated_2) < 1000


def test_sort_items_by_time():
    c = get_dummy_engine().collection
    items_that_can_be_sorted = c.journeys + c.photos + c.flights + c.visits
    sorted_items = sort_items_by_time(items_that_can_be_sorted)
    assert len(sorted_items) == len(items_that_can_be_sorted)
    for i in range(1, len(sorted_items)):
        assert sorted_items[i].get_date_for_sorting() <= sorted_items[i-1].get_date_for_sorting()


def test_decimate_photos_in_space():
    c = get_dummy_engine().collection
    decimated_1 = decimate_photos_in_space(photos=c.photos, radius=100)

    photos_2 = [
        Photo(coordinates=Coordinates(52.278058, 16.794623), original_full_path='a'),
        Photo(coordinates=Coordinates(52.278185, 16.793745), original_full_path='a'),
        Photo(coordinates=Coordinates(52.277763, 16.763738), original_full_path='a'),
        Photo(coordinates=Coordinates(51.248637, 16.780442), original_full_path='a'),
    ]
    decimated_4 = decimate_photos_in_space(photos=photos_2, radius=1)
    assert len(decimated_4) == 4

    decimated_2 = decimate_photos_in_space(photos=photos_2, radius=100)
    assert len(decimated_2) == 3

    decimated_3 = decimate_photos_in_space(photos=photos_2, radius=10000)
    assert len(decimated_3) == 2



if __name__ == '__main__':
    test_filter_items__for_journey()
    test_filter_items__for_time()
    test_get_recently_added_items()
    test_get_recently_modifed_items()
    test_decimate_photos_in_space()
