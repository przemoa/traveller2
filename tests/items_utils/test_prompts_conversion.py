import os

from engine.collection_utils import save_collection_content_to_file
from engine.engine import Engine
from items.collection import Collection
from items.location.admin_entity.city import City
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.location.admin_entity.region_minor import RegionMinor
from items_utils.copy_items import _copy_admin_entity_prompt_with_parents, copy_prompt_item_with_parents_and_belongings
from paths import TEST_TEMPORARY_DIR
from utils.dummy_engine import get_dummy_engine


def _get_admin_entities_counts(c: Collection):
    return [len(c.countries), len(c.regions_major), len(c.regions_minor), len(c.cities)]


def test_copy_admin_entity_prompt_with_parents__only_city_new():
    engine = get_dummy_engine()
    c = engine.collection
    admin_entities_initial_counts = _get_admin_entities_counts(c)

    city_prompt = engine.prompts_collection.cities[-2]  # only city is new
    print(city_prompt.get_short_description())

    new_item = _copy_admin_entity_prompt_with_parents(collection=c, item=city_prompt)
    assert new_item.name == city_prompt.name
    assert new_item.region_minor.region_major.geoname_code == city_prompt.region_minor.region_major.geoname_code

    # only city will be added; region minor, major and country are already in collection
    admin_entities_new_counts = _get_admin_entities_counts(c)
    admin_entities_expected_counts = list(map(sum, zip(admin_entities_initial_counts, [0, 0, 0, 1])))

    assert admin_entities_new_counts == admin_entities_expected_counts


def test_copy_admin_entity_prompt_with_parents__nothing_new():
    engine = get_dummy_engine()
    c = engine.collection
    admin_entities_initial_counts = _get_admin_entities_counts(c)

    city_prompt = engine.prompts_collection.cities[0]  # city exist both in prompts and traveller db collection
    print(city_prompt.get_short_description())

    new_item = _copy_admin_entity_prompt_with_parents(collection=c, item=city_prompt)
    assert new_item.name == city_prompt.name
    assert new_item.region_minor.region_major.geoname_code == city_prompt.region_minor.region_major.geoname_code

    # nothing new in collection
    admin_entities_new_counts = _get_admin_entities_counts(c)
    assert admin_entities_new_counts == admin_entities_initial_counts


def test_copy_admin_entity_prompt_with_parents__city_regions_and_county_new():
    engine = get_dummy_engine()
    pc = engine.prompts_collection
    c = engine.collection
    admin_entities_initial_counts = _get_admin_entities_counts(c)
    
    country_0_prompt = Country(name='Completely new country', iso_code='new_4324')
    pc.add_item(country_0_prompt)

    region_major_0_prompt = RegionMajor(name='Completely new region major', geoname_code='new_1243214')
    region_major_0_prompt.country = country_0_prompt
    pc.add_item(region_major_0_prompt)

    region_minor_0_prompt = RegionMinor(name='Completely new region minor', geoname_code='new_3242323423')
    region_minor_0_prompt.region_major = region_major_0_prompt
    pc.add_item(region_minor_0_prompt)

    city_0_prompt = City(name='Completely new city', geoname_code='new_32423432768')
    city_0_prompt.region_minor = region_minor_0_prompt
    pc.add_item(city_0_prompt)

    new_item = _copy_admin_entity_prompt_with_parents(collection=c, item=city_0_prompt)

    assert new_item.name == city_0_prompt.name
    assert new_item.region_minor.region_major.geoname_code == city_0_prompt.region_minor.region_major.geoname_code

    # city, region minor, major and country will be added as new
    admin_entities_new_counts = _get_admin_entities_counts(c)
    admin_entities_expected_counts = list(map(sum, zip(admin_entities_initial_counts, [1, 1, 1, 1])))

    assert admin_entities_new_counts == admin_entities_expected_counts


def test_copy_dummy_journey():
    engine_1 = get_dummy_engine()

    collection_2 = Collection()
    copy_prompt_item_with_parents_and_belongings(collection=collection_2, item=engine_1.collection.journeys[1])
    assert len(collection_2.journeys) == 1
    assert collection_2.journeys[0].name == 'around poland'
    assert len(collection_2.journeys[0].belongings) == 2
    assert len(collection_2.journeys[0].get_belongings_set_recursively()) == 3
    assert collection_2.journeys[0].belongings[1].belongings[0].original_full_path == '/file/path'
    assert collection_2.journeys[0].belongings[1].location.name == 'Poland'
    assert collection_2.journeys[0].belongings[0].location.country.iso_code == 'IT'
    assert len(collection_2.all_items_by_id) == 8

    file_path = os.path.join(TEST_TEMPORARY_DIR, 'copied_engine_journey.pkl')
    save_collection_content_to_file(file_path=file_path, collection_content=collection_2.to_content())

    engine_3 = Engine()
    engine_3.load_collection(file_path)
    assert len(engine_3.collection.all_items_by_id) == 8

    copy_prompt_item_with_parents_and_belongings(collection=engine_3.collection, item=engine_1.collection.journeys[2])
    assert len(engine_3.collection.all_items_by_id) == 19
    assert len(engine_3.collection.persons) == 3

    file_path_3 = os.path.join(TEST_TEMPORARY_DIR, 'copied_engine_journey.pkl')
    save_collection_content_to_file(file_path=file_path_3, collection_content=engine_3.collection.to_content())

    engine_4 = Engine()
    engine_4.load_collection(file_path)
    assert len(engine_4.collection.journeys) == 2

    assert len(engine_4.collection.all_items_by_id) == 19

    copy_prompt_item_with_parents_and_belongings(collection=engine_4.collection, item=engine_1.collection.journeys[0])
    assert len(engine_4.collection.all_items_by_id) == 25
    assert len(engine_4.collection.airports) == 2


if __name__ == '__main__':
    test_copy_admin_entity_prompt_with_parents__only_city_new()
    test_copy_admin_entity_prompt_with_parents__nothing_new()
    test_copy_admin_entity_prompt_with_parents__city_regions_and_county_new()
    test_copy_dummy_journey()
