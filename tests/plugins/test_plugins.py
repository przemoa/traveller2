from unittest.mock import MagicMock

from common.types.custom_datetime import CustomDateTime
from items.attachment.photo import Photo
from items.collection import Collection
from items.journey import Journey
from items.location.admin_entity.country import Country
from items.visit import Visit
from plugins.examples.autoassign_photos_to_journey_or_visit import AutoassignPhotosToJourneyOrVisit
from utils.misc import Namespace


def test_autoassign_photos_to_journeys_and_visits():
    collection = Collection()

    journey_0 = Journey('journey_0', start_time=CustomDateTime(unix_time=100), end_time=CustomDateTime(unix_time=200))
    journey_1 = Journey('journey_1', start_time=CustomDateTime(unix_time=150))
    journey_2 = Journey('journey_2', start_time=CustomDateTime(unix_time=300), end_time=CustomDateTime(unix_time=400))
    collection.add_item(journey_0)
    collection.add_item(journey_1)
    collection.add_item(journey_2)

    country_0 = Country(name='a', iso_code='a')
    collection.add_item(country_0)

    visit_0 = Visit(location=country_0, start_time=CustomDateTime(unix_time=100), end_time=CustomDateTime(unix_time=200))
    collection.add_item(visit_0)

    visit_1 = Visit(location=country_0, start_time=CustomDateTime(unix_time=350), end_time=CustomDateTime(unix_time=550), name_suffix='xx')
    collection.add_item(visit_1)

    photo_0 = Photo(original_full_path='photo_0', photo_time=CustomDateTime(50))
    photo_1 = Photo(original_full_path='photo_1', photo_time=CustomDateTime(150))
    photo_2 = Photo(original_full_path='photo_2')
    photo_3 = Photo(original_full_path='photo_3', photo_time=CustomDateTime(250))
    photo_4 = Photo(original_full_path='photo_4', photo_time=CustomDateTime(350))
    photo_5 = Photo(original_full_path='photo_5', photo_time=CustomDateTime(950))
    photo_6 = Photo(original_full_path='photo_6', photo_time=CustomDateTime(260), attached_to=journey_1)
    photo_7 = Photo(original_full_path='photo_7', photo_time=CustomDateTime(450))
    collection.add_item(photo_0)
    collection.add_item(photo_1)
    collection.add_item(photo_2)
    collection.add_item(photo_3)
    collection.add_item(photo_4)
    collection.add_item(photo_5)
    collection.add_item(photo_6)
    collection.add_item(photo_7)

    engine = Namespace(collection=collection)
    engine.set_engine_as_modified = MagicMock()
    progress_dialog = MagicMock()

    plugin = AutoassignPhotosToJourneyOrVisit()
    plugin._run(engine=engine, progress_dialog=progress_dialog)

    assert photo_0.attached_to is None
    assert photo_1.attached_to is journey_0
    assert photo_2.attached_to is None
    assert photo_3.attached_to is None
    assert photo_4.attached_to is journey_2
    assert photo_5.attached_to is None
    assert photo_6.attached_to is journey_1
    assert photo_7.attached_to is visit_1


if __name__ == '__main__':
    test_autoassign_photos_to_journeys_and_visits()
