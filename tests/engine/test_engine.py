import os

import pytest

from common import settings
from common.types.custom_datetime import CustomDateTime
from engine.engine import Engine
from items.journey import Journey
from items.location.admin_entity.country import Country
from items.location.admin_entity.region_major import RegionMajor
from items.visit import Visit
from items_utils.filtering import sort_items_by_time
from paths import TEST_TEMPORARY_DIR, FILES_FOR_TESTS_DIR
from utils.dummy_engine import get_dummy_engine


def test_engine_basic():
    settings.load_settings()

    engine_1 = Engine()
    c_1 = engine_1.collection

    journey_1 = Journey(
        name='around world')
    journey_1.start_time = CustomDateTime(1234567)
    c_1.add_item(journey_1)

    country_0 = Country(name='Poland', iso_code='pl')
    c_1.add_item(country_0)

    visit_1 = Visit(location=country_0)
    visit_1.note= 'something important'
    visit_1.start_time = CustomDateTime(1234567)
    visit_1.journey = journey_1
    c_1.add_item(visit_1)

    file_path = os.path.join(TEST_TEMPORARY_DIR, 'test_engine.pkl')
    engine_1.save_collection(file_path)

    engine_2 = Engine()
    engine_2.load_collection(file_path)

    visit = engine_2.collection.visits[0]
    journey = visit.journey  # type: Journey
    assert journey.belongings[0] is visit

    c_2 = engine_2.collection
    assert c_2.journeys[0].item_creation_time is not None
    assert c_2.journeys[0].item_modification_time is not None
    assert c_2.journeys[0].start_time == CustomDateTime(1234567)


def test_dummy_engine():
    settings.load_settings()

    engine = get_dummy_engine()
    pc = engine.prompts_collection

    file_path = os.path.join(TEST_TEMPORARY_DIR, 'dummy_engine.pkl')
    engine.save_collection(file_path)
    engine.save_collection(file_path)

    engine_2 = Engine()
    engine_2.load_collection(file_path)

    c = engine_2.collection

    assert len(c.all_items_by_id) == 35

    assert len(c.persons) == 4
    assert len(c.companions) == 1
    assert c.companions[0].referenced_by[0] == c.journeys[2]
    assert c.companions[0].person_1.name == 'person_1'


    assert len(c.journeys) == 3
    assert c.journeys[1].name == 'around poland'
    assert c.journeys[1].start_time == CustomDateTime(12345678)
    assert len(c.journeys[0].belongings) == 1
    assert len(c.journeys[1].belongings) == 2
    assert len(c.journeys[2].belongings) == 2

    assert len(c.visits) == 4
    assert c.visits[1].journey == c.journeys[1]

    assert len(c.photos) == 6
    assert c.photos[1].note == 'hello note 33'
    assert c.photos[1].attached_to == c.journeys[2]
    assert c.photos[1].camera_model == 'Nexus 5'
    assert c.photos[1].relative_file_path is None
    assert c.photos[1].original_full_path == os.path.join(FILES_FOR_TESTS_DIR, 'IMG_20170218_102010.jpg')

    assert c.photos[1].attached_to == c.journeys[2]
    assert c.journeys[2].belongings[0] == c.visits[3]
    assert c.journeys[2].belongings[1] == c.photos[1]

    assert len(c.files) == 2
    assert c.files[0].attached_to == c.visits[2]
    assert c.visits[2].belongings[0] == c.files[0]

    assert c.files[1].attached_to is None
    # assert c.files[1].original_full_path == '/other/path'
    assert c.files[1].relative_file_path is None

    assert len(c.countries) == 3
    assert c.countries[0].name == 'Poland'
    assert c.countries[1].iso_code == 'IT'

    assert len(c.countries[0].belongings) == 2
    assert len(c.countries[1].belongings) == 1

    assert c.countries[1].belongings[0].name == 'Lombardy'

    assert len(c.regions_major) == 3
    assert len(c.regions_minor) == 2
    assert len(c.cities) == 2

    assert c.cities[0].region_minor.region_major.country.name == 'Poland'

    assert len(pc.countries) == 3
    assert len(pc.regions_major) == 2
    assert len(pc.regions_minor) == 3
    assert len(pc.cities) == 4

    assert c.visits[0] in c.cities[0].referenced_by
    assert len(c.cities[0].referenced_by) == 1

    assert c.visits[2] in c.countries[0].referenced_by

    assert c.journeys[2].start_location.referenced_by[0] == c.journeys[2]
    assert c.cities[1].referenced_by[0].start_location == c.cities[1]

    assert len(c.airports) == 2
    assert len(c.flights) == 1
    assert c.airports[0].referenced_by[0].destination_airport == c.airports[1]
    assert c.airports[0].municipality == 'zielona gora'

    assert c.flights[0].attached_to.location.name == 'Kargowa'

    # test if one can add other item with not unique value for fields
    region_major_with_same_name_different_country = RegionMajor(name=c.regions_major[0].name,
                                                                geoname_code='unique22311',
                                                                country=c.regions_major[2].country)
    assert c.regions_major[2].country != c.regions_major[0].country
    c.add_item(region_major_with_same_name_different_country)

    with pytest.raises(Exception):
        region_major_with_same_code = RegionMajor(name='unique_name34523',
                                                  geoname_code=c.regions_major[0].geoname_code,
                                                  country=c.regions_major[0].country)
        c.add_item(region_major_with_same_code)

    with pytest.raises(Exception):
        region_major_with_same_name_and_country = RegionMajor(name=c.regions_major[0].name,
                                                              geoname_code='unique22311',
                                                              country=c.regions_major[0].country)
        c.add_item(region_major_with_same_name_and_country)

    assert len(c.journeys[2].belongings) == 2
    c.delete_item(c.photos[1])
    assert len(c.photos) == 5
    assert len(c.journeys[2].belongings) == 1

    with pytest.raises(Exception):
        c.delete_item(c.countries[0])
    assert len(pc.countries) == 3

    with pytest.raises(Exception):
        c.delete_item(c.cities[0])  # because is referenced by one visit

    with pytest.raises(Exception):
        c.delete_item(c.cities[1])  # because is referenced by journey 2

    c.delete_item(c.visits[3])
    c.delete_item(c.journeys[2])
    c.delete_item(c.cities[1])


def test_short_descriptions():
    engine = get_dummy_engine()
    for item in engine.collection.all_items_by_id.values():
        item.get_short_description()
        item.copy_fields_without_associated_items()


if __name__ == '__main__':
    test_dummy_engine()
    test_engine_basic()
    test_short_descriptions()
    print('Test done')
