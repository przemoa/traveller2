import os


_SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
_TEST_TEMPORARY_DIR_NAME = 'test_temporary_files'


TEST_TEMPORARY_DIR = os.path.join(_SCRIPT_DIR, _TEST_TEMPORARY_DIR_NAME)
FILES_FOR_TESTS_DIR = os.path.join(_SCRIPT_DIR, 'files_for_tests')


if not os.path.exists(TEST_TEMPORARY_DIR):
    os.makedirs(TEST_TEMPORARY_DIR)
