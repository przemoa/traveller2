# Traveller2 - travel journal

Manage your journeys, trips, places, photos, maps and more...
Python3, PySide2 (Qt), PyQtGraph, Google API, Wikipedia API

# 1. Overview
Application is divided into multiple tabs. A single tab to add a new Journey looks like:

<img src="docs/pics/1_journey.png" width="600">

Tabs can be grouped and moved freely, to form a custom design 
(which will be preserved in the configuration file between application runs).
Example custom layout:

<img src="docs/pics/11_multi_tabs.png" width="600">

## 1.1. Item categories
Application stores the main data in a database, saved as a single file on disc.
Application has the following categories of entries:
- Journey
- Visit
- Photo
- Place
- Airport
- Flight
- Country
- Administrative Region Major
- Administrative Region Minor
- City

Most of the items are connected with other items, e.g. A Visit relates to a specific City, Region, Country or Place, can has linked Photots, Person, etc...  

Each entry has a corresponding tab, containing option to add/edit an entry, with filters.
For example, Visit tab:
<img src="docs/pics/3_visits_view.png" width="600">

After activating filter, one can filter items by some of the attributes:
<img src="docs/pics/4_visits_filtering.png" width="600">


### 1.1.1. City tab:
<img src="docs/pics/6_cities_view.png" width="600">


### 1.1.2. Photo tab:
Apart from already known attributes, photos have a thumbnail (stored in database) 
and can be automatically tagged with Google Image Recognition API.
Apart from tags, also landmarks can be automatically detected, like visible below:

<img src="docs/pics/16_photo_annotation.png" width="600">

### 1.1.3. AdminEntity
4 last entries (Country, Regions, City) form a AdminEntity, with a belonging order: City belong to Region Minor, Region Minor to Region Major, ...
AdminEntity can be specific (all 4 fields) or e.g. only a country and Region Major.

All AdminEntity entries (as well as Place) have Coordinates, so they can be visualized on a map.
To save the user tedious work, prompts database already has the required metadata.
Prompts database for Europe alone contains 80972 entries in 54 countries. When selecting a AdminEntity one can select both from own and prompts database.
Selecting is interactive, with prompts for all entries types during writing.

<img src="docs/pics/20_admin_entity.png" width="600">


## 1.2. Special tabs
There are also special tabs, not associated directly with entries:

### 1.2.1.  Map view
All items which have Coordinates (city, region, place, photo, visit) can be showed on the map:

<img src="docs/pics/14_map_photos_visists.png" width="600">

Photos are represented by a blue circle, whereas other items are represented with pins.
Every item on map can be hovered or clicked, to show details. For photos, also photo thumbnail is showed:

<img src="docs/pics/13_map_photos2.png" width="600">

Items to show on map can be filtered by item type and:
 - time range (e.g. show countries visited between 2018 and 2020) 
 - by Journey (e.g. show all photos from a journey)

Although some photos doesn't have geologalization metadata, but still can be shown on map - 
thanks to automatic landmark recognition, application also gets the location.

Map is also saved in /tmp/ directory, along with all thumbnails, therefore can be easily taken 
and e.g. hosted on the webpage, without need to have the full database. 

See Google API key section to have a map without annoying background "For development purposes only"

### 1.2.2. Database summary
Tab "General Info" presents summary of the database:
<img src="docs/pics/2_database_summary.png" width="600">

### 1.2.3. Browser tab
To easily browse all the data, Browser tab can be used. It allows to easily navigate between all items. 
Ever text which is blue, can be clicked to open item in a browser. E.g. one can click on a Journey, 
see all visits from this journey, click on a visited city to see more information, etc...
Browser supports also go Back/Forward functionality.
<img src="docs/pics/10_browser_tab.png" width="600">

### 1.2.4. Log console
There is also a tab to view logged messages, with configurable display level:

<img src="docs/pics/7_log_console.png" width="600">

Logs are also saved to a file.

### 1.2.5. Timeline
Another interesting tab is the Timeline.

<img src="docs/pics/18_timelin1.png" width="600">

It shows Visits, Flights, Journeys and photos on a timeline. Timeline can be zoomed, scrolled, moved.
After zooming it is readable:

<img src="docs/pics/19_timeline2.png" width="600">

One can click on every "item" on the graph to open it in Browser and see all details.

### 1.2.6. Photos
It is not the tab with a single photo, but with all photos, 
which can be filtered and showed in a built-in viewer (by thumbnails).
A full image (stored on the disc) can be opened after clicking on the thumbnail.
 
<img src="docs/pics/15_photos_multiview.png" width="600">

A interesting feature is ability to filter the images by a keyword (from automatically generated tags).
E.g. one can see all flowers:

<img src="docs/pics/17_photos_filtered.png" width="600">

Or rainbows:

<img src="docs/pics/23_rainbows.png" width="600">

## 1.3. Wikipedia integration
Every input field with potentially meaningful name (e.g. city name, place name, airport) 
has a button to open Wikipedia page for this name, as visible below:

<img src="docs/pics/9_places_wikipedia.png" width="600">

Wikimedia API is used to obtain appropriate search result info, with multiple results to choose in "Search result" combobox. 
Also search languages can be selected in settings.
Photos that have a automatically detected landmark also have active "Wikipedia" button.

## 1.4. Google Maps integration
Every field with Coordinates (city, region, place, photo location) has a button to showi it one the map.
Map is opened in a new window:

<img src="docs/pics/8_show_on_map.png" width="600">


## 1.5. Import/Export functionality
Single item can be exported to a new database file, which can be later imported to another database, by another user.
Item can be exported also with all its belongings, e.g. a journey can be exported with all visited cities, places and photos.


## 1.6. Plugins (tools integration)
Travveler2 supports plugins execution (with defined interface), loaded as a python file and executed (from menu).
E.g. plugin "Autoassign Photos to Journeys", which can be run from Menu->Tools->Plugins. 
<img src="docs/pics/21_plugin_execution.png" width="600">

## 1.7. Settings
In settings there are some useful options exposed, including Google API keys, to communicate with google services (maps and image recognition).
<img src="docs/pics/22_settings.png" width="600">

# 2. Development
 
Traveller uses database of locations to prompt user input (for countries and cities),
but name can be also typed manually.

## 2.1. Supported platforms
 - Linux (run from code and build package)
 - Windows (only run from code)
    
## 2.2. Automated build
### 2.2.1. Build as debian package and install
```
cd $PROJECT_DIR
cd ..
mkdir build_traveller2
../traveller2/build/build_all_linux.sh -p -t -d
sudo apt install ./dist/traveller2_x.y.z.deb
# Now run from terminal 'traveller_gui' or choose entry from start menu 
```
 
## 2.3. Run from sources
- Install Python. On Linux:
```
    sudo apt install python3.7
    sudo apt install python3.7-venv
```
- Setup environment. On Linux:
```
cd traveller
python3.7 -m virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
pip install git+https://github.com/pyqtgraph/pyqtgraph  # because new version of pyqtgraph is required to support PySide2
```
- Run in pycharm
  - mark 'src', 'tests' and 'misc/global_components_creator' directories as source root
  - settings->project interpreter->choose local environment
  - run GUI: src/clients/gui/run_traveller_gui.py
  - to run in "developer" mode instead of "release" add environment variables: IS_DEBUG=True;USE_DEVEL_CONFIG=True 
    It can be configured in pycharm or terminal:
     ```
     export IS_DEBUG=True
     export USE_DEVEL_CONFIG=True 
     ```
     
## 2.4. Tests with code coverage report
```
 cd $PROJECT_DIR
 PYTHONPATH=`pwd`/src:`pwd`/tests:$PYTHONPATH pytest --cov-report=html --cov=src tests/
```
Test coverage report will be generated in a separate directory. 

<img src="docs/pics/24_tests.png" width="800">


## 2.5. Generate prompts database
Issue commands:
```
    cd misc
    ./generate_prompts_database.sh
```

## 2.6. Repo content:
 - `src/`: main GUI application
 - `misc/glabal_data_creator/`: only used to create database with global data

See also [implementation_notes.md](implementation_notes.md) file for more details.

# 3. Obtain google maps API key:
To use some google services without limitations, google API key needs to be obtained and set in Traveller2 settings:
See tutorials online:
 - https://www.iwdagency.com/help/dealer-store-locator/creation-of-server-api-keybrowser-api-key
 - https://elfsight.com/blog/2018/06/how-to-get-google-maps-api-key-guide/
 - https://github.com/googlemaps/google-maps-services-python
 - console.developers.google.com/apis/
 - set google maps API key in settings
    
 
# 4. Obsolete notes and TODOs:
 
## 2.5 ~~Freeze to executable manually~~ DISABLED since 0.2.0, see implementation notes for details:
 ```
 sudo apt-get install python3.7-dev
  
 cd src
 export PYTHONPATH=`pwd`:$PYTHONPATH
 python -c 'import gui_setup ; gui_setup.moc_ui_files()'
 rm -rf dist
 rm -rf build
 IS_DEBUG=False
 pyinstaller run_traveller2_gui.spec
 ```
